package com.ikomet.foodfiestaa.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.beans.ChildCookDetail;
import com.ikomet.foodfiestaa.interfaces.FragmentCommunicatorNew;
import com.ikomet.foodfiestaa.utils.InternetCheck;

public class AdapterCheckBoxFoodSearch extends BaseExpandableListAdapter {

	// new change
	List<ChildCookDetail> header;

	HashMap<String, List<String>> child;
	private static final int GROUP_TYPE_1 = 0;
	private static final int CHILD_TYPE_1 = 0;

	private static final int CHILD_TYPE_UNDEFINED = 1;

	// Define activity context
	private Context mContext;
	private HashMap<String, LinkedHashMap<Integer, String>> child2;
	/*
	 * Here we have a Hashmap containing a String key (can be Integer or other
	 * type but I was testing with contacts so I used contact name as the key)
	 */
	private HashMap<String, List<String>> mListDataChild;
	private HashMap<String, Integer> hashmap;
	// ArrayList that is what each key in the above
	// hashmap points to
	private ArrayList<String> mListDataGroup;

	// Hashmap for keeping track of our checkbox check states
	private HashMap<Integer, boolean[]> mChildCheckStates;

	// Our getChildView & getGroupView use the viewholder patter
	// Here are the viewholders defined, the inner classes are
	// at the bottom
	private ChildViewHolder childViewHolder;

	private GroupViewHolder groupViewHolder;

	/*
	 * For the purpose of this document, I'm only using a single textview in the
	 * group (parent) and child, but you're limited only by your XML view for
	 * each group item :)
	 */
	private String groupText;
	private String childText;
	FragmentCommunicatorNew frag_new;

	/*
	 * Here's the constructor we'll use to pass in our calling activity's
	 * context, group items, and child items
	 */
	/*
	 * public AdapterCheckBox(Context context, ArrayList<String>
	 * listDataGroup,HashMap<String, List<String>> listDataChild){
	 * 
	 * mContext = context; mListDataGroup = listDataGroup; mListDataChild =
	 * listDataChild;
	 * 
	 * // Initialize our hashmap containing our check states here
	 * mChildCheckStates = new HashMap<Integer, boolean[]>(); }
	 */

	/*
	 * public AdapterCheckBox(Context context, List<ChildCookDetail> header,
	 * HashMap<String, List<String>> child, FragmentCommunicatorNew frag_new) {
	 * 
	 * mContext = context; this.header = header; this.child = child;
	 * this.frag_new = frag_new; // Initialize our hashmap containing our check
	 * states here mChildCheckStates = new HashMap<Integer, boolean[]>(); }
	 */
	public AdapterCheckBoxFoodSearch(Context context,
			List<ChildCookDetail> header,
			HashMap<String, LinkedHashMap<Integer, String>> child,
			FragmentCommunicatorNew frag_new) {

		mContext = context;
		this.header = header;
		this.child2 = child;
		this.frag_new = frag_new;
		Log.e("inside adaptercheckboxfood", "search");
		// Initialize our hashmap containing our check states here
		mChildCheckStates = new HashMap<Integer, boolean[]>();
	}

	public int getNumberOfCheckedItemsInGroup(int mGroupPosition) {
		boolean getChecked[] = mChildCheckStates.get(mGroupPosition);
		int count = 0;
		if (getChecked != null) {
			for (int j = 0; j < getChecked.length; ++j) {
				if (getChecked[j] == true)
					count++;
			}
		}
		return count;
	}

	public HashMap<String, Integer> getValueOfCheckedItemsInGroup() {
		String value = "";
		hashmap = new HashMap<>();
		for (int mGroupPosition = 0; mGroupPosition < getGroupCount(); mGroupPosition++) {

			boolean getChecked[] = mChildCheckStates.get(mGroupPosition);
			int count = 0;

			if (getChecked != null) {
				for (int j = 0; j < getChecked.length; ++j) {
					if (getChecked[j] == true) {
						// count++;
						// value = getChild(mGroupPosition, j);
						@SuppressWarnings("unchecked")
						LinkedHashMap<Integer, String> linkedHashMap = (LinkedHashMap<Integer, String>) getChild(
								mGroupPosition, j);

						Integer value1 = (new ArrayList<Integer>(
								linkedHashMap.keySet())).get(j);
						switch (mGroupPosition) {
						case 0:
							hashmap.put("dtype", value1);
							break;
						case 1:
							hashmap.put("order_type", value1);
							break;
						case 2:
							hashmap.put("ctype", value1);
							break;
						default:
							break;
						}
						Log.e("click child", "" + value1);
					}
				}

			}
		}
		/*
		 * if(hashmap!=null) { if(hashmap.containsKey("dtype")) { Log.e("dtype",
		 * hashmap.get("dtype")); }else{ Log.e("dtype else", "part"); }
		 * if(hashmap.containsKey("order_type")){ Log.e("order_type",
		 * hashmap.get("order_type")); }else{ Log.e("order type else", "part");
		 * }if(hashmap.containsKey("ctype")){ Log.e("ctype",
		 * hashmap.get("ctype")); }else{ Log.e("ctype", "part"); }
		 * 
		 * }
		 */
		return hashmap;
	}

	@Override
	public int getGroupCount() {
		Log.e("the group list type",""+header.size());
		return header.size();
		
	}

	/*
	 * This defaults to "public object getGroup" if you auto import the methods
	 * I've always make a point to change it from "object" to whatever item I
	 * passed through the constructor
	 */
	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		return header.get(groupPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {

		// I passed a text string into an activity holding a getter/setter
		// which I passed in through "ExpListGroupItems".
		// Here is where I call the getter to get that text
		ChildCookDetail child = (ChildCookDetail) getGroup(groupPosition);

		groupText = child.getName();
		if (convertView == null) {

			LayoutInflater inflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.filter_group, null);

			// Initialize the GroupViewHolder defined at the bottom of this
			// document
			groupViewHolder = new GroupViewHolder();

			groupViewHolder.mGroupText = (TextView) convertView
					.findViewById(R.id.lblListHeader);

			convertView.setTag(groupViewHolder);
		} else {

			groupViewHolder = (GroupViewHolder) convertView.getTag();
		}

		groupViewHolder.mGroupText.setText(groupText);

		ImageView img_icon = (ImageView) convertView
				.findViewById(R.id.img_icon);
		ImageView img_indicator = (ImageView) convertView
				.findViewById(R.id.img_indicator);
		RelativeLayout lnr_grp = (RelativeLayout) convertView
				.findViewById(R.id.lnr_grp);
		TextView lblListHeader = (TextView) convertView
				.findViewById(R.id.lblListHeader);
		lblListHeader.setTypeface(null, Typeface.BOLD);
		lblListHeader.setText(groupText);
		img_icon.setImageResource(child.getImg_id());
		if (isExpanded) {

			img_indicator.setImageResource(R.drawable.exp_minus);
			lnr_grp.setBackgroundColor(Color.WHITE); //
		} else
			img_indicator.setImageResource(R.drawable.exp_plus);
		lnr_grp.setBackgroundColor(Color.WHITE);
		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		/*
		 * Log.e("test_size", "" +
		 * child2.get(header.get(groupPosition).getName()).size());
		 */
		return child2.get(header.get(groupPosition).getName()).size();

	}

	/*
	 * This defaults to "public object getChild" if you auto import the methods
	 * I've always make a point to change it from "object" to whatever item I
	 * passed through the constructor
	 */

	@Override
	public Object getChild(int groupPosition, int childPosition) {

		return child2.get(header.get(groupPosition).getName());

	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		final int mGroupPosition = groupPosition;
		final int mChildPosition = childPosition;

		LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.filter_child, null);
			childViewHolder = new ChildViewHolder();
			childViewHolder.mChildText = (TextView) convertView
					.findViewById(R.id.lblListItem);
			childViewHolder.mCheckBox = (CheckBox) convertView
					.findViewById(R.id.check);
			convertView.setTag(R.layout.filter_child, childViewHolder);
		}else{
			childViewHolder = (ChildViewHolder) convertView
					.getTag(R.layout.filter_child);
		}
		@SuppressWarnings("unchecked")
		LinkedHashMap<Integer, String> linkedHashMap = (LinkedHashMap<Integer, String>) getChild(
				mGroupPosition, mChildPosition);
		if (linkedHashMap != null) {
			String value = (new ArrayList<String>(linkedHashMap.values()))
					.get(childPosition);
			Log.e("the string value", value);
			childViewHolder.mChildText.setText(value);
		} else {
			Log.e("the linked hash map", "is null");

		}

		childViewHolder.mCheckBox.setOnCheckedChangeListener(null);

		if (mChildCheckStates.containsKey(mGroupPosition)) {
			/*
			 * if the hashmap mChildCheckStates<Integer, Boolean[]> contains
			 * the value of the parent view (group) of this child (aka, the
			 * key), then retrive the boolean array getChecked[]
			 */
			boolean getChecked[] = mChildCheckStates.get(mGroupPosition);

			// set the check state of this position's checkbox based on the
			// boolean value of getChecked[position]
			childViewHolder.mCheckBox
					.setChecked(getChecked[mChildPosition]);

		} else {

			/*
			 * if the hashmap mChildCheckStates<Integer, Boolean[]> does not
			 * contain the value of the parent view (group) of this child
			 * (aka, the key), (aka, the key), then initialize getChecked[]
			 * as a new boolean array and set it's size to the total number
			 * of children associated with the parent group
			 */
			boolean getChecked[] = new boolean[getChildrenCount(mGroupPosition)];

			// add getChecked[] to the mChildCheckStates hashmap using
			// mGroupPosition as the key
			mChildCheckStates.put(mGroupPosition, getChecked);

			// set the check state of this position's checkbox based on the
			// boolean value of getChecked[position]
			childViewHolder.mCheckBox.setChecked(false);
		}

		childViewHolder.mCheckBox
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {

						if(InternetCheck.isInternetConnected(mContext)){
						
						boolean getChecked[] = mChildCheckStates
								.get(mGroupPosition);

						for (int i = 0; i < getChecked.length; i++) {
							getChecked[i] = false;
						}

						getChecked[mChildPosition] = isChecked;
						mChildCheckStates.put(mGroupPosition, getChecked);
						frag_new.checkedData(getValueOfCheckedItemsInGroup());
					/*	frag_new.checkedData(getValueOfCheckedItemsInGroup());
						notifyDataSetChanged();*/
						
						}else{
							Toast.makeText(mContext, "Please connect to Internet", Toast.LENGTH_SHORT).show();
						}
						
					}
				});

		return convertView;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return false;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	public final class GroupViewHolder {

		TextView mGroupText;
	}

	public final class ChildViewHolder {

		TextView mChildText;
		CheckBox mCheckBox;
	}

	
}
