package com.ikomet.foodfiestaa.adapter;

import java.util.ArrayList;

import java.util.Locale;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.beans.OrderChild;
import com.ikomet.foodfiestaa.beans.OrderGroup;
import com.ikomet.foodfiestaa.beans.Orders;
import com.ikomet.foodfiestaa.utils.DialogOrderSummary;

public class OrderExpandListAdapter extends BaseExpandableListAdapter {

	private Context context;
	private ArrayList<OrderGroup> groups;
	private ArrayList<Orders> orderList;
	private ArrayList<OrderGroup> groups_new;
	private ArrayList<Orders> orderList_new;

	public OrderExpandListAdapter(Context context, ArrayList<OrderGroup> groups,
			ArrayList<Orders> orderList) {
		this.context = context;
		this.groups = groups;
		this.orderList = orderList;
		groups_new = new ArrayList<OrderGroup>();
		orderList_new = new ArrayList<Orders>();
		this.groups_new.addAll(groups);
		this.orderList_new.addAll(orderList);
	}

	@Override
	public Object getGroup(int groupPosition) {
		return groups.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return groups.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(final int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		OrderGroup group = (OrderGroup) getGroup(groupPosition);
		if (convertView == null) {
			LayoutInflater inf = (LayoutInflater) this.context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inf.inflate(R.layout.exp_orders_group, null);
		}
		RelativeLayout layoutStatus = (RelativeLayout) convertView
				.findViewById(R.id.rel_ord_1);
		ImageView statusIcon = (ImageView) convertView
				.findViewById(R.id.img_status);
		TextView status = (TextView) convertView.findViewById(R.id.txt_status);
		TextView orderNo = (TextView) convertView.findViewById(R.id.txt_ord_no);
		TextView totalPrice = (TextView) convertView
				.findViewById(R.id.txt_total_price);
		ImageView arrow = (ImageView) convertView.findViewById(R.id.img_arrow);
		// View view = (View) convertView.findViewById(R.id.view1);

		if (status.getText().toString().trim().equalsIgnoreCase("delivered")) {
			statusIcon.setImageResource(R.drawable.deliveredicon);
		} else if(status.getText().toString().trim().equalsIgnoreCase("paid")){
			statusIcon.setImageResource(R.drawable.paidicon);
		}
		status.setText(group.getStatus());
		
		orderNo.setText(group.getOrder_no());
		totalPrice.setText("₹ " + group.getTotal_price());
		orderNo.setPaintFlags(orderNo.getPaintFlags()
				| Paint.UNDERLINE_TEXT_FLAG);

		if (isExpanded) {
			layoutStatus.setBackgroundColor(context.getResources().getColor(
					R.color.colorDarkRed));
			status.setTextColor(Color.WHITE);
			arrow.setImageResource(R.drawable.up);
		} else {
			layoutStatus.setBackgroundColor(Color.WHITE);
			status.setTextColor(context.getResources().getColor(
					R.color.colorDarkRed));
			arrow.setImageResource(R.drawable.down);
		}
		// tv.setText(group.getName());

		orderNo.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				DialogOrderSummary dialog = new DialogOrderSummary(context,
						orderList, groupPosition);
				dialog.show();
				dialog.setCancelable(true);
				
			}
		});

		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		// TODO Auto-generated method stub
		ArrayList<OrderChild> chList = groups.get(groupPosition).getChildList();
		Log.e("size of child", "" + chList.size());
		return chList.size();
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		ArrayList<OrderChild> chList = groups.get(groupPosition).getChildList();
		return chList.get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		OrderChild child = (OrderChild) getChild(groupPosition, childPosition);
		Log.e("child object", child.toString());
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this.context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.exp_child_order, null);
		}
		TextView orderedBy = (TextView) convertView
				.findViewById(R.id.txt_orderedby);
		TextView transId = (TextView) convertView
				.findViewById(R.id.txt_trans_id);
		// TextView orderedDate = (TextView)
		// convertView.findViewById(R.id.txt_order_date);
		// TextView orderedTime = (TextView)
		// convertView.findViewById(R.id.txt_order_time);

		orderedBy.setText(child.getOrdered_by());
		transId.setText(child.getTrans_id());
		// orderedDate.setText(child.getDate());
		// orderedTime.setText(child.getTime());

		return convertView;
	}

	
	// Filter Class
			public void filter(String charText) {
				charText = charText.toLowerCase(Locale.getDefault());
				groups.clear();
				orderList.clear();
				if (charText.length() == 0) {
					groups.addAll(groups_new);
					orderList.addAll(orderList_new);
				} else {
					for (OrderGroup wp : groups_new) {
						if (wp.getOrder_no().toLowerCase(Locale.getDefault())
								.contains(charText) || wp.getChildList().get(0).getOrdered_by().toLowerCase(Locale.getDefault())
								.contains(charText)) {
							groups.add(wp);
						}
					}
					for(Orders od : orderList_new) {
						if (od.getOrder_number().toLowerCase(Locale.getDefault())
								.contains(charText) || od.getConsumer_name().toLowerCase(Locale.getDefault())
								.contains(charText)) {
							orderList.add(od);
						}
					}
				}
				notifyDataSetChanged();
			}
	
}
