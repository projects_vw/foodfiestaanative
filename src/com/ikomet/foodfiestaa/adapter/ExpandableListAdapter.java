package com.ikomet.foodfiestaa.adapter;

import java.util.HashMap;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.beans.ChildCookDetail;

public class ExpandableListAdapter extends BaseExpandableListAdapter{
Context context;
private HashMap<String, List<ChildCookDetail>> _listDataChild;
private List<String> _listDataHeader; 

	public ExpandableListAdapter(Context context,
		HashMap<String, List<ChildCookDetail>> _listDataChild,
		List<String> _listDataHeader) {
	super();
	this.context = context;
	this._listDataChild = _listDataChild;
	this._listDataHeader = _listDataHeader;
}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return _listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition);
		
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return childPosition;
		
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ChildCookDetail detail=(ChildCookDetail)getChild(groupPosition, childPosition);
		
		 if (convertView == null) {
	            LayoutInflater infalInflater = (LayoutInflater) this.context
	                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	       convertView = infalInflater.inflate(R.layout.list_item, null);
	        }
	 
	        TextView txtListChild = (TextView) convertView
	                .findViewById(R.id.lblListItem);
	        ImageView imageview = (ImageView)convertView.findViewById(R.id.img_exp);
	    	TextView lblDescription=(TextView)convertView.findViewById(R.id.lblDescription);
	       if(detail.getName()=="Change Password")
	       {   
	    	   txtListChild.setTextColor(Color.parseColor("#228B22"));
	    	   txtListChild.setText("Change Password");
	    	   
	       }else{
	    	   txtListChild.setTextColor(Color.parseColor("#000000"));
	    	   txtListChild.setText(detail.getName());
	       }
	    	
	    	txtListChild.setText(detail.getName());
	        imageview.setImageResource(detail.getImg_id());
	     
	        lblDescription.setText(detail.getDes_title());
	        
	        lblDescription.setTextColor(Color.parseColor("#228B22"));
	        return convertView;
		}

	@Override
	public int getChildrenCount(int groupPosition) {
		// TODO Auto-generated method stub
		return _listDataChild.get(_listDataHeader.get(groupPosition)).size();
		
	}

	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		return _listDataHeader.get(groupPosition);
		
	
	}

	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return _listDataHeader.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		String grouptitle=(String)getGroup(groupPosition);
		if(convertView==null)
		{
			LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			 convertView = inflater.inflate(R.layout.linear_group, null);
		}
		TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
	
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(grouptitle);
      
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return true;
	}

}
