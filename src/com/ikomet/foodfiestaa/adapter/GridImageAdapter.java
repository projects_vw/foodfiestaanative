package com.ikomet.foodfiestaa.adapter;

import com.ikomet.foodfiestaa.R;
import com.squareup.picasso.Picasso;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class GridImageAdapter extends BaseAdapter {
    private Context mContext;
    private String[] images;
    
    // Constructor
    public GridImageAdapter(Context c, String[] images){
        mContext = c;
        this.images = images;
    }
 
    @Override
    public int getCount() {
        return images.length;
    }
 
    @Override
    public Object getItem(int position) {
        return images[position];
    }
 
    @Override
    public long getItemId(int position) {
        return 0;
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView = new ImageView(mContext);

   		 Picasso.with(mContext).load("http://beta.foodfiestaa.com/"+images[position])
   		    .placeholder(R.drawable.two)
   		    .error(R.drawable.two)
   		    .fit()
   		    .into(imageView);
        
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setLayoutParams(new GridView.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        return imageView;
    }
 
}