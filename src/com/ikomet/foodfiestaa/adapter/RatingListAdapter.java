package com.ikomet.foodfiestaa.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.beans.Reviews;
import com.squareup.picasso.Picasso;

public class RatingListAdapter extends
		RecyclerView.Adapter<RatingListAdapter.ViewHolder> {

	Context context;
	ArrayList<Reviews> reviewsList;

	public RatingListAdapter(Context context, ArrayList<Reviews> reviewsList) {
		super();
		this.context = context;
		this.reviewsList = reviewsList;

	}

	class ViewHolder extends RecyclerView.ViewHolder {
		ImageView profile;
		TextView emailId, txtReview, txtDate, txtTime;
		RatingBar ratingBar;

		public ViewHolder(View arg0) {
			super(arg0);
			// TODO Auto-generated constructor stub
			profile = (ImageView) arg0.findViewById(R.id.img_profile);
			emailId = (TextView) arg0.findViewById(R.id.txt_mail);
			txtReview = (TextView) arg0.findViewById(R.id.txt_review);
			txtDate = (TextView) arg0.findViewById(R.id.txt_date);
			txtTime = (TextView) arg0.findViewById(R.id.txt_time);
			ratingBar = (RatingBar) arg0.findViewById(R.id.ratingBar1);
		}

	}

	@Override
	public int getItemCount() {
		// TODO Auto-generated method stub
		return reviewsList.size();
	}

	@Override
	public void onBindViewHolder(final ViewHolder arg0, int arg1) {
		// TODO Auto-generated method stub
		Picasso.with(context).load(reviewsList.get(arg1).getPic())
				.centerInside().placeholder(R.drawable.two);
		arg0.emailId.setText(reviewsList.get(arg1).getName());
		arg0.txtReview.setText(reviewsList.get(arg1).getMessage());
		arg0.txtDate.setText(reviewsList.get(arg1).getDate());
		arg0.txtTime.setText(reviewsList.get(arg1).getTime());
		arg0.ratingBar.setRating(Float.parseFloat(reviewsList.get(arg1).getRating()));
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
		// TODO Auto-generated method stub
		View v = LayoutInflater.from(arg0.getContext()).inflate(
				R.layout.list_row_rating, arg0, false);
		ViewHolder holder = new ViewHolder(v);
		return holder;
	}

}
