package com.ikomet.foodfiestaa.adapter;

import java.util.ArrayList;


import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.activities.LocationSearchResultPage;
import com.ikomet.foodfiestaa.beans.LocSearchKitchenDetails;
import com.ikomet.foodfiestaa.fragments.Frag_SearchSinglekitchen;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

public class AdptrLocationSearchKitchen extends RecyclerView.Adapter<AdptrLocationSearchKitchen.ViewHolder>{

	Context context;
	ArrayList<LocSearchKitchenDetails> kitchen_details;
FragmentManager frag_manager;
 FragmentTransaction trans;
ActionBar actionbar;
	public AdptrLocationSearchKitchen(Context context,
			ArrayList<LocSearchKitchenDetails> kitchen_details) {
		super();
		this.context = context;
		this.kitchen_details = kitchen_details;
		
	}

	class ViewHolder extends RecyclerView.ViewHolder implements OnClickListener{
		RelativeLayout rel_2;
		TextView txt_kitchenname,txt_location;
		ImageView img_specialtype;
		public ViewHolder(View arg0) {
			super(arg0);
			// TODO Auto-generated constructor stub
			rel_2=(RelativeLayout)arg0.findViewById(R.id.rel_2);
			txt_kitchenname=(TextView)arg0.findViewById(R.id.txt_kitchenname);
			txt_location=(TextView)arg0.findViewById(R.id.txt_location);
			rel_2.setOnClickListener(this);
		}
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			int id=v.getId();
			int pos=getLayoutPosition();
			actionbar = ((ActionBarActivity) context).getSupportActionBar();
			actionbar.hide();
			String kitchen_id=kitchen_details.get(pos).getKitchen_id();
			frag_manager=((ActionBarActivity)context).getSupportFragmentManager();
			trans=frag_manager.beginTransaction();
			Bundle bundle=new Bundle();
			bundle.putString("kitchen_id", kitchen_id);
			bundle.putString("kitchen_name", kitchen_details.get(pos).getKitchen_name());
			bundle.putString("minimumprice", kitchen_details.get(pos).getMinimumprice());
			bundle.putString("location", kitchen_details.get(pos).getLocation());
			
			//	ft.add(R.id.realtabcontent,searchKitchen, "kitchen");
		Frag_SearchSinglekitchen searchKitchen=new Frag_SearchSinglekitchen();
		searchKitchen.setArguments(bundle);
		trans.replace(R.id.realtabcontent, searchKitchen);
		// trans.addToBackStack(null);
		LocationSearchResultPage.fragmentStack.push(searchKitchen);
			Toast.makeText(context, kitchen_id, 1000).show();
			trans.commit();
		
		}
		
	}

	@Override
	public int getItemCount() {
		// TODO Auto-generated method stub
		return kitchen_details.size();
	}

	@Override
	public void onBindViewHolder(final ViewHolder arg0, int arg1) {
		// TODO Auto-generated method stub
		String img_url = kitchen_details.get(arg1).getPic();
		Log.e("the image url is", img_url);
		Target target = new Target() {
			@Override
			public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
				// Bitmap is loaded, use image here

				arg0.rel_2.setBackground(new BitmapDrawable(context
						.getResources(), bitmap));
			}

			@Override
			public void onBitmapFailed(Drawable arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPrepareLoad(Drawable arg0) {
				// TODO Auto-generated method stub

			}
		};
		Picasso.with(context).load(img_url).into(target);
		arg0.txt_kitchenname.setText(kitchen_details.get(arg1).getKitchen_name());
		arg0.txt_location.setText(kitchen_details.get(arg1).getLocation());
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
		// TODO Auto-generated method stub
		View v=LayoutInflater.from(arg0.getContext()).inflate(R.layout.single_search_ritem1, arg0, false);
		ViewHolder holder=new ViewHolder(v);
		return holder;
	}
}
