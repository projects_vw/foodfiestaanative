package com.ikomet.foodfiestaa.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ikomet.foodfiestaa.R;

public class ListAdapterSingleText extends BaseAdapter{

	Context context;
	ArrayList<String> cat = new ArrayList<String>();
	
	public ListAdapterSingleText(Context context, ArrayList<String> cat){
		this.context = context;
		this.cat = cat;
		
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return cat.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return cat.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		if (convertView == null) {
		    LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		    convertView = vi.inflate(R.layout.single_text, parent, false);
		}    
		
		RelativeLayout rl_txtHolder = (RelativeLayout)convertView.findViewById(R.id.rl_singletxt);
		TextView txt = (TextView)convertView.findViewById(R.id.txt_single);
		
		txt.setText(cat.get(position));
		txt.setTypeface(Typeface.createFromAsset(context.getAssets(),
				"MyriadPro-Cond.ttf"));
		
		return convertView;
	}

}
