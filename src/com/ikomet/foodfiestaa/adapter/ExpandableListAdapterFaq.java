package com.ikomet.foodfiestaa.adapter;

import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ikomet.foodfiestaa.R;

public class ExpandableListAdapterFaq extends BaseExpandableListAdapter {

	Context context;
	List<String> header;
	//List<String> child;
	HashMap<String, String> hashmap_child;
	public ExpandableListAdapterFaq(Context context, List<String> header,
			HashMap<String, String> hashmap_child) {
		super();
		this.context = context;
		this.header = header;
		this.hashmap_child = hashmap_child;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
	
		
		return hashmap_child.get(header.get(groupPosition));
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		String childname=(String)getChild(groupPosition, childPosition);
		if(convertView==null)
		{
			LayoutInflater inflator=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView=inflator.inflate(R.layout.list_item, parent, false);
		}
		  TextView txtListChild = (TextView) convertView
	                .findViewById(R.id.lblListItem);
	 
	        txtListChild.setText(childname);
	        Log.e("test_print", childname);
	        return convertView;

	}

	@Override
	public int getChildrenCount(int groupPosition) {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		return header.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return header.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		String grouptitle=(String)getGroup(groupPosition);
		if(convertView==null)
		{
			LayoutInflater inflator=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView=inflator.inflate(R.layout.linear_group, parent, false);
		}
		LinearLayout lnr_grp=(LinearLayout)convertView.findViewById(R.id.lnr_grp);
		TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(grouptitle);
        
        
        if(isExpanded)
        {
        	lnr_grp.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        }
        else
        	lnr_grp.setBackgroundColor(Color.WHITE);
	        return convertView;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return true;
	}

}
