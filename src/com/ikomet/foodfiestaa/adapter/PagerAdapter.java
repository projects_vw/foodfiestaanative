package com.ikomet.foodfiestaa.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.ikomet.foodfiestaa.beans.ConsumerDetailBean;
import com.ikomet.foodfiestaa.beans.CookDetailBean;
import com.ikomet.foodfiestaa.fragments.Frag_editConsumer_address;
import com.ikomet.foodfiestaa.fragments.Frag_editConsumer_general;
import com.ikomet.foodfiestaa.fragments.Frag_editCook_address;
import com.ikomet.foodfiestaa.fragments.Frag_editCook_general;
import com.ikomet.foodfiestaa.fragments.Frag_editCook_payinfo;
import com.ikomet.foodfiestaa.fragments.FragmentPagerHowConsumer;
import com.ikomet.foodfiestaa.fragments.FragmentPagerHowCook;
import com.ikomet.foodfiestaa.fragments.Fragment_pager;

public class PagerAdapter extends FragmentPagerAdapter
{

    public static int marg1;
    CookDetailBean bean_detail;
    ConsumerDetailBean bean_details_consumer;
    int count;
    Fragment frag;
    int val;

    public PagerAdapter(FragmentManager fragmentmanager, int i, int j, CookDetailBean cookdetailbean, ConsumerDetailBean consumerdetailbean)
    {
        super(fragmentmanager);
        frag = null;
        count = i;
        val = j;
        bean_detail = cookdetailbean;
        bean_details_consumer = consumerdetailbean;
        Log.e("fragment no", (new StringBuilder()).append(j).toString());
        Log.e("count no", (new StringBuilder()).append(i).toString());
    }

    /*public PagerAdapter(FragmentManager fragmentmanager, int i, int j, ConsumerDetailBean consumerdetailbean)
    {
        super(fragmentmanager);
        frag = null;
        count = i;
        val = j;
        bean_details_consumer = consumerdetailbean;
        Log.e("fragment no", (new StringBuilder()).append(j).toString());
        Log.e("count no", (new StringBuilder()).append(i).toString());
    }*/
    
    public int getCount()
    {
        return count;
    }

    public Fragment getItem(int i)
    {
        marg1 = i;
        Log.e("test", (new StringBuilder("value of arg0 = ")).append(i).toString());
        frag = null;
        switch (val)
        {
        case 0: 
            i = 0;
            //home activity image slider
            while (i < count) 
            {
                frag = new Fragment_pager();
                Log.e("", "");
                i++;
            }
          break; 
        case 1: 
        	
        	//edit cook profile slider
            switch (i)
            {
            case 0: 
                frag = Frag_editCook_general.newInstance(bean_detail);
                break;

            case 1: 
                frag = Frag_editCook_address.newInstance(bean_detail);
                break;

            case 2:
                frag = Frag_editCook_payinfo.newInstance(bean_detail);
                break;
            }
            break;
        case 2: 
            switch (i)
            {
            case 0: 
                frag = Frag_editConsumer_general.newInstance(bean_details_consumer);
                break;

            case 1: 
                frag = Frag_editConsumer_address.newInstance(bean_details_consumer);
                break;
            }
            break;    
        }
        while (true) 
        {
            return frag;
        }
    }
}
