package com.ikomet.foodfiestaa.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.beans.CartBean;
import com.ikomet.foodfiestaa.beans.LocSrchSingleKitchen;
import com.ikomet.foodfiestaa.beans.SingleKitchenHeaderBean;
import com.ikomet.foodfiestaa.databases.DBHandler;
import com.ikomet.foodfiestaa.fragments.FragmentItemDetail;
import com.ikomet.foodfiestaa.interfaces.Cart;
import com.ikomet.foodfiestaa.utils.DialogVerification;
import com.ikomet.foodfiestaa.utils.SharedPrefs;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

public class AdptrSingleKitchenItems extends
		RecyclerView.Adapter<RecyclerView.ViewHolder> {

	private static final int TYPE_HEADER = 0;
	private static final int TYPE_ITEM = 1;
	Context context;
	CartBean cart_bean;
	Cart cart;
	LocSrchSingleKitchen save_kitchen_bean;
	ArrayList<LocSrchSingleKitchen> save_items;
	SingleKitchenHeaderBean header_bean;
	public ArrayList<LocSrchSingleKitchen> singlekitchenitems;
	int Type, i;
	int count = 0, cart_count = 0;
	String mid, fshortname, pic, foodprice, description, preorder_days,
			preorder_availble, tdyspl_availble, avgrating, kitchen_id;
	int item_quantity;
	DBHandler db;

	// int count=0,count_add=0,count_minus=0;

	public AdptrSingleKitchenItems(Context context,
			SingleKitchenHeaderBean header_bean,
			ArrayList<LocSrchSingleKitchen> singlekitchenitems, int Type) {
		super();
		this.context = context;
		cart = (Cart) context;
		this.header_bean = header_bean;
		this.Type = Type;
		this.singlekitchenitems = singlekitchenitems;
		db = new DBHandler(context);
		// db= new DBHandler(context, avgrating, null, Type);
	}

	class viewHolder extends RecyclerView.ViewHolder implements OnClickListener {
		int cart_count = 0;
		RelativeLayout rel1, rel2;
		TextView txt_title, txt_available, txt_price, txt_count;
		RatingBar ratingBar1;
		ImageButton img_minus, img_add;

		public viewHolder(View arg0) {
			super(arg0);
			// TODO Auto-generated constructor stub
			rel1 = (RelativeLayout) arg0.findViewById(R.id.rel_1);
			rel2 = (RelativeLayout) arg0.findViewById(R.id.rel_2);
			txt_title = (TextView) arg0.findViewById(R.id.txt_title);
			txt_available = (TextView) arg0.findViewById(R.id.txt_available);
			txt_price = (TextView) arg0.findViewById(R.id.txt_price);
			ratingBar1 = (RatingBar) arg0.findViewById(R.id.ratingBar1);
			img_minus = (ImageButton) arg0.findViewById(R.id.img_minus);
			img_add = (ImageButton) arg0.findViewById(R.id.img_add);
			txt_count = (TextView) arg0.findViewById(R.id.txt_count);
			
			rel1.setOnClickListener(this);
			img_add.setOnClickListener(this);
			img_minus.setOnClickListener(this);
			// txt_price.setText(""+count);
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			int img_id = v.getId();
			int pos = getLayoutPosition();
	
			if (img_id == R.id.img_add) {
				singlekitchenitems.get(pos)
				.getKitchen_id();
			if(db.isKitchenAvailable(singlekitchenitems.get(pos)
							.getKitchen_id())){
			
			if(db.isTodayorPre(singlekitchenitems.get(pos).getOrder_type())){	
				
				count = singlekitchenitems.get(pos).getItem_quantity();
				if (count == 0) {
					cart_bean = new CartBean(0, 1, singlekitchenitems.get(pos)
							.getKitchen_id(), singlekitchenitems.get(pos)
							.getMid(), singlekitchenitems.get(pos)
							.getFshortname(), singlekitchenitems.get(pos)
							.getFoodprice(), singlekitchenitems.get(pos).getOrder_type());
					 db.addItem(cart_bean);
					 singlekitchenitems.get(pos).setItem_quantity(1);

					
					
				}else{
					
					db.updateCart(count+1, singlekitchenitems.get(pos)
							.getMid());
					singlekitchenitems.get(pos).setItem_quantity(count+1);
					
				}				
				// save_items.
				cart.cartItem(0);
				notifyDataSetChanged();
			}else{
				DialogVerification verify=new DialogVerification(context, 2, "Order type must be same");
				verify.show();
			}
			
				}
				else{
					 DialogVerification verify=new DialogVerification(context, 2, "Kitchen must be same");
					 verify.show();
					Toast.makeText(context, "kitchen must be same", 1000).show();
				}
			} else if (img_id == R.id.img_minus) {
				
			if(db.isKitchenAvailable(singlekitchenitems.get(pos)
						.getKitchen_id())){
				count = singlekitchenitems.get(pos).getItem_quantity();

				if (count == 1) {

					db.deleteItem(singlekitchenitems.get(pos)
							.getMid());
					singlekitchenitems.get(pos).setItem_quantity(0);
				
				} else {
				if(count>0){
					db.updateCart(count-1, singlekitchenitems.get(pos)
							.getMid());
					singlekitchenitems.get(pos).setItem_quantity(count-1);
				}

				}
				cart.cartItem(0);
				notifyDataSetChanged();
			}
				else
				{
					 DialogVerification verify=new DialogVerification(context, 2, "Kitchen must be same");
					 verify.show();
				Toast.makeText(context, "kitchen must be same", 1000).show();
				}
			} else if (img_id == R.id.rel_1){
				
				String menu_id = singlekitchenitems.get(pos).getMid();
				SharedPrefs.putMenuId(context, menu_id);
				Fragment frag = new FragmentItemDetail();
				FragmentManager fm = ((ActionBarActivity) context).getSupportFragmentManager();
				FragmentTransaction ft = fm.beginTransaction();
				ft.replace(R.id.realtabcontent, frag);
				ft.commit();
								
			}
		}

	}

	class VHeader extends RecyclerView.ViewHolder {
		TextView txt_title, txt_area, txt_minimum;

		public VHeader(View arg0) {
			super(arg0);
			// TODO Auto-generated constructor stub
			txt_area = (TextView) arg0.findViewById(R.id.txt_area);
			txt_title = (TextView) arg0.findViewById(R.id.txt_title);
			txt_minimum = (TextView) arg0.findViewById(R.id.txt_minimum);
		}

	}

	@Override
	public int getItemCount() {
		// TODO Auto-generated method stub
		 Log.e("the kitchen list size", ""+singlekitchenitems.size());
		 if(singlekitchenitems.size()==0)
		 {
			 cart.nilItems();
		 }
		return singlekitchenitems.size();
	}

	@Override
	public void onBindViewHolder(ViewHolder arg0, int arg1) {
		// TODO Auto-generated method stub

		if (arg0 instanceof VHeader) {
			VHeader Vheader = (VHeader) arg0;
			Vheader.txt_area.setText(header_bean.getTxt_area());
			Vheader.txt_title.setText(header_bean.getTxt_title());
			Vheader.txt_minimum.setText(header_bean.getTxt_minimum());

		} else if (arg0 instanceof viewHolder) {
			Log.e("test_find", ""
					+ singlekitchenitems.get(arg1).getItem_quantity());
			final viewHolder viewholder = (viewHolder) arg0;
			pic = singlekitchenitems.get(arg1).getPic().replace(" ", "%20");
			Target target = new Target() {
				@Override
				public void onBitmapLoaded(Bitmap bitmap,
						Picasso.LoadedFrom from) {
					// Bitmap is loaded, use image here

					viewholder.rel2.setBackground(new BitmapDrawable(context
							.getResources(), bitmap));
				}

				@Override
				public void onBitmapFailed(Drawable arg0) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onPrepareLoad(Drawable arg0) {
					// TODO Auto-generated method stub

				}
			};
			Picasso.with(context).load(pic).into(target);

			// initialization of values

			mid = singlekitchenitems.get(arg1).getMid();
			fshortname = singlekitchenitems.get(arg1).getFshortname();
			foodprice = singlekitchenitems.get(arg1).getFoodprice();
			description = singlekitchenitems.get(arg1).getDescription();
			preorder_days = singlekitchenitems.get(arg1).getPreorder_days();
			preorder_availble = singlekitchenitems.get(arg1)
					.getPreorder_availble();
			tdyspl_availble = singlekitchenitems.get(arg1).getTdyspl_availble();
			avgrating = singlekitchenitems.get(arg1).getAvgrating();
			kitchen_id = singlekitchenitems.get(arg1).getKitchen_id();
			item_quantity = singlekitchenitems.get(arg1).getItem_quantity();

			viewholder.txt_title.setText(singlekitchenitems.get(arg1)
					.getFshortname());
			viewholder.txt_price.setText("₹"
					+ singlekitchenitems.get(arg1).getFoodprice());

			viewholder.txt_count.setText(""
					+ singlekitchenitems.get(arg1).getItem_quantity());

			viewholder.ratingBar1.setRating(Float.parseFloat(singlekitchenitems
					.get(arg1).getAvgrating()));

			/*
			 * if(singlekitchenitems.get(arg1).getItem_quantity()>0) {
			 * save_kitchen_bean=new LocSrchSingleKitchen(kitchen_id, mid,
			 * fshortname, pic, foodprice, description, preorder_days,
			 * preorder_availble, tdyspl_availble, avgrating, item_quantity);
			 * save_items.add(save_kitchen_bean); Log.e("the items added",
			 * ""+save_items.size()); }
			 */
			if (Type == 1) {
				viewholder.txt_available.setText("Availabe Quantity:"
						+ singlekitchenitems.get(arg1).getTdyspl_availble());

			} else {
				viewholder.txt_available.setText("Availabe Quantity:"
						+ singlekitchenitems.get(arg1).getPreorder_availble());
			}
		}
	}

	@Override
	public int getItemViewType(int position) {
		if (isPositionHeader(position))
			return TYPE_HEADER;
		return TYPE_ITEM;
	}

	private boolean isPositionHeader(int position) {
		return position == 0;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
		// TODO Auto-generated method stub
		if (arg1 == TYPE_HEADER) {
			View v = LayoutInflater.from(arg0.getContext()).inflate(
					R.layout.single_itemkitchen_header, arg0, false);
			return new VHeader(v);
		} else {
			View v = LayoutInflater.from(arg0.getContext()).inflate(
					R.layout.singlekitchenitem, arg0, false);
			return new viewHolder(v);
		}

	}

}
