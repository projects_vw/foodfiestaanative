package com.ikomet.foodfiestaa.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.beans.DishDetailBean;
import com.ikomet.foodfiestaa.fragments.FragmentEditDish;
import com.ikomet.foodfiestaa.utils.Dialog_preorder_edit;
import com.ikomet.foodfiestaa.utils.ServiceHandler;
import com.ikomet.foodfiestaa.utils.SharedPrefs;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

public class AdptrSignatureDishes extends
		RecyclerView.Adapter<AdptrSignatureDishes.MasonryView> {
	Context context;
	DishDetailBean dish_bean;
	int value_test = 0;
	private ArrayList<DishDetailBean> list_dish;
	int[] imgList = { R.drawable.ic_launcher, R.drawable.ic_launcher,
			R.drawable.ic_launcher, R.drawable.ic_launcher,
			R.drawable.ic_launcher, R.drawable.ic_launcher,
			R.drawable.ic_launcher, R.drawable.ic_launcher,
			R.drawable.ic_launcher, R.drawable.ic_launcher };
	String[] nameList = { "One", "Two", "Three", "Four", "Five", "Six",
			"Seven", "Eight", "Nine", "Ten" };
	String test="";
	/*
	 * public MasonryAdapter(Context context) { this.context = context; }
	 */
	public AdptrSignatureDishes(Context context, ArrayList<DishDetailBean> dish_bean) {
		super();
		this.context = context;
		this.list_dish = dish_bean;
	}

	@Override
	public MasonryView onCreateViewHolder(ViewGroup parent, int viewType) {
		View layoutView = LayoutInflater.from(parent.getContext()).inflate(
				R.layout.single_item_signaturedishes, parent, false);
		MasonryView masonryView = new MasonryView(layoutView);
		return masonryView;
	}

	@Override
	public void onBindViewHolder(final MasonryView holder, int position) {

		dish_bean = list_dish.get(position);
		String img_url = dish_bean.getFpic();

		Target target = new Target() {
			@Override
			public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
				// Bitmap is loaded, use image here

				holder.rel_img.setBackground(new BitmapDrawable(context
						.getResources(), bitmap));
			}

			@Override
			public void onBitmapFailed(Drawable arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPrepareLoad(Drawable arg0) {
				// TODO Auto-generated method stub

			}
		};
		Picasso.with(context).load(img_url).into(target);
		// Picasso.with(context).load(img_url).into((Target) holder.rel_img);

		holder.txt_foodname.setText(dish_bean.getFshortname());
		holder.txt_foodtype.setText(dish_bean.getFoodname());
		holder.txt_price.setText("₹" + dish_bean.getPrice());

		if((dish_bean.getTodayspecial_active().equals("1"))&&(dish_bean.getPreorder_active().equals("1")))
		{
			test = "both";
			holder.img_todayspl.setClickable(true);
			holder.img_preorder.setClickable(true);
			holder.img_todayspl.setImageResource(R.drawable.todayspecial1);
			holder.img_preorder.setImageResource(R.drawable.preorder1);
		}
		else if(dish_bean.getTodayspecial_active().equals("1")){
			test = "today";
			holder.img_todayspl.setClickable(false);
			holder.img_preorder.setClickable(true);
			holder.img_todayspl.setImageResource(R.drawable.todayspecial1);
			holder.img_preorder.setImageResource(R.drawable.greypreorder1);
		}
		else{
			test = "pre";
			holder.img_todayspl.setClickable(true);
			holder.img_preorder.setClickable(false);
			holder.img_todayspl.setImageResource(R.drawable.greytodayspecial1);
			holder.img_preorder.setImageResource(R.drawable.preorder1);
		}
	}

	@Override
	public int getItemCount() {
		return list_dish.size();
	}

	class MasonryView extends RecyclerView.ViewHolder implements
			View.OnClickListener {
		ImageView imageView, img_preorder, img_todayspl;
		TextView txt_foodname, txt_foodtype, txt_price;
		RelativeLayout rel_img, card_item;

		public MasonryView(View arg0) {
			super(arg0);
			rel_img = (RelativeLayout) arg0.findViewById(R.id.rel_2);
			card_item = (RelativeLayout) arg0.findViewById(R.id.rel_1);
			txt_foodname = (TextView) arg0.findViewById(R.id.txt_title);
			txt_foodtype = (TextView) arg0.findViewById(R.id.txt_foodtype);
			img_preorder = (ImageView) arg0.findViewById(R.id.img_preorder);
			img_todayspl = (ImageView) arg0.findViewById(R.id.img_todayspl);
			txt_price = (TextView) arg0.findViewById(R.id.txt_price);
			img_preorder.setOnClickListener(this);
			img_todayspl.setOnClickListener(this);
			card_item.setOnClickListener(this);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			int id = v.getId();
			int pos = getLayoutPosition();
			dish_bean = list_dish.get(pos);
			if((dish_bean.getTodayspecial_active().equals("1"))&&(dish_bean.getPreorder_active().equals("1")))
			{
//				value_test=1;
				test = "both";
				img_todayspl.setClickable(true);
				img_preorder.setClickable(true);
				img_todayspl.setImageResource(R.drawable.todayspecial1);
				img_preorder.setImageResource(R.drawable.preorder1);
			}
			else if(dish_bean.getTodayspecial_active().equals("1")){
				test = "today";
				img_todayspl.setClickable(false);
				img_preorder.setClickable(true);
				img_todayspl.setImageResource(R.drawable.todayspecial1);
				img_preorder.setImageResource(R.drawable.greypreorder1);
			}
			else{
				test = "pre";
				img_todayspl.setClickable(true);
				img_preorder.setClickable(false);
				img_todayspl.setImageResource(R.drawable.greytodayspecial1);
				img_preorder.setImageResource(R.drawable.preorder1);
			}
			String user_id = SharedPrefs.getUid(context), token = SharedPrefs
					.getToken(context);
			
			if (id == R.id.img_preorder) {
				// String foodshortname=list_dish.get(pos).getFshortname();
				// String test=list_dish.get(pos).getTodayspecial();
				/*if(value_test==1)
				{
					String param="id="+user_id+"&token="+token+"&menuid="+dish_bean.getMid()+"&preorder_flag=1";
							
					new async_update().execute("http://beta.foodfiestaa.com/preorderservice",param);
				}
				if (dish_bean.getPreorder_active() == "0") {

					Dialog_preorder_edit preorder = new Dialog_preorder_edit(
							context, list_dish.get(pos).getPreorder_maxorder(),
							list_dish.get(pos).getFshortname(), list_dish.get(
									pos).getMid());
					Toast.makeText(context,
							list_dish.get(pos).getPreorder_active(), 1000).show();
					preorder.show();
				}*/

					if(test.equals("both")){
						String param="id="+user_id+"&token="+token+"&menuid="+dish_bean.getMid()+"&preorder_flag=0";
						new async_update().execute("http://beta.foodfiestaa.com/preorderservice",param);
					}else if(test.equals("today")){
						Dialog_preorder_edit preorder = new Dialog_preorder_edit(
								context, list_dish.get(pos).getPreorder_maxorder(),
								list_dish.get(pos).getFshortname(), list_dish.get(
										pos).getMid());
						Toast.makeText(context,
								list_dish.get(pos).getPreorder_active(), 1000).show();
						preorder.show();
					}
				
			}
		
			if (id == R.id.img_todayspl) {
				 
						/*if(value_test==1)
						{
							String param="id="+user_id+"&token="+token+"&menuid="+dish_bean.getMid()+"&todayspecial_flag=1";
							
							new async_update().execute("http://beta.foodfiestaa.com/todayservice",param);
						}
					DialogTodaySpecial today_special = new DialogTodaySpecial(
							context, list_dish.get(pos).getTodayspecial(),
							list_dish.get(pos).getFshortname(), list_dish.get(
									pos).getMid());
					today_special.show();
					Toast.makeText(context,
							list_dish.get(pos).getPreorder_active(), 1000)
							.show();
				
			}*/ 
				
				if(test.equals("both")){
					String param="id="+user_id+"&token="+token+"&menuid="+dish_bean.getMid()+"&todayspecial_flag=0";
					new async_update().execute("http://beta.foodfiestaa.com/todayservice",param);
				}else if(test.equals("pre")){
					Dialog_preorder_edit preorder = new Dialog_preorder_edit(
							context, list_dish.get(pos).getPreorder_maxorder(),
							list_dish.get(pos).getFshortname(), list_dish.get(
									pos).getMid());
					Toast.makeText(context,
							list_dish.get(pos).getPreorder_active(), 1000).show();
					preorder.show();
				}
				
		}
			
		if(id == R.id.rel_1){
			
			Fragment frag = new FragmentEditDish(context, dish_bean);
			FragmentManager fm = ((FragmentActivity) context).getSupportFragmentManager();
			FragmentTransaction ft = fm.beginTransaction();
			ft.replace(R.id.realtabcontent, frag);
			ft.commit();
		}

	}

	class async_update extends AsyncTask<String,Void, String>
	{

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			ServiceHandler handler=new ServiceHandler();
			Log.e("the params to be passed", params[0]+" "+params[1]);
			String result=handler.makeServiceCall(params[0], 1, params[1]);
			Log.e("Test_service", result);
			return null;
		}
		
	}
}
}
