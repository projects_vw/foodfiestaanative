package com.ikomet.foodfiestaa.fragments;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.support.v4.app.Fragment;
import android.support.v4.content.CursorLoader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.activities.CookActivity;
import com.ikomet.foodfiestaa.activities.CookChangePassword;
import com.ikomet.foodfiestaa.activities.EditCook_profile;
import com.ikomet.foodfiestaa.activities.HomeActivity;
import com.ikomet.foodfiestaa.adapter.ExpandableListAdapter;
import com.ikomet.foodfiestaa.beans.ChildCookDetail;
import com.ikomet.foodfiestaa.beans.ConsumerDetailBean;
import com.ikomet.foodfiestaa.beans.CookDetailBean;
import com.ikomet.foodfiestaa.utils.InternetCheck;
import com.ikomet.foodfiestaa.utils.ServiceHandler;
import com.ikomet.foodfiestaa.utils.SharedPrefs;
import com.squareup.picasso.Picasso;

public class Frag_profile_consumer extends Fragment implements
		View.OnClickListener {

	ExpandableListView exp_list_view;
	HashMap<String, String> info;
	List<String> header_title;
	Context context;
	ConsumerDetailBean bean_consumer_detail;
	List<ChildCookDetail> general_child, address_child;
	private final String USER_AGENT = "Mozilla/5.0";
	HashMap<String, List<ChildCookDetail>> list_child;
	private ProgressDialog pDialog;
	ImageView img_editprofile, img_profile, smiley;
	ExpandableListAdapter listAdapter;
	LinearLayout lnr_profile_turn;
	Dialog dialog;
	protected static final int REQUEST_CAMERA = 0;
	protected static final int SELECT_FILE = 1;
	protected static final int CROP_PIC = 2;
	private Uri picUri;
	Bitmap bitmap=null;
	String img_url;
	View view;
	RelativeLayout noNet, relProfile;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		view = inflater.inflate(R.layout.profilepage, container, false);
		context = getActivity();

		noNet = (RelativeLayout)view.findViewById(R.id.no_net);
		relProfile = (RelativeLayout)view.findViewById(R.id.rel_profile);
		smiley = (ImageView)view.findViewById(R.id.img_sad);
		
		if(InternetCheck.isInternetConnected(getActivity())){
			relProfile.setVisibility(View.VISIBLE);
			noNet.setVisibility(View.GONE);
			
			initializeVar();
			
		}else{
			relProfile.setVisibility(View.GONE);
			noNet.setVisibility(View.VISIBLE);
			
			noNet.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(InternetCheck.isInternetConnected(getActivity())){
						
						Animation a = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_anim);
						a.setDuration(1000);
						smiley.startAnimation(a);
						
						new Handler().postDelayed(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								relProfile.setVisibility(View.VISIBLE);
								noNet.setVisibility(View.GONE);
								initializeVar();
							}
						}, 2000);
																							
						/*relProfile.setVisibility(View.VISIBLE);
						noNet.setVisibility(View.GONE);
						initializeVar();*/
					}
				}
			});
			
		}
		
		
		return view;
	}

	private void initializeVar(){
		
		exp_list_view = (ExpandableListView) view
				.findViewById(R.id.exp_list_profile);
		img_editprofile = (ImageView) view.findViewById(R.id.img_editprofile);
		lnr_profile_turn = (LinearLayout) view
				.findViewById(R.id.lnr_profile_turn);
		img_profile = (ImageView) view.findViewById(R.id.img_profile);
		img_profile.setOnClickListener(this);
		img_editprofile.setOnClickListener(this);
		Log.e("inside profile cook", "yes");
		header_title = new ArrayList<>();
		AddingData();
		
		new async_profile_consumer().execute("");
		
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		
		
	}

	private class async_profile_consumer extends AsyncTask<String, Void, String> {
		String url = "http://beta.foodfiestaa.com/consumerprofile";
		String data, result;
		String id = SharedPrefs.getUid(getActivity()), token = SharedPrefs
				.getToken(getActivity());

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.setMessage("Please wait...");
			pDialog.show();
			// set param

		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub

			try {

				data = "";
				data = "id=" + URLEncoder.encode(id, "UTF-8") + "&token="
						+ URLEncoder.encode(token, "UTF-8");

				ServiceHandler sh = new ServiceHandler();

				result = sh.makeServiceCall(url, ServiceHandler.GET, data);
				Log.e("the result is", result);
				info = new HashMap<>();
				JSONObject data = null;

				JSONArray array = new JSONArray(result);
				for (int i = 0; i < array.length(); i++) {
					JSONObject obj = array.getJSONObject(i);
					String status = obj.getString("status");
					Log.e("status", status);
					data = obj.getJSONObject("data");
					
					bean_consumer_detail = new ConsumerDetailBean(
							data.getString("email"), data.getString("fname"), 
							data.getString("lname"), data.getString("mobile"),
							data.getString("address"), data.getString("city"),
							data.getString("state"), data.getString("country"),
							data.getString("pic"), data.getString("pincode"),
							data.getString("location"));
				
					img_url = "http://beta.foodfiestaa.com/"+data.getString("pic").replace(" ", "%20");
					
				}
				setDataExpandable(bean_consumer_detail);

				list_child = new HashMap<>();
				list_child.put(header_title.get(0), general_child);
				list_child.put(header_title.get(1), address_child);

			} catch (JSONException | UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return result;
		}

	
		
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (pDialog.isShowing()) {
				pDialog.dismiss();
			}
			
			Picasso.with(getActivity()).load(img_url).fit().
			placeholder(R.drawable.two).into(img_profile);
			
			listAdapter = new ExpandableListAdapter(getActivity(), list_child,
					header_title);
			exp_list_view.setAdapter(listAdapter);

			exp_list_view.setOnChildClickListener(new OnChildClickListener() {

				@Override
				public boolean onChildClick(ExpandableListView parent, View v,
						int groupPosition, int childPosition, long id) {
					// TODO Auto-generated method stub
					TextView txtListChild = (TextView) v
							.findViewById(R.id.lblListItem);

					if (txtListChild.getText().toString() == "Change Password") {
						Toast.makeText(getActivity(), "on Change password",
								1000).show();
						Intent intent = new Intent(getActivity(),
								CookChangePassword.class);
						startActivity(intent);
					}

					return false;
				}
			});
		}

}

	public void AddingData() {
		header_title.add("General Information");
		header_title.add("Address Information");

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		if (v.getId() == R.id.img_editprofile) {
			if (bean_consumer_detail == null) {
				Log.e("test_cookdetail", "the bean is null");
			} else {
				Intent intent = new Intent(getActivity(),
						EditCook_profile.class);
				intent.putExtra("object", bean_consumer_detail);
				startActivity(intent);
			}
		}else if(v.getId() == R.id.img_profile){
			
			dialog = new Dialog(getActivity());
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.dialog_edit_picture);
			
			TextView txtDial = (TextView)dialog.findViewById(R.id.txt_heading);
			ImageView imgTakePic = (ImageView)dialog.findViewById(R.id.img_take_pic);
			ImageView imgGalry = (ImageView)dialog.findViewById(R.id.img_gallery);
			
			txtDial.setText("Edit Profile Picture");
			
			imgTakePic.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog.dismiss();
					Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					//intent.putExtra("crop", "true");
					startActivityForResult(intent, REQUEST_CAMERA);
				}
			});
			
			imgGalry.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog.dismiss();
					Intent intent = new Intent(
							Intent.ACTION_PICK,
							android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
					intent.setType("image/*");
					//intent.putExtra("crop", "true");
					startActivityForResult(
							Intent.createChooser(intent, "Select File"),
							SELECT_FILE);
				}
			});
			
			dialog.show();
			
		}
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		/*
		 * if(pDialog.isShowing()) pDialog.dismiss();
		 */
		Log.e("inside the ", "resume of fragment profile cook");
		/*
		 * listAdapter = new ExpandableListAdapter(getActivity(), list_child,
		 * header_title); exp_list_view.setAdapter(listAdapter);
		 */

	}

	public void setDataExpandable(ConsumerDetailBean detail) {
		general_child = new ArrayList<>();
		address_child = new ArrayList<>();
		
		general_child.add(new ChildCookDetail(detail.getEmail(),
				R.drawable.ic_mail, ""));

		general_child.add(new ChildCookDetail(detail.getFname(),
				R.drawable.ic_user_name, ""));

		general_child.add(new ChildCookDetail(detail.getLname(),
				R.drawable.ic_user_name, ""));

		general_child.add(new ChildCookDetail(detail.getMobile(),
				R.drawable.ic_mobile, ""));
		
		general_child.add(new ChildCookDetail("Change Password",
				R.drawable.ic_key,""));

		address_child.add(new ChildCookDetail(detail.getAddress(),
				R.drawable.ic_streetboard1, ""));

		address_child.add(new ChildCookDetail(detail.getCity(),
				R.drawable.ic_location, ""));

		address_child.add(new ChildCookDetail(detail.getState(),
				R.drawable.ic_location, ""));
		
		address_child.add(new ChildCookDetail(detail.getLocation(),
				R.drawable.ic_location, ""));

		address_child.add(new ChildCookDetail(detail.getCountry(),
				R.drawable.ic_globe, ""));

		address_child.add(new ChildCookDetail(detail.getPincode(),
				R.drawable.ic_globe_pincode, ""));

	}
	
	private class EditProfilePic extends AsyncTask<String, Void, String> {
		String edit_img_url = "http://beta.foodfiestaa.com/consumereditprofile";
		String data, result;
		String id = SharedPrefs.getUid(getActivity()), token = SharedPrefs
				.getToken(getActivity());

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.setMessage("loading");
			pDialog.show();
			// set param

		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			
			HttpURLConnection conn = null;
			DataOutputStream dos = null;
			String lineEnd = "\r\n";
			String twoHyphens = "--";
			String boundary = "*****";
			int bytesRead, bytesAvailable, bufferSize;
			byte[] buffer;
			int maxBufferSize = 1 * 1024 * 1024;
			int serverResponseCode = 0;

			try {

				System.setProperty("http.keepAlive", "false");

				//encoding image into a byte array
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] imageBytes = baos.toByteArray();
                
                // open a URL connection to the Servlet
                ByteArrayInputStream fileInputStream = new ByteArrayInputStream(imageBytes);
                URL url = new URL(edit_img_url);
				
				
				// Open a HTTP connection to the URL
				conn = (HttpURLConnection) url.openConnection();
				conn.setDoInput(true); // Allow Inputs
				conn.setDoOutput(true); // Allow Outputs

				conn.setUseCaches(false); // Don't use a Cached Copy
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Accept-Encoding", "");
				// conn.setRequestProperty("Connection", "Keep-Alive");
				conn.setRequestProperty("ENCTYPE", "multipart/form-data");
				conn.setRequestProperty("Content-Type",
						"multipart/form-data;boundary=" + boundary);
				// conn.setRequestProperty("uploaded_file", "profile_picture");
				conn.setRequestProperty("id", id);
				conn.setRequestProperty("token", token);
				conn.setRequestProperty("pic", "pic");
				
				dos = new DataOutputStream(conn.getOutputStream());

				dos.writeBytes(twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=\"id\""
						+ lineEnd + lineEnd + id + lineEnd);

				dos.writeBytes(twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=\"token\""
						+ lineEnd + lineEnd + token + lineEnd);

				// forth parameter - filename
				dos.writeBytes(twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=\"pic\";filename=\""
						+ "pic" + "\"" + lineEnd);

				dos.writeBytes(lineEnd);
			
				// create a buffer of maximum size
				bytesAvailable = fileInputStream.available();
				
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				buffer = new byte[bufferSize];
				
				// read file and write it into form...
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);
				
				while (bytesRead > 0) {
					dos.write(buffer, 0, bufferSize);
					bytesAvailable = fileInputStream.available();
					bufferSize = Math.min(bytesAvailable, maxBufferSize);
					bytesRead = fileInputStream.read(buffer, 0, bufferSize);
				}
				
				// send multipart form data necesssary after file data...
				dos.writeBytes(lineEnd);
				dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

				// Responses from the server (code and message)
				serverResponseCode = conn.getResponseCode();

				String serverResponseMessage = conn.getResponseMessage();

				Log.i("uploadFile", "HTTP Response is : "
						+ serverResponseMessage + ": " + serverResponseCode);

				// close the streams //
				fileInputStream.close();
				dos.flush();
				dos.close();

				try {
					InputStream inp = null;

					inp = new BufferedInputStream(conn.getInputStream());

					ServiceHandler sh = new ServiceHandler();

					data = sh.readStream(inp);
					// Toast.makeText(getApplicationContext(), json,
					// 1000).show();
				} finally {
					conn.disconnect();
				}

			} catch (MalformedURLException ex) {
				Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
			} catch (Exception e) {
				String err = (e.getMessage() == null) ? "SD Card failed" : e
						.getMessage();
				Log.e("The caught exception is: ", err);
			}
			
			try {
				JSONObject obj1=new JSONObject(data);
				result=obj1.getString("message");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
						
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (pDialog.isShowing()) {
				pDialog.dismiss();
			}

			if(result!=null && !result.equalsIgnoreCase("")){
				Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
				new async_profile_consumer().execute();
			}else{
				Toast.makeText(getActivity(), "Error uploading Image, try again later", Toast.LENGTH_SHORT).show();
			}
			
			}

	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == REQUEST_CAMERA) {
//				picUri = data.getData();
				Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
				ByteArrayOutputStream bytes = new ByteArrayOutputStream();
				thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
				File destination = new File(
						Environment.getExternalStorageDirectory(),
						System.currentTimeMillis() + ".jpg");
				FileOutputStream fo;
				try {
					destination.createNewFile();
					picUri = Uri.fromFile(destination);
					fo = new FileOutputStream(destination);
					fo.write(bytes.toByteArray());
					fo.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				// ivImage.setImageBitmap(thumbnail);
				performCrop();

				
			} else if (requestCode == SELECT_FILE) {
				Uri selectedImageUri = data.getData();
				picUri = selectedImageUri;
				String[] projection = { MediaColumns.DATA };
				CursorLoader cursorLoader = new CursorLoader(getActivity(),
						selectedImageUri, projection, null, null, null);
				Cursor cursor = cursorLoader.loadInBackground();
				int column_index = cursor
						.getColumnIndexOrThrow(MediaColumns.DATA);
				cursor.moveToFirst();
				String selectedImagePath = cursor.getString(column_index);
				Log.e("test", "" + selectedImagePath);
				
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inJustDecodeBounds = true;
				BitmapFactory.decodeFile(selectedImagePath, options);
				final int REQUIRED_SIZE = 200;
				int scale = 1;
				while (options.outWidth / scale / 2 >= REQUIRED_SIZE
						&& options.outHeight / scale / 2 >= REQUIRED_SIZE)
					scale *= 2;
				options.inSampleSize = scale;
				options.inJustDecodeBounds = false;
				bitmap = BitmapFactory.decodeFile(selectedImagePath, options);

				performCrop();

			} else if (requestCode == CROP_PIC) { // get the returned data
				Bundle extras = data.getExtras(); // get the cropped bitmap
				//Bitmap thePic = extras.getParcelable("data");
				
				bitmap = extras.getParcelable("data");
								
				Log.e("PIC URI AFTER CROP", picUri + "");
				
				new EditProfilePic().execute();
				
			}
		}

	}
	
	private void performCrop() {
		// take care of exceptions
		try {
			// call the standard crop action intent (the user device may not
			// support it)
			Intent cropIntent = new Intent("com.android.camera.action.CROP");
			// indicate image type and Uri
			Log.e("PIC URI ON CROP", picUri + "");
			cropIntent.setDataAndType(picUri, "image/*");
			// set crop properties
			cropIntent.putExtra("crop", "true");
			// retrieve data on return
			cropIntent.putExtra("return-data", true);
			// start the activity - we handle returning in onActivityResult
			startActivityForResult(cropIntent, CROP_PIC);
		}
		// respond to users whose devices do not support the crop action
		catch (ActivityNotFoundException anfe) {
			Toast toast = Toast.makeText(getActivity(),
					"This device doesn't support the crop action!",
					Toast.LENGTH_SHORT);
			toast.show();
		}

	}
	

}