package com.ikomet.foodfiestaa.fragments;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.activities.ConsumerSignIn;
import com.ikomet.foodfiestaa.activities.LocationSearchResultPage;
import com.ikomet.foodfiestaa.beans.CartBean;
import com.ikomet.foodfiestaa.databases.DBHandler;
import com.ikomet.foodfiestaa.interfaces.Cart;
import com.ikomet.foodfiestaa.utils.SharedPrefs;

public class FragmentCart extends Fragment implements View.OnClickListener {

	View layout;
	TextView date, minOrder;
	TableLayout tableView;
	TableRow rowValues, row_delivery, row_total;
	ImageButton delete;
	TextView foodName, quantity, price;
	TextView subTotal, delivery, total;
	TextView cartEmpty;
	Button checkAsGuest, checkAsUser;
	DBHandler db;
	ArrayList<CartBean> cartList;
	Cart cart;
	DecimalFormat df;
	boolean isSigned;
	RelativeLayout snack;
	
	@Override
	public void onAttach(Context context) {
		// TODO Auto-generated method stub
		super.onAttach(context);
		cart = (Cart)context;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			 ViewGroup container,  Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		layout = inflater.inflate(R.layout.fragment_cart, container, false);
		
		snack = (RelativeLayout)layout.findViewById(R.id.snack);
		
		db = new DBHandler(getActivity());
		
		date = (TextView) layout.findViewById(R.id.txt_date);
		minOrder = (TextView) layout.findViewById(R.id.txt_min_order);
		tableView = (TableLayout) layout.findViewById(R.id.table_cart);

		cartEmpty = (TextView) layout.findViewById(R.id.txt_noitems_incart);
		
		subTotal = (TextView) layout.findViewById(R.id.txt_subtotal_price);
		total = (TextView) layout.findViewById(R.id.txt_total);
				
//		rowValues = (TableRow) layout.findViewById(R.id.row_values);

		checkAsGuest = (Button) layout.findViewById(R.id.bt_checkout_guest);
		checkAsUser = (Button) layout.findViewById(R.id.bt_checkout_user);

		Calendar c = Calendar.getInstance();
		System.out.println("Current time => " + c.getTime());
		
		System.out.println("Sys time in millis => " + c.getTimeInMillis());
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String formattedDate = sdf.format(c.getTime());
		
		System.out.println("Current Data ==== "+ formattedDate);
		
		date.setText(formattedDate);
		
		isSigned = SharedPrefs.hasSPData(getActivity());
						
		if(isSigned){
			checkAsGuest.setVisibility(View.GONE);
//			checkAsUser.setVisibility(View.VISIBLE);
			checkAsUser.setText("CHECK OUT");
		}/*else{
			checkAsGuest.setVisibility(View.VISIBLE);
			checkAsUser.setVisibility(View.VISIBLE);
		}*/
		
		checkAsGuest.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				float tot = Float.parseFloat(total.getText().toString().trim());
				if(tot>300){
					callFragment();
				}else{
					Snackbar.make(snack, "Order should not be less than Rs.300", Snackbar.LENGTH_SHORT).show();
				}
			}
		});
		
		checkAsUser.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				float tot = Float.parseFloat(total.getText().toString().trim());
				if(tot>300){
//					callFragment();
					if(!isSigned){
						Intent intent = new Intent(getActivity(), ConsumerSignIn.class);
						intent.putExtra("gocart", "gocart");
						startActivity(intent);
						getActivity().overridePendingTransition(R.anim.left_in, R.anim.right_out);
					}else{
						callFragment();
					}
				}else{
					Snackbar.make(snack, "Order should not be less than Rs.300", Snackbar.LENGTH_SHORT).show();
				}
				/*Intent intent = new Intent(getActivity(), ConsumerSignIn.class);
				intent.putExtra("gocart", "gocart");
				startActivity(intent);
				getActivity().overridePendingTransition(R.anim.left_in, R.anim.right_out);*/
			}
		});
				
		df = new DecimalFormat("#.##");
		df.setRoundingMode(RoundingMode.HALF_UP);
		
		updateList();
		
		return layout;
		
	}

	private void callFragment(){
		Fragment frag = new FragmentPayment();
		FragmentManager fm = getActivity().getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		LocationSearchResultPage.fragmentStack1.push(frag);
		ft.replace(R.id.realtabcontent, frag);
		ft.commit();
	}
	
	public void updateList(){
		
		if(db.getTotalCount()>0){
			cartList = new ArrayList<CartBean>();
			cartList = db.getAllItems();
			
			double totalPrice = 0;
			
			for (int i = 0; i < cartList.size(); i++) {
				LayoutInflater inflater_row = (LayoutInflater) getActivity()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	
				View tr = inflater_row.inflate(R.layout.single_table_row, null, false);
	
				tr.setId(cartList.get(i).getId() + 1000);
	
				delete = (ImageButton) tr.findViewById(R.id.bt_delete);
//				delete.setPadding(left, top, right, bottom)
				delete.setId(tr.getId()*5);
				delete.setPadding(0, 0, 0, 0);
				
				delete.setOnClickListener(this);
//				RelativeLayout rel = (RelativeLayout) tr.findViewById(R.id.rel_delete);
				foodName = (TextView) tr.findViewById(R.id.txt_item_name);
				foodName.setPadding(60, 0, 0, 0);
				foodName.setText(cartList.get(i).getFood_name());
				
				quantity = (TextView) tr.findViewById(R.id.txt_quantity);
				quantity.setText(String.valueOf(cartList.get(i).getItems_added()));
				
				price = (TextView) tr.findViewById(R.id.txt_price);
				double singleItemTotal = cartList.get(i).getItems_added()*Double.parseDouble(cartList.get(i).getFood_price());
				totalPrice += singleItemTotal;
				
				price.setText(String.valueOf(df.format(singleItemTotal)));

				tableView.addView(tr, i + 1);
			}
			
			total.setText(String.valueOf(df.format(totalPrice)));
			subTotal.setText(String.valueOf(df.format(totalPrice)));
			
		}else{
			tableView.setVisibility(View.GONE);
			checkAsGuest.setVisibility(View.GONE);
			checkAsUser.setVisibility(View.GONE);
			cartEmpty.setVisibility(View.VISIBLE);
		}
		
	}
	
	public void updateTotal(){
		if(db.getTotalCount()>0){
			
			cartList = db.getAllItems();
			double totalPrice = 0;
			for (int i = 0; i < cartList.size(); i++) {
				double singleItemTotal = cartList.get(i).getItems_added()*Double.parseDouble(cartList.get(i).getFood_price());
				totalPrice += singleItemTotal;
			}
						
			total.setText(String.valueOf(df.format(totalPrice)));
			subTotal.setText(String.valueOf(df.format(totalPrice)));
		}else{
			tableView.setVisibility(View.GONE);
			checkAsGuest.setVisibility(View.GONE);
			checkAsUser.setVisibility(View.GONE);
			cartEmpty.setVisibility(View.VISIBLE);
		}
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Toast.makeText(getActivity(), ""+v.getId(), Toast.LENGTH_SHORT).show();
		
			int id = (v.getId()/5) - 1000;
//			tableView.removeAllViews();
//			updateList();
			db.deleteItem(id);
			cart.cartItem(0);
			
			 // row is your row, the parent of the clicked button
            View column = (View) v.getParent();
            View row = (View) column.getParent();
            // container contains all the rows, you could keep a variable somewhere else to the container which you can refer to here
            ViewGroup container = ((ViewGroup)row.getParent());
            // delete the row and invalidate your view so it gets redrawn
            container.removeView(row);
            container.invalidate();
            updateTotal();
//			updateList();
//			v.getParent().
//			Toast.makeText(getApplicationContext(), ""+v.getId(), Toast.LENGTH_SHORT).show();
		
	}

}
