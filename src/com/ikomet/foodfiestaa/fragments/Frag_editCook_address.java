// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ikomet.foodfiestaa.fragments;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.activities.CookActivity;
import com.ikomet.foodfiestaa.beans.CookDetailBean;
import com.ikomet.foodfiestaa.interfaces.FragmentCommunicator;
import com.ikomet.foodfiestaa.utils.DialogCustom;
import com.ikomet.foodfiestaa.utils.DialogVerification;
import com.ikomet.foodfiestaa.utils.InternetCheck;
import com.ikomet.foodfiestaa.utils.ServiceHandler;
import com.ikomet.foodfiestaa.utils.SharedPrefs;

public class Frag_editCook_address extends Fragment {
	View view;
	String city;
	String id;
	String location;
	String pincode;
	String result;
	String state, country;
	String street;
	String token;
	private static final String COOKDETAILBEAN = "cook_details_bean";
	CookDetailBean bean;
	Button btn_next_general;
	EditText edt_area;
	EditText edt_city;
	EditText edt_pincode;
	EditText edt_state, edt_country;
	EditText edt_street;
	Context context;
	public FragmentCommunicator communicator;
	public DialogVerification dialog;
	DialogCustom dialogNet;
	public Frag_editCook_address() {
	}

	public static Frag_editCook_address newInstance(
			CookDetailBean cookdetailbean) {
		Frag_editCook_address frag_editcook_address = new Frag_editCook_address();
		Bundle bundle = new Bundle();
		bundle.putSerializable("cook_details_bean", cookdetailbean);
		frag_editcook_address.setArguments(bundle);
		return frag_editcook_address;
	}

	public View onCreateView(LayoutInflater layoutinflater,
			ViewGroup viewgroup, Bundle bundle) {
		view = layoutinflater.inflate(R.layout.frag_edtcook_address, viewgroup,
				false);
		bean = (CookDetailBean) getArguments().getSerializable(
				"cook_details_bean");
		communicator=(FragmentCommunicator)getActivity();
		
		dialogNet = new DialogCustom();
		edt_street = (EditText) view.findViewById(R.id.edt_street);
		edt_area = (EditText) view.findViewById(R.id.edt_area);
		edt_city = (EditText) view.findViewById(R.id.edt_city);
		edt_state = (EditText) view.findViewById(R.id.edt_state);
		edt_country = (EditText) view.findViewById(R.id.edt_country);
		edt_pincode = (EditText) view.findViewById(R.id.edt_pincode);
		btn_next_general = (Button) view.findViewById(R.id.btn_next_general);
		edt_street.setText(bean.getStreet());
		edt_area.setText(bean.getLocation());
		edt_city.setText(bean.getCity());
		edt_state.setText(bean.getState());
		edt_country.setText(bean.getCountry());
		edt_pincode.setText(bean.getPincode());
		dialog = new DialogVerification(getActivity(), 1,
				"Profile updated successfully");
		btn_next_general
				.setOnClickListener(new android.view.View.OnClickListener() {

					public void onClick(View view) {
						if(InternetCheck.isInternetConnected(getActivity())){
							new async_edtAddress().execute("");
						}else{
							dialogNet.showDialog(getActivity(), "Please connect to Internet", "OK", R.drawable.circle_uncheck);
						}
					}

				});
		return view;
	}

	class async_edtAddress extends AsyncTask<String, Void, String> {
		JSONObject obj1;
		String message, status;

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			street = edt_street.getText().toString();
			location = edt_area.getText().toString();
			city = edt_city.getText().toString();
			state = edt_state.getText().toString();
			country = edt_country.getText().toString();
			pincode = edt_pincode.getText().toString();
			id = SharedPrefs.getUid(getActivity());
			token = SharedPrefs.getToken(getActivity());
			try {
				result = (new ServiceHandler()).makeServiceCall(
						"http://beta.foodfiestaa.com/consumereditprofile",
						2,
						(new StringBuilder("id="))
								.append(URLEncoder.encode(id, "UTF-8"))
								.append("&token=")
								.append(URLEncoder.encode(token, "UTF-8"))
								.append("&street=")
								.append(URLEncoder.encode(street, "UTF-8"))
								.append("&location=")
								.append(URLEncoder.encode(location, "UTF-8"))
								.append("&city=")
								.append(URLEncoder.encode(city, "UTF-8"))
								.append("&state=")
								.append(URLEncoder.encode(state, "UTF-8"))
								.append("&country=")
								.append(URLEncoder.encode(state, "UTF-8"))
								.append("&pincode=")
								.append(URLEncoder.encode(pincode, "UTF-8"))
								.toString());
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				obj1=new JSONObject(result);
				status = obj1.getString("status");
				message = obj1.getString("message");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return message;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (result.equalsIgnoreCase("profile updated successfully")) {
				
				dialog.show();
				
				Button ok = (Button)dialog.findViewById(R.id.btn_ok);
				ok.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						communicator.change_state();
						getActivity().finish();
						dialog.dismiss();
					}
				});
				
				return;
			} else {
				Toast.makeText(getActivity(), result, 1000).show();
				return;
			}

		}

	}

}
