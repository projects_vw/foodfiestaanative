package com.ikomet.foodfiestaa.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.adapter.ExpandableListAdapterFaq;

public class FragFaqCook extends Fragment{

	ExpandableListView exp_list;
	List<String> header_name;
	List<String> child_element;
	HashMap<String,String> child;
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			ViewGroup container,  Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view=inflater.inflate(R.layout.frag_faq_cook, container, false);
		exp_list = (ExpandableListView) view.findViewById(R.id.exp_general_faq);
		Initialize_List_Values();
		ExpandableListAdapterFaq adapter=new ExpandableListAdapterFaq(getActivity(), header_name,child);
		exp_list.setAdapter(adapter);
		return view;
	}
	public void Initialize_List_Values()
	{
		header_name=new ArrayList<>();
		header_name.add("How do I apply to become a Food Fiestaa Cook?");
		header_name.add("Can a Cook be removed from the Food Fiestaa platform?");
		header_name.add("What packaging should I use?");
		header_name.add("What about taxes?");
		header_name.add("How do I contact Consumers who reserve my dishes?");
		header_name.add("What is Food Fiestaa’ payment policy?");
		header_name.add("How are complaints handled?");
		header_name.add("How are refunds handled?");
		header_name.add("What can I do about an unfair or bad review?");
		header_name.add("Is my bank information secure?");
		header_name.add("Do I need to cook in a commercial kitchen?");
		header_name.add("What about food hygiene standards?");
		header_name.add("Can a professional cook or restaurant post on Food Fiestaa?");
		header_name.add("Can a caterer post on Food Fiestaa?");
		header_name.add("Can food trucks use Food Fiestaa?");
		header_name.add("How many dishes can I post on Food Fiestaa?");
		header_name.add("How far into the future can I post a dish for sharing?");
		header_name.add("How many dishes can I post on Food Fiestaa?");
		header_name.add("Can I keep a dish up permanently?");
		
				
		child_element=new ArrayList<>();

		child_element.add("Visit our site www.foodfiestaa.com and register by clicking “Become a Cook” and follow the process. Also, see the How it works workflow chart.");
		
		
		child_element.add("Yes. Refund requests due to bad quality of food, abuse reports, or bad reviews could lead to your removal from the platform.");
		
		
		
		child_element.add("Fiestaa Delivery person will provide the food packing material at the time of pickup " +
				"and it’s cooks responsibility to pack tightly without spill. " +
				"Food packaging could vary significantly depending on the dish you’re sharing (refer packing documentation)." +
				" In general, food containers that are similar to what is provided by reputable dining establishments and are " +
				"environmentally friendly lead to good reviews by Consumers. A variation on the Golden Rule may be helpful: " +
				"serve food unto others and you would have them serve unto you. The Food");
		
		
		
		child_element.add("Food Fiestaa doesn’t collect any applicable sales tax or other " +
				"taxes on your behalf. You are responsible for paying any " +
				"relevant sales taxes or income taxes from your proceeds. " +
				"You will receive an annual sales report from our payment processor to help you determine " +
				"if you owe any taxes. You should contact a tax professional " +
				"to help determine your tax liability.Food Fiestaa collects sales t" +
				"ax on your behalf and forwards it to the appropriate government " +
				"authority in cases when dishes are shared for a fee. " +
				"You are responsible for paying your income taxes. " +
				"You will receive an annual sales report from our payment processor " +
				 "to help you determine if you owe any taxes. You should contact a tax professional to help determine your tax liability.");
		
		child_element.add("No, any questions should be reported to info@foodfiestaa.com and our support team will contact you immediately.");
	
		child_element.add("Food Fiestaa distribute full payment of whatever you charge, less any refunds, within five business days."+
				"Food Fiestaa distribute full payment of whatever you charge, less credit card fees, platform fees, and any refunds, within five business days.(Most cooks receive about 90% of whatever they charge depending on their sharing options)"+
				"You have two options for receiving your payments. The first option is via direct deposit into your bank account. The second option is via a PayPal account. You are able to set up both options within the app and can change your option at any time.");
		
		child_element.add("We usually process payments within three days. There’s also a 48-hour wait period for refunds after the dish is received by the Consumer. So if we process your account on day one, then we would go back 48 hours and provide payment for all dishes transferred prior to the preceding day. It may take another one or two business days for the funds to appear in your bank account after we initiate the payment transfer. This depends on the third-party payment processing timeline.");
		
		child_element.add("We take complaints from both Consumers and Cooks that cannot be resolved independently very seriously. We will contact both parties to try to ascertain the facts and circumstances. Depending on the complaint’s gravity, a full refund may be issued and Consumers or Cooks may be removed from the platform.");
		
		child_element.add("No, you cannot change or cancel dishes once the order is placed by the consumer. If for any reason you have not prepared the food item ordered and the same happens consequently Food Fiestaa has options to remove the cook from their website and application. Please keep in mind that significant changes to dishes, especially when already reserved, may lead to complaints, bad reviews, or refund requests. Cancellations will lead to full refunds.");
		
		child_element.add("In the case of a refund, you don’t get paid for the dish that was refunded, even. Repeated refund requests may lead to your removal from the platform as a Cook.");
		
		child_element.add("We cannot modify or remove reviews unless they’re profane or abusive. Otherwise, we would jeopardize the integrity of the review system. We do allow for you to respond to each review. Your response will appear on the review page when others see the review in question.");
		
		child_element.add("Food Fiestaa doesn’t store any bank account data. Your bank account details are encrypted and sent to our payment processor. We receive a unique payment token in return that can only be used by Food Fiestaa. Our payment processor maintains bank-level security of all data and complies with all applicable state and central data protection and privacy laws.");
		
		child_element.add("That depends on what you are making and where you live. Food Fiestaa is purely a marketplace and a platform. We don’t inspect your kitchen or audit your cooking process frequently. But quality is mandatory.");
		
		child_element.add("You should follow all relevant food safety standards and recommendations when preparing your dishes. The Indian food processing industry is regulated by several laws which govern the aspects of sanitation, licensing and other necessary permits that are required to start up and run a food business. The legislation that dealt with food safety in India was the Prevention of Food Adulteration Act, 1954 (hereinafter referred to as \"PFA\"). So cooks should follow this Act.");
		
		child_element.add("Yes, Food Fiestaa treats all people who want to cook and share their food equally on the platform. Both independent cooks and cook-owners at restaurants can post dishes as long as they don’t exceed the app’s limit on active dishes. (So choose your best dishes and share!)");
		
		child_element.add("Yes, caterers can use Food Fiestaa to continue to share their best dishes too.");
		
		child_element.add("Yes. Food Fiestaa is great for food trucks. You can post your best dishes on Food Fiestaa and then specify your truck’s location as the pickup location as well as the hours when the dishes will be available. Your Consumers can become find and reserve your dishes ahead of time. You can use Food Fiestaa to see which dishes garner the most views from users and develop a following that you could parlay into a larger food venture.");
		
		child_element.add("You are limited to five active ‘pre-order’ dishes and twelve ‘live’ dishes at any one time on the platform. This helps Cooks share only their best dishes and helps our members discover great dishes by reducing the number of dishes from each Cook on the platform. An active dish is defined as an available dish with a sharing period that ends in the future. Please remember that cancelled dishes continue to be considered active until the scheduled sharing period ends.");
		
		child_element.add("Dishes that are posted as ‘Live’ can have an end date of up to 5 days into the future. You can also post a dish to be shared immediately. ‘Pre-orders’ can be remain posted indefinitely as long as they are updated once every months.");
		
		child_element.add("Yes, as long as you continue to update the dish at least once every months. All dishes on Food Fiestaa must have a specific start time and end time for sharing. We do this to encourage Cooks to prepare dishes with a special focus on making them as good as possible.");
		
		
		
		
		child=new HashMap<>();
		
		for(int i=0;i<header_name.size();i++)
		{
			child.put(header_name.get(i), child_element.get(i));
		}
		
				
	}
}
