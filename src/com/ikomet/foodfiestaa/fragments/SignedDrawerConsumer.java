package com.ikomet.foodfiestaa.fragments;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.activities.ContactUs;
import com.ikomet.foodfiestaa.activities.GalleryActivity;
import com.ikomet.foodfiestaa.activities.HelpAndFaq;
import com.ikomet.foodfiestaa.activities.HomeActivity;
import com.ikomet.foodfiestaa.activities.HowItWorksNew;
import com.ikomet.foodfiestaa.activities.StaticPagesActivity;
import com.ikomet.foodfiestaa.adapter.ListAdapterSingleText;
import com.ikomet.foodfiestaa.interfaces.FragmentCommunicator;
import com.ikomet.foodfiestaa.interfaces.FragmentCommunicatorNew;
import com.ikomet.foodfiestaa.utils.ReadStream;
import com.ikomet.foodfiestaa.utils.SharedPrefs;

public class SignedDrawerConsumer extends Fragment implements FragmentCommunicator {

	private ActionBarDrawerToggle mDrawerToggle;
	private DrawerLayout mDrawerLayout;

	RelativeLayout header;
	TextView title;
	TextView name, data, logout;
	ListView drawerList;
	ListAdapterSingleText drawerAdapter;
	ArrayList<String> navDrawer;
	ProgressDialog pDialog;
	View containerView;

	String serviceUrl;
	String cms_name;
	String cms_content;

	Fragment frag;
	Context cxt;

	FragmentCommunicatorNew fcommunicatorNew;

	public SignedDrawerConsumer() {
		// TODO Auto-generated constructor stub
		
	}

	public void getcxt(Context cxt) {
		// TODO Auto-generated constructor stub
		this.cxt = cxt;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflating view layout
		View layout = inflater.inflate(R.layout.fragment_navigation_drawer,
				container, false);

		drawerList = (ListView) layout.findViewById(R.id.list_drawer);

		navDrawer = new ArrayList<String>();

		navDrawer.add("About Us");
		navDrawer.add("Contact US");
		navDrawer.add("How It Works");
		navDrawer.add("Cooking Standards");
		navDrawer.add("Help/FAQ");
		navDrawer.add("Gift/refer a friend");
		navDrawer.add("Gallery");
		navDrawer.add("Testimonials");
		navDrawer.add("Terms & Conditions");
		navDrawer.add("Privacy Policy");
		navDrawer.add("Sign Out");

		drawerAdapter = new ListAdapterSingleText(getActivity(), navDrawer);

		drawerList.setAdapter(drawerAdapter);

		drawerList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				Intent i;
				String url = "http://beta.foodfiestaa.com/cmsservice?&cmsid=";

				switch (position) {
				case 0:
					serviceUrl = url + "2";
					cms_name = "About Us";
					new AttemptLogin().execute();
					break;
				case 1:
					i = new Intent(getActivity(), ContactUs.class);
					startActivity(i);
					break;
				case 2:
					i = new Intent(getActivity(), HowItWorksNew.class);
					startActivity(i);
					break;
				case 3:
					serviceUrl = url + "6";
					cms_name = "Cooking Standards";
					new AttemptLogin().execute();
					break;
				case 4:
					i = new Intent(getActivity(), HelpAndFaq.class);
					startActivity(i);
					break;	
				case 5:
					
					break;
				case 6:
					i = new Intent(getActivity(), GalleryActivity.class);
					startActivity(i);
					break;
				case 7:
					
					break;
				case 8:
					serviceUrl = url + "3";
					cms_name = "Terms & Conditions";
					new AttemptLogin().execute();
					break;
				case 9:
					serviceUrl = url + "4";
					cms_name = "Privacy Policy";
					new AttemptLogin().execute();
					break;
				case 10:	
					SharedPrefs.clearSP(getActivity());
					i = new Intent(getActivity(), HomeActivity.class);
					getActivity().finish();
					startActivity(i);
					break;
				default:
					break;
				}

				mDrawerLayout.closeDrawer(containerView);

			}
		});

		return layout;

	}

	public void setUp(int fragmentId, DrawerLayout drawerLayout,
			final Toolbar toolbar) {
		containerView = getActivity().findViewById(fragmentId);
		mDrawerLayout = drawerLayout;
		mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout,
				toolbar, R.string.drawer_open, R.string.drawer_close) {
			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				getActivity().invalidateOptionsMenu();
			}

			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
				getActivity().invalidateOptionsMenu();
			}

			@Override
			public void onDrawerSlide(View drawerView, float slideOffset) {
				super.onDrawerSlide(drawerView, slideOffset);
				toolbar.setAlpha(1 - slideOffset / 2);
			}
		};

		mDrawerLayout.setDrawerListener(mDrawerToggle);
		mDrawerLayout.post(new Runnable() {
			@Override
			public void run() {
				mDrawerToggle.syncState();
			}
		});

	}

	class AttemptLogin extends AsyncTask<String, String, String> {

		boolean status;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Attempting login...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub
			// Check for success tag

			String json = "";

			URL url = null;
			try {
				url = new URL(serviceUrl);

				HttpURLConnection urlConnection = null;

				urlConnection = (HttpURLConnection) url.openConnection();

				try {
					InputStream in = null;

					in = new BufferedInputStream(urlConnection.getInputStream());

					ReadStream stream = new ReadStream();
					json = stream.readStream(in, url);

				} finally {
					urlConnection.disconnect();
				}

				JSONObject jsonObj;

				jsonObj = new JSONObject(json);

				// json success tag

				status = jsonObj.getBoolean("status");

				if (status) {
					cms_content = jsonObj.getString("cms_content");
					return "success";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;

		}

		protected void onPostExecute(String value) {
			// dismiss the dialog once product deleted
			if (pDialog.isShowing()) {
				pDialog.dismiss();
			}
			System.out.println("value = " + value);
			if (value != null) {
				Intent intent = new Intent(getActivity(),
						StaticPagesActivity.class);
				intent.putExtra("cms_name", cms_name);
				intent.putExtra("cms_content", cms_content);
				startActivity(intent);

			}

		}

	}

	@Override
	public void passDataToFragment() {
		// TODO Auto-generated method stub
		mDrawerLayout.openDrawer(containerView);
	}

	@Override
	public void change_state() {
		// TODO Auto-generated method stub

	}

	@Override
	public void Add_click() {
		// TODO Auto-generated method stub

	}

	@Override
	public void locationToolbar(boolean isLocationSerach) {
		// TODO Auto-generated method stub
		
	}

}
