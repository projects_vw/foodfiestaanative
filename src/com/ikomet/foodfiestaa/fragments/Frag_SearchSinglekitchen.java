package com.ikomet.foodfiestaa.fragments;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.activities.LocationSearchResultPage;
import com.ikomet.foodfiestaa.adapter.AdptrSingleKitchenItems;
import com.ikomet.foodfiestaa.beans.LocSrchSingleKitchen;
import com.ikomet.foodfiestaa.beans.SingleKitchenHeaderBean;
import com.ikomet.foodfiestaa.databases.DBHandler;
import com.ikomet.foodfiestaa.interfaces.FragmentCommunicatorNew;
import com.ikomet.foodfiestaa.utils.DialogFilter;
import com.ikomet.foodfiestaa.utils.InternetCheck;
import com.ikomet.foodfiestaa.utils.ServiceHandler;
import com.ikomet.foodfiestaa.utils.SpacesItemDecoration;

public class Frag_SearchSinglekitchen extends Fragment implements
		OnClickListener, OnCheckedChangeListener, FragmentCommunicatorNew {

	View view;
	RecyclerView recycler_view;
	SingleKitchenHeaderBean header_bean;
	ProgressDialog pDialog;
	TextView txt_title, txt_area, txt_minimum;
	LocSrchSingleKitchen singlekitechenitems;
	public static ArrayList<LocSrchSingleKitchen> arr_singlekitchenitems, selected_food;
	String TYPE = "1";
	ImageView img_back,img_filter,smiley;
	FragmentManager frag_manager;
	String kitchen_id;
	FragmentTransaction ft;
	public AdptrSingleKitchenItems single;
	LinearLayout lnr_profile_turn;
	ToggleButton tg_btn;
	LinearLayout linear_loc;
	RelativeLayout rel_loc;
	ArrayList<String> food_name;
	AutoCompleteTextView auto_foodsearch;
	FragmentCommunicatorNew frag_new;
	DialogFilter dialog;
	String param = null;
	RelativeLayout noNet, fragSrchSnglKtchn;
		
	@Override
	public void onAttach(Context context) {
		// TODO Auto-generated method stub
		super.onAttach(context);
		frag_new = (FragmentCommunicatorNew)this;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		view = inflater.inflate(R.layout.frag_singlekitchendetails, container,
				false);
		
		noNet = (RelativeLayout) view.findViewById(R.id.no_net);
		fragSrchSnglKtchn = (RelativeLayout) view.findViewById(R.id.rel_srch_single_ktchn);
		smiley = (ImageView)view.findViewById(R.id.img_sad);
		
		if(InternetCheck.isInternetConnected(getActivity())){
			noNet.setVisibility(View.GONE);
			fragSrchSnglKtchn.setVisibility(View.VISIBLE);
			initializeVar();
		}else{
			noNet.setVisibility(View.VISIBLE);
			fragSrchSnglKtchn.setVisibility(View.GONE);
			
			noNet.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(InternetCheck.isInternetConnected(getActivity())){
						
						Animation a = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_anim);
						a.setDuration(1000);
						smiley.startAnimation(a);
						
						new Handler().postDelayed(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								fragSrchSnglKtchn.setVisibility(View.VISIBLE);
								noNet.setVisibility(View.GONE);
								initializeVar();
							}
						}, 2000);
												
						/*noNet.setVisibility(View.GONE);
						fragSrchSnglKtchn.setVisibility(View.VISIBLE);
						initializeVar();*/
					}
				}
			});
			
		}
		
		
		return view;
	}

	private void initializeVar(){
		kitchen_id = getArguments().getString("kitchen_id");
		Log.e("the kitchen id is", kitchen_id);
		
		header_bean = new SingleKitchenHeaderBean(getArguments().getString(
				"kitchen_name"), getArguments().getString("location"),
				"Minimum Price is:" + getArguments().getString("minimumprice"));
		lnr_profile_turn = (LinearLayout) view
				.findViewById(R.id.lnr_profile_turn);
		
		tg_btn = (ToggleButton)view.findViewById(R.id.tg_btn);
		tg_btn.setOnCheckedChangeListener(this);
		
		linear_loc = (LinearLayout) view.findViewById(R.id.linear_loc);
		rel_loc = (RelativeLayout) view.findViewById(R.id.rel_loc);
		img_filter=(ImageView)view.findViewById(R.id.img_filter);
		img_filter.setOnClickListener(this);
		
		auto_foodsearch = (AutoCompleteTextView) view.findViewById(R.id.auto_foodsearch);
		food_name = new ArrayList<String>();
		
		/*
		 * txt_title = (TextView) view.findViewById(R.id.txt_title); txt_area =
		 * (TextView) view.findViewById(R.id.txt_area); txt_minimum = (TextView)
		 * view.findViewById(R.id.txt_minimum);
		 * txt_title.setText(getArguments().getString("kitchen_name"));
		 * txt_area.setText(getArguments().getString("location"));
		 * txt_minimum.setText("Minimum Price is:" +
		 * getArguments().getString("minimumprice"));
		 */
		recycler_view = (RecyclerView) view.findViewById(R.id.recycler_view);
		GridLayoutManager manager = new GridLayoutManager(getActivity(), 2);
		img_back = (ImageView) view.findViewById(R.id.img_back);
		img_back.setOnClickListener(this);
		// recycler_view.setLayoutManager(new
		// StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL));

		manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
			@Override
			public int getSpanSize(int position) {
				return position == 0 ? 2 : 1;
			}
		});
		recycler_view.setLayoutManager(manager);
		SpacesItemDecoration decoration = new SpacesItemDecoration(20, 20);
		recycler_view.addItemDecoration(decoration);
		pDialog = new ProgressDialog(getActivity());
		try {
			param = "&kitchen_id=" + URLEncoder.encode(kitchen_id, "UTF-8")
					+ "&type=" + URLEncoder.encode(TYPE, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		new AsyncSingleKitchenItemsList().execute(param,TYPE);
	}
	
	class AsyncSingleKitchenItemsList extends AsyncTask<String, Void, String> {

		String result = null, param = null, status;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog.setMessage("Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			ServiceHandler handler = new ServiceHandler();
			param = "kitchen_id=" + params[0] + "&type=" + TYPE;

			result = handler.makeServiceCall(
					"http://beta.foodfiestaa.com/kitchensearch",
					ServiceHandler.POST, param);
			try {
				JSONObject obj = new JSONObject(result);
				arr_singlekitchenitems = new ArrayList<>();
				arr_singlekitchenitems.add(new LocSrchSingleKitchen());
				if (obj.getBoolean("status")) {
					JSONArray arr = obj.getJSONArray("data");
					status = "true";
					for (int i = 0; i < arr.length(); i++) {
						JSONObject obj1 = arr.getJSONObject(i);

						singlekitechenitems = new LocSrchSingleKitchen();

						singlekitechenitems.setMid(obj1.getString("mid"));
						singlekitechenitems.setKitchen_id(kitchen_id);
						singlekitechenitems.setFshortname(obj1
								.getString("fshortname"));
						singlekitechenitems.setFoodprice(obj1
								.getString("foodprice"));
						singlekitechenitems.setDescription(obj1
								.getString("description"));
						
						if(obj1.has("preorder_days")){
							singlekitechenitems.setPreorder_days(obj1
									.getString("preorder_days"));
						}else{
							singlekitechenitems.setPreorder_days("0");
						}
																		
						if(obj1.has("preorder_availble")){
							singlekitechenitems.setPreorder_availble(obj1
									.getString("preorder_availble"));
						}else{
							singlekitechenitems.setPreorder_availble("0");
						}
						
						if(obj1.has("tdyspl_availble")){
							singlekitechenitems.setTdyspl_availble(obj1
									.getString("tdyspl_availble"));
						}else{
							singlekitechenitems.setTdyspl_availble("0");
						}
												
						singlekitechenitems.setAvgrating(obj1
								.getString("avgrating"));
						singlekitechenitems
								.setPic("http://beta.foodfiestaa.com/"
										+ obj1.getString("pic").replace(" ",
												"%20"));
						singlekitechenitems.setOrder_type(Integer.parseInt(params[1]));
						
						food_name.add(obj1.getString("fshortname"));
						
						DBHandler db = new DBHandler(getActivity());
						int item_added = db.getItemCount(obj1.getString("mid"));

						singlekitechenitems.setItem_quantity(item_added);
						
						arr_singlekitchenitems.add(singlekitechenitems);

					}

				} else {
					status = "false";
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return status;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pDialog.dismiss();
			if (result.equalsIgnoreCase("true")) {

				if (arr_singlekitchenitems.size() == 0) {
					lnr_profile_turn.setVisibility(View.VISIBLE);

				} else {
					single = new AdptrSingleKitchenItems(getActivity(),
							header_bean, arr_singlekitchenitems, 1);
					recycler_view.setAdapter(single);
					
					ArrayAdapter<String> adapter = new ArrayAdapter<String>(
							getActivity(), R.layout.spinner_item_location,
							food_name);

					auto_foodsearch.setThreshold(1);
					auto_foodsearch.setAdapter(adapter);
					auto_foodsearch
							.setOnItemClickListener(new OnItemClickListener() {

								@Override
								public void onItemClick(AdapterView<?> arg0,
										View arg1, int arg2, long arg3) { // TODO
																			// Auto-generated
																			// method

									Log.e("location", auto_foodsearch.getText()
											.toString());
									String foodname = auto_foodsearch.getText()
											.toString();
									selected_food = new ArrayList<LocSrchSingleKitchen>();
									for (int i = 0; i < arr_singlekitchenitems
											.size(); i++) {

										if (foodname
												.equalsIgnoreCase(arr_singlekitchenitems
														.get(i).getFshortname())) {
											Log.e("inside if", "part");
											LocSrchSingleKitchen temp = arr_singlekitchenitems
													.get(i);
											selected_food.add(new LocSrchSingleKitchen());
											selected_food.add(temp);
																						
											single = new AdptrSingleKitchenItems(getActivity(),
													header_bean, selected_food, 1);
											recycler_view.setAdapter(single);
											
											single.notifyDataSetChanged();
											break;
										} else {
											Toast.makeText(getActivity(),
													"inside else part", 1000)
													.show();
										}

									}

								}
							});
				}
					
				

			} else {
				Toast.makeText(getActivity(), result, 1000).show();
			}

		}
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId() == R.id.img_back){
			frag_manager = ((ActionBarActivity) getActivity())
					.getSupportFragmentManager();
			ft = frag_manager.beginTransaction();
			Frag_SearchKitchen searchKitchen = new Frag_SearchKitchen();
			ft.replace(R.id.realtabcontent, searchKitchen);
			LocationSearchResultPage.fragmentStack.pop();
			ft.commit();
		}else if(v.getId() == R.id.img_filter){
			
			dialog=new DialogFilter(getActivity(), frag_new,Frag_SearchKitchen.diet2,Frag_SearchKitchen.cusine2,1);
			dialog.show();
			
		}
		
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		// TODO Auto-generated method stub
		if (isChecked) {
			linear_loc.setVisibility(View.VISIBLE);
			rel_loc.setVisibility(View.VISIBLE);

		} else {
			linear_loc.setVisibility(View.GONE);
			rel_loc.setVisibility(View.GONE);
		}
	
	}

	@Override
	public void passnewDataToFragment(String val1, String val2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void checkedData(HashMap<String, Integer> hashmap) {
		// TODO Auto-generated method stub
		if (hashmap != null) {
			try {
				param = param + "kitchen_id="
						+ URLEncoder.encode(kitchen_id, "UTF-8");

				if (hashmap.containsKey("dtype")) {
					Log.e("dtype", "" + hashmap.get("dtype"));
					
						param = param
								+ "&dtype="
								+ URLEncoder.encode(hashmap.get("dtype")
										.toString(), "UTF-8");
					
				} else {
					Log.e("dtype else", "part");
				}
				if (hashmap.containsKey("order_type")) {
					Log.e("order_type", "" + hashmap.get("order_type"));
					TYPE=hashmap.get("order_type").toString();
					param = param+
							"&order_type="+URLEncoder.encode(hashmap.get("order_type").toString(), "UTF-8");
				} else {
					Log.e("order type else", "part");
				}
				if (hashmap.containsKey("ctype")) {
				
					Log.e("ctype", "" + hashmap.get("ctype"));
					param = param+"&ctype="+URLEncoder.encode(hashmap.get("ctype").toString(), "UTF-8");
				} else {
					Log.e("c_type", "part");
				}
				new AsyncSingleKitchenItemsList().execute(param,TYPE);
			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} else {
			Log.e("hashmap is ", "not null");
		}
	}

}
