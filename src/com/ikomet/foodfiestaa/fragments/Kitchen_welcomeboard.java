package com.ikomet.foodfiestaa.fragments;

import android.content.Context;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.activities.CookActivity;
import com.ikomet.foodfiestaa.interfaces.FragmentCommunicator;
import com.ikomet.foodfiestaa.interfaces.Toggle;

public class Kitchen_welcomeboard extends Fragment implements
		FragmentCommunicator, Toggle, OnClickListener {

	LinearLayout lnr_profile_turn;
	Context context;
	TextView txt_tap, txt_add;

	FragmentManager manager;
	FragmentTransaction transaction;
	Fragment frag;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.welcome_board, container, false);
		context = getActivity();
		((CookActivity) context).toggle = this;
		// txt_add=(TextView)view.findViewById(R.id.txt_add);
		lnr_profile_turn = (LinearLayout) view
				.findViewById(R.id.lnr_profile_turn);
		txt_tap = (TextView) view.findViewById(R.id.txt_tap);
		// txt_add.setOnClickListener(this);
		txt_tap.setOnClickListener(this);
		return view;
	}

	@Override
	public void ControlToggle(boolean value) {
		// TODO Auto-generated method stub
		if (value) {
			lnr_profile_turn.setVisibility(View.GONE);

		} else {
			lnr_profile_turn.setVisibility(View.VISIBLE);

		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		manager = getActivity().getSupportFragmentManager();
		transaction = manager.beginTransaction();
		int id = v.getId();
		if (id == R.id.txt_tap) {
			frag = new Frag_signaturedishes();
			transaction.replace(R.id.realtabcontent, frag);
			transaction.commit();
		}
		/*
		 * else if(id==R.id.txt_add) { frag=new Frag_AddDish();
		 * transaction.replace(R.id.realtabcontent, frag); transaction.commit();
		 * }
		 */
	}

	@Override
	public void passDataToFragment() {
		// TODO Auto-generated method stub

	}

	@Override
	public void change_state() {
		// TODO Auto-generated method stub

	}

	@Override
	public void Add_click() {
		// TODO Auto-generated method stub

	}

	@Override
	public void EditDish_flnt_actionbar() {
		// TODO Auto-generated method stub

	}

	@Override
	public void locationToolbar(boolean isLocationSerach) {
		// TODO Auto-generated method stub
		
	}

}
