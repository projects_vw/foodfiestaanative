package com.ikomet.foodfiestaa.fragments;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.adapter.PagerAdapter;
import com.ikomet.foodfiestaa.interfaces.FragmentCommunicator;
import com.ikomet.foodfiestaa.interfaces.FragmentCommunicatorNew;
import com.ikomet.foodfiestaa.utils.CirclePageIndicator;

public class FragmentHome extends Fragment {

	int position;
	Timer swipeTimer;

	int numberOfViewPagerChildren = 5;

	RelativeLayout layout_one, layout_two;
	private ViewPager viewpager;
	CirclePageIndicator indicator;
	LinearLayout linear_toolbar, linear_location, linear_howitworks,
			linear_becomecook;
	ImageButton nav_drawer;
	FragmentCommunicator fcommunicator;
	FragmentCommunicatorNew fcommunicatorNew;
	Context context;
	
	public FragmentHome(){
		
	}
	
	public FragmentHome(Context context){
		this.context = context;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View layout = inflater
				.inflate(R.layout.fragment_home, container, false);

		viewpager = (ViewPager) layout.findViewById(R.id.pager);
		indicator = (CirclePageIndicator) layout.findViewById(R.id.indicator);
		linear_toolbar = (LinearLayout) layout
				.findViewById(R.id.linear_toolbar);
		linear_location = (LinearLayout) layout
				.findViewById(R.id.linear_location);
		linear_becomecook = (LinearLayout) layout
				.findViewById(R.id.linear_becomecook);
		linear_howitworks = (LinearLayout) layout
				.findViewById(R.id.linear_howitworks);
		nav_drawer = (ImageButton)layout.findViewById(R.id.bt_nav_drawer);
		
		linear_howitworks.setOnClickListener(new View.OnClickListener() {
			// DialogTest test=new DialogTest(MainActivity.this);
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

			}
		});
		
		nav_drawer.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				fcommunicator = (FragmentCommunicator)getActivity();
				fcommunicator.passDataToFragment();
				/*fcommunicatorNew = (FragmentCommunicatorNew)getActivity();
				fcommunicatorNew.passnewDataToFragment();*/
			}
		});
		
		PagerAdapter adapt = new PagerAdapter(getActivity()
				.getSupportFragmentManager(), numberOfViewPagerChildren, 0,null, null);

		viewpager.setAdapter(adapt);
		indicator.setViewPager(viewpager);
		position = viewpager.getCurrentItem();

		swipeTimer = new Timer();
		swipeTimer.schedule(new TimerTask() {

			@Override
			public void run() {
				
				if(context==null)
				Log.e("Activity is null", "activity is null");
				((Activity) context).runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if (position == numberOfViewPagerChildren) {
							position = 0;
						} else {
							position = position + 1;

						}
						viewpager.setCurrentItem(position, true);
						// /viewPager.setCurrentItem(position ++, true);
					}
				});
			}
		}, 500, 6000);


		return layout;
	}

}
