package com.ikomet.foodfiestaa.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.beans.CookDetailBean;
import com.ikomet.foodfiestaa.interfaces.FragmentCommunicator;
import com.ikomet.foodfiestaa.utils.DialogCustom;
import com.ikomet.foodfiestaa.utils.DialogVerification;
import com.ikomet.foodfiestaa.utils.InternetCheck;
import com.ikomet.foodfiestaa.utils.ServiceHandler;
import com.ikomet.foodfiestaa.utils.SharedPrefs;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import org.json.JSONException;
import org.json.JSONObject;

public class Frag_editCook_payinfo extends Fragment {

	private static final String COOKDETAILBEAN = "cook_details_bean";
	String acname;
	String acno;
	CookDetailBean bean;
	Button btn_next_general;
	EditText edt_ac_name;
	EditText edt_account_mail;
	EditText edt_account_phone;
	EditText edt_bank_ac_no;
	EditText edt_branch_address;
	EditText edt_ifsc_code;
	String id;
	String ifsc;
	String paypal_email_or_ph;
	String routing;
	String token;
	View view;
	public DialogVerification dialog;
	public FragmentCommunicator communicator;
	DialogCustom dialogNet;

	public Frag_editCook_payinfo() {
	}

	public static Frag_editCook_payinfo newInstance(
			CookDetailBean cookdetailbean) {
		Frag_editCook_payinfo frag_editcook_payinfo = new Frag_editCook_payinfo();
		Bundle bundle = new Bundle();
		bundle.putSerializable("cook_details_bean", cookdetailbean);
		frag_editcook_payinfo.setArguments(bundle);
		return frag_editcook_payinfo;
	}

	public void initialize() {
		edt_bank_ac_no = (EditText) view.findViewById(R.id.edt_bank_ac_no);
		edt_ac_name = (EditText) view.findViewById(R.id.edt_ac_name);
		edt_branch_address = (EditText) view
				.findViewById(R.id.edt_branch_address);
		edt_ifsc_code = (EditText) view.findViewById(R.id.edt_ifsc_code);
		edt_account_mail = (EditText) view.findViewById(R.id.edt_account_mail);
		edt_account_phone = (EditText) view
				.findViewById(R.id.edt_account_phone);
		btn_next_general = (Button) view.findViewById(R.id.btn_next_general);
	}

	public View onCreateView(LayoutInflater layoutinflater,
			ViewGroup viewgroup, Bundle bundle) {
		view = layoutinflater.inflate(R.layout.frag_editcook_payinfo, viewgroup, false);
		dialogNet = new DialogCustom();
		initialize();
		id = SharedPrefs.getUid(getActivity());
		token = SharedPrefs.getToken(getActivity());
		dialog = new DialogVerification(getActivity(), 1,
				"Profile updated successfully");
		communicator = (FragmentCommunicator) getActivity();
		bean = (CookDetailBean) getArguments().getSerializable(
				"cook_details_bean");
		if (!bean.getPaypal_mail_or_phone().equalsIgnoreCase("")) {
			edt_bank_ac_no.setVisibility(View.GONE);
			edt_ac_name.setVisibility(View.GONE);
			edt_branch_address.setVisibility(View.GONE);
			edt_ifsc_code.setVisibility(View.GONE);
			edt_account_mail.setVisibility(View.VISIBLE);
			edt_account_mail.setText(bean.getPaypal_mail_or_phone());
			paypal_email_or_ph = edt_account_mail.getText().toString();
		} else {
			edt_bank_ac_no.setVisibility(View.VISIBLE);
			edt_ac_name.setVisibility(View.VISIBLE);
			edt_branch_address.setVisibility(View.VISIBLE);
			edt_ifsc_code.setVisibility(View.VISIBLE);
			edt_account_mail.setVisibility(View.GONE);
			edt_bank_ac_no.setText(bean.getAcno());
			edt_ac_name.setText(bean.getAcname());
			edt_branch_address.setText(bean.getRouting());
			edt_ifsc_code.setText(bean.getIfsc());
			acno = edt_bank_ac_no.getText().toString();
			acname = edt_ac_name.getText().toString();
			routing = edt_branch_address.getText().toString();
			ifsc = edt_ifsc_code.getText().toString();
		}
		btn_next_general.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (bean.getPaypal_mail_or_phone().equalsIgnoreCase("")) {
					if(InternetCheck.isInternetConnected(getActivity())){
						new async_edt_payinfo().execute("bank");
					}else{
						dialogNet.showDialog(getActivity(), "Please connect to Internet", "OK", R.drawable.circle_uncheck);
					}
					/*
					 * (new async_edt_payinfo()) .execute(new String[] { "bank"
					 * });
					 */
					return;
				} else {
					if(InternetCheck.isInternetConnected(getActivity())){
						new async_edt_payinfo().execute("paypal");
					}else{
						dialogNet.showDialog(getActivity(), "Please connect to Internet", "OK", R.drawable.circle_uncheck);
					}
					return;
				}
			}

		});
		return view;
	}

	class async_edt_payinfo extends AsyncTask<String, Void, String> {
		String param;
		JSONObject obj1;
		ServiceHandler handler = new ServiceHandler();
		String result, status, message;

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			if (params[0].equalsIgnoreCase("bank")) {
				try {
					param = (new StringBuilder("&id="))
							.append(URLEncoder.encode(id, "UTF-8"))
							.append("&token=")
							.append(URLEncoder.encode(token, "UTF-8"))
							.append("&acno=")
							.append(URLEncoder.encode(acno, "UTF-8"))
							.append("&acname=")
							.append(URLEncoder.encode(acname, "UTF-8"))
							.append("&routing=")
							.append(URLEncoder.encode(routing, "UTF-8"))
							.append("&ifsc=")
							.append(URLEncoder.encode(ifsc, "UTF-8"))
							.toString();
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				result = handler.makeServiceCall(
						"http://50.57.127.125:10131/editprofileservice", 2,
						param);
			} else {
				try {
					param = (new StringBuilder("&id="))
							.append(URLEncoder.encode(id, "UTF-8"))
							.append("&token=")
							.append(URLEncoder.encode(token, "UTF-8"))
							.append("&paypal_email_or_ph=")
							.append(URLEncoder.encode(paypal_email_or_ph,
									"UTF-8")).toString();

				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				result = handler.makeServiceCall(
						"http://50.57.127.125:10131/editprofileservice", 2,
						param);
			}

			try {
				obj1 = new JSONObject(result);
				status = obj1.getString("status");
				message = obj1.getString("message");

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return message;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (result.equalsIgnoreCase("profile updated successfully")) {

				dialog.show();

				Button ok = (Button) dialog.findViewById(R.id.btn_ok);
				ok.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						communicator.change_state();
						getActivity().finish();
						dialog.dismiss();
					}
				});

				return;
			} else {
				Toast.makeText(getActivity(), result, 1000).show();
				return;
			}
		}

	}
}
