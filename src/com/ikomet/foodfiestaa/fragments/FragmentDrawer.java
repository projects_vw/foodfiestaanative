package com.ikomet.foodfiestaa.fragments;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONObject;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.plus.Plus;
import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.activities.BecomeACook;
import com.ikomet.foodfiestaa.activities.ConsumerSignIn;
import com.ikomet.foodfiestaa.activities.ContactUs;
import com.ikomet.foodfiestaa.activities.CookSignIn;
import com.ikomet.foodfiestaa.activities.GalleryActivity;
import com.ikomet.foodfiestaa.activities.HelpAndFaq;
import com.ikomet.foodfiestaa.activities.HomeActivity;
import com.ikomet.foodfiestaa.activities.HowItWorksNew;
import com.ikomet.foodfiestaa.activities.MainActivity;
import com.ikomet.foodfiestaa.activities.StaticPagesActivity;
import com.ikomet.foodfiestaa.activities.TestActivity;
import com.ikomet.foodfiestaa.adapter.ListAdapterSingleText;
import com.ikomet.foodfiestaa.fragments.SignedDrawerConsumer.AttemptLogin;
import com.ikomet.foodfiestaa.interfaces.FragmentCommunicator;
import com.ikomet.foodfiestaa.interfaces.FragmentCommunicatorNew;
import com.ikomet.foodfiestaa.utils.DialogReferFriend;
import com.ikomet.foodfiestaa.utils.DialogTest;
import com.ikomet.foodfiestaa.utils.DialogVerification;
import com.ikomet.foodfiestaa.utils.InternetCheck;
import com.ikomet.foodfiestaa.utils.ReadStream;
import com.ikomet.foodfiestaa.utils.SharedPrefs;

public class FragmentDrawer extends Fragment implements FragmentCommunicator {

	private ActionBarDrawerToggle mDrawerToggle;
	private DrawerLayout mDrawerLayout;

	RelativeLayout header;
	TextView title;
	TextView name, data, logout;
	ListView drawerList;
	ListAdapterSingleText drawerAdapter;
	ArrayList<String> navDrawer;
	ProgressDialog pDialog;
	ProgressBar pBar;
	View containerView;

	String serviceUrl;
	String cms_name;
	String cms_content;

	Fragment frag;
	Context cxt;

	FragmentCommunicatorNew fcommunicatorNew;
	Dialog dialog;

	public FragmentDrawer() {
		// TODO Auto-generated constructor stub
	}

	public void getcxt(Context cxt) {
		// TODO Auto-generated constructor stub
		this.cxt = cxt;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflating view layout
		View layout = inflater.inflate(R.layout.fragment_navigation_drawer,
				container, false);
		
		noNetDialog();

		drawerList = (ListView) layout.findViewById(R.id.list_drawer);

		navDrawer = new ArrayList<String>();

		if (SharedPrefs.hasSPData(getActivity())) {

			navDrawer.add("About Us");
			navDrawer.add("Contact US");
			navDrawer.add("How It Works");
			navDrawer.add("Cooking Standards");
			navDrawer.add("Help/FAQ");
			navDrawer.add("Gift/refer a friend");
			navDrawer.add("Gallery");
			navDrawer.add("Testimonials");
			navDrawer.add("Terms & Conditions");
			navDrawer.add("Privacy Policy");
			navDrawer.add("Sign Out");

		} else {

			navDrawer.add("Sign In");
			navDrawer.add("Sign Up");
			navDrawer.add("About Us");
			navDrawer.add("Contact US");
			navDrawer.add("How It Works");
			navDrawer.add("Cooking Standards");
			navDrawer.add("Help/FAQ");
			navDrawer.add("Gallery");
			navDrawer.add("Testimonials");
			navDrawer.add("Terms & Conditions");
			navDrawer.add("Privacy Policy");

		}
		// navDrawer.add("Sign Out");

		drawerAdapter = new ListAdapterSingleText(getActivity(), navDrawer);

		drawerList.setAdapter(drawerAdapter);

		drawerList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub

				Intent i;
				String url = "http://beta.foodfiestaa.com/cmsservice?&cmsid=";
				
				if (SharedPrefs.hasSPData(getActivity())) {

					switch (position) {
					case 0:
						if(InternetCheck.isInternetConnected(getActivity())){
							serviceUrl = url + "2";
							cms_name = "About Us";
							new AttemptLogin().execute();
						}else{
							dialog.show();
						}
						break;
					case 1:
						i = new Intent(getActivity(), ContactUs.class);
						startActivity(i);
						break;
					case 2:
						i = new Intent(getActivity(), HowItWorksNew.class);
						startActivity(i);
						break;
					case 3:
						if(InternetCheck.isInternetConnected(getActivity())){
							serviceUrl = url + "6";
							cms_name = "Cooking Standards";
							new AttemptLogin().execute();
						}else{
							dialog.show();
						}
						break;
					case 4:
						i = new Intent(getActivity(), HelpAndFaq.class);
						startActivity(i);
						break;
					case 5:
						DialogReferFriend refer=new DialogReferFriend(getActivity());
						refer.show();
						break;
					case 6:
						if(InternetCheck.isInternetConnected(getActivity())){
							i = new Intent(getActivity(), GalleryActivity.class);
							startActivity(i);
						}else{
							dialog.show();
						}
						break;
					case 7:
						
						break;
					case 8:
						if(InternetCheck.isInternetConnected(getActivity())){
							serviceUrl = url + "3";
							cms_name = "Terms & Conditions";
							new AttemptLogin().execute();
						}else{
							dialog.show();
						}
						break;
					case 9:
						if(InternetCheck.isInternetConnected(getActivity())){
							serviceUrl = url + "4";
							cms_name = "Privacy Policy";
							new AttemptLogin().execute();
						}else{
							dialog.show();
						}
						break;
					case 10:
						SharedPrefs.clearSP(getActivity());
						i = new Intent(getActivity(), HomeActivity.class);
						getActivity().finish();
						startActivity(i);
						break;
					default:
						break;
					}

				} else {

					switch (position) {
					case 0:
						DialogTest test = new DialogTest(getActivity(), true);
						test.show();
						break;
					case 1:

						DialogTest test1 = new DialogTest(getActivity(), false);
						test1.show();
						break;
					case 2:
						if(InternetCheck.isInternetConnected(getActivity())){
							serviceUrl = url + "2";
							cms_name = "About Us";
							new AttemptLogin().execute();
						}else{
							dialog.show();
						}
						break;
					case 3:
						i = new Intent(getActivity(), ContactUs.class);
						startActivity(i);
						break;
					case 4:
						i = new Intent(getActivity(), HowItWorksNew.class);
						startActivity(i);
						break;
					case 5:
						if(InternetCheck.isInternetConnected(getActivity())){
							serviceUrl = url + "6";
							cms_name = "Cooking Standards";
							new AttemptLogin().execute();
						}else{
							dialog.show();
						}
						break;
					case 6:
						i = new Intent(getActivity(), HelpAndFaq.class);
						startActivity(i);
						break;
					case 7:
						if(InternetCheck.isInternetConnected(getActivity())){
							i = new Intent(getActivity(), GalleryActivity.class);
							startActivity(i);
						}else{
							dialog.show();
						}
						break;
					case 8:
						
						break;
					case 9:
						if(InternetCheck.isInternetConnected(getActivity())){
							serviceUrl = url + "3";
							cms_name = "Terms & Conditions";
							new AttemptLogin().execute();
						}else{
							dialog.show();
						}
						break;
					case 10:
						if(InternetCheck.isInternetConnected(getActivity())){
							serviceUrl = url + "4";
							cms_name = "Privacy Policy";
							new AttemptLogin().execute();
						}else{
							dialog.show();
						}
						break;
					default:
						break;
					}
				}
				
				mDrawerLayout.closeDrawer(containerView);

			}
		});

		return layout;

	}
	
	private void noNetDialog(){
		dialog = new Dialog(getActivity());
		dialog.getWindow().requestFeature(1);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
		dialog.setContentView(R.layout.dialog_verification_signup);
//		dialog.setCancelable(false);
		TextView txtTitle = (TextView)dialog.findViewById(R.id.txt_content);
		ImageView imgCross = (ImageView)dialog.findViewById(R.id.img_tick);
		Button ok = (Button) dialog.findViewById(R.id.btn_ok);
		txtTitle.setText("Please connect to Internet");
		imgCross.setImageResource(R.drawable.circle_uncheck);
		ok.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
	}
	
	public void setUp(int fragmentId, DrawerLayout drawerLayout,
			final Toolbar toolbar) {
		containerView = getActivity().findViewById(fragmentId);
		mDrawerLayout = drawerLayout;
		mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout,
				toolbar, R.string.drawer_open, R.string.drawer_close) {
			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				getActivity().invalidateOptionsMenu();
			}

			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
				getActivity().invalidateOptionsMenu();
			}

			@Override
			public void onDrawerSlide(View drawerView, float slideOffset) {
				super.onDrawerSlide(drawerView, slideOffset);
				toolbar.setAlpha(1 - slideOffset / 2);
			}
		};

		mDrawerLayout.setDrawerListener(mDrawerToggle);
		mDrawerLayout.post(new Runnable() {
			@Override
			public void run() {
				mDrawerToggle.syncState();
			}
		});

	}

	class AttemptLogin extends AsyncTask<String, String, String> {

		boolean status;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			// pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

			pDialog.show();
			// pBar = new ProgressBar(getActivity());
			// pBar.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progressbar_custom));

		}

		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub
			// Check for success tag

			String json = "";

			URL url = null;
			try {
				url = new URL(serviceUrl);

				HttpURLConnection urlConnection = null;

				urlConnection = (HttpURLConnection) url.openConnection();

				try {
					InputStream in = null;

					in = new BufferedInputStream(urlConnection.getInputStream());

					ReadStream stream = new ReadStream();
					json = stream.readStream(in, url);

				} finally {
					urlConnection.disconnect();
				}

				JSONObject jsonObj;

				jsonObj = new JSONObject(json);

				// json success tag

				status = jsonObj.getBoolean("status");

				if (status) {
					cms_content = jsonObj.getString("cms_content");
					return "success";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;

		}

		protected void onPostExecute(String value) {
			// dismiss the dialog once product deleted
			if (pDialog.isShowing()) {
				pDialog.dismiss();
			}
			// pBar.setActivated(false);
			System.out.println("value = " + value);
			if (value != null) {

				Intent intent = new Intent(getActivity(),
						StaticPagesActivity.class);
				intent.putExtra("cms_name", cms_name);
				intent.putExtra("cms_content", cms_content);
				startActivity(intent);

			}

		}

	}

	@Override
	public void passDataToFragment() {
		// TODO Auto-generated method stub
		mDrawerLayout.openDrawer(containerView);
	}

	@Override
	public void change_state() {
		// TODO Auto-generated method stub

	}

	@Override
	public void Add_click() {
		// TODO Auto-generated method stub

	}

	@Override
	public void locationToolbar(boolean isLocationSerach) {
		// TODO Auto-generated method stub
		
	}

}
