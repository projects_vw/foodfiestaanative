package com.ikomet.foodfiestaa.fragments;

import java.net.URLEncoder;

import java.util.ArrayList;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.activities.HomeActivity;
import com.ikomet.foodfiestaa.adapter.OrderExpandListAdapter;
import com.ikomet.foodfiestaa.beans.OrderChild;
import com.ikomet.foodfiestaa.beans.OrderGroup;
import com.ikomet.foodfiestaa.beans.OrderedItems;
import com.ikomet.foodfiestaa.beans.Orders;
import com.ikomet.foodfiestaa.utils.InternetCheck;
import com.ikomet.foodfiestaa.utils.ServiceHandler;
import com.ikomet.foodfiestaa.utils.SharedPrefs;

public class Frag_orderCook extends Fragment implements View.OnClickListener {

	RelativeLayout todaysSpl, preOrder;
	ImageView imgTodaysSpl, imgPreOrder, smiley;
	TextView txtTodaysSpl, txtPreOrder;
	EditText search;
	View view;
	ExpandableListView expandList;
	OrderExpandListAdapter ExpAdapter;
	ArrayList<Orders> orderList,orderListTdy;
	ArrayList<OrderedItems> orderedItemsList, orderedItemsListTdy;
	ArrayList<OrderGroup> groups, groupsTdy;
	ProgressDialog pDialog;
	RelativeLayout snacklayout,rel_orders,rel_nonet;
	boolean ispreorder;
	public static final String ORDERS_LIST_URL = "http://beta.foodfiestaa.com/orderservice";

	@Override
	public View onCreateView(LayoutInflater inflater,
		ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		//super.onCreate(savedInstanceState);
		//setContentView(R.layout.orders);
		view=inflater.inflate(R.layout.frg_cook_orders,container, false);
		rel_orders=(RelativeLayout)view.findViewById(R.id.rel_orders);
		rel_nonet=(RelativeLayout)view.findViewById(R.id.no_net);
		smiley = (ImageView)view.findViewById(R.id.img_sad);
		if(InternetCheck.isInternetConnected(getActivity())){
			rel_orders.setVisibility(View.VISIBLE);
			rel_nonet.setVisibility(View.GONE);
			
			variableInitialize();
			
		}else{
			rel_orders.setVisibility(View.GONE);
			rel_nonet.setVisibility(View.VISIBLE);
			
			rel_nonet.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(InternetCheck.isInternetConnected(getActivity())){
						
						Animation a = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_anim);
						a.setDuration(1000);
						smiley.startAnimation(a);
						
						new Handler().postDelayed(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								rel_orders.setVisibility(View.VISIBLE);
								rel_nonet.setVisibility(View.GONE);
								variableInitialize();
							}
						}, 2000);
																							
						/*rel_orders.setVisibility(View.VISIBLE);
						rel_nonet.setVisibility(View.GONE);
						variableInitialize();*/
					}
				}
			});
			
		}
	
		
		return view;
		
	}

	public void variableInitialize(){
		todaysSpl = (RelativeLayout)view. findViewById(R.id.rel_todaysspl);
		preOrder = (RelativeLayout)view. findViewById(R.id.rel_pre_order);
		snacklayout = (RelativeLayout)view. findViewById(R.id.snack_order);
		imgTodaysSpl = (ImageView)view. findViewById(R.id.img_todaysspl);
		imgPreOrder = (ImageView)view. findViewById(R.id.img_pre_order);

		txtTodaysSpl = (TextView)view. findViewById(R.id.txt_todaysspl);
		txtPreOrder = (TextView) view.findViewById(R.id.txt_pre_order);

		search = (EditText) view.findViewById(R.id.ed_search);

		expandList = (ExpandableListView) view.findViewById(R.id.exp_order);
		// ExpAdapter = new ExpandListAdapter(TestActivity1.this, groups);
		orderList = new ArrayList<Orders>();
		orderListTdy = new ArrayList<Orders>();
		groups = new ArrayList<OrderGroup>();
		groupsTdy = new ArrayList<OrderGroup>();

		todaysSpl.setOnClickListener(this);
		preOrder.setOnClickListener(this);

		inTodaysSpl();

		new OrderList().execute();

		  //this code for adjusting the group indicator into right side of the view
	
       //expandList.setIndicatorBounds(width - GetDipsFromPixel(50), width - GetDipsFromPixel(10));

		expandList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
			
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v,
					int groupPosition, long id) {
				// TODO Auto-generated method stub
				int idd = v.getId();
					
				if(idd == R.id.txt_ord_no){
					Toast.makeText(getActivity(), "Clicked text", 1000).show();
					return true;
				}else{
					return false;
				}
				
				
			}
		});
		
	}
	/*@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return super.onCreateView(inflater, container, savedInstanceState);
	}*/

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		int id = v.getId();

		if (id == R.id.rel_todaysspl) {
			if(orderListTdy!=null){
				inTodaysSpl();
				ExpAdapter = new OrderExpandListAdapter(getActivity(), groupsTdy, orderListTdy);
				expandList.setAdapter(ExpAdapter);
				ExpAdapter.notifyDataSetChanged();
			}
		}

		if (id == R.id.rel_pre_order) {
			if(orderList!=null){
				inPreOrder();
				ExpAdapter = new OrderExpandListAdapter(getActivity(), groups, orderList);
				expandList.setAdapter(ExpAdapter);
				ExpAdapter.notifyDataSetChanged();
			}
		}

	}

	@SuppressWarnings({ "deprecation" })
	public void inTodaysSpl() {

		ispreorder = false;
		
		todaysSpl.setBackgroundColor(Color.WHITE);
		preOrder.setBackgroundColor(getResources().getColor(R.color.colorGrey));

		imgTodaysSpl.setImageResource(R.drawable.todayspecial1);
		txtTodaysSpl
				.setTextColor(getResources().getColor(R.color.colorPrimary));

		imgPreOrder.setImageResource(R.drawable.greypreorder1);
		txtPreOrder.setTextColor(getResources().getColor(
				R.color.textColorPrimary));

	}

	@SuppressWarnings({ "deprecation" })
	public void inPreOrder() {

		ispreorder = true;
		
		todaysSpl
				.setBackgroundColor(getResources().getColor(R.color.colorGrey));
		preOrder.setBackgroundColor(Color.WHITE);

		imgTodaysSpl.setImageResource(R.drawable.greytodayspecial1);
		txtTodaysSpl.setTextColor(getResources().getColor(
				R.color.textColorPrimary));

		imgPreOrder.setImageResource(R.drawable.preorder1);
		txtPreOrder.setTextColor(getResources().getColor(R.color.colorPrimary));

	}

	/*public int GetDipsFromPixel(float pixels)
    {
     // Get the screen's density scale
     final float scale = getResources().getDisplayMetrics().density;
     // Convert the dps to pixels, based on density scale
     return (int) (pixels * scale + 0.5f);
    }*/
	
	class OrderList extends AsyncTask<String, String, String> {

		boolean status;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();

		}

		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub
			// Check for success tag
			try {
				String json = "";

				ServiceHandler sh = new ServiceHandler();

				String id = SharedPrefs.getUid(getActivity());
			String token = SharedPrefs.getToken(getActivity());
				
		/*		String id = "282";
				String token = "uay0v51KQ4";
*/
				String params = "id=" + URLEncoder.encode(id, "UTF-8")
						+ "&token=" + URLEncoder.encode(token, "UTF-8");

				json = sh.makeServiceCall(ORDERS_LIST_URL, ServiceHandler.POST,
						params);

				JSONObject jsonObj;

				jsonObj = new JSONObject(json);

				status = jsonObj.getBoolean("status");

				if (status) {
					// return jsonObj.getString("message");

					JSONArray jarOrder = jsonObj.getJSONArray("order");
					groups = new ArrayList<OrderGroup>();
					groupsTdy = new ArrayList<OrderGroup>();
					for (int i = 0; i < jarOrder.length(); i++) {

						JSONObject jobjOrder = jarOrder.getJSONObject(i);

						String order_id = jobjOrder.getString("order_id");
						String order_number = jobjOrder
								.getString("order_number");
						String status = jobjOrder.getString("status");
						String order_date = jobjOrder.getString("order_date");
						String order_time = jobjOrder.getString("order_time");
						String delivery_date = jobjOrder
								.getString("delivery_date");
						String delivery_time = jobjOrder
								.getString("delivery_time");
						String order_type = jobjOrder.getString("order_type");
						String consumer_name = jobjOrder
								.getString("consumer_name");
						String txnid = jobjOrder.getString("txnid");
						String total_price = jobjOrder.getString("total_price");

						JSONArray jarOrdItem = jobjOrder
								.getJSONArray("order_items");
						orderedItemsList = new ArrayList<OrderedItems>();
						orderedItemsListTdy = new ArrayList<OrderedItems>();
						ArrayList<OrderChild> childs = new ArrayList<OrderChild>();
						OrderChild child = new OrderChild(consumer_name, txnid, order_date, order_time);
						childs.add(child);
						if(order_type.equalsIgnoreCase("preorder")){
							groups.add(new OrderGroup(status, order_number, total_price, childs));
						}else{
							groupsTdy.add(new OrderGroup(status, order_number, total_price, childs));
						}
						for (int j = 0; j < jarOrdItem.length(); j++) {

							JSONObject jobjOrdItem = jarOrdItem
									.getJSONObject(j);

							String item_name = jobjOrdItem
									.getString("item_name");
							String qty = jobjOrdItem.getString("qty");
							String avaliable_order = jobjOrdItem
									.getString("avaliable_order");
							String price = jobjOrdItem.getString("price");
							String oid = jobjOrdItem.getString("oid");

							OrderedItems ordereditem = new OrderedItems(
									item_name, qty, avaliable_order, price, oid);
							
							if(order_type.equalsIgnoreCase("preorder")){
								orderedItemsList.add(ordereditem);
							}else{
								orderedItemsListTdy.add(ordereditem);
							}

						}
						
						if(order_type.equalsIgnoreCase("preorder")){
								orderList.add(new Orders(order_id, order_number,
										status, order_date, order_time, delivery_date,
										delivery_time, order_type, consumer_name,
										txnid, total_price, orderedItemsList));
						}else{
								orderListTdy.add(new Orders(order_id, order_number,
										status, order_date, order_time, delivery_date,
										delivery_time, order_type, consumer_name,
										txnid, total_price, orderedItemsListTdy));
						}
					}

					return "success";

				} else {

					return jsonObj.getString("data");

				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;

		}

		protected void onPostExecute(String value) {
			// dismiss the dialog once product deleted
			if (pDialog.isShowing()) {
				pDialog.dismiss();
			}
			
			System.out.println("value = " + value);
			if (value != null) {

				if (status) {
					if(ispreorder){
						ExpAdapter = new OrderExpandListAdapter(getActivity(), groups, orderList);
					}else{
						ExpAdapter = new OrderExpandListAdapter(getActivity(), groupsTdy, orderListTdy);
					}
					
					expandList.setAdapter(ExpAdapter);
					
					search.addTextChangedListener(new TextWatcher() {
						
						@Override
						public void onTextChanged(CharSequence s, int start, int before, int count) {
							// TODO Auto-generated method stub
							String text = search.getText().toString().toLowerCase(Locale.getDefault());
							ExpAdapter.filter(text);
						}
						
						@Override
						public void beforeTextChanged(CharSequence s, int start, int count,
								int after) {
							// TODO Auto-generated method stub
							
						}
						
						@Override
						public void afterTextChanged(Editable s) {
							// TODO Auto-generated method stub
							
						}
					});
					
				}

				Snackbar.make(snacklayout, value, Snackbar.LENGTH_SHORT).show();

			}

		}

	}

}
