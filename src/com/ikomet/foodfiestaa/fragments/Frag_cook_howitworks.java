package com.ikomet.foodfiestaa.fragments;

import java.util.Timer;
import java.util.TimerTask;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.utils.SwipeGestureDetector;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ViewFlipper;

public class Frag_cook_howitworks extends Fragment{
	ViewFlipper flipper;
	Timer swipeTimer;
	int position;
	@Override
	public View onCreateView(LayoutInflater inflater,
			 ViewGroup container,  Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view=inflater.inflate(R.layout.frag_cook_howitworks, container,false);
		flipper=(ViewFlipper)view.findViewById(R.id.viewflipper1);
	
		
		Log.e("current item", ""+flipper.getDisplayedChild());
		final GestureDetector detector = new GestureDetector(new SwipeGestureDetector(flipper,getActivity()));
		flipper.setOnTouchListener(new View.OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				detector.onTouchEvent(event);
				return true;
			}
		});
		Log.e("test_frag", "inside_fragment_eeeeeee");
		
		position = flipper.getDisplayedChild();

		swipeTimer = new Timer();
		swipeTimer.schedule(new TimerTask() {

			@Override
			public void run() {
				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if (position == 3) {
							position = 0;
						} else {
							position = position + 1;

						}
						flipper.setDisplayedChild(position);
						// /viewPager.setCurrentItem(position ++, true);
					}
				});
			}
		}, 500, 3000);

	

		return view;
	}

	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
		
		
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		swipeTimer.cancel();
		
	}

	
}
