package com.ikomet.foodfiestaa.fragments;

import java.net.URLEncoder;

import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.utils.InternetCheck;
import com.ikomet.foodfiestaa.utils.ServiceHandler;
import com.ikomet.foodfiestaa.utils.SharedPrefs;
import com.ikomet.foodfiestaa.utils.ShowHideToolbar;
import com.squareup.picasso.Picasso;

public class FragmentItemDetail extends Fragment implements
		View.OnClickListener {

	View layout;
	ImageView imageItem, smiley;
	TextView txtItemName, txtFoodType, txtDietType, txtCuisineType,
			txtDishType;
	ProgressDialog pDialog;
	String menu_id;
	private static final String MENU_DETAIL_URL = "http://beta.foodfiestaa.com/menudetailservice";
	String foodName, description, ftype, dtype, ctype, foodPic;
	ImageButton back, rate;
	ShowHideToolbar toolbarInstance;
	RelativeLayout fragItemDetail, noNet;
	
	@Override
	public void onAttach(Context context) {
		// TODO Auto-generated method stub
		super.onAttach(context);
		toolbarInstance = (ShowHideToolbar)context;
		toolbarInstance.hideToolbar();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		layout = inflater.inflate(R.layout.frag_itemdetail, container, false);

		noNet = (RelativeLayout)layout.findViewById(R.id.no_net);
		fragItemDetail = (RelativeLayout)layout.findViewById(R.id.rel_item_detail);
		smiley = (ImageView)layout.findViewById(R.id.img_sad);
		
		if(InternetCheck.isInternetConnected(getActivity())){
			fragItemDetail.setVisibility(View.VISIBLE);
			noNet.setVisibility(View.GONE);
			
			initializeVar();
			
		}else{
			fragItemDetail.setVisibility(View.GONE);
			noNet.setVisibility(View.VISIBLE);
			
			noNet.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(InternetCheck.isInternetConnected(getActivity())){
						
						Animation a = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_anim);
						a.setDuration(1000);
						smiley.startAnimation(a);
						
						new Handler().postDelayed(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								fragItemDetail.setVisibility(View.VISIBLE);
								noNet.setVisibility(View.GONE);
								initializeVar();
							}
						}, 2000);
						
					}
				}
			});
			
		}
		
		

		return layout;
	}

	private void initializeVar(){
		
		imageItem = (ImageView) layout.findViewById(R.id.img_item);
		txtItemName = (TextView) layout.findViewById(R.id.txt_item_name);

		txtFoodType = (TextView) layout.findViewById(R.id.txt_food_type);
		txtDietType = (TextView) layout.findViewById(R.id.txt_diet_type);
		txtCuisineType = (TextView) layout.findViewById(R.id.txt_cuisine_type);
		txtDishType = (TextView) layout.findViewById(R.id.txt_dish_type);

		back = (ImageButton) layout.findViewById(R.id.bt_back);
		rate = (ImageButton) layout.findViewById(R.id.bt_rate);

		back.setOnClickListener(this);
		rate.setOnClickListener(this);

		new MenuDetail().execute();
		
	}
	
	class MenuDetail extends AsyncTask<String, String, String> {

		boolean status;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Please Wait ...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub
			// Check for success tag

			String json = null;

			try {

				String menu_id = SharedPrefs.getMenuId(getActivity());

				// you need to encode ONLY the values of the parameters
				String param = "menu_id=" + URLEncoder.encode(menu_id, "UTF-8");

				ServiceHandler sh = new ServiceHandler();

				json = sh.makeServiceCall(MENU_DETAIL_URL, ServiceHandler.POST,
						param);

				JSONObject jsonObj = new JSONObject(json);

				status = jsonObj.getBoolean("status");

				if (status) {

					JSONObject jsonData = jsonObj.getJSONObject("data");

					jsonData.getString("avgrating");
					foodName = jsonData.getString("food_name");
					jsonData.getString("description");
					ftype = jsonData.getString("ftype");
					dtype = jsonData.getString("dtype");
					ctype = jsonData.getString("ctype");
					jsonData.getString("preorder_days");
					jsonData.getString("foodprice");
					foodPic = "http://beta.foodfiestaa.com/"
							+ jsonData.getString("pic").replace(" ", "%20");
					jsonData.getString("preorder_maxorder");

					// return "LogIn successfull";
					return "success";
				} else {

					return null;
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;

		}

		protected void onPostExecute(String value) {
			// dismiss the dialog once product deleted
			if (pDialog.isShowing()) {
				pDialog.dismiss();
			}

			if (value != null) {
				if (status) {
					Picasso.with(getActivity()).load(foodPic)
							.placeholder(R.drawable.two).into(imageItem);
					txtItemName.setText(foodName);
					txtFoodType.setText("Food Type\t" + ftype);
					txtDietType.setText("Diet Type\t" + dtype);
					txtCuisineType.setText("Cuisine Type\t" + ctype);
				}
			}

		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.bt_back:

			break;
		case R.id.bt_rate:
			Fragment frag = new FragmentAddRating();
			FragmentManager fm = getActivity().getSupportFragmentManager();
			FragmentTransaction ft = fm.beginTransaction();
			ft.replace(R.id.realtabcontent, frag);
			ft.commit();
			break;
		default:
			break;
		}
	}

}
