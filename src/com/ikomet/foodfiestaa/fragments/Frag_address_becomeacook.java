package com.ikomet.foodfiestaa.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.beans.CookAddressInformation;
import com.ikomet.foodfiestaa.beans.CookGeneralInformation;
import com.ikomet.foodfiestaa.fragments.Fragment_general_becomeacook.CheckData;
import com.ikomet.foodfiestaa.utils.Validator;

public class Frag_address_becomeacook extends Fragment {

	Button btn_next_general;
	FragmentManager fragManager;
	ImageView img_circle1;
	FragmentTransaction transaction;
	Fragment frag;

	EditText street1, street2, location, city, state, country, pincode;
	String st1, st2, loc, cty, stat, cntry, zip;
	boolean validFields, nullCheck;
	Validator validator;
	CookGeneralInformation generalInfo;
	View view;
	
	public Frag_address_becomeacook(){
		
	}
	
	public Frag_address_becomeacook(CookGeneralInformation generalInfo){
		this.generalInfo = generalInfo;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		view = inflater.inflate(R.layout.fragment_address_becomeacook,
				container, false);
		btn_next_general = (Button) view.findViewById(R.id.btn_next_general);
		
		street1 = (EditText)view.findViewById(R.id.edt_street1);
		street2 = (EditText)view.findViewById(R.id.edt_street2);
		location = (EditText)view.findViewById(R.id.edt_location);
		city = (EditText)view.findViewById(R.id.edt_city);
		city.setText("Chennai");
		state = (EditText)view.findViewById(R.id.edt_state);
		state.setText("Tamil Nadu");
		country = (EditText)view.findViewById(R.id.edt_country);
		country.setText("India");
		pincode = (EditText)view.findViewById(R.id.edt_pincode);
		
		validator = new Validator(getActivity().getApplicationContext());
		
		validFields = validator.validateAddress(street1, street2, location, city, state, country, pincode);
		
		btn_next_general.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				validFields = validator.validateAddress(street1, street2, location, city, state, country, pincode);
				
				nullCheck = validator.nullCheckAddress(street1, street2, location, city, state, country, pincode);
				
				if(validFields && nullCheck){
					
					st1=street1.getText().toString().trim();
					st2=street2.getText().toString().trim();
					loc=location.getText().toString().trim();
					cty=city.getText().toString().trim();
					stat=state.getText().toString().trim();
					cntry=country.getText().toString().trim();
					zip=pincode.getText().toString().trim();
					
					CookAddressInformation addrInfo = new CookAddressInformation(st1, st2, loc, cty, stat, cntry, zip);
				
				
				fragManager = getActivity().getSupportFragmentManager();
				transaction = fragManager.beginTransaction();
				frag = new Frag_payinfo_becomeacook(generalInfo, addrInfo);
				transaction.replace(R.id.frag_becomeacook, frag);
				transaction.commit();
				
				}
			}
		});
		return view;
	}

}
