package com.ikomet.foodfiestaa.fragments;

import java.util.Timer;
import java.util.TimerTask;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.adapter.PagerAdapter;
import com.ikomet.foodfiestaa.utils.CirclePageIndicator;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class FragmentHowConsumer extends Fragment {

	ViewPager viewpager;
	CirclePageIndicator indicator;
	PagerAdapter adapter;
	int numberOfViewPagerChildren = 3;
	int position = 0;
	Timer swipeTimer;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View layout = inflater.inflate(
				R.layout.fragment_new_customer_how_it_works, container, false);

		viewpager = (ViewPager) layout.findViewById(R.id.how_consumer);
		indicator = (CirclePageIndicator) layout.findViewById(R.id.indicator);

		adapter = new PagerAdapter(getActivity().getSupportFragmentManager(),
				numberOfViewPagerChildren, 1,null, null);

		
		viewpager.setAdapter(adapter);
		indicator.setViewPager(viewpager);
		position = viewpager.getCurrentItem();

		swipeTimer = new Timer();
		swipeTimer.schedule(new TimerTask() {

			@Override
			public void run() {
				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if (position == numberOfViewPagerChildren) {
							position = 0;
						} else {
							position = position + 1;

						}
						viewpager.setCurrentItem(position, true);
						// /viewPager.setCurrentItem(position ++, true);
					}
				});
			}
		}, 500, 2000);

		return layout;
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		swipeTimer.cancel();
	}

	
	
}
