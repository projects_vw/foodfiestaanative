package com.ikomet.foodfiestaa.fragments;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.provider.MediaStore.MediaColumns;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.utils.DialogCustom;
import com.ikomet.foodfiestaa.utils.InternetCheck;
import com.ikomet.foodfiestaa.utils.ServiceHandler;
import com.ikomet.foodfiestaa.utils.SharedPrefs;
import com.tmxlr.lib.driodvalidatorlight.Form;
import com.tmxlr.lib.driodvalidatorlight.helper.Range;
import com.tmxlr.lib.driodvalidatorlight.helper.RegexTemplate;

public class FragmentAddDish extends Fragment {

	Switch switch1, switch2;
	ImageView photoHolder;
	EditText fshortName, fdescription, maxOrder, maxOrderPC, pricePerOrder,
			ffPrice;
	EditText selectfType, selectdType, selectcType, daysBeforePre,
			availStarttime, availEndtime;
	View view3;
	Button upload, add;
	
	RelativeLayout relSnack;
	protected static final int REQUEST_CAMERA = 0;
	protected static final int SELECT_FILE = 1;
	protected static final int CROP_PIC = 2;
	private Uri picUri;
	String imagePath = "";
	public final String ADD_DISH_URL = "http://beta.foodfiestaa.com/adddishservice";
	Form form;
	ProgressDialog pDialog;
	Bitmap bitmap=null;
	Dialog dialog;
	DialogCustom dialogCheck;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View layout = inflater
				.inflate(R.layout.frag_add_dish, container, false);
		
		dialogCheck = new DialogCustom();
		upload = (Button) layout.findViewById(R.id.bt_upload_img);
		photoHolder = (ImageView) layout.findViewById(R.id.img_photo);
		fshortName = (EditText) layout.findViewById(R.id.ed_fshort_name);
		fdescription = (EditText) layout.findViewById(R.id.ed_fdescription);
		selectfType = (EditText) layout.findViewById(R.id.ed_select_ftype);
		selectdType = (EditText) layout.findViewById(R.id.ed_select_dtype);
		selectcType = (EditText) layout.findViewById(R.id.ed_select_ctype);
		switch1 = (Switch) layout.findViewById(R.id.switch1);
		maxOrder = (EditText) layout.findViewById(R.id.ed_max_order);
		view3 = (View) layout.findViewById(R.id.view3);
		switch2 = (Switch) layout.findViewById(R.id.switch2);
		maxOrderPC = (EditText) layout.findViewById(R.id.max_order_per_cons);
		daysBeforePre = (EditText) layout.findViewById(R.id.sel_days_bef_pre);
		availStarttime = (EditText) layout.findViewById(R.id.sel_av_starttime);
		availEndtime = (EditText) layout.findViewById(R.id.sel_av_endtime);
		pricePerOrder = (EditText) layout.findViewById(R.id.ed_price_perorder);
		ffPrice = (EditText) layout.findViewById(R.id.ed_ff_price);
		add = (Button) layout.findViewById(R.id.bt_add);

		relSnack = (RelativeLayout) layout.findViewById(R.id.rel_snack);

		form = new Form(getActivity());
		verifyForm();
		add.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (switch1.isChecked()) {
					checkSwitch1();
				} else {
					form.clear();
					if (switch2.isChecked()) {
						checkSwitch2();
					}
					verifyForm();
				}

				if (switch2.isChecked()) {
					checkSwitch2();
				} else {
					form.clear();
					if (switch1.isChecked()) {
						checkSwitch1();
					}
					verifyForm();
				}

				if (form.validate()) {

					if (!switch1.isChecked() && !switch2.isChecked()) {
						Snackbar snackbar = Snackbar.make(relSnack,
								"Please enable Today's Special or Pre-order",
								Snackbar.LENGTH_LONG);
						snackbar.show();
					} else {
						if (photoHolder.getVisibility() == View.VISIBLE) {
							if(InternetCheck.isInternetConnected(getActivity())){
								new AddDishProcess().execute();
							}else{
								dialogCheck.showDialog(getActivity(), "Please connect to Internet", "OK", R.drawable.circle_uncheck);
							}
						} else {
							Snackbar snackbar = Snackbar.make(relSnack,
									"Please add an image for dish",
									Snackbar.LENGTH_LONG);
							snackbar.show();
						}
					}

				} else {
					Snackbar snackbar = Snackbar.make(relSnack,
							"Please fill all the fields", Snackbar.LENGTH_LONG);
					snackbar.show();
				}
			}
		});

		upload.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				dialog = new Dialog(getActivity());
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(R.layout.dialog_edit_picture);
				
				TextView txtDial = (TextView)dialog.findViewById(R.id.txt_heading);
				ImageView imgTakePic = (ImageView)dialog.findViewById(R.id.img_take_pic);
				ImageView imgGalry = (ImageView)dialog.findViewById(R.id.img_gallery);
				
				txtDial.setText("Upload Image");
				
				imgTakePic.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog.dismiss();
						Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
						//intent.putExtra("crop", "true");
						startActivityForResult(intent, REQUEST_CAMERA);
					}
				});
				
				imgGalry.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog.dismiss();
						Intent intent = new Intent(
								Intent.ACTION_PICK,
								android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
						intent.setType("image/*");
						//intent.putExtra("crop", "true");
						startActivityForResult(
								Intent.createChooser(intent, "Select File"),
								SELECT_FILE);
					}
				});
				
				dialog.show();
				
//				selectImage();
			}
		});

		selectfType.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPopupfType();
			}
		});

		selectdType.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPopupdType();
			}
		});

		selectcType.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPopupcType();
			}
		});

		daysBeforePre.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPopdaysbefPre();
			}
		});

		availStarttime.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPopupStartTime();
			}
		});

		availEndtime.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPopupEndTime();
			} 
		});

		switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				if (isChecked) {
					maxOrder.setVisibility(View.VISIBLE);
					view3.setVisibility(View.VISIBLE);
				} else {
					maxOrder.setVisibility(View.GONE);
					view3.setVisibility(View.GONE);
				}
			}
		});

		switch2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				if (isChecked) {
					maxOrderPC.setVisibility(View.VISIBLE);
					daysBeforePre.setVisibility(View.VISIBLE);
					availStarttime.setVisibility(View.VISIBLE);
					availEndtime.setVisibility(View.VISIBLE);
				} else {
					maxOrderPC.setVisibility(View.GONE);
					daysBeforePre.setVisibility(View.GONE);
					availStarttime.setVisibility(View.GONE);
					availEndtime.setVisibility(View.GONE);
				}
			}
		});

		pricePerOrder.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				if (!pricePerOrder.getText().toString().trim()
						.equalsIgnoreCase("")) {
					int val = Integer.parseInt(pricePerOrder.getText()
							.toString().trim());
					int ffprice = (int) (val * 0.35);
					Log.e("test Price", ffprice + "");
					ffPrice.setText(String.valueOf(val + ffprice));
				} else {
					ffPrice.setText("");
				}

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		return layout;
	}

	public void showPopupfType() {
		final String[] foodTypes = { "Breakfast", "Lunch", "Sweets & Snacks",
				"Dinner", "Desserts", "Drinks", "Combo", "Bakery",
				"ServedAllDay" };
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Select Food Type");
		builder.setItems(foodTypes, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				// Do something with the selection
				selectfType.setTextColor(Color.BLACK);
				selectfType.setText(foodTypes[item]);
				dialog.dismiss();
			}
		});

		AlertDialog alert = builder.create();
		alert.show();

	}

	public void showPopupdType() {
		final String[] dietTypes = { "Vegetarian", "Non-Vegetarian" };
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Select Diet Type");
		builder.setItems(dietTypes, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				// Do something with the selection
				selectdType.setTextColor(Color.BLACK);
				selectdType.setText(dietTypes[item]);
				dialog.dismiss();
			}
		});

		AlertDialog alert = builder.create();
		alert.show();

	}

	public void showPopupcType() {
		final String[] cuisineTypes = { "Indian", "Chinese", "Thai", "French",
				"Italian", "Mexican", "Japanese", "Greek", "Lebanese", "Malay",
				"American", "Others" };
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Select Cuisine Type");
		builder.setItems(cuisineTypes, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				// Do something with the selection
				selectcType.setTextColor(Color.BLACK);
				selectcType.setText(cuisineTypes[item]);
				dialog.dismiss();
			}
		});

		AlertDialog alert = builder.create();
		alert.show();

	}

	public void showPopdaysbefPre() {
		final String[] days = { "1" };
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Days Before Pre-order");
		builder.setItems(days, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				// Do something with the selection
				daysBeforePre.setTextColor(Color.BLACK);
				daysBeforePre.setText(days[item]);
				dialog.dismiss();
			}
		});

		AlertDialog alert = builder.create();
		alert.show();

	}

	public void showPopupStartTime() {
		final String[] time = { "8:00 AM", "8:30 AM", "9:00 AM", "9:30 AM",
				"10:00 AM", "10:30 AM", "11:00 AM", "11:30 AM", "12:00 PM",
				"12:30 PM", "1:00 PM", "1:30 PM", "2:00 PM", "2:30 PM",
				"3:00 PM", "3:30 PM", "4:00 PM", "4:30 PM", "5:00 PM",
				"5:30 PM", "6:00 PM", "6:30 PM", "7:00 PM", "7:30 PM",
				"8:00 PM", "8:30 PM", "9:00 PM", "9:30 PM" };
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Available Start Time");
		builder.setItems(time, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				// Do something with the selection
				availStarttime.setTextColor(Color.BLACK);
				availStarttime.setText(time[item]);
				dialog.dismiss();
			}
		});

		AlertDialog alert = builder.create();
		alert.show();

	}

	public void showPopupEndTime() {
		final String[] time = { "8:00 AM", "8:30 AM", "9:00 AM", "9:30 AM",
				"10:00 AM", "10:30 AM", "11:00 AM", "11:30 AM", "12:00 PM",
				"12:30 PM", "1:00 PM", "1:30 PM", "2:00 PM", "2:30 PM",
				"3:00 PM", "3:30 PM", "4:00 PM", "4:30 PM", "5:00 PM",
				"5:30 PM", "6:00 PM", "6:30 PM", "7:00 PM", "7:30 PM",
				"8:00 PM", "8:30 PM", "9:00 PM", "9:30 PM" };
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Available End Time");
		builder.setItems(time, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				// Do something with the selection
				availEndtime.setTextColor(Color.BLACK);
				availEndtime.setText(time[item]);
				dialog.dismiss();
			}
		});

		AlertDialog alert = builder.create();
		alert.show();

	}

	private void selectImage() {
		final CharSequence[] items = { "Take Photo", "Choose from Library",
				"Cancel" };

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Add Photo!");
		builder.setItems(items, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int item) {

				if (items[item].equals("Take Photo")) {
					Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					//intent.putExtra("crop", "true");
					startActivityForResult(intent, REQUEST_CAMERA);
				} else if (items[item].equals("Choose from Library")) {
					Intent intent = new Intent(
							Intent.ACTION_PICK,
							android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
					intent.setType("image/*");
					//intent.putExtra("crop", "true");
					startActivityForResult(
							Intent.createChooser(intent, "Select File"),
							SELECT_FILE);
				} else if (items[item].equals("Cancel")) {
					dialog.dismiss();
				}
			}
		});
		builder.show();
	}

	/*
	 * @Override public void onActivityResult(int requestCode, int resultCode,
	 * Intent data) { super.onActivityResult(requestCode, resultCode, data); if
	 * (resultCode == Activity.RESULT_OK) { if (requestCode == REQUEST_CAMERA) {
	 * Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
	 * ByteArrayOutputStream bytes = new ByteArrayOutputStream();
	 * thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes); File
	 * destination = new File( Environment.getExternalStorageDirectory(),
	 * System.currentTimeMillis() + ".jpg"); imagePath =
	 * destination.getAbsolutePath(); FileOutputStream fo; try {
	 * destination.createNewFile(); fo = new FileOutputStream(destination);
	 * fo.write(bytes.toByteArray()); fo.close(); } catch (FileNotFoundException
	 * e) { e.printStackTrace(); } catch (IOException e) { e.printStackTrace();
	 * } photoHolder.setVisibility(View.VISIBLE);
	 * photoHolder.setImageBitmap(thumbnail); } else if (requestCode ==
	 * SELECT_FILE) { Uri selectedImageUri = data.getData(); File auxFile = new
	 * File(selectedImageUri.toString()); imagePath = auxFile.getAbsolutePath();
	 * String[] projection = { MediaColumns.DATA }; CursorLoader cursorLoader =
	 * new CursorLoader(getActivity(), selectedImageUri, projection, null, null,
	 * null); Cursor cursor = cursorLoader.loadInBackground(); int column_index
	 * = cursor .getColumnIndexOrThrow(MediaColumns.DATA); cursor.moveToFirst();
	 * String selectedImagePath = cursor.getString(column_index); Bitmap bm;
	 * BitmapFactory.Options options = new BitmapFactory.Options();
	 * options.inJustDecodeBounds = true;
	 * BitmapFactory.decodeFile(selectedImagePath, options); final int
	 * REQUIRED_SIZE = 200; int scale = 1; while (options.outWidth / scale / 2
	 * >= REQUIRED_SIZE && options.outHeight / scale / 2 >= REQUIRED_SIZE) scale
	 * *= 2; options.inSampleSize = scale; options.inJustDecodeBounds = false;
	 * bm = BitmapFactory.decodeFile(selectedImagePath, options);
	 * 
	 * photoHolder.setVisibility(View.VISIBLE); photoHolder.setImageBitmap(bm);
	 * } } }
	 */

	/*@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == REQUEST_CAMERA) {

				picUri = data.getData();
				Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
				ByteArrayOutputStream bytes = new ByteArrayOutputStream();
				thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
				File destination = new File(
						Environment.getExternalStorageDirectory(),
						System.currentTimeMillis() + ".jpg");
				//imagePath = destination.getAbsolutePath();
				FileOutputStream fo;
				try {
					destination.createNewFile();
					fo = new FileOutputStream(destination);
					fo.write(bytes.toByteArray());
					fo.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				//picUri = Uri.fromFile(destination);
				Log.e("PIC URI TAKE PIC", picUri + "");
				performCrop(); // ivImage.setImageBitmap(thumbnail);

				photoHolder.setVisibility(View.VISIBLE);
				photoHolder.setImageBitmap(thumbnail);
			}
			// adapter.notifyDataSetChanged();
		} else if (requestCode == SELECT_FILE) {

			Bundle bundle = data.getExtras();
			picUri = data.getData();
			//File auxFile = new File(picUri.toString());
			//imagePath = auxFile.getAbsolutePath();
			Log.e("PIC URI SELECT FILE", picUri + "");
			performCrop(); // user is returning from cropping the image
		} else if (requestCode == CROP_PIC) { // get the returned data
			Bundle extras = data.getExtras(); // get the cropped bitmap
			//Bitmap thePic = extras.getParcelable("data");
			
			bitmap = extras.getParcelable("data");
			
			//picUri = getImageUri(getActivity(), thePic);
			//File auxFile = new File(picUri.toString());
			//imagePath = auxFile.getAbsolutePath();
			photoHolder.setVisibility(View.VISIBLE);
			photoHolder.setImageBitmap(bitmap);
			Log.e("PIC URI AFTER CROP", picUri + "");
		}
	}*/
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == REQUEST_CAMERA) {
//				picUri = data.getData();
				Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
				ByteArrayOutputStream bytes = new ByteArrayOutputStream();
				thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
				File destination = new File(
						Environment.getExternalStorageDirectory(),
						System.currentTimeMillis() + ".jpg");
				FileOutputStream fo;
				try {
					destination.createNewFile();
					picUri = Uri.fromFile(destination);
					fo = new FileOutputStream(destination);
					fo.write(bytes.toByteArray());
					fo.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				// ivImage.setImageBitmap(thumbnail);
				performCrop();
				//photoHolder.setVisibility(View.VISIBLE);
				//photoHolder.setImageBitmap(thumbnail);
				//adapter.notifyDataSetChanged();
			} else if (requestCode == SELECT_FILE) {
				Uri selectedImageUri = data.getData();
				picUri = selectedImageUri;
				String[] projection = { MediaColumns.DATA };
				CursorLoader cursorLoader = new CursorLoader(getActivity(),
						selectedImageUri, projection, null, null, null);
				Cursor cursor = cursorLoader.loadInBackground();
				int column_index = cursor
						.getColumnIndexOrThrow(MediaColumns.DATA);
				cursor.moveToFirst();
				String selectedImagePath = cursor.getString(column_index);
				Log.e("test", "" + selectedImagePath);
				Bitmap bm = null;
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inJustDecodeBounds = true;
				BitmapFactory.decodeFile(selectedImagePath, options);
				final int REQUIRED_SIZE = 200;
				int scale = 1;
				while (options.outWidth / scale / 2 >= REQUIRED_SIZE
						&& options.outHeight / scale / 2 >= REQUIRED_SIZE)
					scale *= 2;
				options.inSampleSize = scale;
				options.inJustDecodeBounds = false;
				bm = BitmapFactory.decodeFile(selectedImagePath, options);
				// ivImage.setImageBitmap(bm);
				performCrop();
				//photoHolder.setVisibility(View.VISIBLE);
				//photoHolder.setImageBitmap(bm);
				//adapter.notifyDataSetChanged();
			} else if (requestCode == CROP_PIC) { // get the returned data
				Bundle extras = data.getExtras(); // get the cropped bitmap
				//Bitmap thePic = extras.getParcelable("data");
				
				bitmap = extras.getParcelable("data");
				
				//picUri = getImageUri(getActivity(), bitmap);
				//File auxFile = new File(picUri.toString());
				//imagePath = auxFile.getAbsolutePath();
				photoHolder.setVisibility(View.VISIBLE);
				photoHolder.setImageBitmap(bitmap);
				Log.e("PIC URI AFTER CROP", picUri + "");
			}
		}

	}
	

	private void performCrop() {
		// take care of exceptions
		try {
			// call the standard crop action intent (the user device may not
			// support it)
			Intent cropIntent = new Intent("com.android.camera.action.CROP");
			// indicate image type and Uri
			Log.e("PIC URI ON CROP", picUri + "");
			cropIntent.setDataAndType(picUri, "image/*");
			// set crop properties
			cropIntent.putExtra("crop", "true");
			// retrieve data on return
			cropIntent.putExtra("return-data", true);
			// start the activity - we handle returning in onActivityResult
			startActivityForResult(cropIntent, CROP_PIC);
		}
		// respond to users whose devices do not support the crop action
		catch (ActivityNotFoundException anfe) {
			Toast toast = Toast.makeText(getActivity(),
					"This device doesn't support the crop action!",
					Toast.LENGTH_SHORT);
			toast.show();
		}

	}
	
	public String getPath(Uri uri) {
		// just some safety built in
		if (uri == null) {
			// TODO perform some logging or show user feedback
			return null;
		}
		// try to retrieve the image from the media store first
		// this will only work for images selected from gallery
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = getActivity().managedQuery(uri, projection, null, null,
				null);
		if (cursor != null) {
			int column_index = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		}
		// this is our fallback here
		return uri.getPath();
	}

	public Uri getImageUri(Context inContext, Bitmap inImage) {
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
		String path = Images.Media.insertImage(inContext.getContentResolver(),
				inImage, "Title", null);
		return Uri.parse(path);
	}

	

	class AddDishProcess extends AsyncTask<String, Void, String> {

		String response = null;
String message=null;
String foodname;
		AddDishProcess() {
			Log.e("inside", "adddish async task");
		}

		@Override
		public void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Attempting login...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();

		}

		@Override
		public void onProgressUpdate(Void... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
		}

		public String doInBackground(String... params) {

			HttpURLConnection conn = null;
			DataOutputStream dos = null;
			String lineEnd = "\r\n";
			String twoHyphens = "--";
			String boundary = "*****";
			int bytesRead, bytesAvailable, bufferSize;
			byte[] buffer;
			int maxBufferSize = 1 * 1024 * 1024;
			int serverResponseCode = 0;

			String id = SharedPrefs.getUid(getActivity());
			String token = SharedPrefs.getToken(getActivity());
			// String id ="270";
			// String token = "ulBdWIaSjN";
			foodname = fshortName.getText().toString().trim();
			String description = fdescription.getText().toString().trim();
			String foodtype = selectfType.getText().toString().trim();
			String cuisinetype = selectcType.getText().toString().trim();
			String diettype = selectdType.getText().toString().trim();
			String price = pricePerOrder.getText().toString().trim();
			// String foodpic = "";
			String preorderdays = daysBeforePre.getText().toString().trim();
			String preorder_maxorder = maxOrderPC.getText().toString().trim();
			String start_time = availStarttime.getText().toString().trim();
			String end_time = availEndtime.getText().toString().trim();
			String todayspecial_order = maxOrder.getText().toString().trim();

			try {

				System.setProperty("http.keepAlive", "false");

				// open a URL connection to the Servlet
				//Log.e("PIC URI", "" + picUri);
				//bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
				//File file = new File(imagePath);
				//FileInputStream fileInputStream = new FileInputStream(file);
				// ByteArrayInputStream fileInputStream = new
				// ByteArrayInputStream(imageBytes);
				

				//encoding image into a byte array
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] imageBytes = baos.toByteArray();
                
                // open a URL connection to the Servlet
                ByteArrayInputStream fileInputStream = new ByteArrayInputStream(imageBytes);
                URL url = new URL(ADD_DISH_URL);
				
				
				// Open a HTTP connection to the URL
				conn = (HttpURLConnection) url.openConnection();
				conn.setDoInput(true); // Allow Inputs
				conn.setDoOutput(true); // Allow Outputs

				conn.setUseCaches(false); // Don't use a Cached Copy
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Accept-Encoding", "");
				// conn.setRequestProperty("Connection", "Keep-Alive");
				conn.setRequestProperty("ENCTYPE", "multipart/form-data");
				conn.setRequestProperty("Content-Type",
						"multipart/form-data;boundary=" + boundary);
				// conn.setRequestProperty("uploaded_file", "profile_picture");
				conn.setRequestProperty("id", id);
				conn.setRequestProperty("token", token);
				conn.setRequestProperty("foodname", foodname);
				conn.setRequestProperty("description", description);
				conn.setRequestProperty("foodtype", foodtype);
				conn.setRequestProperty("cuisinetype", cuisinetype);
				conn.setRequestProperty("diettype", diettype);
				conn.setRequestProperty("price", price);
				conn.setRequestProperty("foodpic", "foodpic");
				if (switch2.isChecked()) {
					conn.setRequestProperty("preorderdays", preorderdays);
					conn.setRequestProperty("preorder_maxorder",
							preorder_maxorder);
					conn.setRequestProperty("start_time", start_time);
					conn.setRequestProperty("end_time", end_time);
				}
				if (switch1.isChecked()) {
					conn.setRequestProperty("todayspecial_order",
							todayspecial_order);
				}
				dos = new DataOutputStream(conn.getOutputStream());

				dos.writeBytes(twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=\"id\""
						+ lineEnd + lineEnd + id + lineEnd);

				dos.writeBytes(twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=\"token\""
						+ lineEnd + lineEnd + token + lineEnd);

				dos.writeBytes(twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=\"foodname\""
						+ lineEnd + lineEnd + foodname + lineEnd);

				dos.writeBytes(twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=\"description\""
						+ lineEnd + lineEnd + description + lineEnd);

				dos.writeBytes(twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=\"foodtype\""
						+ lineEnd + lineEnd + foodtype + lineEnd);

				dos.writeBytes(twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=\"cuisinetype\""
						+ lineEnd + lineEnd + cuisinetype + lineEnd);

				dos.writeBytes(twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=\"diettype\""
						+ lineEnd + lineEnd + diettype + lineEnd);

				dos.writeBytes(twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=\"price\""
						+ lineEnd + lineEnd + price + lineEnd);

				if (switch2.isChecked()) {

					dos.writeBytes(twoHyphens + boundary + lineEnd);
					dos.writeBytes("Content-Disposition: form-data; name=\"preorderdays\""
							+ lineEnd + lineEnd + preorderdays + lineEnd);

					dos.writeBytes(twoHyphens + boundary + lineEnd);
					dos.writeBytes("Content-Disposition: form-data; name=\"preorder_maxorder\""
							+ lineEnd + lineEnd + preorder_maxorder + lineEnd);

					dos.writeBytes(twoHyphens + boundary + lineEnd);
					dos.writeBytes("Content-Disposition: form-data; name=\"start_time\""
							+ lineEnd + lineEnd + start_time + lineEnd);

					dos.writeBytes(twoHyphens + boundary + lineEnd);
					dos.writeBytes("Content-Disposition: form-data; name=\"end_time\""
							+ lineEnd + lineEnd + end_time + lineEnd);

				}

				if (switch1.isChecked()) {

					dos.writeBytes(twoHyphens + boundary + lineEnd);
					dos.writeBytes("Content-Disposition: form-data; name=\"todayspecial_order\""
							+ lineEnd + lineEnd + todayspecial_order + lineEnd);
				}

				// forth parameter - filename
				dos.writeBytes(twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=\"foodpic\";filename=\""
						+ "foodpic" + "\"" + lineEnd);

				
				
				dos.writeBytes(lineEnd);

				/*ByteArrayOutputStream stream = new ByteArrayOutputStream();
				bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
				stream.toByteArray();
				ByteArrayInputStream ipstream = new ByteArrayInputStream(stream.toByteArray());
				bytesAvailable = ipstream.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				buffer = new byte[bufferSize];
				bytesRead = ipstream.read(buffer, 0, bufferSize);
				while (bytesRead > 0) {
					dos.write(buffer, 0, bufferSize);
					bytesAvailable = ipstream.available();
					bufferSize = Math.min(bytesAvailable, maxBufferSize);
					bytesRead = ipstream.read(buffer, 0, bufferSize);
				}*/
				
				// create a buffer of maximum size
				bytesAvailable = fileInputStream.available();
				
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				buffer = new byte[bufferSize];
				
				// read file and write it into form...
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);
				
				while (bytesRead > 0) {
					dos.write(buffer, 0, bufferSize);
					bytesAvailable = fileInputStream.available();
					bufferSize = Math.min(bytesAvailable, maxBufferSize);
					bytesRead = fileInputStream.read(buffer, 0, bufferSize);
				}
				
				// send multipart form data necesssary after file data...
				dos.writeBytes(lineEnd);
				dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

				// Responses from the server (code and message)
				serverResponseCode = conn.getResponseCode();

				String serverResponseMessage = conn.getResponseMessage();

				Log.i("uploadFile", "HTTP Response is : "
						+ serverResponseMessage + ": " + serverResponseCode);

				// close the streams //
				fileInputStream.close();
				dos.flush();
				dos.close();

				try {
					InputStream inp = null;

					inp = new BufferedInputStream(conn.getInputStream());

					ServiceHandler sh = new ServiceHandler();

					response = sh.readStream(inp);
					// Toast.makeText(getApplicationContext(), json,
					// 1000).show();
				} finally {
					conn.disconnect();
				}

			} catch (MalformedURLException ex) {
				Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
			} catch (Exception e) {
				String err = (e.getMessage() == null) ? "SD Card failed" : e
						.getMessage();
				Log.e("The caught exception is: ", err);
			}
			
			try {
				JSONObject obj1=new JSONObject(response);
				message=obj1.getString("message");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return message;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			if (pDialog.isShowing()) {
				pDialog.dismiss();
			}
//			if (result != null) {
//				Toast.makeText(getActivity(), "the result is" + result,
//						Toast.LENGTH_SHORT).show();
//			}
			
			final Dialog dialog_changed = new Dialog(getActivity());
			dialog_changed.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog_changed.getWindow().setBackgroundDrawable(
					new ColorDrawable(0));
			dialog_changed.setContentView(R.layout.dialog_verification_signup);
			final TextView txt_content = (TextView) dialog_changed
					.findViewById(R.id.txt_content);
			ImageView img_tick=(ImageView)dialog_changed.findViewById(R.id.img_tick);
			
			if (result.equalsIgnoreCase("food added") ){
				txt_content.setText(foodname+" will be added successfully once approved by FoodFiestaa");
				dialog_changed.show();
			} else {
				Toast.makeText(getActivity(), "the message is" + message,Toast.LENGTH_SHORT).show();
			}

			Button btn_ok = (Button) dialog_changed.findViewById(R.id.btn_ok);
			btn_ok.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					dialog_changed.dismiss();
				}
			});

			
		}

	}

	public void verifyForm() {
		form.check(fshortName, RegexTemplate.NOT_EMPTY_PATTERN,
				"Must not be empty");
		form.check(fdescription, RegexTemplate.NOT_EMPTY_PATTERN,
				"Must not be empty");
		form.check(selectfType, RegexTemplate.NOT_EMPTY_PATTERN,
				"Please select a Food type");
		form.check(selectdType, RegexTemplate.NOT_EMPTY_PATTERN,
				"Please select a Diet type");
		form.check(selectcType, RegexTemplate.NOT_EMPTY_PATTERN,
				"Please select a Cuisine type");
		form.check(pricePerOrder, RegexTemplate.NOT_EMPTY_PATTERN,
				"Must not be empty");
		form.check(ffPrice, RegexTemplate.NOT_EMPTY_PATTERN,
				"Must not be empty");
	}

	public void checkSwitch1() {
		form.check(maxOrder, RegexTemplate.NOT_EMPTY_PATTERN,
				"Must not be empty");
		form.checkValue(maxOrder, Range.equalsOrMoreAndEqualsOrLess(1, 100),
				"Max Order must be 1 - 100");
	}

	public void checkSwitch2() {
		form.check(maxOrderPC, RegexTemplate.NOT_EMPTY_PATTERN,
				"Must not be empty");
		form.checkValue(maxOrderPC, Range.equalsOrMoreAndEqualsOrLess(1, 100),
				"Max Order Per Customer must be 1 - 100");
		form.check(daysBeforePre, RegexTemplate.NOT_EMPTY_PATTERN,
				"Please select the days before pre-order");
		form.check(availStarttime, RegexTemplate.NOT_EMPTY_PATTERN,
				"Please select the start time");
		form.check(maxOrderPC, RegexTemplate.NOT_EMPTY_PATTERN,
				"Please select the end time");
	}

}
