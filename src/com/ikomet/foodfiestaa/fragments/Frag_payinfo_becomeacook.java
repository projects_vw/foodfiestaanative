package com.ikomet.foodfiestaa.fragments;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import org.json.JSONObject;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.activities.CookSignIn;
import com.ikomet.foodfiestaa.activities.MainActivity;
import com.ikomet.foodfiestaa.beans.CookAddressInformation;
import com.ikomet.foodfiestaa.beans.CookGeneralInformation;
import com.ikomet.foodfiestaa.beans.CookPaymentInformation;
import com.ikomet.foodfiestaa.utils.DialogCustom;
import com.ikomet.foodfiestaa.utils.DialogVerification;
import com.ikomet.foodfiestaa.utils.InternetCheck;
import com.ikomet.foodfiestaa.utils.ReadStream;
import com.ikomet.foodfiestaa.utils.Validator;

public class Frag_payinfo_becomeacook extends Fragment implements
		OnCheckedChangeListener, View.OnClickListener {
	RadioGroup radiogroup;
	RadioButton bank, paypal;
	View view;
	Button btn_next_general;
	TextView txt_or;
	boolean validate_bankinfo, validate_paypal;

	EditText edt_ac_no, edt_ac_name, edt_branch_add, edt_ifsc_code,
			edt_acc_mail, edt_acc_phone;
	String accountNo, AccountName, branchAddress, Ifsc, paypalMail,
			paypalPhone;
	Validator validator;
	CookGeneralInformation generalInfo;
	CookAddressInformation addrInfo;
	CookPaymentInformation paymentInfo;
	ProgressDialog pDialog;

	public static final int PAYPAL = 0;
	public static final int BANK = 1;
	public static final int PAYPAL_AND_BANK = 2;
	
	DialogCustom dialog;

	public Frag_payinfo_becomeacook() {

	}

	public Frag_payinfo_becomeacook(CookGeneralInformation generalInfo,
			CookAddressInformation addrInfo) {
		this.generalInfo = generalInfo;
		this.addrInfo = addrInfo;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		view = inflater.inflate(R.layout.frag_payinfo_becomeacook, container,
				false);
		
		dialog = new DialogCustom();
		
		radiogroup = (RadioGroup) view.findViewById(R.id.radio_grp);
		bank = (RadioButton) view.findViewById(R.id.radio_button_bank);
		paypal = (RadioButton) view.findViewById(R.id.radio_button_paypal);
		initialize();

		radiogroup.setOnCheckedChangeListener(this);

		// btn_next_general.setEnabled(false);

		btn_next_general.setOnClickListener(this);

		validator = new Validator(getActivity().getApplicationContext());

		validate_bankinfo = validator.validateBankInfo(edt_ac_no, edt_ac_name,
				edt_branch_add, edt_ifsc_code);

		Log.e("test", "" + validate_bankinfo);
		validate_paypal = validator.validatePaypal(edt_acc_mail, edt_acc_phone);
		return view;
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		// TODO Auto-generated method stub
		switch (checkedId) {
		case R.id.radio_button_bank:
			Log.e("hai", "inside_bankview");
			View_Bank();
			break;
		case R.id.radio_button_paypal:
			Log.e("hai", "inside_paypalview");
			View_paypal();
			break;

		default:
			break;
		}
	}

	public void initialize() {
		edt_ac_no = (EditText) view.findViewById(R.id.edt_bank_ac_no);
		edt_ac_name = (EditText) view.findViewById(R.id.edt_ac_name);
		edt_branch_add = (EditText) view.findViewById(R.id.edt_branch_address);
		edt_ifsc_code = (EditText) view.findViewById(R.id.edt_ifsc_code);

		edt_acc_mail = (EditText) view.findViewById(R.id.edt_account_mail);
		edt_acc_phone = (EditText) view.findViewById(R.id.edt_account_phone);

		txt_or = (TextView) view.findViewById(R.id.txt_or);
		btn_next_general = (Button) view.findViewById(R.id.btn_next_general);

	}

	// to show the transition btw paypal and bank hiding views
	public void View_Bank() {
		edt_acc_mail.setVisibility(View.GONE);
		edt_acc_phone.setVisibility(View.GONE);
		edt_ac_no.setVisibility(View.VISIBLE);
		edt_ac_name.setVisibility(View.VISIBLE);
		edt_ifsc_code.setVisibility(View.VISIBLE);
		edt_branch_add.setVisibility(View.VISIBLE);
		txt_or.setVisibility(View.GONE);

	}

	public void View_paypal() {
		edt_acc_mail.setVisibility(View.VISIBLE);
		edt_acc_phone.setVisibility(View.VISIBLE);
		edt_ac_no.setVisibility(View.GONE);
		edt_ac_name.setVisibility(View.GONE);
		edt_ifsc_code.setVisibility(View.GONE);
		edt_branch_add.setVisibility(View.GONE);
		txt_or.setVisibility(View.VISIBLE);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		if (v.getId() == R.id.btn_next_general) {
		
			validate_bankinfo = validator.validateBankInfo(edt_ac_no,
					edt_ac_name, edt_branch_add, edt_ifsc_code);

			validate_paypal = validator.validatePaypal(edt_acc_mail,
					edt_acc_phone);

			
		if(!isPayPalOkay() && !isBankAccountOkay()){
			
			if(bank.isChecked()){
				validator.nullCheckBankDetails(edt_ac_no, edt_ac_name, edt_branch_add, edt_ifsc_code);
			}else{
				if(edt_acc_mail.getText().toString().equalsIgnoreCase("") && edt_acc_phone.getText().toString().equalsIgnoreCase("")){
					edt_acc_mail.setError("Please fill this field");
				}
			}
			
			
		}else{
		
			
			if (isPayPalOkay() && isBankAccountOkay()) {
				if(InternetCheck.isInternetConnected(getActivity())){
					new CheckData(PAYPAL_AND_BANK).execute();
				}else{
					dialog.showDialog(getActivity(), "Please connect to Internet", "OK", R.drawable.circle_uncheck);
				}
			} else if (isPayPalOkay() || isBankAccountOkay()) {

				if (isPayPalOkay()) {
					if(InternetCheck.isInternetConnected(getActivity())){
						new CheckData(PAYPAL).execute();
					}else{
						dialog.showDialog(getActivity(), "Please connect to Internet", "OK", R.drawable.circle_uncheck);
					}
				} else {
					if(InternetCheck.isInternetConnected(getActivity())){
						new CheckData(BANK).execute();
					}else{
						dialog.showDialog(getActivity(), "Please connect to Internet", "OK", R.drawable.circle_uncheck);
					}
				}

			} else {
				Toast.makeText(getActivity().getApplicationContext(), "Data not valid", Toast.LENGTH_SHORT).show();
			}

		}
	}
	}

	public boolean isPayPalOkay() {

		if (edt_acc_mail.getText().toString().trim().equalsIgnoreCase("")
				&& edt_acc_phone.getText().toString().trim()
						.equalsIgnoreCase("")) {

			return false;
		} else {
			if (edt_acc_mail.getText().toString().trim().equalsIgnoreCase("")
					|| !isValidEmail(edt_acc_mail.getText().toString().trim())) {
				if (edt_acc_phone.getText().toString().trim()
						.equalsIgnoreCase("")
						|| edt_acc_phone.getText().toString().trim().length() < 10) {
					return false;
				} else {
					return true;
				}
			} else {
				return true;
			}

		}

	}

	public boolean isBankAccountOkay() {

		if (edt_ac_no.getText().toString().trim().equalsIgnoreCase("")
				|| edt_ac_name.getText().toString().trim().equalsIgnoreCase("")
				|| edt_branch_add.getText().toString().equalsIgnoreCase("")
				|| edt_ifsc_code.getText().toString().equalsIgnoreCase("")) {
			return false;
		} else if (edt_ac_no.getText().toString().trim().length() < 15) {
			return false;
		} else if (edt_ifsc_code.getText().toString().length() < 7) {

			return false;
		} else {
			return true;
		}

	}

	public final static boolean isValidEmail(CharSequence target) {
		if (target == null) {
			return false;
		} else {
			return android.util.Patterns.EMAIL_ADDRESS.matcher(target)
					.matches();
		}
	}

	class CheckData extends AsyncTask<String, String, String> {

		int paypalorbank;

		CheckData(int paypalorbank) {
			this.paypalorbank = paypalorbank;
		}

		boolean status;
		String errors = "";
		String mail, passw, cnfmpass, fnm, lnm, mob, kitchen;
		String st1, st2, loc, city, state, country, pin;
		String bnk_acc_no, bnk_acc_name, branch_add, ifsc, paypal_mail,
				paypal_phone;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Registering User");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub
			// Check for success tag

			String json = "";

			URL url = null;
			try {

				url = new URL(Fragment_general_becomeacook.REG_URL);

				mail = generalInfo.getEmail();
				passw = generalInfo.getPassword();
				cnfmpass = generalInfo.getConfirm_password();
				fnm = generalInfo.getFname();
				lnm = generalInfo.getLname();
				mob = generalInfo.getMobile();
				kitchen = generalInfo.getKitchen_name();
								
				st1 = addrInfo.getStreet1();
				st2 = addrInfo.getStreet2();
				loc = addrInfo.getLocation();
				city = addrInfo.getCity();
				state = addrInfo.getState();
				country = addrInfo.getCountry();
				pin = addrInfo.getPincode();
				
				paypal_mail = edt_acc_mail.getText().toString().trim();
				paypal_phone = edt_acc_phone.getText().toString().trim();
				
				bnk_acc_no = edt_ac_no.getText().toString().trim();
				bnk_acc_name = edt_ac_name.getText().toString().trim();
				branch_add = edt_branch_add.getText().toString().trim();
				ifsc = edt_ifsc_code.getText().toString().trim();
				
				String param="";
				
//				email,password,password_confirmation,fname,lname,mobile,street,pincode,
//				location,kitchen,accountno,accountname,bank_addr,ifsc,paypal_email_or_phone
				
				param = "email=" + URLEncoder.encode(mail, "UTF-8")
						+ "&password=" + URLEncoder.encode(passw, "UTF-8")
						+ "&password_confirmation=" + URLEncoder.encode(cnfmpass, "UTF-8")
						+ "&fname=" + URLEncoder.encode(fnm, "UTF-8")
						+ "&lname=" + URLEncoder.encode(lnm, "UTF-8")
						+ "&mobile=" + URLEncoder.encode(mob, "UTF-8")
						+ "&street=" + URLEncoder.encode(st1, "UTF-8")
						+ "&pincode=" + URLEncoder.encode(pin, "UTF-8")
						+ "&location=" + URLEncoder.encode(loc, "UTF-8")
						+ "&kitchen=" + URLEncoder.encode(kitchen, "UTF-8");		
				
				if(paypalorbank == PAYPAL){
					// you need to encode ONLY the values of the parameters
					if(!paypal_mail.equalsIgnoreCase("")){
			
					param = param + "&paypal_email_or_phone=" + URLEncoder.encode(paypal_mail, "UTF-8");
				
					}else{
						
						param = param + "&paypal_email_or_phone=" + URLEncoder.encode(paypal_phone, "UTF-8");
												
					}
								
					}else if(paypalorbank == BANK){
						
						
						param = param
								+ "&accountno=" + URLEncoder.encode(bnk_acc_no, "UTF-8")
								+ "&accountname=" + URLEncoder.encode(bnk_acc_name, "UTF-8")
								+ "&bank_addr=" + URLEncoder.encode(branch_add, "UTF-8")
								+ "&ifsc=" + URLEncoder.encode(ifsc, "UTF-8");
								
					}else{
						
						
						param = param
								+ "&accountno=" + URLEncoder.encode(bnk_acc_no, "UTF-8")
								+ "&accountname=" + URLEncoder.encode(bnk_acc_name, "UTF-8")
								+ "&bank_addr=" + URLEncoder.encode(branch_add, "UTF-8")
								+ "&ifsc=" + URLEncoder.encode(ifsc, "UTF-8")
								+ "&paypal_email_or_phone=" + URLEncoder.encode(paypal_phone, "UTF-8");
						
					}
				
				HttpURLConnection urlConnection = null;

				urlConnection = (HttpURLConnection) url.openConnection();
				//add reuqest header
				//urlConnection.setRequestMethod("POST");
				urlConnection.setDoOutput(true);
//				urlConnection.setRequestProperty("User-Agent", USER_AGENT);
//				urlConnection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
												
				// Send post request
				urlConnection.setDoOutput(true);
				OutputStream wr = new BufferedOutputStream(urlConnection.getOutputStream());
				wr.write(param.getBytes());
				wr.flush();
				wr.close();

				int responseCode = urlConnection.getResponseCode();
				System.out.println("\nSending 'POST' request to URL : " + Fragment_general_becomeacook.REG_URL);
				System.out.println("Post parameters : " + param);
				System.out.println("Response Code : " + responseCode);
				
				try {
					InputStream in = null;

					in = new BufferedInputStream(urlConnection.getInputStream());
					
					ReadStream stream = new ReadStream();
					json = stream.readStream(in, url);
					// Toast.makeText(getApplicationContext(), json,
					// 1000).show();
				} finally {
					urlConnection.disconnect();
				}
				JSONObject jobj = new JSONObject(json);
				
				String msg = jobj.getString("message");
								
				return msg;
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;

		}

		protected void onPostExecute(String value) {
			// dismiss the dialog once product deleted
			if (pDialog.isShowing()) {
				pDialog.dismiss();
			}

			if (!value.equalsIgnoreCase("")) {
				
				DialogVerification verify=new DialogVerification(getActivity(),0 ,"A Verification mail sent successfully");
				verify.show();
				getActivity().finish();
				/*dialog = new Dialog(getActivity());
				dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(R.layout.dialog_verification_signup);
				Button dialogButton = (Button) dialog.findViewById(R.id.btn_ok);
				dialogButton.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog.dismiss();
						getActivity().finish();
					}
				});
				Toast.makeText(getActivity(), value, Toast.LENGTH_SHORT).show();*/

			} else {
				
				/*fragManager = getActivity().getSupportFragmentManager();
				transaction = fragManager.beginTransaction();
				frag = new Frag_address_becomeacook(generalInfo);
				transaction.replace(R.id.frag_becomeacook, frag);
				transaction.commit();*/

			}

		}

	}

}
