package com.ikomet.foodfiestaa.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.adapter.ExpandableListAdapterFaq;

public class FragFaqGeneral extends Fragment {

	ExpandableListView exp_list;
	List<String> header_name;
	List<String> child_element;
HashMap<String,String> child;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.frag_faq_general, container,
				false);
		exp_list = (ExpandableListView) view.findViewById(R.id.exp_general_faq);
		Initialize_List_Values();
		ExpandableListAdapterFaq adapter=new ExpandableListAdapterFaq(getActivity(), header_name,child);
		exp_list.setAdapter(adapter);
		
		return view;

	}

	public void Initialize_List_Values()
	{
		header_name=new ArrayList<>();
		header_name.add("What is Food Fiestaa?");
		header_name.add("How does it work?");
		header_name.add("Is this legal?");
		
		child_element=new ArrayList<>();
		child_element.add("Food Fiestaa is a market place platform for cooks to post their signature dish and consumers"+ 
		"to order the food via our website, application or services.The value we add for cooks is the opportunity to post their food items,"+
		"get an order and earn. For Consumers, we offer the chance to discover food experiences near their location.");
		child_element.add("Registered Consumers can order food via web or mobile application based on Today’ Special or Pre-Order Required. Once the order is place a notification is send to cooks who in turn accepts the order and notifies the Consumer to pick the order when ready.");
		child_element.add("Food Fiestaa is a sharing economy business and the food items on Food Fiestaa are not publicly available. They are only available through our website and our app. Our cooks must register in our website or via our mobile app and our consumers can search and order food in our website or via our app based on the Terms & Conditions of Food Fiestaa. (In other words, you couldn’t stumble " +
				"upon our offerings as you could with those from food vendors such as " +
				"restaurants or street carts.) Other sharing economy business, such as Uber, Airbnb and oyorooms," +
				" operate in much the same manner.It’s important to note that we are not a food facility but rather a food marketplace." +
				"(Think of us as the world’s largest food court.)");
 
		child=new HashMap<>();
		child.put(header_name.get(0), child_element.get(0));
		child.put(header_name.get(1), child_element.get(1));
		child.put(header_name.get(2), child_element.get(2));
		
	}
}
