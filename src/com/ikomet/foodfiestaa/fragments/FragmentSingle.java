package com.ikomet.foodfiestaa.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ikomet.foodfiestaa.R;



public class FragmentSingle extends Fragment{

	TextView txtTitle, txtDetail;
	String title, detail;
	
	public FragmentSingle(String title, String detail){
		this.title = title;
		this.detail = detail;
				
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			 ViewGroup container, Bundle savedInstanceState) {
		
		 View layout = inflater.inflate(R.layout.activity_static, container, false);
		
		 txtTitle = (TextView)layout.findViewById(R.id.txt_title);
		 txtDetail = (TextView)layout.findViewById(R.id.txt_detail);
		
		 txtTitle.setText(title);
		 txtDetail.setText(Html.fromHtml(detail));
		 
		 return layout;
		
	}

	
	
}
