package com.ikomet.foodfiestaa.fragments;

import java.io.UnsupportedEncodingException;

import java.net.URLEncoder;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.activities.CookActivity;
import com.ikomet.foodfiestaa.adapter.AdptrSignatureDishes;
import com.ikomet.foodfiestaa.beans.DishDetailBean;
import com.ikomet.foodfiestaa.interfaces.Toggle;
import com.ikomet.foodfiestaa.utils.InternetCheck;
import com.ikomet.foodfiestaa.utils.ServiceHandler;
import com.ikomet.foodfiestaa.utils.SharedPrefs;
import com.ikomet.foodfiestaa.utils.SpacesItemDecoration;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class Frag_signaturedishes extends Fragment implements Toggle {

	View view;

	LinearLayout lnr_profile_turn;
	RecyclerView recyclerview;
	Context context;
	AdptrSignatureDishes adapter;
	LinearLayout lnr_signdishes;
	RelativeLayout rel_nonet;
	String mid, fshortname, fdes, foodname, diettype, cuisientype, preorder,
			todayspecial, price, foodprice, fpic, status, updated_at,
			todayspecial_active, preorder_active, is_delete, preorder_maxorder,
			start_time, end_time, is_front_new_arrival, is_front_veg,
			is_front_non_veg, is_front;
	ArrayList<DishDetailBean> list_detail_bean;
	ImageView smiley;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		view = inflater
				.inflate(R.layout.frag_signaturedishes, container, false);
		rel_nonet = (RelativeLayout) view.findViewById(R.id.no_net);
		lnr_signdishes = (LinearLayout) view.findViewById(R.id.lnr_signdishes);
		smiley = (ImageView)view.findViewById(R.id.img_sad);

		if (InternetCheck.isInternetConnected(getActivity())) {
			rel_nonet.setVisibility(View.GONE);
			lnr_signdishes.setVisibility(View.VISIBLE);
			Log.e("network", "is there");
			variableInitialize();
		} else {
			rel_nonet.setVisibility(View.VISIBLE);
			lnr_signdishes.setVisibility(View.GONE);
			Log.e("network", "no network");
			rel_nonet.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (InternetCheck.isInternetConnected(getActivity())) {
						
						Animation a = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_anim);
						a.setDuration(1000);
						smiley.startAnimation(a);
						
						new Handler().postDelayed(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								lnr_signdishes.setVisibility(View.VISIBLE);
								rel_nonet.setVisibility(View.GONE);
								variableInitialize();
							}
						}, 2000);
												
						/*lnr_signdishes.setVisibility(View.VISIBLE);
						rel_nonet.setVisibility(View.GONE);
						variableInitialize();*/
					}
				}
			});

		}

		return view;
	}

	public void variableInitialize() {
		lnr_profile_turn = (LinearLayout) view
				.findViewById(R.id.lnr_profile_turn);
		context = getActivity();
		((CookActivity) context).toggle = this;

		recyclerview = (RecyclerView) view.findViewById(R.id.recyclerlist);
		recyclerview.setLayoutManager(new StaggeredGridLayoutManager(2,
				StaggeredGridLayoutManager.VERTICAL));

		SpacesItemDecoration decoration = new SpacesItemDecoration(20, 20);
		recyclerview.addItemDecoration(decoration);
		new asyn_mydishes().execute();
	}

	class asyn_mydishes extends AsyncTask<String, Void, String> {
		String id = SharedPrefs.getUid(getActivity()), token = SharedPrefs
				.getToken(getActivity());
		String data, result, status;

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			ServiceHandler handler = new ServiceHandler();

			try {
				data = "id=" + URLEncoder.encode(id, "UTF-8") + "&token="
						+ URLEncoder.encode(token, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			result = handler.makeServiceCall(
					"http://beta.foodfiestaa.com/mysignservice", 1, data);
			list_detail_bean = new ArrayList<>();

			if (result != null) {
				try {
					JSONObject obj1 = new JSONObject(result);
					status = obj1.getString("status");
					Log.i("test", status + "is");
					if (obj1.getBoolean("status")) {
						JSONObject obj2;
						JSONArray arr1 = obj1.getJSONArray("data");
						for (int i = 0; i < arr1.length(); i++) {
							obj2 = arr1.getJSONObject(i);
							mid = obj2.getString("mid");
							fshortname = obj2.getString("fshortname");
							fdes = obj2.getString("fdes");
							if(obj2.has("foodname")){
								foodname = obj2.getString("foodname");
							}else{
								foodname = "";
							}
							if(obj2.has("diettype")){
								diettype = obj2.getString("diettype");
							}else{
								diettype = "";
							}
							if(obj2.has("cuisientype")){
								cuisientype = obj2.getString("cuisientype");
							}else{
								cuisientype = "";
							}
							
							
							preorder = obj2.getString("preorder");
							todayspecial = obj2.getString("todayspecial");
							price = obj2.getString("price");
							foodprice = obj2.getString("foodprice");
							fpic = "http://beta.foodfiestaa.com/"
									+ obj2.getString("fpic");
							status = obj2.getString("status");
							updated_at = obj2.getString("updated_at");
							todayspecial_active = obj2
									.getString("todayspecial_active");
							preorder_active = obj2.getString("preorder_active");
							is_delete = obj2.getString("is_delete");
							preorder_maxorder = obj2
									.getString("preorder_maxorder");
							start_time = obj2.getString("start_time");
							end_time = obj2.getString("end_time");
							is_front_new_arrival = obj2
									.getString("is_front_new_arrival");
							is_front_veg = obj2.getString("is_front_veg");
							is_front_non_veg = obj2
									.getString("is_front_non_veg");
							is_front = obj2.getString("is_front");

							list_detail_bean.add(new DishDetailBean(mid,
									fshortname, fdes, foodname, diettype,
									cuisientype, preorder, todayspecial, price,
									foodprice, fpic, status, updated_at,
									todayspecial_active, preorder_active,
									is_delete, preorder_maxorder, start_time,
									end_time, is_front_new_arrival,
									is_front_veg, is_front_non_veg, is_front));

							Log.i("the mid is", mid);
						}
					} else {
						Log.e("Invalid", "invalid ID");
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {
				Log.e("all is ", "right");
			}

			Log.e("result is", result);
			return status;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			adapter = new AdptrSignatureDishes(getActivity(), list_detail_bean);
			recyclerview.setAdapter(adapter);

		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}

	}

	@Override
	public void ControlToggle(boolean value) {
		// TODO Auto-generated method stub
		if (value) {
			lnr_profile_turn.setVisibility(View.GONE);

		} else {
			lnr_profile_turn.setVisibility(View.VISIBLE);

		}
	}

	@Override
	public void EditDish_flnt_actionbar() {
		// TODO Auto-generated method stub
		
	}
}
