package com.ikomet.foodfiestaa.fragments;

import java.io.UnsupportedEncodingException;

import java.net.URLEncoder;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.beans.DishDetailBean;
import com.ikomet.foodfiestaa.fragments.FragmentAddDish.AddDishProcess;
import com.ikomet.foodfiestaa.activities.CookActivity;
import com.ikomet.foodfiestaa.interfaces.FragmentCommunicator;
import com.ikomet.foodfiestaa.interfaces.FragmentCommunicatorNew;
import com.ikomet.foodfiestaa.interfaces.Toggle;
import com.ikomet.foodfiestaa.utils.DialogCustom;
import com.ikomet.foodfiestaa.utils.InternetCheck;
import com.ikomet.foodfiestaa.utils.ServiceHandler;
import com.ikomet.foodfiestaa.utils.SharedPrefs;
import com.squareup.picasso.Picasso;
import com.tmxlr.lib.driodvalidatorlight.Form;
import com.tmxlr.lib.driodvalidatorlight.helper.Range;
import com.tmxlr.lib.driodvalidatorlight.helper.RegexTemplate;

public class FragmentEditDish extends Fragment implements FragmentCommunicatorNew{

	Switch switch_tdy, switch_pre;
	ImageView photoHolder;
	EditText fshortName, fdescription, maxOrder, maxOrderPC, pricePerOrder,
			ffPrice;
	EditText selectfType, selectdType, selectcType, daysBeforePre,
			availStarttime, availEndtime;
	View view3;
	Button upload, editDish;
	Toggle toggle;
	
	RelativeLayout relSnack;
	public final String EDIT_DISH_URL = "http://beta.foodfiestaa.com/editdishservice";
	Form form;
	ProgressDialog pDialog;
	Context context;
	DishDetailBean dish;
	View layout;
	public final String TODAYS_SERVICE = "http://beta.foodfiestaa.com/todayservice";
	public final String PRE_ORDER_SERVICE = "http://beta.foodfiestaa.com/preorderservice";
	public final String DELETE_DISH_URL = "http://beta.foodfiestaa.com/deletedishservice";
	FragmentCommunicator ccommunicator;
	Dialog dialog_confirm;
	DialogCustom dialog;
	
	@Override
	public void onAttach(Context context) {
		// TODO Auto-generated method stub
		super.onAttach(context);
		ccommunicator = (FragmentCommunicator)getActivity();
		
	}
	public FragmentEditDish() {
	
	}
	
	public FragmentEditDish(Context context, DishDetailBean dish) {
		this.context = context;
		this.dish = dish;
		toggle=(Toggle)context;
		toggle.EditDish_flnt_actionbar();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		layout = inflater.inflate(R.layout.frag_edit_dish, container, false);
		dialog = new DialogCustom();
		ccommunicator.passDataToFragment();
		((CookActivity)getActivity()).communicator = this;
		photoHolder = (ImageView) layout.findViewById(R.id.img_photo);
		fshortName = (EditText) layout.findViewById(R.id.ed_fshort_name);
		fdescription = (EditText) layout.findViewById(R.id.ed_fdescription);
		selectfType = (EditText) layout.findViewById(R.id.ed_select_ftype);
		selectdType = (EditText) layout.findViewById(R.id.ed_select_dtype);
		selectcType = (EditText) layout.findViewById(R.id.ed_select_ctype);
		switch_tdy = (Switch) layout.findViewById(R.id.switch1);
		maxOrder = (EditText) layout.findViewById(R.id.ed_max_order);
		view3 = (View) layout.findViewById(R.id.view3);
		switch_pre = (Switch) layout.findViewById(R.id.switch2);
		maxOrderPC = (EditText) layout.findViewById(R.id.max_order_per_cons);
		daysBeforePre = (EditText) layout.findViewById(R.id.sel_days_bef_pre);
		availStarttime = (EditText)layout. findViewById(R.id.sel_av_starttime);
		availEndtime = (EditText) layout.findViewById(R.id.sel_av_endtime);
		pricePerOrder = (EditText) layout.findViewById(R.id.ed_price_perorder);
		ffPrice = (EditText) layout.findViewById(R.id.ed_ff_price);
		editDish = (Button) layout.findViewById(R.id.bt_add);

		relSnack = (RelativeLayout) layout.findViewById(R.id.rel_snack);

		Picasso.with(getActivity()).load(dish.getFpic())
		    .placeholder(R.drawable.two)
		    .error(R.drawable.two)
		    .into(photoHolder);
		fshortName.setText(dish.getFshortname());
		fdescription.setText(dish.getFdes());
		selectfType.setText(dish.getFoodname());
		selectdType.setText(dish.getDiettype());
		selectcType.setText(dish.getCuisientype());
		
		form = new Form(getActivity());
		verifyForm();
		
		int today = Integer.parseInt(dish.getTodayspecial_active());
		if(today==1){
			switch_tdy.setChecked(true);
			maxOrder.setText(dish.getTodayspecial());
			maxOrder.setVisibility(View.VISIBLE);
			form.clear();
			checkSwitch1();
		}else{
			switch_tdy.setChecked(false);
		}
		
		int pre = Integer.parseInt(dish.getPreorder_active());
		if(pre == 1){
			switch_pre.setChecked(true);
			maxOrderPC.setText(dish.getPreorder_maxorder());
			daysBeforePre.setText(dish.getPreorder());
			availStarttime.setText(dish.getStart_time());
			availEndtime.setText(dish.getEnd_time());
			maxOrderPC.setVisibility(View.VISIBLE);
			daysBeforePre.setVisibility(View.VISIBLE);
			availStarttime.setVisibility(View.VISIBLE);
			availEndtime.setVisibility(View.VISIBLE);
			form.clear();
			checkSwitch2();
		}else{
			switch_pre.setChecked(false);
		}
		
		if(today==1 && pre==1){
			form.clear();
			checkSwitch1();
			checkSwitch2();
		}
		
		verifyForm();
		
		pricePerOrder.setText(dish.getPrice());
		if (!pricePerOrder.getText().toString().trim()
				.equalsIgnoreCase("")) {
			int val = Integer.parseInt(pricePerOrder.getText().toString().trim());
			double ffprice = (val*0.35);
			ffPrice.setText(String.valueOf(val+ffprice));
		}
		
		editDish.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if (switch_tdy.isChecked()) {
					checkSwitch1();
				} else {
					form.clear();
					if (switch_pre.isChecked()) {
						checkSwitch2();
					}
					verifyForm();
				}

				if (switch_pre.isChecked()) {
					checkSwitch2();
				} else {
					form.clear();
					if (switch_tdy.isChecked()) {
						checkSwitch1();
					}
					verifyForm();
				}

				if (form.validate()) {

					
					if (!switch_tdy.isChecked() && !switch_pre.isChecked()) {
						Snackbar snackbar = Snackbar.make(relSnack,
								"Please enable Today's Special or Pre-order",
								Snackbar.LENGTH_LONG);
						snackbar.show();
					} else {
						if(InternetCheck.isInternetConnected(getActivity())){
							new EditDish().execute();
						}else{
							dialog.showDialog(getActivity(), "Please connect to Internet", "OK", R.drawable.circle_uncheck);
						}
					}				

				} else {
					Snackbar snackbar = Snackbar.make(relSnack,
							"Please fill all the fields", Snackbar.LENGTH_LONG);
					snackbar.show();
				}
			
			}
		});
		
		selectfType.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPopupfType();
			}
		});

		selectdType.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPopupdType();
			}
		});

		selectcType.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPopupcType();
			}
		});

		daysBeforePre.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPopdaysbefPre();
			}
		});

		availStarttime.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPopupStartTime();
			}
		});

		availEndtime.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPopupEndTime();
			}
		});

		switch_tdy.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				if (isChecked) {
					maxOrder.setVisibility(View.VISIBLE);
					view3.setVisibility(View.VISIBLE);
				} else {
					maxOrder.setVisibility(View.GONE);
					view3.setVisibility(View.GONE);
				}
			}
		});

		switch_pre.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				if (isChecked) {
					maxOrderPC.setVisibility(View.VISIBLE);
					daysBeforePre.setVisibility(View.VISIBLE);
					availStarttime.setVisibility(View.VISIBLE);
					availEndtime.setVisibility(View.VISIBLE);
				} else {
					maxOrderPC.setVisibility(View.GONE);
					daysBeforePre.setVisibility(View.GONE);
					availStarttime.setVisibility(View.GONE);
					availEndtime.setVisibility(View.GONE);
				}
			}
		});

		pricePerOrder.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				if (!pricePerOrder.getText().toString().trim()
						.equalsIgnoreCase("")) {
					int val = Integer.parseInt(pricePerOrder.getText().toString().trim());
					double ffprice = (val*0.35);
					ffPrice.setText(String.valueOf(val+ffprice));
				}

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		
		return layout;
	}

	public void showPopupfType() {
		final String[] foodTypes = { "Breakfast", "Lunch", "Sweets & Snacks",
				"Dinner", "Desserts", "Drinks", "Combo", "Bakery",
				"ServedAllDay" };
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Select Food Type");
		builder.setItems(foodTypes, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				// Do something with the selection
				selectfType.setTextColor(Color.BLACK);
				selectfType.setText(foodTypes[item]);
				dialog.dismiss();
			}
		});

		AlertDialog alert = builder.create();
		alert.show();

	}

	public void showPopupdType() {
		final String[] dietTypes = { "Vegetarian", "Non-Vegetarian" };
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Select Diet Type");
		builder.setItems(dietTypes, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				// Do something with the selection
				selectdType.setTextColor(Color.BLACK);
				selectdType.setText(dietTypes[item]);
				dialog.dismiss();
			}
		});

		AlertDialog alert = builder.create();
		alert.show();

	}

	public void showPopupcType() {
		final String[] cuisineTypes = { "Indian", "Chinese", "Thai", "French",
				"Italian", "Mexican", "Japanese", "Greek", "Lebanese", "Malay",
				"American", "Others" };
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Select Cuisine Type");
		builder.setItems(cuisineTypes, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				// Do something with the selection
				selectcType.setTextColor(Color.BLACK);
				selectcType.setText(cuisineTypes[item]);
				dialog.dismiss();
			}
		});

		AlertDialog alert = builder.create();
		alert.show();

	}

	public void showPopdaysbefPre() {
		final String[] days = { "1" };
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Days Before Pre-order");
		builder.setItems(days, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				// Do something with the selection
				daysBeforePre.setTextColor(Color.BLACK);
				daysBeforePre.setText(days[item]);
				dialog.dismiss();
			}
		});

		AlertDialog alert = builder.create();
		alert.show();

	}

	public void showPopupStartTime() {
		final String[] time = { "8:00 AM", "8:30 AM", "9:00 AM", "9:30 AM",
				"10:00 AM", "10:30 AM", "11:00 AM", "11:30 AM", "12:00 PM",
				"12:30 PM", "1:00 PM", "1:30 PM", "2:00 PM", "2:30 PM",
				"3:00 PM", "3:30 PM", "4:00 PM", "4:30 PM", "5:00 PM",
				"5:30 PM", "6:00 PM", "6:30 PM", "7:00 PM", "7:30 PM",
				"8:00 PM", "8:30 PM", "9:00 PM", "9:30 PM" };
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Available Start Time");
		builder.setItems(time, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				// Do something with the selection
				availStarttime.setTextColor(Color.BLACK);
				availStarttime.setText(time[item]);
				dialog.dismiss();
			}
		});

		AlertDialog alert = builder.create();
		alert.show();

	}

	public void showPopupEndTime() {
		final String[] time = { "8:00 AM", "8:30 AM", "9:00 AM", "9:30 AM",
				"10:00 AM", "10:30 AM", "11:00 AM", "11:30 AM", "12:00 PM",
				"12:30 PM", "1:00 PM", "1:30 PM", "2:00 PM", "2:30 PM",
				"3:00 PM", "3:30 PM", "4:00 PM", "4:30 PM", "5:00 PM",
				"5:30 PM", "6:00 PM", "6:30 PM", "7:00 PM", "7:30 PM",
				"8:00 PM", "8:30 PM", "9:00 PM", "9:30 PM" };
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Available End Time");
		builder.setItems(time, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				// Do something with the selection
				availEndtime.setTextColor(Color.BLACK);
				availEndtime.setText(time[item]);
				dialog.dismiss();
			}
		});

		AlertDialog alert = builder.create();
		alert.show();

	}

	class EditDish extends AsyncTask<String, String, String> {

		boolean status;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Please Wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub
			// Check for success tag

			String json = "";

			try {

				String id, token, foodtype, cuisinetype, diettype, price,
				preorderdays, preorder_maxorder, start_time, end_time, todayspecial_order;

				id = SharedPrefs.getUid(getActivity());
				token = SharedPrefs.getToken(getActivity());
				foodtype = selectfType.getText().toString().trim();
				cuisinetype = selectcType.getText().toString().trim();
				diettype = selectdType.getText().toString().trim();
				todayspecial_order = maxOrder.getText().toString().trim();
				preorder_maxorder = maxOrderPC.getText().toString().trim();
				preorderdays = daysBeforePre.getText().toString().trim();
				start_time = availStarttime.getText().toString().trim();
				end_time = availEndtime.getText().toString().trim();
				price = pricePerOrder.getText().toString().trim();
				
				int today = Integer.parseInt(dish.getTodayspecial_active());
				String tdy_param = "";
				String pre_param=null;
				if(today==1){
					if(!switch_tdy.isChecked()){
//						new EnableDisableSwitches().execute(TODAYS_SERVICE);
						tdy_param = "id=" + URLEncoder.encode(SharedPrefs.getUid(getActivity()), "UTF-8")
								+ "&token=" + URLEncoder.encode(SharedPrefs.getToken(getActivity()), "UTF-8")
								+ "&menuid=" + URLEncoder.encode(dish.getMid(), "UTF-8")
								+ "&todayspecial_flag=0";
					
						ServiceHandler sh = new ServiceHandler();
						json = sh.makeServiceCall(TODAYS_SERVICE, ServiceHandler.GET,
								tdy_param);

						JSONObject jsonObj = new JSONObject(json);

						status = jsonObj.getBoolean("status");
						if(!status){
							return "false";
						}
						
					}
				}else{
					if(switch_tdy.isChecked()){
//						new EnableDisableSwitches().execute(TODAYS_SERVICE);
						tdy_param = "id=" + URLEncoder.encode(SharedPrefs.getUid(getActivity()), "UTF-8")
								+ "&token=" + URLEncoder.encode(SharedPrefs.getToken(getActivity()), "UTF-8")
								+ "&menuid=" + URLEncoder.encode(dish.getMid(), "UTF-8")
								+ "&todayspecial_flag=1";
						
						ServiceHandler sh = new ServiceHandler();
						json = sh.makeServiceCall(TODAYS_SERVICE, ServiceHandler.GET,
								tdy_param);

						JSONObject jsonObj = new JSONObject(json);

						status = jsonObj.getBoolean("status");
						if(!status){
							return "false";
						}
						
					}
				}
								
				int pre_order = Integer.parseInt(dish.getTodayspecial_active());
				if(pre_order==1){
					if(!switch_pre.isChecked()){
					//	new EnableDisableSwitches().execute(PRE_ORDER_SERVICE);
						pre_param="id=" + URLEncoder.encode(SharedPrefs.getUid(getActivity()), "UTF-8")
								+ "&token=" + URLEncoder.encode(SharedPrefs.getToken(getActivity()), "UTF-8")
								+ "&menuid=" + URLEncoder.encode(dish.getMid(), "UTF-8")
								+ "&preorder_flag=0";
						
						ServiceHandler sh1 = new ServiceHandler();
						json = sh1.makeServiceCall(PRE_ORDER_SERVICE, ServiceHandler.GET,
								pre_param);

						JSONObject jsonObj1 = new JSONObject(json);

						status = jsonObj1.getBoolean("status");
						if(!status){
							return "false";
						}
						
					}
				}else{
					if(switch_pre.isChecked()){
					//	new EnableDisableSwitches().execute(PRE_ORDER_SERVICE);
						pre_param="id=" + URLEncoder.encode(SharedPrefs.getUid(getActivity()), "UTF-8")
								+ "&token=" + URLEncoder.encode(SharedPrefs.getToken(getActivity()), "UTF-8")
								+ "&menuid=" + URLEncoder.encode(dish.getMid(), "UTF-8")
								+ "&preorder_flag=1";
						
						ServiceHandler sh1 = new ServiceHandler();
						json = sh1.makeServiceCall(PRE_ORDER_SERVICE, ServiceHandler.GET,
								pre_param);

						JSONObject jsonObj1 = new JSONObject(json);

						status = jsonObj1.getBoolean("status");
						if(!status){
							return "false";
						}
						
					}
					
				}
								
				String param = null;
							
				if(switch_tdy.isChecked() && switch_pre.isChecked()){
					param = "id=" + URLEncoder.encode(id, "UTF-8")
							+ "&token=" + URLEncoder.encode(token, "UTF-8")
							+ "&menuid=" + URLEncoder.encode(dish.getMid(), "UTF-8")
							+ "&foodtype=" + URLEncoder.encode(foodtype, "UTF-8")
							+ "&cuisinetype=" + URLEncoder.encode(cuisinetype, "UTF-8")
							+ "&diettype=" + URLEncoder.encode(diettype, "UTF-8")
							+ "&todayspecial_order=" + URLEncoder.encode(todayspecial_order, "UTF-8")
							+ "&preorder_maxorder="	+ URLEncoder.encode(preorder_maxorder, "UTF-8") 
							+ "&preorderdays=" + URLEncoder.encode(preorderdays, "UTF-8")
							+ "&start_time=" + URLEncoder.encode(start_time, "UTF-8") 
							+ "&end_time=" + URLEncoder.encode(end_time, "UTF-8")
							+ "&price=" + URLEncoder.encode(price, "UTF-8");
				}else if(switch_tdy.isChecked()){
					param = "id=" + URLEncoder.encode(id, "UTF-8")
							+ "&token=" + URLEncoder.encode(token, "UTF-8")
							+ "&menuid=" + URLEncoder.encode(dish.getMid(), "UTF-8")
							+ "&foodtype=" + URLEncoder.encode(foodtype, "UTF-8")
							+ "&cuisinetype=" + URLEncoder.encode(cuisinetype, "UTF-8")
							+ "&diettype=" + URLEncoder.encode(diettype, "UTF-8")
							+ "&todayspecial_order=" + URLEncoder.encode(todayspecial_order, "UTF-8")
							+ "&price=" + URLEncoder.encode(price, "UTF-8");
				}else{
					param = "id=" + URLEncoder.encode(id, "UTF-8")
							+ "&token=" + URLEncoder.encode(token, "UTF-8")
							+ "&menuid=" + URLEncoder.encode(dish.getMid(), "UTF-8")
							+ "&foodtype=" + URLEncoder.encode(foodtype, "UTF-8")
							+ "&cuisinetype=" + URLEncoder.encode(cuisinetype, "UTF-8")
							+ "&diettype=" + URLEncoder.encode(diettype, "UTF-8")
							+ "&preorder_maxorder="	+ URLEncoder.encode(preorder_maxorder, "UTF-8") 
							+ "&preorderdays=" + URLEncoder.encode(preorderdays, "UTF-8")
							+ "&start_time=" + URLEncoder.encode(start_time, "UTF-8") 
							+ "&end_time=" + URLEncoder.encode(end_time, "UTF-8")
							+ "&price=" + URLEncoder.encode(price, "UTF-8");
				}
				
				ServiceHandler sh2 = new ServiceHandler();
				json = sh2.makeServiceCall(EDIT_DISH_URL, ServiceHandler.POST,
						param);

				JSONObject jsonObj2 = new JSONObject(json);

				status = jsonObj2.getBoolean("status");

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (status) {

				return "success";
			} else {
				return "failure";

			}

		}

		
		protected void onPostExecute(String value) {
			// dismiss the dialog once product deleted
			if (pDialog.isShowing()) {
				pDialog.dismiss();
			}
			if(value.equalsIgnoreCase("false")){
				Toast.makeText(getActivity(), "Problem editting dish",
						Toast.LENGTH_SHORT).show();
			}

			if (value.equalsIgnoreCase("success")) {

				Toast.makeText(getActivity(), "Succes11111",
						Toast.LENGTH_SHORT).show();
				Fragment frag = new Frag_signaturedishes();
				FragmentManager fm = getActivity().getSupportFragmentManager();
				FragmentTransaction ft = fm.beginTransaction();
				ft.replace(R.id.realtabcontent, frag);
				ft.commit();

			} else if (value.equalsIgnoreCase("failure")) {
				Toast.makeText(getActivity(), "Problem1111",
						Toast.LENGTH_SHORT).show();

			}

		}

	}

	class DeleteDish extends AsyncTask<String, String, String> {

		boolean status;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog_confirm.dismiss();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Please Wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub
			// Check for success tag

			String json = "";

			try {

				String id, token;

				id = SharedPrefs.getUid(getActivity());
				token = SharedPrefs.getToken(getActivity());
				
				String param = "id=" + URLEncoder.encode(id, "UTF-8")
							+ "&token=" + URLEncoder.encode(token, "UTF-8")
							+ "&menuid=" + URLEncoder.encode(dish.getMid(), "UTF-8");
							
				
				ServiceHandler sh = new ServiceHandler();
				json = sh.makeServiceCall(DELETE_DISH_URL, ServiceHandler.POST,
						param);

				JSONObject jsonObj = new JSONObject(json);

				status = jsonObj.getBoolean("status");

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (status) {

				return "success";
			} else {
				return "failure";

			}

		}

		protected void onPostExecute(String value) {
			// dismiss the dialog once product deleted
			if (pDialog.isShowing()) {
				pDialog.dismiss();
			}

			if (value.equalsIgnoreCase("success")) {

				Toast.makeText(getActivity(), "Success",
						Toast.LENGTH_SHORT).show();
				Fragment frag = new Frag_signaturedishes();
				FragmentManager fm = getActivity().getSupportFragmentManager();
				FragmentTransaction ft = fm.beginTransaction();
				ft.replace(R.id.realtabcontent, frag);
				ft.commit();
				

			} else if (value.equalsIgnoreCase("failure")) {
				Toast.makeText(getActivity(), "Problem",
						Toast.LENGTH_SHORT).show();

			}

		}

	}

	public void verifyForm() {
		form.check(fshortName, RegexTemplate.NOT_EMPTY_PATTERN,
				"Must not be empty");
		form.check(fdescription, RegexTemplate.NOT_EMPTY_PATTERN,
				"Must not be empty");
		form.check(selectfType, RegexTemplate.NOT_EMPTY_PATTERN,
				"Please select a Food type");
		form.check(selectdType, RegexTemplate.NOT_EMPTY_PATTERN,
				"Please select a Diet type");
		form.check(selectcType, RegexTemplate.NOT_EMPTY_PATTERN,
				"Please select a Cuisine type");
		form.check(pricePerOrder, RegexTemplate.NOT_EMPTY_PATTERN,
				"Must not be empty");
		form.check(ffPrice, RegexTemplate.NOT_EMPTY_PATTERN,
				"Must not be empty");
	}

	public void checkSwitch1() {
		form.check(maxOrder, RegexTemplate.NOT_EMPTY_PATTERN,
				"Must not be empty");
		form.checkValue(maxOrder, Range.equalsOrMoreAndEqualsOrLess(1, 100),
				"Max Order must be 1 - 100");
	}

	public void checkSwitch2() {
		form.check(maxOrderPC, RegexTemplate.NOT_EMPTY_PATTERN,
				"Must not be empty");
		form.checkValue(maxOrderPC, Range.equalsOrMoreAndEqualsOrLess(1, 100),
				"Max Order Per Customer must be 1 - 100");
		form.check(daysBeforePre, RegexTemplate.NOT_EMPTY_PATTERN,
				"Please select the days before pre-order");
		form.check(availStarttime, RegexTemplate.NOT_EMPTY_PATTERN,
				"Please select the start time");
		form.check(maxOrderPC, RegexTemplate.NOT_EMPTY_PATTERN,
				"Please select the end time");
	}
	
	
	@Override
	public void passnewDataToFragment(String val1, String val2) {
		// TODO Auto-generated method stub
		dialog_confirm = new Dialog(getActivity());
		dialog_confirm.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog_confirm.getWindow().setBackgroundDrawable(
				new ColorDrawable(0));
		dialog_confirm.setContentView(R.layout.dialog_forgotpassword);
		
		TextView txtTitle = (TextView) dialog_confirm.findViewById(R.id.txt_title);
		ImageView imgTitle = (ImageView) dialog_confirm.findViewById(R.id.img_title);
		EditText email = (EditText)dialog_confirm.findViewById(R.id.edt_email);
		Button ok = (Button)dialog_confirm.findViewById(R.id.btn_submit);
		Button cancel = (Button)dialog_confirm.findViewById(R.id.btn_cancel);
		
		email.setVisibility(View.GONE);
		imgTitle.setImageResource(R.drawable.delete_icon);
		txtTitle.setText("Are you sure you want to delete this dish?");
		ok.setText("Okay");
		
		ok.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new DeleteDish().execute();
			}
		});

		cancel.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog_confirm.dismiss();
			}
		});
		dialog_confirm.setCancelable(false);
		dialog_confirm.show();
	}
	
	@Override
	public void checkedData(HashMap<String, Integer> hashmap) {
		// TODO Auto-generated method stub
		
	}
	}



	

