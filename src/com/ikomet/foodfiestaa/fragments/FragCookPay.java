package com.ikomet.foodfiestaa.fragments;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.utils.ServiceHandler;
import com.ikomet.foodfiestaa.utils.SharedPrefs;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class FragCookPay extends Fragment implements OnClickListener {

	View view;
	RelativeLayout todaysSpl, preOrder, snacklayout;
	ExpandableListView exp_order;
	ImageView imgTodaysSpl, imgPreOrder;
	TextView txtTodaysSpl, txtPreOrder;
	ProgressDialog pDialog;
	String PAY_HISTORY_COOK = "http://beta.foodfiestaa.com/payhistoryservice";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		view = inflater.inflate(R.layout.frg_cook_orders, container, false);
		initialize();

		return view;

	}

	public void initialize() {
		snacklayout = (RelativeLayout) view.findViewById(R.id.snack_order);

		todaysSpl = (RelativeLayout) view.findViewById(R.id.rel_todaysspl);
		preOrder = (RelativeLayout) view.findViewById(R.id.rel_pre_order);

		imgTodaysSpl = (ImageView) view.findViewById(R.id.img_todaysspl);
		imgPreOrder = (ImageView) view.findViewById(R.id.img_pre_order);

		txtTodaysSpl = (TextView) view.findViewById(R.id.txt_todaysspl);
		txtPreOrder = (TextView) view.findViewById(R.id.txt_pre_order);
		todaysSpl.setOnClickListener(this);
		preOrder.setOnClickListener(this);
	}

	public class AsyncPayHistoryCook extends AsyncTask<Void, Void, String> {
		String param = "", response = "",order_id,order_number,status,order_date,order_time,delivery_date,delivery_time,order_type,consumer_name,txnid,total_price;
		ServiceHandler handler;
		boolean Status;
		String item_name,qty,avaliable_order,price,oid;

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pDialog.dismiss();
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("please wait...");
			pDialog.show();
		}

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			String id = SharedPrefs.getUid(getActivity());
			String token = SharedPrefs.getToken(getActivity());
			handler = new ServiceHandler();
			try {
				param = "id=" + URLEncoder.encode(id, "UTF-8") + "&token="
						+ URLEncoder.encode(token, "UTF-8");
				response = handler.makeServiceCall(PAY_HISTORY_COOK, 2, param);
				Log.e("the response is", response);
				/*
				{
				    "status": "true",
				    "order": [
				        {
				            "order_id": 274,
				            "order_number": "ON00274",
				            "status": "Paid by Consumer",
				            "order_date": "03/05/2016",
				            "order_time": "03:30 pm",
				            "delivery_date": "2016-05-03 15:30:29",
				            "delivery_time": "within an hour",
				            "order_type": "todayspecial",
				            "consumer_name": "tuhfd",
				            "txnid": "dafa1105333a7133623d",
				            "total_price": 438.75,
				            "order_items": [
				                {
				                    "item_name": "Dosa",
				                    "qty": 2,
				                    "avaliable_order": 5,
				                    "price": 135,
				                    "oid": "ON00274"
				                },
				                {
				                    "item_name": "Idly",
				                    "qty": 2,
				                    "avaliable_order": 8,
				                    "price": 13.5,
				                    "oid": "ON00274"
				                },
				                {
				                    "item_name": "Pongal",
				                    "qty": 3,
				                    "avaliable_order": 67,
				                    "price": 47.25,
				                    "oid": "ON00274"
				                }
				            ]
				        },
				        {
				            "order_id": 266,
				            "order_number": "ON00266",
				            "status": "Paid by Consumer",
				            "order_date": "28/04/2016",
				            "order_time": "07:33 pm",
				            "delivery_date": "2016-04-28 19:33:16",
				            "delivery_time": "within an hour",
				            "order_type": "todayspecial",
				            "consumer_name": "Vijay",
				            "txnid": "3f381f1c1ffc46f1dc9f",
				            "total_price": 405,
				            "order_items": [
				                {
				                    "item_name": "Dosa",
				                    "qty": 3,
				                    "avaliable_order": 7,
				                    "price": 135,
				                    "oid": "ON00266"
				                }
				            ]
				        }
				    ]
				}*/
				try {
					JSONObject obj=new JSONObject(response);
					Status=obj.getBoolean("status");
					if(Status){
						JSONArray arr=obj.getJSONArray("order");
						for(int i=0;i<arr.length();i++){
							JSONObject obj_object=arr.getJSONObject(i);
							order_id=obj_object.getString("order_id");
							order_number=obj_object.getString("order_number");
							status=obj_object.getString("status");
							delivery_date=obj_object.getString("delivery_date");
							delivery_time=obj_object.getString("delivery_time");
							order_type=obj_object.getString("order_type");
							consumer_name=obj_object.getString("consumer_name");
							txnid=obj_object.getString("txnid");
							total_price=obj_object.getString("total_price");
							Log.e("the values are",order_id+"  "+order_date+" "+delivery_date+" "+delivery_time+" "+order_type+" "+consumer_name);
						
							JSONArray arr_items_ordered=obj_object.getJSONArray("order_items");
							
							for(int j=0;j<arr_items_ordered.length();j++){
								JSONObject obj_items_ordered=arr_items_ordered.getJSONObject(j);
								item_name=obj_items_ordered.getString("item_name");
								qty=obj_items_ordered.getString("qty");
								avaliable_order=obj_items_ordered.getString("qty");
								price=obj_items_ordered.getString("price");
								oid=obj_items_ordered.getString("oid");
							}
						}
						
					}
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int v_id = v.getId();
		if (v_id == R.id.rel_todaysspl) {
			inTodaysSpl();

		} else if (v_id == R.id.rel_pre_order) {
			inPreOrder();
		}
	}

	public void inTodaysSpl() {

		// ispreorder = false;

		todaysSpl.setBackgroundColor(Color.WHITE);
		preOrder.setBackgroundColor(getResources().getColor(R.color.colorGrey));

		imgTodaysSpl.setImageResource(R.drawable.todayspecial1);
		txtTodaysSpl
				.setTextColor(getResources().getColor(R.color.colorPrimary));

		imgPreOrder.setImageResource(R.drawable.greypreorder1);
		txtPreOrder.setTextColor(getResources().getColor(
				R.color.textColorPrimary));

	}

	public void inPreOrder() {

		// ispreorder = true;

		todaysSpl
				.setBackgroundColor(getResources().getColor(R.color.colorGrey));
		preOrder.setBackgroundColor(Color.WHITE);

		imgTodaysSpl.setImageResource(R.drawable.greytodayspecial1);
		txtTodaysSpl.setTextColor(getResources().getColor(
				R.color.textColorPrimary));

		imgPreOrder.setImageResource(R.drawable.preorder1);
		txtPreOrder.setTextColor(getResources().getColor(R.color.colorPrimary));

	}

}
