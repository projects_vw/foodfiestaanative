package com.ikomet.foodfiestaa.fragments;

import java.io.UnsupportedEncodingException;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.activities.HomeActivity;
import com.ikomet.foodfiestaa.activities.LocationSearchResultPage;
import com.ikomet.foodfiestaa.adapter.AdptrLocationSearchKitchen;
import com.ikomet.foodfiestaa.beans.LocSearchKitchenDetails;
import com.ikomet.foodfiestaa.interfaces.FragmentCommunicatorNew;
import com.ikomet.foodfiestaa.interfaces.Toggle;
import com.ikomet.foodfiestaa.utils.DialogFilter;
import com.ikomet.foodfiestaa.utils.InternetCheck;
import com.ikomet.foodfiestaa.utils.ServiceHandler;
import com.ikomet.foodfiestaa.utils.SharedPrefs;
import com.ikomet.foodfiestaa.utils.SpacesItemDecoration;

public class Frag_SearchKitchen extends Fragment implements
		ConnectionCallbacks, OnConnectionFailedListener, LocationListener,
		FragmentCommunicatorNew, GpsStatus.Listener, Toggle, View.OnClickListener {
	View view;
	RecyclerView recycler_view;
	String location_id;
	public final String TYPE_TODAYSPECIAL = "1", TYPE_PREORDER = "2";
	ProgressDialog pDialog;
	LocSearchKitchenDetails details;
	ArrayList<LocSearchKitchenDetails> arr_kitchen_detail;
	AdptrLocationSearchKitchen adapter_location;
	public static final String SEARCH_SERVICE_URL = "http://beta.foodfiestaa.com/searchservice";
	public static final String LOCATION_SEARCH_URL = "http://beta.foodfiestaa.com/locationSearch";
	public static final String DIET_SERVICE = "http://beta.foodfiestaa.com/diettypeservice";
	boolean isSigned;
	String lat, lon;
	Location loc;
	GoogleApiClient mLocationClient;
	private LocationRequest mLocationRequest;
	String param = "", url = "";
	LinearLayout locText;
	RelativeLayout locHolder;
	AutoCompleteTextView locSearch;
	ImageView filter;
	ArrayList<String> location_list;
	ArrayList<Integer> location_list_id;
	ActionBar actionbar;
	LocationManager locationManager;
	DialogFilter dialog;
	public FragmentCommunicatorNew frag_new;
	LinkedHashMap<Integer,String> linkedHashMap;
	ImageView img_filter, smiley;
	RelativeLayout noNet;
	LinearLayout fragSrchKtchn;
	public static LinkedHashMap<Integer, String> diet2, order2, cusine2, distance2;
	
	@Override
	public void onAttach(Context context) {
		// TODO Auto-generated method stub
		super.onAttach(context);
		((LocationSearchResultPage) context).communicator = this;
		((LocationSearchResultPage) context).toggle = this;
		frag_new = (FragmentCommunicatorNew)this;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		view = inflater.inflate(R.layout.frag_search_kitchen, container, false);

		noNet = (RelativeLayout)view.findViewById(R.id.no_net);
		fragSrchKtchn = (LinearLayout)view.findViewById(R.id.lnr_srch_ktchn);
		smiley = (ImageView)view.findViewById(R.id.img_sad);
		
		if(InternetCheck.isInternetConnected(getActivity())){
			fragSrchKtchn.setVisibility(View.VISIBLE);
			noNet.setVisibility(View.GONE);
			
			initializeVar();
			
		}else{
			fragSrchKtchn.setVisibility(View.GONE);
			noNet.setVisibility(View.VISIBLE);
			
			noNet.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(InternetCheck.isInternetConnected(getActivity())){
						
						Animation a = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_anim);
						a.setDuration(1000);
						smiley.startAnimation(a);
						
						new Handler().postDelayed(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								fragSrchKtchn.setVisibility(View.VISIBLE);
								noNet.setVisibility(View.GONE);
								initializeVar();
							}
						}, 2000);
						
						/*fragSrchKtchn.setVisibility(View.VISIBLE);
						noNet.setVisibility(View.GONE);
						initializeVar();*/
					}
				}
			});
			
		}
		
		
		
		// new AsyncLocSearch().execute(location_id);
		return view;
	}

	public void Param_Creation() {
		try {
			Log.e("location id in param creation",
					SharedPrefs.get_LocationId(getActivity()));
			param = "location="
					+ URLEncoder.encode(
							SharedPrefs.get_LocationId(getActivity()), "UTF-8")
					+ "&type=" + URLEncoder.encode(TYPE_TODAYSPECIAL, "UTF-8");
			url = LOCATION_SEARCH_URL;
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	/*class TypeList extends AsyncTask<String, String, String> {

		boolean status;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Please Wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub
			// Check for success tag

			String json = "";
			linkedHashMap = new LinkedHashMap<Integer, String>();
			try{
			
				String id = SharedPrefs.getUid(getActivity());
				String token = SharedPrefs.getToken(getActivity());
				
				// you need to encode ONLY the values of the parameters
				String param = "id=" + URLEncoder.encode(id, "UTF-8")
						+ "&token=" + URLEncoder.encode(token, "UTF-8");

				ServiceHandler sh = new ServiceHandler();

				json = sh.makeServiceCall(DIET_SERVICE, ServiceHandler.POST, param);
							
				JSONObject jsonObj = new JSONObject(json);

				status = jsonObj.getBoolean("status");

				if (status) {

					JSONArray jarData = jsonObj.getJSONArray("data");
					
					for(int i = 0 ; i< jarData.length();i++){
					
					JSONObject jobjData = jarData.getJSONObject(i);
				
					String idd = jobjData.getString("id");
					String name = jobjData.getString("name");
					
					}
					
					return "success";
				} else {
					return jsonObj.getString("errorMessage");

				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			return json;

		}

		protected void onPostExecute(String value) {
			// dismiss the dialog once product deleted
			if (pDialog.isShowing()) {
				pDialog.dismiss();
			}

			if (value.equalsIgnoreCase("success")) {

			} else {
				
			}

		}

		
	}*/
		
	private void initializeVar(){
		
		locText = (LinearLayout) view.findViewById(R.id.linear_loc);
		locHolder = (RelativeLayout) view.findViewById(R.id.rel_loc);
		locSearch = (AutoCompleteTextView) view.findViewById(R.id.txt_search);
		filter = (ImageView) view.findViewById(R.id.img_filter);
		filter.setOnClickListener(this);

		actionbar = ((ActionBarActivity) getActivity()).getSupportActionBar();
		actionbar.show();

		isSigned = SharedPrefs.hasSPData(getActivity());
		img_filter=(ImageView)view.findViewById(R.id.img_filter);
		img_filter.setOnClickListener(this);
		new AsyncDietType().execute();
		// ((LocationSearchResultPage) getActivity()).communicator = this;
		if (isSigned) {

			if (mLocationClient == null) {
				mLocationClient = new GoogleApiClient.Builder(getActivity())
						.addConnectionCallbacks(this)
						.addOnConnectionFailedListener(this)
						.addApi(LocationServices.API).build();
			}
			
			mLocationClient.connect();
			
			locationManager = (LocationManager) getActivity()
                     .getSystemService(Context.LOCATION_SERVICE);

			locationManager.addGpsStatusListener(this);
			/*
			 * lat = getArguments().getString("lat"); lon =
			 * getArguments().getString("long");
			 */

		} else {
			location_id = SharedPrefs.get_LocationId(getActivity());
			// location_id = getArguments().getString("location_id");
			Log.e("the location_id is in the arguments", location_id);
		}

		recycler_view = (RecyclerView) view.findViewById(R.id.recycler_view);
		recycler_view.setLayoutManager(new GridLayoutManager(getActivity(),2));		
		/*recycler_view.setLayoutManager(new StaggeredGridLayoutManager(2,
				StaggeredGridLayoutManager.VERTICAL));*/

		SpacesItemDecoration decoration = new SpacesItemDecoration(20, 20);
		recycler_view.addItemDecoration(decoration);

		
			if (isSigned) {
				if (SharedPrefs.get_FragmentState(getActivity(), "FragmentA") == 0) {
					}else{
					Log.e("sharedPref Value for signed in",
							SharedPrefs.get_FragmentState(getActivity(),
									"FragmentA") + "");

					adapter_location = new AdptrLocationSearchKitchen(
							getActivity(),
							LocationSearchResultPage.arr_kitchen_detail);
					recycler_view.setAdapter(adapter_location);
					ArrayAdapter<String> adapter = new ArrayAdapter<String>(
							getActivity(), R.layout.spinner_item_location,
							LocationSearchResultPage.location_list);
					locSearch.setThreshold(1);
					locSearch.setAdapter(adapter);

					locSearch.setOnItemClickListener(new OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> arg0, View arg1,
								int arg2, long arg3) { // TODO Auto-generated method

							Log.e("location", locSearch.getText().toString());
							String location_name = locSearch.getText().toString();

							for (int i = 0; i < LocationSearchResultPage.location_list
									.size(); i++) {

								if (location_name
										.equalsIgnoreCase(LocationSearchResultPage.location_list
												.get(i))) {
									int id = LocationSearchResultPage.location_list_id
											.get(i);
									SharedPrefs.put_locationId(getActivity(),
											String.valueOf(id));
									Log.e("ID and LOCation ",
											" hyutrygfj "
													+ SharedPrefs
															.get_LocationId(getActivity()));
									break;
								}

							}

							Param_Creation();
							new AsyncLocSearch().execute(param, url);
						}
					});

				}
				
			} else {
				
				/*param = "location=" + URLEncoder.encode(location_id, "UTF-8")
						+ "&type="
						+ URLEncoder.encode(TYPE_TODAYSPECIAL, "UTF-8");
				url = LOCATION_SEARCH_URL;*/
//				new AsyncLocSearch().execute(param, url);
				
				if (SharedPrefs.get_FragmentState(getActivity(), "FragmentA") == 0) {
					Log.e("sharedPref Value",
							SharedPrefs.get_FragmentState(getActivity(),
									"FragmentA") + "");

					// Log.e("sharedPref Value", "");
					Param_Creation();
					new AsyncLocSearch().execute(param, url);

				} else {
					Log.e("sharedPref Value",
							SharedPrefs.get_FragmentState(getActivity(),
									"FragmentA") + "");

					adapter_location = new AdptrLocationSearchKitchen(
							getActivity(),
							LocationSearchResultPage.arr_kitchen_detail);
					recycler_view.setAdapter(adapter_location);
					ArrayAdapter<String> adapter = new ArrayAdapter<String>(
							getActivity(), R.layout.spinner_item_location,
							LocationSearchResultPage.location_list);
					locSearch.setThreshold(1);
					locSearch.setAdapter(adapter);

					locSearch.setOnItemClickListener(new OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> arg0, View arg1,
								int arg2, long arg3) { // TODO Auto-generated method

							Log.e("location", locSearch.getText().toString());
							String location_name = locSearch.getText().toString();

							for (int i = 0; i < LocationSearchResultPage.location_list
									.size(); i++) {

								if (location_name
										.equalsIgnoreCase(LocationSearchResultPage.location_list
												.get(i))) {
									int id = LocationSearchResultPage.location_list_id
											.get(i);
									SharedPrefs.put_locationId(getActivity(),
											String.valueOf(id));
									Log.e("ID and LOCation ",
											" hyutrygfj "
													+ SharedPrefs
															.get_LocationId(getActivity()));
									break;
								}

							}

							Param_Creation();
							new AsyncLocSearch().execute(param, url);
						}
					});

				}
				
			}
		
	}
	
	class AsyncLocSearch extends AsyncTask<String, Void, String> {
		String result = null, param = null;
		String status;
	
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			Log.e("inside async", "method");
			pDialog.setMessage("Please wait...");
		
			pDialog.setTitle("Loading");
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			ServiceHandler handler = new ServiceHandler();
			result = handler.makeServiceCall(params[1], ServiceHandler.POST,
					params[0]);
			Log.e("the location search result is", result);
			try {
				JSONObject obj = new JSONObject(result);
				JSONObject obj1;
				if (obj.getBoolean("status")) {
					LocationSearchResultPage.arr_kitchen_detail = new ArrayList<>();
					JSONArray arr = obj.getJSONArray("data");
					for (int i = 0; i < arr.length(); i++) {
						obj1 = arr.getJSONObject(i);
						LocationSearchResultPage.details = new LocSearchKitchenDetails(
								obj1.getString("kitchen_id"),
								"http://beta.foodfiestaa.com/"
										+ obj1.getString("pic").replace(" ",
												"%20"),
								obj1.getString("location"),
								obj1.getString("location_id"),
								obj1.getString("kitchen_name"),
								obj1.getString("minimumprice"));
						LocationSearchResultPage.arr_kitchen_detail.add(LocationSearchResultPage.details);
					}
					status = "true";
				} else {
					status = "false";
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return status;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(pDialog.isShowing()){
				pDialog.dismiss();
			}
			Log.e("the result is", result);
			if (result.equalsIgnoreCase("false")) {
				Toast.makeText(getActivity(), "returns false", 1000).show();

			} else {
				new Async_LocationList().execute();
				adapter_location = new AdptrLocationSearchKitchen(
						getActivity(), LocationSearchResultPage.arr_kitchen_detail);
				recycler_view.setAdapter(adapter_location);

			}
		}
	}

	@Override
	public void onLocationChanged(Location arg0) {
		// TODO Auto-generated method stub
		/*Toast.makeText(getActivity(), "Location received: " + arg0.toString(),
				Toast.LENGTH_SHORT).show();*/
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onConnected(Bundle arg0) {
		// TODO Auto-generated method stub
		mLocationRequest = LocationRequest.create();
		mLocationRequest
				.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
		mLocationRequest.setInterval(1000); // Update location every second

		LocationServices.FusedLocationApi.requestLocationUpdates(
				mLocationClient, mLocationRequest, this);

		Toast.makeText(getActivity(), "Connected", Toast.LENGTH_SHORT).show();
		
		loc = getLastLocation();
		if (loc != null) {
			
			Toast.makeText(getActivity(),
					"" + loc.getLatitude() + "\n" + loc.getLongitude(),
					Toast.LENGTH_SHORT).show();
			lat = String.valueOf(loc.getLatitude());
			lon = String.valueOf(loc.getLongitude());
			Log.e("inside on connecteddd method", "lat longgg");
			/*SharedPrefs.putLat(getActivity(), lat);
			SharedPrefs.putLong(getActivity(), lon);*/
			
			try {
				param = "lat=" + URLEncoder.encode(lat, "UTF-8") + "&lang="
						+ URLEncoder.encode(lon, "UTF-8");
				url = SEARCH_SERVICE_URL;
				new AsyncLocSearch().execute(param, url);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		} else {
			Toast.makeText(getActivity(), "loc is null", Toast.LENGTH_SHORT)
					.show();
		}

	}

	public Location getLastLocation() {
		Location mLastLocation = LocationServices.FusedLocationApi
				.getLastLocation(mLocationClient);

		if (mLastLocation != null) {
			Toast.makeText(
					getActivity(),
					"" + mLastLocation.getLatitude() + "\n"
							+ mLastLocation.getLongitude(), Toast.LENGTH_LONG)
					.show();
			lat = String.valueOf(mLastLocation.getLatitude());
			lon = String.valueOf(mLastLocation.getLongitude());
			return mLastLocation;
		} else {
			Toast.makeText(getActivity(), "could not get location",
					Toast.LENGTH_LONG).show();
			return mLastLocation;
		}
	}

	@Override
	public void onConnectionSuspended(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void passnewDataToFragment(String val1, String val2) {
		// TODO Auto-generated method stub
		/*locText.setVisibility(View.VISIBLE);
		locHolder.setVisibility(View.VISIBLE);*/
	}

	class Async_LocationList extends AsyncTask<Void, Void, String> {

		String result = null;

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			ServiceHandler handler = new ServiceHandler();
			result = handler.makeServiceCall(
					"http://beta.foodfiestaa.com/locationservice", 1);
			LocationSearchResultPage.location_list = new ArrayList<>();
			LocationSearchResultPage.location_list_id = new ArrayList<>();
			try {
				JSONArray arr1 = new JSONArray(result);
				for (int i = 0; i < arr1.length(); i++) {
					JSONObject obj1 = arr1.getJSONObject(i);
					LocationSearchResultPage.location_list.add(obj1.getString("area"));
					LocationSearchResultPage.location_list_id.add(obj1.getInt("id"));
					Log.e("the location id and area",LocationSearchResultPage. location_list.get(i)
							+ " " + LocationSearchResultPage.location_list_id.get(i).toString());
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(pDialog.isShowing()){
				pDialog.dismiss();
			}
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(
					getActivity(), R.layout.spinner_item_location,
					LocationSearchResultPage.location_list);
			locSearch.setThreshold(1);
			locSearch.setAdapter(adapter);
			
			locSearch.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					Log.e("location_position", "" + arg2);
					Log.e("location", locSearch.getText().toString());
					String location_name = locSearch.getText().toString();
					for (int i = 0; i < LocationSearchResultPage.location_list
							.size(); i++) {

						if (location_name
								.equalsIgnoreCase(LocationSearchResultPage.location_list
										.get(i))) {
							int id = LocationSearchResultPage.location_list_id
									.get(i);
							SharedPrefs.put_locationId(getActivity(),
									String.valueOf(id));
							Log.e("ID and LOCation ", " hyutrygfj "
									+ SharedPrefs.get_LocationId(getActivity()));
							break;
						}

					}

					Param_Creation();
					new AsyncLocSearch().execute(param, url);
				}
			});
			SharedPrefs.put_FragmentState(getActivity(), "FragmentA", 1);

		}

		}

	@Override
	public void onGpsStatusChanged(int event) {
		Log.e("state changed", " status changed "+ event );
		// TODO Auto-generated method stub
		if(event == GpsStatus.GPS_EVENT_STARTED){
			Toast.makeText(getActivity(), "GPS enabled", Toast.LENGTH_SHORT).show();
		}else if(event == GpsStatus.GPS_EVENT_STOPPED){
			Toast.makeText(getActivity(), "GPS disbled", Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void ControlToggle(boolean value) {
		// TODO Auto-generated method stub
		if (value) {
			locText.setVisibility(View.VISIBLE);
			locHolder.setVisibility(View.VISIBLE);
		} else {
			Log.e("Inside else", "control toggle false");
			locText.setVisibility(View.GONE);
			locHolder.setVisibility(View.GONE);
		}
	}

	@Override
	public void EditDish_flnt_actionbar() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
	
		if(v.getId() == R.id.img_filter){
		
		dialog=new DialogFilter(getActivity(), frag_new,diet2,cusine2,0);
		dialog.show();
		
		}
		//dialog.sh
	}

	class AsyncDietType extends AsyncTask<Void, Void, String> {

		String url_cuisine = "", response = "", url_diet = "",
				response_diet = "";
		String id = SharedPrefs.getUid(getActivity()), token = SharedPrefs
				.getToken(getActivity());
		ServiceHandler handler, handler_diet;

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (result.equalsIgnoreCase("success")) {
				order2 = new LinkedHashMap<>();
				order2.put(1, "Today's Special");
				order2.put(2, "Pre-Order");
			
			}

		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			handler = new ServiceHandler();
			diet2 = new LinkedHashMap<>();
		
			url_cuisine = "http://beta.foodfiestaa.com/cuisinetypeservice?id="
					+ id + "&token=" + token;
			Log.e("the url is", url_cuisine);
			response = handler.makeServiceCall(url_cuisine, 1);
			Log.e("the response is ", response);
			try {
				JSONObject obj_cusine1 = new JSONObject(response);
				boolean cusine_status = obj_cusine1.getBoolean("status");
				if (cusine_status) {
					cusine2 = new LinkedHashMap<>();
					JSONArray arr_cusine = obj_cusine1.getJSONArray("data");
					for (int i = 0; i < arr_cusine.length(); i++) {
						JSONObject obj_cusinedata = arr_cusine.getJSONObject(i);
						cusine2.put(obj_cusinedata.getInt("id"),
								obj_cusinedata.getString("name"));
					}
				//	child2.put(header_name.get(2).getName(), cusine2);

					url_diet = "http://beta.foodfiestaa.com/diettypeservice?id="
							+ id + "&token=" + token;
					handler_diet = new ServiceHandler();
					response_diet = handler_diet.makeServiceCall(url_diet, 1);
					Log.e("response diet", response_diet);
					JSONObject obj_diet = new JSONObject(response_diet);
					boolean diet_status = obj_diet.getBoolean("status");
					if (diet_status) {
						JSONArray arr_diet = obj_diet.getJSONArray("data");
						for (int j = 0; j < arr_diet.length(); j++) {
							JSONObject obj_dietdata = arr_diet.getJSONObject(j);
							diet2.put(obj_dietdata.getInt("id"),
									obj_dietdata.getString("name"));
						}
						//child2.put(header_name.get(0).getName(), diet2);
					} else {
						return "diet_status_false";
					}
					return "success";
				} else {
					return "cuisine_status_false";
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}
	}
	
	@Override
	public void checkedData(HashMap<String, Integer> hashmap) {
		// TODO Auto-generated method stub
		if(hashmap!=null)
    	{
			try {
				Log.e("location id in param creation",
						SharedPrefs.get_LocationId(getActivity()));
				param = "location="
						+ URLEncoder.encode(
								SharedPrefs.get_LocationId(getActivity()), "UTF-8")
						+ "&type=" + URLEncoder.encode(TYPE_TODAYSPECIAL, "UTF-8");
				url = LOCATION_SEARCH_URL;
		
			
			
    		if(hashmap.containsKey("dtype"))
    		{
    			Log.e("dtype", ""+hashmap.get("dtype"));
    			param=param+"&dtype="+URLEncoder.encode(hashmap.get("dtype").toString(), "UTF-8");
    		}else{
    			Log.e("dtype else", "part");
    		}
    		if(hashmap.containsKey("order_type")){
    			Log.e("order_type", ""+hashmap.get("order_type"));
    			param=param+"&type="+URLEncoder.encode(hashmap.get("order_type").toString(), "UTF-8");
    		}else{
    			Log.e("order type else", "part");
    		}if(hashmap.containsKey("ctype")){
    			Log.e("ctype", ""+hashmap.get("ctype"));
    			param=param+"&ctype="+URLEncoder.encode(hashmap.get("ctype").toString(), "UTF-8");
    		}else{
    			Log.e("c_type", "part");
    		}
    		if(hashmap.containsKey("distance"))
    		{
    			param=param+"&distance="+URLEncoder.encode(hashmap.get("distance").toString(), "UTF-8");
    		}
    		else
    		{
    			Log.e("distance", "part");
    		}
    		Log.e("param creation in checkedData", param);
    		new AsyncLocSearch().execute(param, url);
//    		dialog.dismiss();
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    	}
		else{
			Log.e("hash map is null", "null hashmap");
		}
	}

}
