package com.ikomet.foodfiestaa.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.adapter.PagerAdapter;

public class FragmentPagerHowConsumer extends Fragment {

	int count = PagerAdapter.marg1;
	TextView title, descr;
	ImageView content;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		View layout = inflater.inflate(R.layout.fragment_pagerelement, container,false);
	
		title = (TextView)layout.findViewById(R.id.txt_cook);
		content = (ImageView)layout.findViewById(R.id.img_how_consumer);
		descr = (TextView)layout.findViewById(R.id.txt_consumer);
		
		switch (count) {
		case 0:
			title.setText("Find Favourite");
			content.setImageResource(R.drawable.findfavorites);
			descr.setText(R.string.find_favourites_desc);
			break;
		case 1:
			title.setText("Order & Payment");
			
			content.setImageResource(R.drawable.orderpayment);
			descr.setText(R.string.order_payment_desc);
			break;
		case 2:
			title.setText("Enjoy Food");
			content.setImageResource(R.drawable.enjoyfood);
			descr.setText(R.string.enjoy_food_desc);
			break;	
			
		default:
			break;
		}
		
		return layout;
		
	}

}
