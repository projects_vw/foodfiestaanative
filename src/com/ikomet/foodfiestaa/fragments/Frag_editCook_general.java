package com.ikomet.foodfiestaa.fragments;

import android.os.AsyncTask;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.R.id;
import com.ikomet.foodfiestaa.activities.EditCook_profile;
import com.ikomet.foodfiestaa.beans.CookDetailBean;
import com.ikomet.foodfiestaa.interfaces.FragmentCommunicator;
import com.ikomet.foodfiestaa.utils.DialogCustom;
import com.ikomet.foodfiestaa.utils.DialogVerification;
import com.ikomet.foodfiestaa.utils.InternetCheck;
import com.ikomet.foodfiestaa.utils.ServiceHandler;
import com.ikomet.foodfiestaa.utils.SharedPrefs;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import org.json.JSONException;
import org.json.JSONObject;

public class Frag_editCook_general extends Fragment  {

	private static final String COOKDETAILBEAN = "cook_details_bean";
	CookDetailBean bean;
	Button btn_next_general;
	EditText edt_first_name;
	EditText edt_mobile_no;
	EditText edt_second_name;
	public DialogVerification dialog;
	public FragmentCommunicator communicator;
	DialogCustom dialogCustom;
	
	public Frag_editCook_general() {
		
	}

	public static Frag_editCook_general newInstance(
			CookDetailBean cookdetailbean) {
		Frag_editCook_general frag_editcook_general = new Frag_editCook_general();
		Bundle bundle = new Bundle();
		bundle.putSerializable("cook_details_bean", cookdetailbean);
		frag_editcook_general.setArguments(bundle);
		return frag_editcook_general;
	}

	public View onCreateView(LayoutInflater layoutinflater,
			ViewGroup viewgroup, Bundle bundle) {

		View view = layoutinflater.inflate(R.layout.frag_edtcook_general,
				viewgroup, false);
		
		dialogCustom = new DialogCustom();
		communicator=(FragmentCommunicator)getActivity();
		edt_first_name = (EditText) view.findViewById(R.id.edt_first_name);
		edt_second_name = (EditText) view.findViewById(R.id.edt_second_name);
		edt_mobile_no = (EditText) view.findViewById(R.id.edt_mobile_no);
		btn_next_general= (Button) view.findViewById(R.id.btn_next_general);
		bean = (CookDetailBean) getArguments().getSerializable(
				"cook_details_bean");
		Log.e("test_bean", bean.toString());
		edt_first_name.setText(bean.getFname());
		edt_second_name.setText(bean.getLname());
		edt_mobile_no.setText(bean.getMobile());
		dialog = new DialogVerification(getActivity(), 1,
				"Profile updated successfully");
		
		btn_next_general.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*async_test test = new async_test();
				test.execute("");*/
				if(InternetCheck.isInternetConnected(getActivity())){
					new async_test().execute("");
				}else{
					dialogCustom.showDialog(getActivity(), "Please connect to Internet", "OK", R.drawable.circle_uncheck);
				}
			}
		});
		return view;
	}

	class async_test extends AsyncTask<String, Void, String> {
		String param, result;
		String status, message;
		JSONObject obj1;
		String fname = edt_first_name.getText().toString();
		String lname = edt_second_name.getText().toString();
		String mobile = edt_mobile_no.getText().toString();
		String id = SharedPrefs.getUid(getActivity());
		String token = SharedPrefs.getToken(getActivity());

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub

			try {
				String param = "id=" + URLEncoder.encode(id, "UTF-8")
						+ "&token=" + URLEncoder.encode(token, "UTF-8")
						+ "&fname=" + URLEncoder.encode(fname, "UTF-8")
						+ "&lname=" + URLEncoder.encode(lname, "UTF-8")
						+ "&mobile=" + URLEncoder.encode(mobile, "UTF-8");
				ServiceHandler handler = new ServiceHandler();
				result = handler.makeServiceCall(
						"http://beta.foodfiestaa.com/editprofileservice", 2,
						param);
				try {
					obj1 = new JSONObject(result);
					status = obj1.getString("status");
					message = obj1.getString("message");

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return message;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (result.equalsIgnoreCase("profile updated successfully")) {
				
				dialog.show();
				
				Button ok = (Button)dialog.findViewById(R.id.btn_ok);
				ok.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						communicator.change_state();
						getActivity().finish();
						dialog.dismiss();
					}
				});
				
				return;
			} else {
				Toast.makeText(getActivity(), result, 1000).show();
				return;
			}

		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}

	}
}
