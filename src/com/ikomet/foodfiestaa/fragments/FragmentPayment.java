package com.ikomet.foodfiestaa.fragments;

import java.math.RoundingMode;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.activities.GalleryActivity;
import com.ikomet.foodfiestaa.activities.PaymentActivity;
import com.ikomet.foodfiestaa.beans.CartBean;
import com.ikomet.foodfiestaa.databases.DBHandler;
import com.ikomet.foodfiestaa.interfaces.Cart;
import com.ikomet.foodfiestaa.utils.DialogCustom;
import com.ikomet.foodfiestaa.utils.InternetCheck;
import com.ikomet.foodfiestaa.utils.ServiceHandler;
import com.ikomet.foodfiestaa.utils.SharedPrefs;
import com.tmxlr.lib.driodvalidatorlight.Form;
import com.tmxlr.lib.driodvalidatorlight.helper.Range;
import com.tmxlr.lib.driodvalidatorlight.helper.RegexTemplate;

public class FragmentPayment extends Fragment implements OnClickListener{

	View layout;
	EditText email, fname, lname, mobile;
	TextView txtEmail, addressLabel;
	EditText address, chennai, tamilNadu, india, pincode;
	AutoCompleteTextView location;
	LinearLayout buttons;
	Button save, cancel;
	TextView txtAddress, txtMobile, txtChangeAddress;
	TableLayout tableView;
	TableRow rowValues, row_delivery, row_total;
	ImageButton delete;
	TextView foodName, quantity, price;
	TextView subTotal, delivery, total;
	TextView cartEmpty;
	Button contShopping, contPayment;
	DBHandler db;
	ArrayList<CartBean> cartList;
	Cart cart;
	DecimalFormat df;
	boolean isSigned;
	ProgressDialog pDialog;
	private final String GET_PROFILE_INFO = "http://beta.foodfiestaa.com/consumerprofile";
	private final String GET_DROPDOWN = "http://beta.foodfiestaa.com/locationservice";
	private final String UPDATE_PROFILE = "http://beta.foodfiestaa.com/consumereditprofile";
	Form form;
	HashMap<String,Integer> hm;
	String items[];
	ArrayAdapter<String> autoAdapter;
	String loc_id = "";
	RelativeLayout snack, noNet, relPayment;
	ImageView smiley;
	DialogCustom dialogNet;
	
	@Override
	public void onAttach(Context context) {
		// TODO Auto-generated method stub
		super.onAttach(context);
		cart = (Cart)context;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		layout = inflater.inflate(R.layout.fragment_payment, container, false);

		dialogNet = new DialogCustom();
		noNet = (RelativeLayout)layout.findViewById(R.id.no_net);
		relPayment = (RelativeLayout)layout.findViewById(R.id.rel_payment);
		smiley = (ImageView)layout.findViewById(R.id.img_sad);
		
		if(InternetCheck.isInternetConnected(getActivity())){
			relPayment.setVisibility(View.VISIBLE);
			noNet.setVisibility(View.GONE);
			
			initializeVar();
			
		}else{
			relPayment.setVisibility(View.GONE);
			noNet.setVisibility(View.VISIBLE);
			
			noNet.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(InternetCheck.isInternetConnected(getActivity())){
						
						Animation a = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_anim);
						a.setDuration(1000);
						smiley.startAnimation(a);
						
						new Handler().postDelayed(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								relPayment.setVisibility(View.VISIBLE);
								noNet.setVisibility(View.GONE);
								initializeVar();
							}
						}, 2000);
						
						/*relPayment.setVisibility(View.VISIBLE);
						noNet.setVisibility(View.GONE);
						initializeVar();*/
					}
				}
			});
			
		}
		
		return layout;
		
	}
	
	private void initializeVar(){
				
		snack = (RelativeLayout)layout.findViewById(R.id.snack);
		
		db = new DBHandler(getActivity());
		isSigned = SharedPrefs.hasSPData(getActivity());
		form = new Form(getActivity());
		
		email = (EditText) layout.findViewById(R.id.ed_email);
		fname = (EditText) layout.findViewById(R.id.ed_fname);
		lname = (EditText) layout.findViewById(R.id.ed_lname);
		mobile = (EditText) layout.findViewById(R.id.ed_mobile);
		
		txtEmail = (TextView) layout.findViewById(R.id.txt_email);
		
		addressLabel = (TextView) layout.findViewById(R.id.txt_address_label);
		
		address = (EditText) layout.findViewById(R.id.ed_address);
		location = (AutoCompleteTextView) layout.findViewById(R.id.ed_location);
		chennai = (EditText) layout.findViewById(R.id.ed_chennai);
		tamilNadu = (EditText) layout.findViewById(R.id.ed_tamilnadu);
		india = (EditText) layout.findViewById(R.id.ed_india);
		pincode = (EditText) layout.findViewById(R.id.ed_pincode);
		
		buttons = (LinearLayout) layout.findViewById(R.id.linear_buttons);
		
		save = (Button) layout.findViewById(R.id.bt_save);
		cancel = (Button) layout.findViewById(R.id.bt_cancel);
		
		txtAddress = (TextView) layout.findViewById(R.id.txt_address);
		txtMobile = (TextView) layout.findViewById(R.id.txt_mobile);
		txtChangeAddress = (TextView) layout.findViewById(R.id.txt_change_address);
		
		tableView = (TableLayout) layout.findViewById(R.id.table_cart);

		cartEmpty = (TextView) layout.findViewById(R.id.txt_noitems_incart);		
		subTotal = (TextView) layout.findViewById(R.id.txt_subtotal_price);
		total = (TextView) layout.findViewById(R.id.txt_total);
		
		contShopping = (Button) layout.findViewById(R.id.bt_continue_shop);
		contPayment = (Button) layout.findViewById(R.id.bt_continue_pay);
		
		txtChangeAddress.setOnClickListener(this);
		save.setOnClickListener(this);
		cancel.setOnClickListener(this);
		
		contShopping.setOnClickListener(this);
		contPayment.setOnClickListener(this);
				
		location.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				String area = autoAdapter.getItem(position);
				loc_id = hm.get(area).toString();
			}
		});
		
		/*if(!isSigned){
			form.check(email, RegexTemplate.NOT_EMPTY_PATTERN, "Please enter your email id");
			form.check(email, RegexTemplate.EMAIL_PATTERN, "Please enter a valid email id");
			form.check(fname, RegexTemplate.NOT_EMPTY_PATTERN, "Please enter your first name");
			form.check(lname, RegexTemplate.NOT_EMPTY_PATTERN, "Please enter your last name");
			form.check(mobile, RegexTemplate.PHONE_PATTERN, "Please enter a valid mobile number");
			form.check(address, RegexTemplate.NOT_EMPTY_PATTERN, "Please enter a your address");
			form.check(pincode, RegexTemplate.NOT_EMPTY_PATTERN, "Please enter your pincode");
		}*/
		
		new GetDropdown().execute();
				
		df = new DecimalFormat("#.##");
		df.setRoundingMode(RoundingMode.HALF_UP);
		
		updateList();
		
	}
	
	public void signedInitialView(){
		
		form.clear();
		
		email.setVisibility(View.GONE);
		fname.setVisibility(View.GONE);
		lname.setVisibility(View.GONE);
		mobile.setVisibility(View.GONE);
		
		txtEmail.setVisibility(View.VISIBLE);
		
		addressLabel.setVisibility(View.GONE);
		
		address.setVisibility(View.GONE);
		location.setVisibility(View.GONE);
		chennai.setVisibility(View.GONE);
		tamilNadu.setVisibility(View.GONE);
		india.setVisibility(View.GONE);
		pincode.setVisibility(View.GONE);

		buttons.setVisibility(View.GONE);
		
		txtAddress.setVisibility(View.VISIBLE);
		txtMobile.setVisibility(View.VISIBLE);
		txtChangeAddress.setVisibility(View.VISIBLE);
		
		form.check(address, RegexTemplate.NOT_EMPTY_PATTERN, "Please fill the address");
		form.check(location, RegexTemplate.NOT_EMPTY_PATTERN, "Please fill the location");
		form.check(pincode, RegexTemplate.NOT_EMPTY_PATTERN, "Please fill the pincode");
		
	}

	public void notSignedView(){
		
		form.clear();
		
		email.setVisibility(View.VISIBLE);
		fname.setVisibility(View.VISIBLE);
		lname.setVisibility(View.VISIBLE);
		mobile.setVisibility(View.VISIBLE);
		
		txtEmail.setVisibility(View.GONE);
		
		addressLabel.setVisibility(View.GONE);
		
		address.setVisibility(View.VISIBLE);
		location.setVisibility(View.VISIBLE);
		chennai.setVisibility(View.VISIBLE);
		tamilNadu.setVisibility(View.VISIBLE);
		india.setVisibility(View.VISIBLE);
		pincode.setVisibility(View.VISIBLE);

		buttons.setVisibility(View.GONE);
		
		txtAddress.setVisibility(View.GONE);
		txtMobile.setVisibility(View.GONE);
		txtChangeAddress.setVisibility(View.GONE);
		
		form.check(email, RegexTemplate.NOT_EMPTY_PATTERN, "Please fill the address");
		form.check(fname, RegexTemplate.NOT_EMPTY_PATTERN, "Please fill the first name");
		form.check(lname, RegexTemplate.NOT_EMPTY_PATTERN, "Please fill the last name");
		form.check(mobile, RegexTemplate.NOT_EMPTY_PATTERN, "Please fill the mobile number");
		form.check(address, RegexTemplate.NOT_EMPTY_PATTERN, "Please fill the address");
		form.check(location, RegexTemplate.NOT_EMPTY_PATTERN, "Please fill the location");
		form.check(pincode, RegexTemplate.NOT_EMPTY_PATTERN, "Please fill the pincode");
//		form.checkLength(pincode, Range<Comparable<C>>.equal(6), errMsg)
	}	
	
	public void changeAddressView(){
				
		address.setVisibility(View.VISIBLE);
		location.setVisibility(View.VISIBLE);
		chennai.setVisibility(View.VISIBLE);
		tamilNadu.setVisibility(View.VISIBLE);
		india.setVisibility(View.VISIBLE);
		pincode.setVisibility(View.VISIBLE);

		buttons.setVisibility(View.VISIBLE);
		
		txtAddress.setVisibility(View.GONE);
		txtMobile.setVisibility(View.GONE);
		txtChangeAddress.setVisibility(View.GONE);
		
		form.check(address, RegexTemplate.NOT_EMPTY_PATTERN, "Please fill the address");
		form.check(location, RegexTemplate.NOT_EMPTY_PATTERN, "Please fill the location");
		form.check(pincode, RegexTemplate.NOT_EMPTY_PATTERN, "Please fill the pincode");
				
	}	
	
	public void updateList(){
		
		if(db.getTotalCount()>0){
			cartList = new ArrayList<CartBean>();
			cartList = db.getAllItems();
			
			double totalPrice = 0;
			
			for (int i = 0; i < cartList.size(); i++) {
				LayoutInflater inflater_row = (LayoutInflater) getActivity()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	
				View tr = inflater_row.inflate(R.layout.single_table_row, null, false);
	
				tr.setId(cartList.get(i).getId() + 1000);
	
				delete = (ImageButton) tr.findViewById(R.id.bt_delete);

				delete.setId(tr.getId()*5);
				delete.setPadding(0, 0, 0, 0);
				
				delete.setOnClickListener(this);
//				RelativeLayout rel = (RelativeLayout) tr.findViewById(R.id.rel_delete);
				foodName = (TextView) tr.findViewById(R.id.txt_item_name);
				foodName.setPadding(60, 0, 0, 0);
				foodName.setText(cartList.get(i).getFood_name());
				
				quantity = (TextView) tr.findViewById(R.id.txt_quantity);
				quantity.setText(String.valueOf(cartList.get(i).getItems_added()));
				
				price = (TextView) tr.findViewById(R.id.txt_price);
				double singleItemTotal = cartList.get(i).getItems_added()*Double.parseDouble(cartList.get(i).getFood_price());
				totalPrice += singleItemTotal;
				
				price.setText(String.valueOf(df.format(singleItemTotal)));

				tableView.addView(tr, i + 1);
			}
			
			total.setText(String.valueOf(df.format(totalPrice)));
			subTotal.setText(String.valueOf(df.format(totalPrice)));
			
		}else{
			tableView.setVisibility(View.GONE);
			cartEmpty.setVisibility(View.VISIBLE);
		}
		
	}
	
	public void updateTotal(){
		if(db.getTotalCount()>0){
			
			cartList = db.getAllItems();
			double totalPrice = 0;
			for (int i = 0; i < cartList.size(); i++) {
				double singleItemTotal = cartList.get(i).getItems_added()*Double.parseDouble(cartList.get(i).getFood_price());
				totalPrice += singleItemTotal;
			}
						
			total.setText(String.valueOf(df.format(totalPrice)));
			subTotal.setText(String.valueOf(df.format(totalPrice)));
		}else{
			tableView.setVisibility(View.GONE);
			cartEmpty.setVisibility(View.VISIBLE);
		}
	}

	public String getCurrentDate(){
		Calendar c = Calendar.getInstance();
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss a");	
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String formattedDate = sdf.format(c.getTime());
		System.out.println(formattedDate);
		return formattedDate;
	}
	
	/*public String getTransId(){
		Calendar c = Calendar.getInstance();
		System.out.println("Sys time in millis => " + c.getTimeInMillis());
		String transId = "TXN"+c.getTimeInMillis();
		return transId;
	}*/
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Toast.makeText(getActivity(), ""+v.getId(), Toast.LENGTH_SHORT).show();
		switch (v.getId()) {
		case R.id.bt_delete:
			

			break;

		case R.id.txt_change_address:
			
			changeAddressView();
						
			break;
			
		case R.id.bt_save:
			
//			changeAddressView();
			if(form.validate() && !loc_id.equalsIgnoreCase("")){
				if(InternetCheck.isInternetConnected(getActivity())){
					new UpdateProfile().execute();
				}else{
					dialogNet.showDialog(getActivity(), "Please connect to Internet", "OK", R.drawable.circle_uncheck);
				}
			}else if(loc_id.equalsIgnoreCase("")){
				Toast.makeText(getActivity(), "Please select a location from the list", Toast.LENGTH_SHORT).show();
			}
			
			
			break;
			
		case R.id.bt_cancel:
			
			signedInitialView();
			
			break;	
			
		case R.id.bt_continue_shop:
			
			Fragment frag = new Frag_SearchKitchen();
			FragmentManager fm = getActivity().getSupportFragmentManager();
			FragmentTransaction ft = fm.beginTransaction();
			ft.replace(R.id.realtabcontent, frag);
			ft.commit();
						
			break;
		
		case R.id.bt_continue_pay:
			
//			Toast.makeText(getActivity(), "Kitchen_id"+db.getKitchenId(), Toast.LENGTH_SHORT).show();
			float tot = Float.parseFloat(total.getText().toString().trim());
			if(tot>300){
				if(form.validate()){
					if(InternetCheck.isInternetConnected(getActivity())){
				Intent intent = new Intent(getActivity(), PaymentActivity.class);
				
				String emailId = email.getText().toString().trim();
				String pay_location = location.getText().toString().trim();
				String firstName = fname.getText().toString().trim();
				String addrs = address.getText().toString().trim();
				String phon = mobile.getText().toString().trim();
				String totalAmount = total.getText().toString().trim();
				String kitchen_id = db.getKitchenId();
				String guest_user = "";
				if(isSigned){
					guest_user = "0";
				}else{
					guest_user = "1";
				}
//				String order_data = "";
				String order_date = getCurrentDate();
				String order_type = String.valueOf(db.getOrderType());
//				String transaction_id = getTransId();
				
				/*String emailId = email.getText().toString().trim();
				String pay_location = location.getText().toString().trim();
				String firstName = fname.getText().toString().trim();
				String addrs = address.getText().toString().trim();
				String phon = mobile.getText().toString().trim();
				String totalAmount = total.getText().toString().trim();
				String kitchen_id = db.getKitchenId();
				String guest_user = "";
				String order_data = "";
				String order_date = "";
				String transaction_id = "";
				String status = "1";*/
				
				Bundle bundle = new Bundle();
				bundle.putString("emailId", emailId);
				bundle.putString("pay_location", pay_location);
				bundle.putString("firstName", firstName);
				bundle.putString("addrs", addrs);
				bundle.putString("phon", phon);
				bundle.putString("totalAmount", totalAmount);
				bundle.putString("kitchen_id", kitchen_id);
				bundle.putString("guest_user", guest_user);
//				bundle.putString("order_data", order_data);
				bundle.putString("order_date", order_date);
				bundle.putString("order_type", order_type);
//				bundle.putString("transaction_id", transaction_id);
				
				intent.putExtras(bundle);
				
			startActivity(intent);
			getActivity().overridePendingTransition(R.anim.right_in, R.anim.left_out);
					}else{
						dialogNet.showDialog(getActivity(), "Please connect to Internet", "OK", R.drawable.circle_uncheck);
					}
				}else{
					Snackbar.make(snack, "Please fill all fields", Snackbar.LENGTH_SHORT).show();
				}
			}else{
				Snackbar.make(snack, "Order should not be less than Rs.300", Snackbar.LENGTH_SHORT).show();
			}
						
			break;	
			
		default:
			
			int id = (v.getId()/5) - 1000;

			db.deleteItem(id);
			cart.cartItem(0);
			
			 // row is your row, the parent of the clicked button
            View column = (View) v.getParent();
            View row = (View) column.getParent();
            // container contains all the rows, you could keep a variable somewhere else to the container which you can refer to here
            ViewGroup container = ((ViewGroup)row.getParent());
            // delete the row and invalidate your view so it gets redrawn
            container.removeView(row);
            container.invalidate();
            updateTotal();
			
			break;
		}
	}
	
	class GetDropdown extends AsyncTask<String, String, String> {

		boolean status;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Please Wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub
			// Check for success tag

			String json = "";

			try {

				ServiceHandler sh = new ServiceHandler();

				json = sh.makeServiceCall(GET_DROPDOWN, ServiceHandler.GET);

				JSONArray jsonArr = new JSONArray(json);

				hm=new HashMap<String,Integer>();
				items = new String[jsonArr.length()];
				
				for(int i=0;i<jsonArr.length();i++){
					
					JSONObject jobj = jsonArr.getJSONObject(i);
					
					Integer idi = jobj.getInt("id");
					String area = jobj.getString("area");
					
					hm.put(area, idi);
					items[i] = area;
					
				}
					return "success";
			
			} catch (Exception e) {
				e.printStackTrace();
			}
			return "failure";

		}

		protected void onPostExecute(String value) {
			// dismiss the dialog once product deleted
			if (pDialog.isShowing()) {
				pDialog.dismiss();
			}

			if(value.equalsIgnoreCase("success")){
				autoAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,items);
					location.setAdapter(autoAdapter);
					location.setThreshold(1);
					
					if(isSigned){
						 new GetProfileDetails().execute();
//						signedInitialView();
					}else{
						notSignedView();
					}
					
			}

		}
	
	}
	
	class UpdateProfile extends AsyncTask<String, String, String> {

		boolean status;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Attempting login...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub
			// Check for success tag

			String json = "";

			try {

				String id = SharedPrefs.getUid(getActivity());
				String token = SharedPrefs.getToken(getActivity());
				String adrs = address.getText().toString().trim();
				String area = location.getText().toString().trim();
				loc_id = hm.get(area).toString();
				String pin = pincode.getText().toString().trim();
				
				// you need to encode ONLY the values of the parameters
				String param = "id=" + URLEncoder.encode(id, "UTF-8")
						+ "&token=" + URLEncoder.encode(token, "UTF-8")
						+ "&address=" + URLEncoder.encode(adrs, "UTF-8")
						+ "&location=" + URLEncoder.encode(loc_id, "UTF-8")
						+ "&pincode=" + URLEncoder.encode(pin, "UTF-8");

				ServiceHandler sh = new ServiceHandler();

				json = sh.makeServiceCall(UPDATE_PROFILE, ServiceHandler.POST, param);

				JSONObject jsonObj = new JSONObject(json);

				status = jsonObj.getBoolean("status");
				
				if (status) {
					
					return "success";
				} else {
					return "failure";
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			return json;

		}

		protected void onPostExecute(String value) {
			// dismiss the dialog once product deleted
			if (pDialog.isShowing()) {
				pDialog.dismiss();
			}
			
			if(value.equalsIgnoreCase("success")){
				new GetProfileDetails().execute();
			}
			
		}

	}
		
	public class GetProfileDetails extends AsyncTask<String, String, String> {

		boolean status;
		String semail,sfname,slname,smobile,saddress,scity,sstate,scountry,spincode,slocation;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Attempting login...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub
			// Check for success tag

			String json = "";

			try {

				String id = SharedPrefs.getUid(getActivity());
				String token = SharedPrefs.getToken(getActivity());

				// you need to encode ONLY the values of the parameters
				String param = "id=" + URLEncoder.encode(id, "UTF-8")
						+ "&token=" + URLEncoder.encode(token, "UTF-8");

				ServiceHandler sh = new ServiceHandler();

				json = sh.makeServiceCall(GET_PROFILE_INFO, ServiceHandler.GET, param);

				JSONArray jar = new JSONArray(json);
				
				JSONObject jsonObj = jar.getJSONObject(0);

				status = jsonObj.getBoolean("status");
				
				if (status) {

					JSONObject jsonData = jsonObj.getJSONObject("data");
					
					semail = jsonData.getString("email");
					sfname = jsonData.getString("fname");
					slname = jsonData.getString("lname");
					smobile = jsonData.getString("mobile");
					saddress = jsonData.getString("address");
					scity = jsonData.getString("city");
					sstate = jsonData.getString("state");
					scountry = jsonData.getString("country");
//					String spic = jsonData.getString("pic");
					spincode = jsonData.getString("pincode");
					slocation = jsonData.getString("location");
													
					return "success";
				} else {
					return "failure";

				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			return json;

		}

		protected void onPostExecute(String value) {
			// dismiss the dialog once product deleted
			if (pDialog.isShowing()) {
				pDialog.dismiss();
			}

			if (value.equalsIgnoreCase("success")) {

				if(!semail.equalsIgnoreCase("") || semail!=null){
					email.setText(semail);
					txtEmail.setText(semail);
				}
				
				if(!sfname.equalsIgnoreCase("") || sfname!=null){
					fname.setText(sfname);
				}
				
				if(!slname.equalsIgnoreCase("") || slname!=null){
					lname.setText(slname);
				}
				
				if(!smobile.equalsIgnoreCase("") || smobile!=null){
					mobile.setText(smobile);
				}
				
				if(!saddress.equalsIgnoreCase("") || saddress!=null){
					address.setText(saddress);
				}
				
				if(!scity.equalsIgnoreCase("") || scity!=null){
					chennai.setText(scity);
				}
				
				if(!sstate.equalsIgnoreCase("") || sstate!=null){
					tamilNadu.setText(sstate);
				}
				
				if(!scountry.equalsIgnoreCase("") || scountry!=null){
					india.setText(scountry);
				}
				
				if(!spincode.equalsIgnoreCase("") || spincode!=null){
					pincode.setText(spincode);
				}
				
				if(!slocation.equalsIgnoreCase("") || slocation!=null){
					location.setText(slocation);
				}
				
				txtAddress.setText(saddress+"\n"+slocation+"\n"+scity+" - "+spincode);
				
				signedInitialView();
				
//				new GetDropdown().execute();
				
			} else if (value.equalsIgnoreCase("failure")) {
				Toast.makeText(getActivity(), "Problem getting user info", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(getActivity(), value,
						Toast.LENGTH_SHORT).show();
			}

		}

	}
	
}


