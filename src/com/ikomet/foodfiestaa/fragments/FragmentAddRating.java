package com.ikomet.foodfiestaa.fragments;

import java.net.URLEncoder;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.adapter.RatingListAdapter;
import com.ikomet.foodfiestaa.beans.Reviews;
import com.ikomet.foodfiestaa.utils.DialogCustom;
import com.ikomet.foodfiestaa.utils.DividerItemDecoration;
import com.ikomet.foodfiestaa.utils.InternetCheck;
import com.ikomet.foodfiestaa.utils.ServiceHandler;
import com.ikomet.foodfiestaa.utils.SharedPrefs;

public class FragmentAddRating extends Fragment implements View.OnClickListener{

	View layout;
	ImageButton filter;
	RecyclerView listFeedback;
	EditText writeReview;
	ImageButton rateStar, enter;
	ProgressDialog pDialog;
	private static final String GET_REVIEWS_URL = "http://beta.foodfiestaa.com/getreviews";
	private static final String ADD_RATING_URL = "http://beta.foodfiestaa.com/addreview";
	ArrayList<Reviews> reviewsList;
	RatingListAdapter adapter;
	RatingBar ratingbar;
	View view3;
	int trigger = 0;
	RelativeLayout fragAddRating, noNet;
	ImageView smiley;
	DialogCustom dialogNet;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		layout = inflater.inflate(R.layout.fragment_add_rating, container, false);
		
		dialogNet = new DialogCustom();
		noNet = (RelativeLayout)layout.findViewById(R.id.no_net);
		fragAddRating = (RelativeLayout)layout.findViewById(R.id.rel_add_rate);
		smiley = (ImageView)layout.findViewById(R.id.img_sad);
		
		if(InternetCheck.isInternetConnected(getActivity())){
			fragAddRating.setVisibility(View.VISIBLE);
			noNet.setVisibility(View.GONE);
			
			initializeVar();
			
		}else{
			fragAddRating.setVisibility(View.GONE);
			noNet.setVisibility(View.VISIBLE);
			
			noNet.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(InternetCheck.isInternetConnected(getActivity())){
						
						Animation a = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_anim);
						a.setDuration(1000);
						smiley.startAnimation(a);
						
						new Handler().postDelayed(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								fragAddRating.setVisibility(View.VISIBLE);
								noNet.setVisibility(View.GONE);
								initializeVar();
							}
						}, 2000);
						
					}
				}
			});
			
		}
		
		return layout;
		
	}
	
	private void initializeVar(){
		
		filter = (ImageButton)layout.findViewById(R.id.img_filter);
		listFeedback = (RecyclerView)layout.findViewById(R.id.list_review);

		writeReview = (EditText)layout.findViewById(R.id.ed_write_review);
		rateStar = (ImageButton)layout.findViewById(R.id.bt_rate);
		enter = (ImageButton)layout.findViewById(R.id.bt_enter);
		ratingbar = (RatingBar)layout.findViewById(R.id.rate_dish);
		view3 = (View)layout.findViewById(R.id.view3);
		
		LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
		listFeedback.setLayoutManager(layoutManager);
		listFeedback.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
		listFeedback.setItemAnimator(new DefaultItemAnimator());
		
		filter.setOnClickListener(this);
		enter.setOnClickListener(this);
		rateStar.setOnClickListener(this);
		
		new GetReviews().execute();
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		switch (v.getId()) {
		case R.id.bt_enter:
				if(ratingbar.getRating()>0){
					if(InternetCheck.isInternetConnected(getActivity())){
						new AddRating().execute();
					}else{
						dialogNet.showDialog(getActivity(), "Please connect to Internet", "OK", R.drawable.circle_uncheck);
					}
				}else{
					Toast.makeText(getActivity(), "Please add a star rating for the dish", Toast.LENGTH_SHORT).show();
				}
			break;
 
		case R.id.bt_rate:
			if(trigger == 0){
				ratingbar.setVisibility(View.VISIBLE);
				view3.setVisibility(View.VISIBLE);
				trigger = 1;
			}else{
				ratingbar.setVisibility(View.GONE);
				view3.setVisibility(View.GONE);
				trigger = 0;
			}
			break;
			
		default:
			break;
		}
		
	}
	
	class AddRating extends AsyncTask<String, String, String> {
		
		boolean status;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Please Wait ...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}
		
		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			
			String json = "";
			
			try {
				
				String id = SharedPrefs.getUid(getActivity());
				String token = SharedPrefs.getToken(getActivity());
				String menu_id = SharedPrefs.getMenuId(getActivity());
				String message = writeReview.getText().toString().trim();
				String rating = String.valueOf(ratingbar.getRating());
				
			    String param = "id=" + URLEncoder.encode(id, "UTF-8") + "&token=" + URLEncoder.encode(token, "UTF-8")
			    				+"&menu_id=" + URLEncoder.encode(menu_id, "UTF-8") + "&message=" + URLEncoder.encode(message, "UTF-8")
			    				+"&rating=" + URLEncoder.encode(rating, "UTF-8");
			    			    
			    ServiceHandler sh = new ServiceHandler();
			    
			    json = sh.makeServiceCall(ADD_RATING_URL, ServiceHandler.POST, param);
			    
			    /*{
			        "status": "true",
			        "message": "reviews added"
			    }*/
			    
			    JSONObject jobj = new JSONObject(json);
			    
			    status = jobj.getBoolean("status");
			    
			    if(status){
			    	return "success";
			    }else{
			    	return jobj.toString();
			    }
				
			}catch(Exception e){
				
			}
			
			return null;
		}
		
		protected void onPostExecute(String value) {
			// dismiss the dialog once product deleted
			if (pDialog.isShowing()) {
				pDialog.dismiss();
			}
			
			if(status){
				if(value != null){
					if(value.equalsIgnoreCase("success")){
						Toast.makeText(getActivity(), "Review added successfully", Toast.LENGTH_SHORT).show();
						new GetReviews().execute();
					}else{
						Toast.makeText(getActivity(), ""+value, Toast.LENGTH_SHORT).show();
					}
				}
			}
			
		}

	}

	class GetReviews extends AsyncTask<String, String, String> {

		boolean status;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Please Wait ...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub
			// Check for success tag

			String json = "";

			try {
				
				String menu_id = SharedPrefs.getMenuId(getActivity());
				// you need to encode ONLY the values of the parameters
				String param = "menu_id=" + URLEncoder.encode(menu_id, "UTF-8");

				ServiceHandler sh = new ServiceHandler();

				json = sh.makeServiceCall(GET_REVIEWS_URL, ServiceHandler.GET, param);
				System.out.println(json);
				JSONObject jsonObj = new JSONObject(json);

				status = jsonObj.getBoolean("status");
				
				if (status) {

					reviewsList = new ArrayList<Reviews>();
					
			if(jsonObj.has("data")){
									
				JSONArray jarReview = jsonObj.getJSONArray("data");	
					
				for(int i=0;i<jarReview.length();i++){
				
				JSONObject jobjData = jarReview.getJSONObject(i);
				
				String name = jobjData.getString("name");
				String pic = "http://beta.foodfiestaa.com/"+jobjData.getString("pic").replace(" ", "%20");
				String message = jobjData.getString("message");
				String rating = jobjData.getString("rating");
				String date = jobjData.getString("date");
				String time = jobjData.getString("time");
				
				reviewsList.add(new Reviews(name, pic, message, rating, date, time));
				
				}
				
				return "success";
				
			}else{
				return jsonObj.getString("message");		
		     }
					
			} else {
				return "failure";
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			return json;

		}

		protected void onPostExecute(String value) {
			// dismiss the dialog once product deleted
			if (pDialog.isShowing()) {
				pDialog.dismiss();
			}

			 if(value!=null){
				 
				 if(value.equalsIgnoreCase("success")){
					 
					 if(reviewsList!=null && reviewsList.size()>0){
						adapter = new RatingListAdapter(getActivity(), reviewsList);
						listFeedback.setAdapter(adapter);
					 }
					 
				 }else{
					 Toast.makeText(getActivity(), ""+value, Toast.LENGTH_SHORT).show();
				 }
				 
			 }else{
				 
			 }
		}

	}

	
}
