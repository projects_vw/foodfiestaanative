package com.ikomet.foodfiestaa.fragments;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.activities.HowItWorks;
import com.ikomet.foodfiestaa.adapter.PagerAdapter;
import com.ikomet.foodfiestaa.adapter.PagerAdapterHowitworks;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class FragmentPagerHowCook extends Fragment {

	int count = PagerAdapterHowitworks.marg2;

	TextView title, descr;
	ImageView content;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View layout = inflater.inflate(
				R.layout.fragment_customer_howitworks_pagerelement, container,
				false);
		/*
		 * if(HowItWorks.check){ count=2; }else{ count = HowItWorks.position; }
		 */

		title = (TextView) layout.findViewById(R.id.txt_cook);
		content = (ImageView) layout.findViewById(R.id.img_how_consumer);
		descr = (TextView) layout.findViewById(R.id.txt_consumer);

		switch (count) {
		case 0:
			title.setText("Prepare Food");
			content.setImageResource(R.drawable.preparefood);
			descr.setText(R.string.prepare_food);
			break;
		case 1:
			title.setText("Deliver & Share");
			content.setImageResource(R.drawable.delivershare);
			descr.setText(R.string.deliver_share);
			break;
		case 2:
			title.setText("Earn Money");
			content.setImageResource(R.drawable.earnmoney);
			descr.setText(R.string.earn_money);
			break;

		default:
			break;
		}
		/*
		 * switch (count) { case 0: Log.e("test",
		 * "case "+count+": "+HowItWorks.iscook); if (HowItWorks.iscook) {
		 * title.setText("Prepare Food");
		 * content.setImageResource(R.drawable.preparefood);
		 * descr.setText(R.string.prepare_food); } else {
		 * title.setText("Find Favourite");
		 * content.setImageResource(R.drawable.findfavorites);
		 * descr.setText(R.string.find_favourites_desc); } break; case 1:
		 * Log.e("test", "case "+count+": "+HowItWorks.iscook); if
		 * (HowItWorks.iscook) { title.setText("Deliver & Share");
		 * content.setImageResource(R.drawable.delivershare);
		 * descr.setText(R.string.deliver_share); } else {
		 * title.setText("Order & Payment");
		 * content.setImageResource(R.drawable.orderpayment);
		 * descr.setText(R.string.order_payment_desc); } HowItWorks.check=false;
		 * break; case 2: Log.e("test", "case "+count+": "+HowItWorks.iscook);
		 * if (HowItWorks.iscook) { title.setText("Earn Money");
		 * content.setImageResource(R.drawable.earnmoney);
		 * descr.setText(R.string.earn_money); } else {
		 * title.setText("Enjoy Food");
		 * content.setImageResource(R.drawable.enjoyfood);
		 * descr.setText(R.string.enjoy_food_desc); } HowItWorks.check=true;
		 * break;
		 * 
		 * default: break; }
		 */
		return layout;

	}

}
