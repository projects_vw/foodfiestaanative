package com.ikomet.foodfiestaa.fragments;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.adapter.PagerAdapter;

import android.os.Bundle;



import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class Fragment_pager extends Fragment {
	int count = PagerAdapter.marg1;
	ImageView img_background;
	TextView txt_title;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_pager, container,
				false);
		img_background = (ImageView) view.findViewById(R.id.img_background);
		txt_title=(TextView)view.findViewById(R.id.txt_title);
		switch (count) {
		case 0:
			img_background.setImageResource(R.drawable.img_pager1_test);
			txt_title.setText("Strawberry topping dessert");
			break;
		case 1:
			img_background.setImageResource(R.drawable.img_pager_test2_two);
			txt_title.setText("pasta with fork");
						break;
		case 2:
			img_background.setImageResource(R.drawable.img_pager3);
			txt_title.setText("Chocolate Dessert");
			break;
		case 3:
			img_background.setImageResource(R.drawable.img_pager4_test);
			txt_title.setText("Grilled Chicken");
			break;
		case 4:
			img_background.setImageResource(R.drawable.img_pager5_test);
			txt_title.setText("2 dosa");
			break;
		default:
			img_background.setImageResource(R.drawable.img_pager1_test);
			txt_title.setText("Strawberry topping dessert");
			break;

		}

		return view;
	}
}
