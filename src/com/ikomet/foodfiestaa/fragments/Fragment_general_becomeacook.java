package com.ikomet.foodfiestaa.fragments;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.activities.CookSignIn;
import com.ikomet.foodfiestaa.activities.HomeActivity;
import com.ikomet.foodfiestaa.beans.CookGeneralInformation;
import com.ikomet.foodfiestaa.utils.DialogCustom;
import com.ikomet.foodfiestaa.utils.InternetCheck;
import com.ikomet.foodfiestaa.utils.ReadStream;
import com.ikomet.foodfiestaa.utils.Validator;

public class Fragment_general_becomeacook extends Fragment {

	Button btn_next_general;
	FragmentManager fragManager;
	ImageView img_circle1;
	FragmentTransaction transaction;
	Fragment frag;

	EditText email, pass, cnfm_pass, fname, lname, mobile, kitchenname;
	Validator validator;
	boolean validFields, nullCheck;

	ProgressDialog pDialog;

	public static final String REG_URL = "http://beta.foodfiestaa.com/cooksignupservice";
	View view;
	DialogCustom dialog;

	@Override
	public View onCreateView(final LayoutInflater inflater,
			final ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		view = inflater.inflate(R.layout.frag_general_becomeacook, container,
				false);

		dialog = new DialogCustom();
		initializeVariables();
		// email.addTextChangedListener(validator.watchemail(email));

		if (view != null)
			return view;
		else
			return null;
	}

	private void initializeVariables() {

		img_circle1 = (ImageView) view.findViewById(R.id.img_circle1);
		btn_next_general = (Button) view.findViewById(R.id.btn_next_general);

		email = (EditText) view.findViewById(R.id.edt_email);
		pass = (EditText) view.findViewById(R.id.edt_pwd);
		cnfm_pass = (EditText) view.findViewById(R.id.edt_cnfrm_pwd);
		fname = (EditText) view.findViewById(R.id.edt_first_name);
		lname = (EditText) view.findViewById(R.id.edt_second_name);
		mobile = (EditText) view.findViewById(R.id.edt_mobile_no);
		kitchenname = (EditText) view.findViewById(R.id.edt_kitchen_name);

		validator = new Validator(getActivity());

		validFields = validator.validateStep1(email, pass, cnfm_pass, fname,
				lname, mobile, kitchenname);


		btn_next_general.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				validFields = validator.validateStep1(email, pass, cnfm_pass,
						fname, lname, mobile, kitchenname);

				nullCheck = validator.nullCheckStep1(email, pass, cnfm_pass,
						fname, lname, mobile, kitchenname);

				if (validFields && nullCheck) {

					if (InternetCheck.isInternetConnected(getActivity())) {

						new CheckData().execute();

					} else {
						dialog.showDialog(getActivity(), "Please connect to Internet", "OK", R.drawable.circle_uncheck);
					}

					/*
					 * fragManager = getActivity().getSupportFragmentManager();
					 * transaction = fragManager.beginTransaction(); frag = new
					 * Frag_address_becomeacook();
					 * transaction.replace(R.id.frag_becomeacook, frag);
					 * transaction.commit();
					 */

				}
			}
		});
		
	}

	class CheckData extends AsyncTask<String, String, String> {

		boolean status;
		String errors = "";
		String mail, passw, cnfmpass, fnm, lnm, mob, kitchen;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Attempting login...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub
			// Check for success tag

			String json = "";

			URL url = null;
			try {

				url = new URL(REG_URL);

				mail = email.getText().toString().trim();
				passw = pass.getText().toString().trim();
				cnfmpass = cnfm_pass.getText().toString().trim();
				fnm = fname.getText().toString().trim();
				lnm = lname.getText().toString().trim();
				mob = mobile.getText().toString().trim();
				kitchen = kitchenname.getText().toString().trim();
				// you need to encode ONLY the values of the parameters
				String param = "email=" + URLEncoder.encode(mail, "UTF-8")
						+ "&kitchen=" + URLEncoder.encode(kitchen, "UTF-8");

				HttpURLConnection urlConnection = null;

				urlConnection = (HttpURLConnection) url.openConnection();
				// add reuqest header
				// urlConnection.setRequestMethod("POST");
				urlConnection.setDoOutput(true);
				// urlConnection.setRequestProperty("User-Agent", USER_AGENT);
				// urlConnection.setRequestProperty("Accept-Language",
				// "en-US,en;q=0.5");

				// Send post request
				urlConnection.setDoOutput(true);
				OutputStream wr = new BufferedOutputStream(
						urlConnection.getOutputStream());
				wr.write(param.getBytes());
				wr.flush();
				wr.close();

				int responseCode = urlConnection.getResponseCode();
				System.out.println("\nSending 'POST' request to URL : "
						+ REG_URL);
				System.out.println("Post parameters : " + param);
				System.out.println("Response Code : " + responseCode);

				try {
					InputStream in = null;

					in = new BufferedInputStream(urlConnection.getInputStream());

					ReadStream stream = new ReadStream();
					json = stream.readStream(in, url);
					// Toast.makeText(getApplicationContext(), json,
					// 1000).show();
				} finally {
					urlConnection.disconnect();
				}
				JSONObject jobj = new JSONObject(json);

				JSONObject jobj_msg = jobj.getJSONObject("message");

				boolean isemailExist = jobj_msg.has("email");

				if (isemailExist) {
					JSONArray jar_email = jobj_msg.getJSONArray("email");
					for (int i = 0; i < jar_email.length(); i++) {
						errors = errors + jar_email.getString(i) + "\n";
					}
				}

				boolean iskitchenExist = jobj_msg.has("kitchen");

				if (iskitchenExist) {
					JSONArray jar_kitchen = jobj_msg.getJSONArray("kitchen");
					for (int i = 0; i < jar_kitchen.length(); i++) {
						errors = errors + jar_kitchen.getString(i) + "\n";
					}
				}

				return errors;

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;

		}

		protected void onPostExecute(String value) {
			// dismiss the dialog once product deleted
			if (pDialog.isShowing()) {
				pDialog.dismiss();
			}

			if (!value.equalsIgnoreCase("")) {
				Toast.makeText(getActivity(), value, Toast.LENGTH_SHORT).show();

			} else {

				CookGeneralInformation generalInfo = new CookGeneralInformation(
						mail, passw, cnfmpass, fnm, lnm, mob, kitchen);

				fragManager = getActivity().getSupportFragmentManager();
				transaction = fragManager.beginTransaction();
				frag = new Frag_address_becomeacook(generalInfo);
				transaction.replace(R.id.frag_becomeacook, frag);
				transaction.commit();

			}

		}

	}

}
