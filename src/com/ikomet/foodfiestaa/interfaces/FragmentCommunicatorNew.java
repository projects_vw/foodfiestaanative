package com.ikomet.foodfiestaa.interfaces;

import java.util.HashMap;

public interface FragmentCommunicatorNew {
	public void passnewDataToFragment(String val1, String val2);
	public void checkedData(HashMap<String, Integer> hashmap);
}
