package com.ikomet.foodfiestaa.interfaces;

public interface FragmentCommunicator{
	   public void passDataToFragment();
	   public void change_state();
	   public void Add_click();
	   public void locationToolbar(boolean isLocationSerach);
	}
