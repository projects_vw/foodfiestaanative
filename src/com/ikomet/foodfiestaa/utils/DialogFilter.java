package com.ikomet.foodfiestaa.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupExpandListener;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.adapter.AdapterCheckBox;
import com.ikomet.foodfiestaa.adapter.AdapterCheckBoxFoodSearch;
import com.ikomet.foodfiestaa.beans.ChildCookDetail;
import com.ikomet.foodfiestaa.interfaces.FragmentCommunicatorNew;

public class DialogFilter extends Dialog {

	Context context;
	ExpandableListView exp_list;

	LinkedHashMap<Integer, String> diet2, order2, cusine2;
	List<ChildCookDetail> header_name;
	List<String> header_name1, diet1, order1, cusine1, distance1;
	HashMap<String, List<String>> child1;
	public HashMap<String, LinkedHashMap<Integer, String>> child2;

	int temp_pos = -1;
	private HashMap<Integer, boolean[]> mChildCheckStates;
	FragmentCommunicatorNew frag_new;
	int var_food_search;

	public DialogFilter(Context context, FragmentCommunicatorNew frag_new,LinkedHashMap<Integer, String> diet2,LinkedHashMap<Integer, String> cusine2,
			int var_foodSearch) {
		super(context, R.style.cust_dialog);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.var_food_search = var_foodSearch;
		this.diet2=diet2;
		this.cusine2=cusine2;
		this.frag_new = frag_new;
	}
	
	
	public DialogFilter(Context context, FragmentCommunicatorNew frag_new,
			int var_foodSearch) {
		super(context, R.style.cust_dialog);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.var_food_search = var_foodSearch;
		this.frag_new = frag_new;
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		dismiss();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);

		// getWindow().setBackgroundDrawable(new ColorDrawable(0));
		setContentView(R.layout.dialog_filter);
		exp_list = (ExpandableListView) findViewById(R.id.exp_list);
		header_name = new ArrayList<>();
		header_name.add(new ChildCookDetail("Diet Type", R.drawable.note_icon,
				""));
		header_name.add(new ChildCookDetail("Order Type", R.drawable.order_new,
				""));
		header_name.add(new ChildCookDetail("Cusine Type",
				R.drawable.graduation_new, ""));
		order2 = new LinkedHashMap<>();
		order2.put(1, "Today's Special");
		order2.put(2, "Pre-Order");
		child2 = new HashMap<>();
		if(diet2 !=null && cusine2!=null)
		{
		child2.put(header_name.get(0).getName(), diet2);
		child2.put(header_name.get(1).getName(), order2);
		child2.put(header_name.get(2).getName(), cusine2);
		if(var_food_search==0)
		{
		header_name.add(new ChildCookDetail("Distance",
				R.drawable.yellow_location, ""));
		AdapterCheckBox adapter_check = new AdapterCheckBox(
				context, header_name, child2, frag_new);
		exp_list.setAdapter(adapter_check);
	}else{
		AdapterCheckBoxFoodSearch adapter_check = new AdapterCheckBoxFoodSearch(
				context, header_name, child2, frag_new);
		exp_list.setAdapter(adapter_check);
	}
		exp_list.setOnGroupExpandListener(new OnGroupExpandListener() {
			int previousGroup = -1;

			@Override
			public void onGroupExpand(int groupPosition) {
				if (groupPosition != previousGroup)
					exp_list.collapseGroup(previousGroup);
				previousGroup = groupPosition;
			}
		});
		//new AsyncDietType().execute();

		WindowManager.LayoutParams WMLP = getWindow().getAttributes();
		WMLP.dimAmount = 0.0f; // Dim level. 0.0 - no dim, 1.0 - completely
		setTitle("Filter");
		WMLP.gravity = Gravity.TOP | Gravity.RIGHT;
		WMLP.x = 0; // x position
		WMLP.y = 230; // y position

		getWindow().setAttributes(WMLP);

	}

	
}
}
