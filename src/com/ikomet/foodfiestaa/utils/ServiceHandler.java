package com.ikomet.foodfiestaa.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class ServiceHandler {

	static String response = null;
	public final static int GET = 1;
	public final static int POST = 2;
	private final String USER_AGENT = "Mozilla/5.0";

	public ServiceHandler() {

	}

	/**
	 * Making service call
	 * 
	 * @url - url to make request
	 * @method - http request method
	 * */

	public String makeServiceCall(String url, int method) {
		return this.makeServiceCall(url, method, null);
	}

	/**
	 * Making service call
	 * 
	 * @url - url to make request
	 * @method - http request method
	 * @params - http request params
	 * */
	public String makeServiceCall(String url, int method, String params) {

		URL urll = null;
		try {

			if (method == GET) {
				if (params == null) {
					urll = new URL(url);

				} else {
					urll = new URL(url + "?" + params);
				}

				HttpURLConnection urlConnection = null;

				urlConnection = (HttpURLConnection) urll.openConnection();

				try {
					InputStream in = null;

					in = new BufferedInputStream(urlConnection.getInputStream());

					response = readStream(in);

				} finally {
					urlConnection.disconnect();
				}
				//System.out.println(response);
				// return json;

			} else {

				urll = new URL(url);

				HttpURLConnection urlConnection = null;

				urlConnection = (HttpURLConnection) urll.openConnection();
				// add reuqest header
				// urlConnection.setRequestMethod("POST");
				urlConnection.setDoOutput(true);
				urlConnection.setRequestProperty("User-Agent", USER_AGENT);
				urlConnection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

				// Send post request
				urlConnection.setDoOutput(true);
				OutputStream wr = new BufferedOutputStream(
						urlConnection.getOutputStream());
				wr.write(params.getBytes());
				wr.flush();
				wr.close();

				int responseCode = urlConnection.getResponseCode();
				System.out.println("\nSending 'POST' request to URL : " + urll);
				System.out.println("Post parameters : " + params);
				System.out.println("Response Code : " + responseCode);

				try {
					InputStream in = null;

					in = new BufferedInputStream(urlConnection.getInputStream());

					response = readStream(in);
					// Toast.makeText(getApplicationContext(), json,
					// 1000).show();
				} finally {
					urlConnection.disconnect();
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;

	}

	public String readStream(InputStream in) {
		String data = "";
		try {

			BufferedReader reader = new BufferedReader(
					new InputStreamReader(in));
			StringBuilder result = new StringBuilder();
			String line;
			while ((line = reader.readLine()) != null) {
				result.append(line);
			}

			System.out.println(result.toString());
			data = result.toString();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return data;

	}

}
