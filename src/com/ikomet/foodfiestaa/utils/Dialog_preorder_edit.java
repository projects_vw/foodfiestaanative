package com.ikomet.foodfiestaa.utils;

import java.io.UnsupportedEncodingException;

import java.net.URLEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.fragments.Frag_signaturedishes;
import com.ikomet.foodfiestaa.utils.DialogTodaySpecial.async_editdish;
import com.tmxlr.lib.driodvalidatorlight.Form;
import com.tmxlr.lib.driodvalidatorlight.helper.Range;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

public class Dialog_preorder_edit extends Dialog {
	ProgressDialog pDialog;
	TextView txt_foodshortname;
	Context context;
	EditText edt_preorder;
	Form form;
	Button btn_submit;
	Spinner spinner;
	String preorder_maxorder, foodshortname, mid;
	ImageView img_cross;
	FragmentManager manager;
	FragmentTransaction transaction;
	Fragment frag;

	public Dialog_preorder_edit(Context context, String preorder_maxorder,
			String foodshortname, String mid) {
		super(context);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.preorder_maxorder = preorder_maxorder;
		this.foodshortname = foodshortname;
		this.mid = mid;
		pDialog=new ProgressDialog(context);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		getWindow().setBackgroundDrawable(new ColorDrawable(0));
		setContentView(R.layout.dialog_preorderedit);
		btn_submit=(Button)findViewById(R.id.btn_submit);
		String[] categories = { "1" };
		spinner = (Spinner) findViewById(R.id.edt_spnr_days);
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context,
				android.R.layout.simple_spinner_item, categories);

		// Drop down layout style - list view with radio button
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(dataAdapter);
		txt_foodshortname = (TextView) findViewById(R.id.txt_foodshortname);
		edt_preorder = (EditText) findViewById(R.id.edt_preorder);
		img_cross = (ImageView) findViewById(R.id.img_cross);
		/* edt_spnr_days=(EditText)findViewById(R.id.edt_spnr_days); */

		txt_foodshortname.setText(foodshortname);
		edt_preorder.setText(preorder_maxorder);
		btn_submit.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				form = new Form(context);
				form.checkValue(edt_preorder, Range.equalsOrMoreAndEqualsOrLess(1, 100), "MaxOrder is 1-100");
				if(form.validate()){
					
					new async_editdish().execute(edt_preorder.getText().toString(),"1");
				}
				
			}
		});
		img_cross.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dismiss();
			}
		});
	}
	class async_editdish extends AsyncTask<String, Void, String>
	{
		boolean status2;
		String result,param,result2,param2,result3;
		ServiceHandler handler;
		String id = SharedPrefs.getUid(context), token = SharedPrefs
				.getToken(context);
		/*http://50.57.127.125:10131/editdishservice
	*/
		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			handler=new ServiceHandler();
			Log.i("param value", params[1]);
			try {
				param="id="+URLEncoder.encode(id,"UTF-8")+"&token="+URLEncoder.encode(token, "UTF-8")+"&menuid="+URLEncoder.encode(mid, "UTF-8")+"&preorder_maxorder="+URLEncoder.encode(params[0], "UTF-8")/*+"&preorder_active="+URLEncoder.encode("1", "UTF-8")+"&todayspecial_active="+URLEncoder.encode("0", "UTF-8")*/;
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			result=handler.makeServiceCall("http://beta.foodfiestaa.com/editdishservice",2, param);
			Log.e("the result is ", result);
	try {
		JSONObject obj1=new JSONObject(result);
		boolean status=obj1.getBoolean("status");
		if(status)
		{
			param2="id="+URLEncoder.encode(id,"UTF-8")+"&token="+URLEncoder.encode(token, "UTF-8")+"&menuid="+URLEncoder.encode(mid, "UTF-8")+"&preorder_flag="+URLEncoder.encode("1", "UTF-8");
			Log.e("inside", "success status");
			result2=handler.makeServiceCall("http://beta.foodfiestaa.com/preorderservice", 1, param2);
			Log.e("result2 is", result2);
			JSONObject obj2=new JSONObject(result2);
			status2=obj2.getBoolean("status");
			if(status2)
			{
				dismiss();
//result3=handler.makeServiceCall("http://beta.foodfiestaa.com/todayservice", 1, param2);

				return "yes";
			}
			else return "no";
		}
		else
		{
			Log.e("status_value", ""+status);
		}
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (UnsupportedEncodingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
	
			if(result=="yes")
			{
				final Dialog success=new Dialog(context);
				success.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
				success.getWindow().setBackgroundDrawable(new ColorDrawable(0));
				success.setContentView(R.layout.dialog_verification_signup);
				TextView txt_content=(TextView)success.findViewById(R.id.txt_content);
				Button btn_ok=(Button)success.findViewById(R.id.btn_ok);
				txt_content.setText("Changes Will be Reflected once it is approved by FoodFiestaa");
				
				btn_ok.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						success.dismiss();
						FragmentActivity activity=(FragmentActivity)context;
						manager = activity.getSupportFragmentManager();
						transaction = manager.beginTransaction();
						frag=new Frag_signaturedishes();
						transaction.replace(R.id.realtabcontent, frag);
						transaction.commit();
					
					}
				});
				
				success.show();
				
			}
			else
			{}
			pDialog.dismiss();
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog.setTitle("Loading..");
			pDialog.setMessage("Please wait");
			pDialog.show();
		}
		}
}
