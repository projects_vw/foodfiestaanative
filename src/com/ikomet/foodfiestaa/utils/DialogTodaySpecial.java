package com.ikomet.foodfiestaa.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.activities.ContactUs;
import com.ikomet.foodfiestaa.fragments.Frag_signaturedishes;
import com.tmxlr.lib.driodvalidatorlight.Form;
import com.tmxlr.lib.driodvalidatorlight.helper.Range;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class DialogTodaySpecial extends Dialog {

	Button btn_submit;
	String today_specialmaxorder, foodshortname;
	Context context;
	ImageView img_cross;
	TextView txt_foodshortname;
	EditText edt_todayspecial;
	String mid;
	Form form;
	ProgressDialog pDialog;
	FragmentManager manager;
	FragmentTransaction transaction;
	Fragment frag;

	public DialogTodaySpecial(Context context, String today_specialmaxorder,
			String foodshortname,String mid) {
		super(context);
		// TODO Auto-generated constructor stub
		this.today_specialmaxorder = today_specialmaxorder;
		this.foodshortname = foodshortname;
		this.context = context;
		this.mid=mid;
		pDialog=new ProgressDialog(context);

	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		getWindow().setBackgroundDrawable(new ColorDrawable(0));
		setContentView(R.layout.dialog_todayspecial);
		btn_submit=(Button)findViewById(R.id.btn_submit);
		txt_foodshortname=(TextView)findViewById(R.id.txt_foodshortname);
		edt_todayspecial=(EditText)findViewById(R.id.edt_todayorder);
		edt_todayspecial.setText(today_specialmaxorder);
		txt_foodshortname.setText(foodshortname);
		img_cross = (ImageView) findViewById(R.id.img_cross);
		btn_submit.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				form = new Form(context);
				form.checkValue(edt_todayspecial, Range.equalsOrMoreAndEqualsOrLess(1, 100), "MaxOrder is 1-100");
				if(form.validate()){
					
					new async_editdish().execute(edt_todayspecial.getText().toString());
				}
			}
		});
		img_cross.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dismiss();
			}
		});
	}
	class async_editdish extends AsyncTask<String, Void, String>
	{
		String result,param,param2,result2;
		ServiceHandler handler;
		boolean status,status2;
		String id = SharedPrefs.getUid(context), token = SharedPrefs
				.getToken(context);
	
		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			handler=new ServiceHandler();
			try {
				param="id="+URLEncoder.encode(id,"UTF-8")+"&token="+URLEncoder.encode(token, "UTF-8")+"&menuid="+URLEncoder.encode(mid, "UTF-8")+"&todayspecial="+URLEncoder.encode(params[0], "UTF-8");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			result=handler.makeServiceCall("http://beta.foodfiestaa.com/editdishservice", 2, param);
			Log.e("the result is ", result);
			try {
				JSONObject obj1=new JSONObject(result);
				status=obj1.getBoolean("status");
			if(status)
				{
					param2="id="+URLEncoder.encode(id,"UTF-8")+"&token="+URLEncoder.encode(token, "UTF-8")+"&menuid="+URLEncoder.encode(mid, "UTF-8")+"&todayspecial_flag="+URLEncoder.encode("1", "UTF-8");
					result2=handler.makeServiceCall("http://beta.foodfiestaa.com/todayservice", 1, param2);
					Log.e("result2 is", result2);
					JSONObject obj2=new JSONObject(result2);
					status2=obj2.getBoolean("status");
					if(status2)
					{
						
		//result3=handler.makeServiceCall("http://beta.foodfiestaa.com/todayservice", 1, param2);

						return "yes";
					}
					else return "no";
				}
				else
				{
					Log.e("status_value", ""+status);
					
				}
			} catch (JSONException | UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(result=="yes")
			{
				
				Log.e("the result of postexecute is ", "Yes");
				
				FragmentActivity activity=(FragmentActivity)context;
				manager = activity.getSupportFragmentManager();
				transaction = manager.beginTransaction();
				frag=new Frag_signaturedishes();
				transaction.replace(R.id.realtabcontent, frag);
				transaction.commit();
				dismiss();
			}
			else
			{Log.e("the result of postexecute is ", "NO");}

			pDialog.dismiss();
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog.setTitle("Loading..");
			pDialog.setMessage("Please wait");
			pDialog.show();
		}
		}
}
