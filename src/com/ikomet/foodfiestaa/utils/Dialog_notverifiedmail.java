package com.ikomet.foodfiestaa.utils;

import com.ikomet.foodfiestaa.R;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

public class Dialog_notverifiedmail extends Dialog implements android.view.View.OnClickListener{

	Context context;
	ImageView btn_resend_mail,btn_close;
	public Dialog_notverifiedmail(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		this.context=context;
		Log.e("we are inside", "notverifiedmail class");
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		setContentView(R.layout.dialog_notverifiedaccount);
		btn_resend_mail=(ImageView)findViewById(R.id.btn_resend_mail);
		btn_close=(ImageView)findViewById(R.id.btn_close);
		btn_resend_mail.setOnClickListener(this);
		btn_close.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int view_id=v.getId();
		if(view_id==R.id.btn_close)
		{
			dismiss();
		}
		else if(view_id==R.id.btn_resend_mail)
		{
			
		}
		else{
			
		}
	}

}
