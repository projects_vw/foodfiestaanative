package com.ikomet.foodfiestaa.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class InternetCheck {

	public static boolean isInternetConnected(Context ctx) {
		ConnectivityManager connectivityMgr = (ConnectivityManager) ctx
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo wifi = connectivityMgr
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		NetworkInfo mobile = connectivityMgr
				.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		
		if (wifi != null) {
			if (wifi.isConnected()) {
				if(wifi.isConnectedOrConnecting()){
					return true;
				}
			}
		}
		if (mobile != null) {
			if (mobile.isConnected()) {
				if(mobile.isConnectedOrConnecting()){
					return true;
				}
			}
		}
		return false;
	}
	
}
