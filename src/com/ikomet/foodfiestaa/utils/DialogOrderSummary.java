package com.ikomet.foodfiestaa.utils;

import java.util.ArrayList;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.beans.OrderedItems;
import com.ikomet.foodfiestaa.beans.Orders;

public class DialogOrderSummary extends Dialog {

	Context context;
	ImageView close;
	TextView itemName, Quantity, Price, Total, subTotalTitle, subTotal;
	String item_name = "", qty = "", price = "", total = "";
	ArrayList<Orders> orderList;
	int groupPosition;

	public DialogOrderSummary(Context context, ArrayList<Orders> orderList,
			int groupPosition) {
		super(context);
		this.context = context;
		this.orderList = orderList;
		this.groupPosition = groupPosition;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_order_summary);

		itemName = (TextView) findViewById(R.id.txt_item_name);
		Quantity = (TextView) findViewById(R.id.txt_quantity);
		Price = (TextView) findViewById(R.id.txt_price);
		Total = (TextView) findViewById(R.id.txt_total);
		close = (ImageView) findViewById(R.id.img_exit);
		subTotalTitle = (TextView) findViewById(R.id.title_subtotal);
		subTotal = (TextView) findViewById(R.id.txt_subtotal);
		
		ArrayList<OrderedItems> items = orderList.get(groupPosition)
				.getOrItems();
		for (int i = 0; i < items.size(); i++) {
			if(i == items.size()-1){
				item_name = item_name + items.get(i).getItem_name();
				qty = qty + items.get(i).getQty();
				price = price + "₹ " + items.get(i).getPrice();
				total = total + "₹ " + Double.parseDouble(items.get(i).getQty())
						* Double.parseDouble(items.get(i).getPrice());
			}else{
				item_name = item_name + items.get(i).getItem_name() + "\n\n";
				qty = qty + items.get(i).getQty() + "\n\n";
				price = price + "₹ " + items.get(i).getPrice() + "\n\n";
				total = total + "₹ " + Double.parseDouble(items.get(i).getQty())
						* Double.parseDouble(items.get(i).getPrice()) + "\n\n";
			}
			
		}

		itemName.setText(item_name);
		Quantity.setText(qty);
		Price.setText(price);
		Total.setText(total);
		subTotalTitle.setText("Subtotal\n\n Shipping\n\n Total");
		subTotal.setText("₹ "+orderList.get(groupPosition).getTotal_price()+"\n\n"+"Free\n\n ₹ "+orderList.get(groupPosition).getTotal_price());

		close.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dismiss();
			}
		});

	}

}
