package com.ikomet.foodfiestaa.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class SharedPrefs {

    public static final String EMAIL = "email";
    public static final String TOKEN = "token";
    public static final String UNAME = "username";
    public static final String UID = "userid";
    public static final String PASS = "password";
    public static final String USER = "user";
    public static final String SOCIAL = "social";
    public static final String MENU_ID = "menu_id";
    public static final String LAT = "latitude";
    public static final String LONG = "longitude";
    
    public static final String CHECK_STORE = "checkstore";
    
    public static SharedPreferences prefLogin;
	public static final String MyPREFERENCES = "MyPrefs";
    
	public static void put_FragmentState(Context context, String Fragment_name,
			int var) {
		prefLogin = context.getSharedPreferences(MyPREFERENCES,
				Context.MODE_PRIVATE);
		Editor editor = prefLogin.edit();
		editor.putInt(Fragment_name, var);
		editor.commit();
	}

	public static int get_FragmentState(Context context, String Fragment_name) {
		prefLogin = context.getSharedPreferences(MyPREFERENCES,
				Context.MODE_PRIVATE);
		int int_state = prefLogin.getInt(Fragment_name, 0);
		return int_state;

	}
    
    public static void writeSP(Context context, String username, String email, String token, String password){
     Log.e("sharedpref", "values are :"+username+email+token+password);
    	prefLogin=context.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
       	 Editor editor = prefLogin.edit();
       	 editor.putString(UNAME, username);
         editor.putString(EMAIL, email);
       	 editor.putString(TOKEN, token);
       	 editor.putString(PASS, password);
         editor.commit();
        
    }
    
    public static void putLat(Context context, String Lat){
        Log.e("sharedpref", "values are :"+Lat);
       	prefLogin=context.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
          	 Editor editor = prefLogin.edit();
          	 editor.putString(LAT, Lat);
             editor.commit();
      }
    
    public static void putLong(Context context, String Long){
        Log.e("sharedpref", "values are :"+Long);
       	prefLogin=context.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
          	 Editor editor = prefLogin.edit();
          	 editor.putString(LONG, Long);
             editor.commit();
       }
    
    public static void putLong(Context context, String userid, String token, String user){
        Log.e("sharedpref", "values are :"+userid+token);
       	prefLogin=context.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
          	 Editor editor = prefLogin.edit();
          	 editor.putString(UID, userid);
             editor.putString(TOKEN, token);
             editor.putString(USER, user);
          	 editor.commit();
           
       }
    
    public static void putSP(Context context, String userid, String token, String user){
        Log.e("sharedpref", "values are :"+userid+token);
       	prefLogin=context.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
          	 Editor editor = prefLogin.edit();
          	 editor.putString(UID, userid);
             editor.putString(TOKEN, token);
             editor.putString(USER, user);
          	 editor.commit();
           
       }
    
    public static void putSocialLogin(Context context, String type){
        Log.e("sharedpref", "values are :"+type);
       	prefLogin=context.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
          	 Editor editor = prefLogin.edit();
          	 editor.putString(SOCIAL, type);
             editor.commit();
       }
    
    public static void putMenuId(Context context, String menu_id){
        Log.e("sharedpref", "values are :"+menu_id);
       	prefLogin=context.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
          	 Editor editor = prefLogin.edit();
          	 editor.putString(MENU_ID, menu_id);
             editor.commit();
       }
	   
    public static String getSP(Context context){
    	prefLogin=context.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
    	String username = prefLogin.getString(UNAME, "");
    	String email = prefLogin.getString(EMAIL, "");
    	String token = prefLogin.getString(TOKEN, "");
    	String password = prefLogin.getString(PASS, "");
    	 Log.e("sharedretr", "Ret values are :"+username+email+token+password);
    	/*String username = "visalatchi";
    	String email = "visalatchis.m@gmail.com";
    	String token = "3215656648686564";*/
    	
    	if(!username.equalsIgnoreCase("")&&!email.equalsIgnoreCase("")&&!token.equalsIgnoreCase("")&&!password.equalsIgnoreCase("")){
    	
    	return username+"/"+email+"/"+token+"/"+password;
    	
    	}else{
    		
    		return null;
    	}
//    	return email;
//    	return token;
    }
    
    public static boolean hasSPData(Context context){
    	prefLogin=context.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
    	String userid = prefLogin.getString(UID, "");
    	String token = prefLogin.getString(TOKEN, "");
    	Log.e("sharedretr", "Ret values are :"+userid+token);
    	/*String username = "visalatchi";
    	String email = "visalatchis.m@gmail.com";
    	String token = "3215656648686564";*/
    	
    	if(!userid.equalsIgnoreCase("")&&!token.equalsIgnoreCase("")){
    	
    	return true;
    	
    	}else{
    		
    		return false;
    	}

    }
    
    public static boolean canGetSPData(Context context){
    	prefLogin=context.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
    	String username = prefLogin.getString(UNAME, "");
    	String email = prefLogin.getString(EMAIL, "");
    	String token = prefLogin.getString(TOKEN, "");
    	String password = prefLogin.getString(PASS, "");
    	 Log.e("sharedretr", "Ret values are :"+username+email+token+password);
    	/*String username = "visalatchi";
    	String email = "visalatchis.m@gmail.com";
    	String token = "3215656648686564";*/
    	
    	if(!username.equalsIgnoreCase("")&&!email.equalsIgnoreCase("")&&!token.equalsIgnoreCase("")&&!password.equalsIgnoreCase("")){
    	
    	return true;
    	
    	}else{
    		
    		return false;
    	}

    }
    
    public static String getSocialLogin(Context context){
    	prefLogin=context.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
    	String type = prefLogin.getString(SOCIAL, "");
    
    	if(!type.equalsIgnoreCase("")){
    	
    	return type;
    	
    	}else{
    		
    		return null;
    	}
    }
    
    public static String getLat(Context context){
    	prefLogin=context.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
    	String type = prefLogin.getString(LAT, "");
    
    	if(!type.equalsIgnoreCase("")){
    	
    	return type;
    	
    	}else{
    		
    		return null;
    	}
    }
    
    public static String getLong(Context context){
    	prefLogin=context.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
    	String type = prefLogin.getString(LONG, "");
    
    	if(!type.equalsIgnoreCase("")){
    	
    	return type;
    	
    	}else{
    		
    		return null;
    	}
    }
    
    public static void removeLatLong(Context context){
    	prefLogin=context.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
    	Editor editor = prefLogin.edit();
    	editor.remove(LAT);
    	editor.remove(LONG);
    	editor.commit();
    }
    
    public static String getMenuId(Context context){
    	prefLogin=context.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
    	String menu_id = prefLogin.getString(MENU_ID, "");
    
    	if(!menu_id.equalsIgnoreCase("")){
    	
    	return menu_id;
    	
    	}else{
    		
    		return null;
    	}
    }
    
    public static String getEmail(Context context){
    	prefLogin=context.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
    	String email = prefLogin.getString(EMAIL, "");
    
    	if(!email.equalsIgnoreCase("")){
    	
    	return email;
    	
    	}else{
    		
    		return null;
    	}
    }
    
    public static String getToken(Context context){
    	prefLogin=context.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
    	String token = prefLogin.getString(TOKEN, "");
    
    	if(!token.equalsIgnoreCase("")){
    	
    	return token;
    	
    	}else{
    		
    		return null;
    	}
    }
    
    public static String getUid(Context context){
    	prefLogin=context.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
    	String userid = prefLogin.getString(UID, "");
    	    	
    	if(!userid.equalsIgnoreCase("")){
    	
    	return userid;
    	
    	}else{
    		
    		return null;
    	}
    }
    
    public static String getUname(Context context){
    	prefLogin=context.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
    	String username = prefLogin.getString(UNAME, "");
    	    	
    	if(!username.equalsIgnoreCase("")){
    	
    	return username;
    	
    	}else{
    		
    		return null;
    	}
    }
    
    public static String getPassword(Context context){
    	prefLogin=context.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
    	String password = prefLogin.getString(PASS, "");
    	    	    	
    	if(!password.equalsIgnoreCase("")){
    	
    	return password;
    	
    	}else{
    		
    		return null;
    	}
    }
    
    public static String getUser(Context context){
    	prefLogin=context.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
    	String user = prefLogin.getString(USER, "");
    	    	    	
    	if(!user.equalsIgnoreCase("")){
    	
    	return user;
    	
    	}else{
    		
    		return null;
    	}
    }
    
    public static void changeToken(Context context, String token){
    	prefLogin=context.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
    	Editor editor = prefLogin.edit();
    	editor.putString(TOKEN, token);
    	editor.commit();
    	 Log.e("New token", "New token is :"+token);
    	
    }
    
    public static void put_locationId(Context context, String location_id) {
		prefLogin = context.getSharedPreferences(MyPREFERENCES,
				Context.MODE_PRIVATE);
		Editor editor = prefLogin.edit();
		editor.putString("location_id", location_id);
		editor.commit();
	}

	public static String get_LocationId(Context context) {
		prefLogin = context.getSharedPreferences(MyPREFERENCES,
				Context.MODE_PRIVATE);
		String username = prefLogin.getString("location_id", "");
		return username;
	}
    
	public static void clearLocationId(Context context) {
		prefLogin = context.getSharedPreferences(MyPREFERENCES,
				Context.MODE_PRIVATE);
		Editor editor = prefLogin.edit();
		editor.remove("location_id");
		editor.commit();
		System.out.println("Location id value is "+prefLogin.getString("location_id", ""));
	
	}
	
    public static void clearSP(Context context){
    	prefLogin=context.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
    	Editor editor = prefLogin.edit();
    	editor.clear();
    	editor.commit();
    }
            
    public static void updateStorecheck(Context context, boolean value){
    	prefLogin=context.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
    	Editor editor = prefLogin.edit();
    	editor.clear();
    	editor.commit();
    }
    
    public static boolean getStorecheck(Context context){
    	prefLogin=context.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
    	boolean value = prefLogin.getBoolean(CHECK_STORE, false);
    	return value;
    }
    
}
