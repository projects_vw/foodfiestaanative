package com.ikomet.foodfiestaa.utils;

import com.ikomet.foodfiestaa.R;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class DialogCustom {

//	Context context;
	Dialog dialog;
	TextView txtTitle;
	ImageView imgCross;
	Button ok;

	/*public DialogCustom(Context context, Dialog dialog){
		this.context = context;
		this.dialog = dialog;
	}*/
	
	public void showDialog(Context context, String message, String btnText, int ImageResource){
		dialog = new Dialog(context);
		dialog.getWindow().requestFeature(1);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
		dialog.setContentView(R.layout.dialog_verification_signup);
//		dialog.setCancelable(false);
		txtTitle = (TextView)dialog.findViewById(R.id.txt_content);
		imgCross = (ImageView)dialog.findViewById(R.id.img_tick);
		ok = (Button) dialog.findViewById(R.id.btn_ok);
		txtTitle.setText(message);
		imgCross.setImageResource(ImageResource);
		ok.setText(btnText);
		ok.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		dialog.show();
	}
	
	public void dismissDialog(){
		dialog.dismiss();
	}
	
}
