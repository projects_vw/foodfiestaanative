package com.ikomet.foodfiestaa.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONException;
import org.json.JSONObject;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.activities.CookChangePassword;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class DialogForgotPwd extends Dialog implements View.OnClickListener{

	Context context;
	Button btn_submit,btn_cancel;
	ProgressDialog pDialog;
	String usertype;
	EditText edt_email;
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = 
		    Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

	public DialogForgotPwd(Context context,String usertype) {
		super(context);
		this.context=context;
		this.usertype=usertype;
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		setContentView(R.layout.dialog_forgotpassword);
		btn_submit=(Button)findViewById(R.id.btn_submit);
		btn_cancel=(Button)findViewById(R.id.btn_cancel);
		edt_email=(EditText)findViewById(R.id.edt_email);
		edt_email.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(edt_email.getText().toString().equalsIgnoreCase(""))
				{
					edt_email.setError("Please fill a field");
				}else if(!validate(edt_email.getText().toString()))
				{
					edt_email.setError("Enter a valid email id");
					
				}
			}
			
		});
		btn_submit.setOnClickListener(this);
		btn_cancel.setOnClickListener(this);
		pDialog=new ProgressDialog(context);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id=v.getId();
		if(id==R.id.btn_cancel)
		{
			dismiss();
		}
		if(id==R.id.btn_submit)
		{
		if(validate(edt_email.getText().toString()))
		{
			new AsyncCookForgotPwd().execute();
		}
		else
		{	
			if(edt_email.getText().toString().equalsIgnoreCase(""))
				edt_email.setError("Please fill the field");
			else
			edt_email.setError("Enter a valid email id");
			
		}
		}
		

	}

	class AsyncCookForgotPwd extends AsyncTask<Void, Void, String>
	{
String param,message,result;
boolean status;
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dismiss();
			pDialog.setTitle("Loading");
			pDialog.setMessage("Please wait..");
			pDialog.show();
		}

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			param="email="+edt_email.getText().toString()+"&user_type="+usertype;
			
			ServiceHandler handler=new ServiceHandler();
			result=handler.makeServiceCall("http://beta.foodfiestaa.com/forgotpassword", 2, param);
			try {
				JSONObject obj1=new JSONObject(result);
				status=obj1.getBoolean("status");
				if(status)
				{
					message=obj1.getString("message");
					return message;
					
				}else{
					
					message=obj1.getString("errorMessage");
					return message;
				}
			
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null;
		}
		

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pDialog.dismiss();
			Log.e("the result value is", result);

			final Dialog dialog_changed = new Dialog(context);
			dialog_changed.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog_changed.getWindow().setBackgroundDrawable(
					new ColorDrawable(0));
			dialog_changed.setContentView(R.layout.dialog_verification_signup);
			final TextView txt_content = (TextView) dialog_changed
					.findViewById(R.id.txt_content);
			ImageView img_tick=(ImageView)dialog_changed.findViewById(R.id.img_tick);
			
			if (result.equalsIgnoreCase("reset link sent to mail") ){
				Log.e("Reset link sent to mail", result);
				txt_content.setText("Reset link sent to mail");
			} else if (result.equalsIgnoreCase("User not exist")) {
				Log.e("incorrect old password", result);
				txt_content.setText("User not exist");
				img_tick.setImageResource(R.drawable.circle_uncheck);
			}

			Button btn_ok = (Button) dialog_changed.findViewById(R.id.btn_ok);
			btn_ok.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					dialog_changed.dismiss();
				}
			});
			dialog_changed.show();


		}
	}
	
	public static boolean validate(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(emailStr);
        return matcher.find();
}
}
