package com.ikomet.foodfiestaa.utils;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

public class NetworkReceiver extends BroadcastReceiver{

	Dialog dialog;
	
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		boolean noConnectivity = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
		String reason = intent.getStringExtra(ConnectivityManager.EXTRA_REASON);
		boolean isFailover = intent.getBooleanExtra(ConnectivityManager.EXTRA_IS_FAILOVER, false);

		NetworkInfo currentNetworkInfo = (NetworkInfo) intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
		NetworkInfo otherNetworkInfo = (NetworkInfo) intent.getParcelableExtra(ConnectivityManager.EXTRA_OTHER_NETWORK_INFO);
		
		dialog = NoInternet.internetConnectionFinder(context);
		
		if(currentNetworkInfo.isConnected()){
			Toast.makeText(context, "Connected", Toast.LENGTH_LONG).show();
			if(dialog.isShowing()){
				dialog.dismiss();
			}
		}else{
			Toast.makeText(context, "Not Connected", Toast.LENGTH_LONG).show();
			dialog.show();
		}
	}

}
