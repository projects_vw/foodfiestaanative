package com.ikomet.foodfiestaa.utils;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.activities.BecomeACook;
import com.ikomet.foodfiestaa.activities.ConsumerSignIn;
import com.ikomet.foodfiestaa.activities.ConsumerSignUpActivity;
import com.ikomet.foodfiestaa.activities.CookSignIn;

public class DialogTest extends Dialog implements View.OnClickListener{
	Context cxt;
	TextView signing;
	ImageView img_cook_signin,img_customer_signin;
	boolean signin;
	public DialogTest(Context context, boolean signin) {
		super(context);
		this.cxt=context;
		this.signin = signin;
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_signup);
		
		signing = (TextView)findViewById(R.id.signing);
		img_cook_signin=(ImageView)findViewById(R.id.img_cook_icon);
		img_customer_signin=(ImageView)findViewById(R.id.img_customer_icon);
		img_customer_signin.setOnClickListener(this);
		img_cook_signin.setOnClickListener(this);
	
		if(signin){
			signing.setText("SIGN IN AS");
		}else{
			signing.setText("SIGN UP AS");
		}
	
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int img_id=v.getId();
		
		if(img_id==R.id.img_cook_icon)
		{
			if(signin){
				Intent intent = new Intent(cxt, CookSignIn.class);
				cxt.startActivity(intent);
			}else{
				Intent intent1 = new Intent(cxt, BecomeACook.class);
				cxt.startActivity(intent1);
			}
			//img_cook_signin.setImageResource(R.drawable.capgreyicon);
			Toast.makeText(getContext(), "cook sign in", 1000).show();
			
		}
		else
		{
			if(signin){
				Intent intent = new Intent(cxt, ConsumerSignIn.class);
				cxt.startActivity(intent);
			}else{
				Intent intent1 = new Intent(cxt, ConsumerSignUpActivity.class);
				cxt.startActivity(intent1);
			}
			//img_customer_signin.setImageResource(R.drawable.dishgreyicon);
			Toast.makeText(getContext(), "consumer sign in", 1000).show();
			
		}
		this.dismiss();
	}

	
}

