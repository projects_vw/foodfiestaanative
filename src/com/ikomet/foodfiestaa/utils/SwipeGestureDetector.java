package com.ikomet.foodfiestaa.utils;


import com.ikomet.foodfiestaa.R;

import android.content.Context;

import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.animation.AnimationUtils;
import android.widget.ViewFlipper;

public class SwipeGestureDetector extends SimpleOnGestureListener {
ViewFlipper flipper;
Context cxt;
private static final int SWIPE_MIN_DISTANCE = 120;
private static final int SWIPE_THRESHOLD_VELOCITY = 200;

	public SwipeGestureDetector(ViewFlipper flipper,Context cxt) {
	super();
	this.flipper = flipper;
	this.cxt=cxt;
}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		// TODO Auto-generated method stub
		try {
			// right to left swipe
			if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
				flipper.setInAnimation(AnimationUtils.loadAnimation(cxt,R.anim.left_in));
				flipper.setOutAnimation(AnimationUtils.loadAnimation(cxt, R.anim.left_out));					
				flipper.showNext();
				return true;
			} else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
				flipper.setInAnimation(AnimationUtils.loadAnimation(cxt, R.anim.right_in));
				flipper.setOutAnimation(AnimationUtils.loadAnimation(cxt,R.anim.right_out));
				flipper.showPrevious();
				return true;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

}


