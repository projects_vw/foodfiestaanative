package com.ikomet.foodfiestaa.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ikomet.foodfiestaa.R;
import com.tmxlr.lib.driodvalidatorlight.Form;
import com.tmxlr.lib.driodvalidatorlight.helper.RegexTemplate;

public class DialogReferFriend extends Dialog implements
		android.view.View.OnClickListener {
	EditText edt_mail;
	Button btn_refer;
	ProgressDialog pDialog;
	Context context;
	public static final String REFER_FRIEND = "http://beta.foodfiestaa.com/refermyfriend";
	Form form;

	public DialogReferFriend(Context context) {
		super(context);
		this.context = context;
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_refer_friend);
		edt_mail = (EditText) findViewById(R.id.edt_mail);
		btn_refer = (Button) findViewById(R.id.refer_mail);
		btn_refer.setOnClickListener(this);
		form = new Form(context);
		form.check(edt_mail, RegexTemplate.NOT_EMPTY_PATTERN,
				"Please fill this field");
		form.check(edt_mail, RegexTemplate.EMAIL_PATTERN,
				"Please enter a valid email id");
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (form.validate()) {
			if (InternetCheck.isInternetConnected(context)) {
				new AsyncReferFriend().execute(edt_mail.getText().toString());

			} else {
				Toast.makeText(context,
						"Please check your internet connection",
						Toast.LENGTH_SHORT).show();
			}
		}

	}

	class AsyncReferFriend extends AsyncTask<String, Void, String> {
		String result = "";

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String id = SharedPrefs.getUid(context);
			String token = SharedPrefs.getToken(context);
			ServiceHandler handler = new ServiceHandler();
			try {
				String param = "id=" + URLEncoder.encode(id, "UTF-8")
						+ "&token=" + URLEncoder.encode(token, "UTF-8")
						+ "&refermail=" + URLEncoder.encode(params[0], "UTF-8");
				result = handler.makeServiceCall(REFER_FRIEND,
						ServiceHandler.POST, param);
				Log.e("the param is", param);
				Log.e("the result is ", result);
				try {
					JSONObject obj = new JSONObject(result);
					boolean status = obj.getBoolean("status");
					if (status) {
						return obj.getString("message");
					} else {
						if(obj.has("errorMessage")){
							JSONObject errorMsg = obj.optJSONObject("errorMessage");

							if (errorMsg == null) {
								return obj.getString("errorMessage");
							} else {
								if (errorMsg.has("refermail")){
									JSONArray jar = errorMsg.getJSONArray("refermail");
									return jar.getString(0);
								}
							}
							return obj.getString("errorMessage");
						}
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pDialog.dismiss();
			dismiss();
			DialogCustom custom = new DialogCustom();
			if (result != null) {
				if (result.equalsIgnoreCase("Successfully Sent")) {
					custom.showDialog(context, result, "ok",
							R.drawable.circle_check);
				} else {
					custom.showDialog(context, result, "ok",
							R.drawable.circle_uncheck);
				}
			} else {

			}
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(context);
			pDialog.setMessage("Please wait...");
			pDialog.show();
		}

	}
}
