package com.ikomet.foodfiestaa.utils;

public interface ShowHideToolbar {

	public void showToolbar();

	public void hideToolbar();

}
