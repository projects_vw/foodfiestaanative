package com.ikomet.foodfiestaa.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

public class ReadStream {

	String data;
	
	public String readStream(InputStream in, URL url){
	
	try {
				
	BufferedReader reader = new BufferedReader(new InputStreamReader(in));
	StringBuilder result = new StringBuilder();
	String line;
	while((line = reader.readLine()) != null) {
	    result.append(line);
	}
	
	System.out.println(result.toString());
	data = result.toString();
	
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return data;
	
	}
}
