package com.ikomet.foodfiestaa.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;

public class Validator {

	Context context;
	Activity activity;
	TextWatcher emailWatcher, passwordWatcher, confirmpwdWatcher, fnameWatcher,
			lnameWatcher, mobileWatcher, kitchennameWatcher;
	// public EditText email, password, cnfmpass, fname, lname, mobile,
	// kitchenname;

	TextWatcher street1Watcher, street2Watcher, locationWatcher, cityWatcher,
			stateWatcher, countryWatcher, pincodeWatcher;

	private final static String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{8,20})";
	private final static String IND_PINCODE_PATTERN = "((?=.*[0-9]).{6})";
	// private final static String IFSC_CODE = "((?=.*[0-9]).{4,6})";
	// private final static String ACCOUNT_NO_PATTERN="((?=.*[0-9]).{15,20})";
	// private final static String
	// ACCOUNT_NAME_PATTERN="((?=.*[a-zA-Z]).{3,20})";
	// private final static String
	// BRANCH_NAME_PATTERN="((?=.*[a-z])(?=.*[A-Z])";

	TextWatcher accNumWatcher, accNameWatcher, branchAddWatcher, IfscWatcher;

	TextWatcher paypalMailWatcher, paypalMobileWatcher;

	public Validator(Context context) {
		this.context = context;
	}

	public boolean validateStep1(final EditText email, final EditText password,
			final EditText cnfmpass, final EditText fname,
			final EditText lname, final EditText mobile,
			final EditText kitchenname) {
		// this.email = email;

		// validation for email on focus changed
		email.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if (!hasFocus) {
					if (email.getText().toString().trim().equalsIgnoreCase("")) {
						YoYo.with(Techniques.Wobble).duration(1000)
								.playOn(email);
						email.setError("Please fill this field");
					}
				} else {
					// email.setError(null);
				}
			}
		});

		// validation for email using text watcher on typing
		emailWatcher = new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (s.toString().trim().equalsIgnoreCase("")) {
					email.setError("Please fill this field");
				} else if (!isValidEmail(s.toString().trim())) {
					email.setError("Please enter a valid mail id");
				} else {
					email.setError(null);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

		};

		// adding the text watcher for email
		email.addTextChangedListener(emailWatcher);

		// validation for password on focus changed
		password.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if (!hasFocus) {
					if (password.getText().toString().trim()
							.equalsIgnoreCase("")) {
						YoYo.with(Techniques.Wobble).duration(1000)
								.playOn(password);
						password.setError("Please fill this field");
					}
				} else {
					// password.setError(null);
				}
			}
		});

		// validation for password using text watcher on typing
		passwordWatcher = new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (s.toString().trim().equalsIgnoreCase("")) {
					password.setError("Please fill this field");
				} else if (!isValidPassword(s.toString().trim())) {
					password.setError("Please use minimum of 8 characters containing upper case, lowercase, numbers and non alpha-numeric characters");
				} else {
					password.setError(null);
				}
			}
		};

		// adding the text watcher for password
		password.addTextChangedListener(passwordWatcher);

		cnfmpass.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if (!hasFocus) {
					if (cnfmpass.getText().toString().trim()
							.equalsIgnoreCase("")) {
						YoYo.with(Techniques.Wobble).duration(1000)
								.playOn(cnfmpass);
						cnfmpass.setError("Please fill this field");
					}
				} else {

				}
			}
		});

		confirmpwdWatcher = new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (s.toString().trim().equalsIgnoreCase("")) {
					cnfmpass.setError("Please fill this field");
				} else if (!s.toString().trim()
						.equalsIgnoreCase(password.getText().toString().trim())) {
					cnfmpass.setError("Password mismatch");
				} else {
					cnfmpass.setError(null);
				}
			}
		};

		cnfmpass.addTextChangedListener(confirmpwdWatcher);

		fname.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if (!hasFocus) {
					if (fname.getText().toString().trim().equalsIgnoreCase("")) {
						YoYo.with(Techniques.Wobble).duration(1000)
								.playOn(fname);
						fname.setError("Please fill this field");
					}
				}
			}
		});

		fnameWatcher = new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (s.toString().trim().equalsIgnoreCase("")) {
					fname.setError("Please fill this field");
				} else if (s.toString().trim().length() < 3) {
					fname.setError("First name should not be less than 3 characters");
				} else {
					fname.setError(null);
				}
			}
		};

		fname.addTextChangedListener(fnameWatcher);

		lname.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if (!hasFocus) {
					if (lname.getText().toString().trim().equalsIgnoreCase("")) {
						YoYo.with(Techniques.Wobble).duration(1000)
								.playOn(lname);
						lname.setError("Please fill this field");
					}
				}
			}
		});

		lnameWatcher = new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (s.toString().trim().equalsIgnoreCase("")) {
					lname.setError("Please fill this field");
				} else {
					lname.setError(null);
				}
			}
		};

		lname.addTextChangedListener(lnameWatcher);

		mobile.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if (!hasFocus) {
					if (mobile.getText().toString().trim().equalsIgnoreCase("")) {
						YoYo.with(Techniques.Wobble).duration(1000)
								.playOn(mobile);
						mobile.setError("Please fill this field");
					}
				}
			}
		});

		mobileWatcher = new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (s.toString().trim().equalsIgnoreCase("")) {
					mobile.setError("Please fill this field");
				} else if (s.toString().trim().length() < 10) {
					mobile.setError("Please enter a valid mobile number");
				} else {
					mobile.setError(null);
				}
			}
		};

		mobile.addTextChangedListener(mobileWatcher);

		kitchenname.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if (!hasFocus) {
					if (kitchenname.getText().toString().trim()
							.equalsIgnoreCase("")) {
						YoYo.with(Techniques.Wobble).duration(1000)
								.playOn(kitchenname);
						kitchenname.setError("Please fill this field");
					}
				}
			}
		});

		kitchennameWatcher = new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (s.toString().trim().equalsIgnoreCase("")) {
					kitchenname.setError("Please fill this field");
				} else if (s.toString().trim().length() < 6) {
					kitchenname
							.setError("Kitchen name must be minimum 6 characters");
				} else {
					kitchenname.setError(null);
				}
			}
		};

		kitchenname.addTextChangedListener(kitchennameWatcher);

		if (email.getError() == null && password.getError() == null
				&& cnfmpass.getError() == null && fname.getError() == null
				&& lname.getError() == null && mobile.getError() == null
				&& kitchenname.getError() == null) {

			return true;

		} else {
			return false;
		}

	}

	public boolean nullCheckStep1(EditText email, EditText password,
			EditText cnfmpass, EditText fname, EditText lname, EditText mobile,
			EditText kitchenname) {

		if (email.getText().toString().trim().equalsIgnoreCase("")
				|| password.getText().toString().trim().equalsIgnoreCase("")
				|| cnfmpass.getText().toString().trim().equalsIgnoreCase("")
				|| fname.getText().toString().trim().equalsIgnoreCase("")
				|| lname.getText().toString().trim().equalsIgnoreCase("")
				|| mobile.getText().toString().trim().equalsIgnoreCase("")
				|| kitchenname.getText().toString().trim().equalsIgnoreCase("")) {

			if (email.getText().toString().trim().equalsIgnoreCase("")) {
				email.setError("Please fill this field");
			} else if (!isValidEmail(email.getText().toString().trim())) {
				email.setError("Please enter a valid mail id");
			} else {
				email.setError(null);
			}

			if (password.getText().toString().trim().equalsIgnoreCase("")) {
				password.setError("Please fill this field");
			} else if (!isValidPassword(password.getText().toString().trim())) {
				password.setError("Please use minimum of 8 characters containing upper case, lowercase, numbers and non alpha-numeric characters");
			} else {
				password.setError(null);
			}

			if (cnfmpass.getText().toString().trim().equalsIgnoreCase("")) {
				cnfmpass.setError("Please fill this field");
			} else if (!cnfmpass.getText().toString().trim()
					.equalsIgnoreCase(password.getText().toString().trim())) {
				cnfmpass.setError("Password mismatch");
			} else {

			}

			if (fname.getText().toString().trim().equalsIgnoreCase("")) {
				fname.setError("Please fill this field");
			} else if (fname.getText().toString().trim().length() < 3) {
				fname.setError("First name should not be less than 3 characters");
			} else {
				fname.setError(null);
			}

			if (lname.getText().toString().trim().equalsIgnoreCase("")) {
				lname.setError("Please fill this field");
			} else {
				lname.setError(null);
			}

			if (mobile.getText().toString().trim().equalsIgnoreCase("")) {
				mobile.setError("Please fill this field");
			} else if (mobile.getText().toString().trim().length() < 10) {
				mobile.setError("Please enter a valid mobile number");
			} else {
				mobile.setError(null);
			}

			if (kitchenname.getText().toString().trim().equalsIgnoreCase("")) {
				kitchenname.setError("Please fill this field");
			} else if (kitchenname.getText().toString().trim().length() < 6) {
				kitchenname
						.setError("Kitchen name must be minimum 6 characters");
			} else {
				kitchenname.setError(null);
			}

			return false;
		} else {
			return true;
		}

	}

	public boolean nullCheckStep2( EditText oldpwd,
			EditText newpwd,EditText cnfpwd) {

		if (oldpwd.getText().toString().trim().equalsIgnoreCase("")
				|| newpwd.getText().toString().trim().equalsIgnoreCase("")
				|| cnfpwd.getText().toString().trim().equalsIgnoreCase("")) {


			if (newpwd.getText().toString().trim().equalsIgnoreCase("")) {
				newpwd.setError("Please fill this field");
			} else if (!isValidPassword(newpwd.getText().toString().trim())) {
				newpwd.setError("Please use minimum of 8 characters containing upper case, lowercase, numbers and non alpha-numeric characters");
			} else {
				newpwd.setError(null);
			}

			if (cnfpwd.getText().toString().trim().equalsIgnoreCase("")) {
				cnfpwd.setError("Please fill this field");
			} else if (!cnfpwd.getText().toString().trim()
					.equalsIgnoreCase(newpwd.getText().toString().trim())) {
				cnfpwd.setError("Password mismatch");
			} else {

			}

			if (oldpwd.getText().toString().trim().equalsIgnoreCase("")) {
				oldpwd.setError("Please fill this field");
			} else {
				oldpwd.setError(null);
			}


			return false;
		} else {
			return true;
		}

	}
	public boolean validateAddress(final EditText street1,
			final EditText street2, final EditText location,
			final EditText city, final EditText state, final EditText country,
			final EditText pincode) {

		street1.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if (!hasFocus) {
					if (street1.getText().toString().trim()
							.equalsIgnoreCase("")) {
						YoYo.with(Techniques.Wobble).duration(1000)
								.playOn(street1);
						street1.setError("Please fill this field");
					}
				}
			}
		});

		street1Watcher = new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (s.toString().trim().equalsIgnoreCase("")) {
					street1.setError("Please fill this field");
				} else {
					street1.setError(null);
				}
			}
		};

		street1.addTextChangedListener(street1Watcher);

		street2.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if (!hasFocus) {
					if (street2.getText().toString().trim()
							.equalsIgnoreCase("")) {
						YoYo.with(Techniques.Wobble).duration(1000)
								.playOn(street2);
						street2.setError("Please fill this field");
					}
				}
			}
		});

		street2Watcher = new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (s.toString().trim().equalsIgnoreCase("")) {
					street2.setError("Please fill this field");
				} else {
					street2.setError(null);
				}
			}
		};

		street2.addTextChangedListener(street2Watcher);

		location.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if (!hasFocus) {
					if (location.getText().toString().trim()
							.equalsIgnoreCase("")) {
						YoYo.with(Techniques.Wobble).duration(1000)
								.playOn(location);
						location.setError("Please fill this field");
					}
				}
			}
		});

		locationWatcher = new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (s.toString().trim().equalsIgnoreCase("")) {
					location.setError("Please fill this field");
				} else {
					location.setError(null);
				}
			}
		};

		location.addTextChangedListener(locationWatcher);

		pincode.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if (!hasFocus) {
					if (pincode.getText().toString().trim()
							.equalsIgnoreCase("")) {
						YoYo.with(Techniques.Wobble).duration(1000)
								.playOn(pincode);
						pincode.setError("Please fill this field");
					}
				}
			}
		});

		pincodeWatcher = new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (s.toString().trim().equalsIgnoreCase("")) {
					pincode.setError("Please fill this field");
				} else if (!isValidPincode(s.toString().trim())) {
					pincode.setError("Please enter a valid pincode");
				} else {
					pincode.setError(null);
				}
			}
		};

		pincode.addTextChangedListener(pincodeWatcher);

		if (street1.getError() == null && street2.getError() == null
				&& location.getError() == null && pincode.getError() == null) {

			return true;

		} else {
			return false;
		}

	}

	public boolean nullCheckAddress(final EditText street1,
			final EditText street2, final EditText location,
			final EditText city, final EditText state, final EditText country,
			final EditText pincode) {

		if (street1.getText().toString().trim().equalsIgnoreCase("")
				|| street2.getText().toString().trim().equalsIgnoreCase("")
				|| location.getText().toString().trim().equalsIgnoreCase("")
				|| city.getText().toString().trim().equalsIgnoreCase("")
				|| country.getText().toString().trim().equalsIgnoreCase("")
				|| pincode.getText().toString().trim().equalsIgnoreCase("")) {

			if (street1.getText().toString().trim().equalsIgnoreCase("")) {
				street1.setError("Please fill this field");
			} else {
				street1.setError(null);
			}

			if (street2.getText().toString().trim().equalsIgnoreCase("")) {
				street2.setError("Please fill this field");
			} else {
				street2.setError(null);
			}

			if (location.getText().toString().trim().equalsIgnoreCase("")) {
				location.setError("Please fill this field");
			} else {
				location.setError(null);
			}

			if (pincode.getText().toString().trim().equalsIgnoreCase("")) {
				pincode.setError("Please fill this field");
			} else if (!isValidPincode(pincode.getText().toString().trim())) {
				pincode.setError("Please enter a valid pincode");
			} else {
				pincode.setError(null);
			}

			return false;

		} else {
			return true;
		}

	}

	public boolean validateBankInfo(final EditText accountNumber,
			final EditText accountName, final EditText branchAddress,
			final EditText ifsc) {
		// this.email = email;

		// account number validation
		accountNumber
				.setOnFocusChangeListener(new View.OnFocusChangeListener() {

					@Override
					public void onFocusChange(View v, boolean hasFocus) {
						// TODO Auto-generated method stub

						if (!hasFocus) {
							if (accountNumber.getText().toString().trim()
									.equalsIgnoreCase("")) {
								YoYo.with(Techniques.Wobble).duration(1000)
										.playOn(accountNumber);
								accountNumber
										.setError("Please fill this field");
							}
						} else {
							// password.setError(null);
						}

					}
				});

		accountNumber.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (s.toString().trim().equalsIgnoreCase("")) {
					accountNumber.setError("please fill the field");

				} else if (s.toString().trim().length() < 15) {
					accountNumber.setError("Invalid account number");
				} else {
					accountNumber.setError(null);
				}
			}

		});

		// Account name validation
		accountName.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if (!hasFocus) {
					if (accountName.getText().toString().trim()
							.equalsIgnoreCase("")) {
						YoYo.with(Techniques.Wobble).duration(1000)
								.playOn(accountName);
						accountName.setError("Please fill this field");
					}
				} else {
					// password.setError(null);
				}
			}
		});

		accountName.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (s.toString().trim().equalsIgnoreCase("")) {
					accountName.setError("Please fill this field");
				} else {
					accountName.setError(null);
				}
				/*
				 * if(accountName.getText().toString().trim().equalsIgnoreCase(""
				 * )) { accountName.setError("Account Name is required."); }
				 * else if(!isValidAccountName(s.toString().trim())) {
				 * accountName.setError("Invalid Account Name"); } else {
				 * accountName.setError(null); }
				 */
			}
		});
		// validation for email on focus changed

		// validation of branch address

		branchAddress.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub

				if (!hasFocus) {
					if (branchAddress.getText().toString().trim()
							.equalsIgnoreCase("")) {
						YoYo.with(Techniques.Wobble).duration(1000)
								.playOn(branchAddress);
						branchAddress.setError("Please fill this field");
					} else {
					}

				}
			}
		});
		branchAddress.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (s.toString().trim().equalsIgnoreCase("")) {
					branchAddress.setError("Please fill this field");
				} else {
					branchAddress.setError(null);
				}

				/*
				 * if(s.toString().trim().equalsIgnoreCase("")) {
				 * branchAddress.setError("Please fill this field"); } else
				 * if(!isValidBranchAddress(s.toString().trim())) {
				 * branchAddress.setError("Branch Address is invalid"); } else {
				 * branchAddress.setError(null);
				 * 
				 * }
				 */
			}
		});

		// Validation of IFSC Code
		ifsc.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if (!hasFocus) {
					if (ifsc.getText().toString().trim().equalsIgnoreCase("")) {
						YoYo.with(Techniques.Wobble).duration(1000)
								.playOn(ifsc);
						ifsc.setError("Please fill this field");
					} else {
					}

				}

			}
		});
		ifsc.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (s.toString().trim().equalsIgnoreCase("")) {
					ifsc.setError("Please fill this field");
				} else if (s.toString().trim().length() < 7) {
					ifsc.setError("Invalid IFS Code");
				} else {
					ifsc.setError(null);
				}

				/*
				 * if(s.toString().trim().equalsIgnoreCase("")) {
				 * ifsc.setError("Please fill this field"); } else
				 * if(!isValidIFSC(s.toString().trim())) {
				 * ifsc.setError("IFSC Code is invalid"); } else {
				 * branchAddress.setError(null);
				 * 
				 * }
				 */

			}
		});

		return false;

	}

	public boolean nullCheckBankDetails(final EditText bankAccNo,
			final EditText bankAccName, final EditText bankAddr,
			final EditText ifsc) {

		if (bankAccNo.getText().toString().trim().equalsIgnoreCase("")
				|| bankAccNo.getText().toString().trim().length() < 15
				|| bankAccName.getText().toString().trim().equalsIgnoreCase("")
				|| bankAddr.getText().toString().trim().equalsIgnoreCase("")
				|| ifsc.getText().toString().trim().equalsIgnoreCase("")
				|| ifsc.getText().toString().trim().length() < 7) {

			if (bankAccNo.getText().toString().trim().equalsIgnoreCase("")) {
				bankAccNo.setError("Please fill this field");
			} else if (bankAccNo.getText().toString().trim().length() < 15) {
				bankAccNo.setError("Invalid Account Number");
			} else {
				bankAccNo.setError(null);
			}

			if (bankAccName.getText().toString().trim().equalsIgnoreCase("")) {
				bankAccName.setError("Please fill this field");
			} else {
				bankAccName.setError(null);
			}

			if (bankAddr.getText().toString().trim().equalsIgnoreCase("")) {
				bankAddr.setError("Please fill this field");
			} else {
				bankAddr.setError(null);
			}

			if (ifsc.getText().toString().trim().equalsIgnoreCase("")) {
				ifsc.setError("Please fill this field");
			} else if (ifsc.getText().toString().trim().length() < 7) {
				ifsc.setError("Invalid IFSC");
			} else {
				ifsc.setError(null);
			}

			return false;

		} else {
			return true;
		}

	}

	/*public boolean nullCheckPaypalDetails(final EditText paypal_mail,
			final EditText paypal_phone) {

		
		if (paypal_mail.getText().toString().trim().equalsIgnoreCase("")
				|| !isValidEmail(paypal_mail.getText().toString().trim())
				|| paypal_phone.getText().toString().trim().equalsIgnoreCase("")
				|| paypal_phone.getText().toString().trim().length()<10){
			
			if(!paypal_mail.getText().toString().trim().equalsIgnoreCase("") && isValidEmail(paypal_mail.getText().toString().trim())){
				
				return true;
			}else if(!paypal_phone.getText().toString().trim().equalsIgnoreCase("") && paypal_phone.getText().toString().trim().length()<10){
				return true;
			}else{
				return false;
			}
			
			
			
				
		} else {
			return true;
		}

	}*/

	public boolean nullCheckPaypalMail(final EditText paypal_mail) {

		if (paypal_mail.getText().toString().trim().equalsIgnoreCase("")
				|| !isValidEmail(paypal_mail.getText().toString().trim())
				){
			
			if(paypal_mail.getText().toString().trim().equalsIgnoreCase("")){
				paypal_mail.setError("Please fill this field");
			}else if(!isValidEmail(paypal_mail.getText().toString().trim())){
				paypal_mail.setError("Invalid mail id");
			}
			
				return false;
			}else{
				return true;
			}
			
	}
	
	public boolean nullCheckPaypalMobile(final EditText paypal_phone) {

		if (paypal_phone.getText().toString().trim().equalsIgnoreCase("")
				|| paypal_phone.getText().toString().trim().length()<10){
			
			if(paypal_phone.getText().toString().trim().equalsIgnoreCase("")){
				paypal_phone.setError("Please fill this field");
			}else if(paypal_phone.getText().toString().trim().length()<10){
				paypal_phone.setError("Invalid mobile number");
			}
			
				return false;
			}else{
				return true;
			}
			
	}
	
	public boolean validatePaypal(final EditText paypal_mail,
			final EditText paypal_phone) {

		paypalMailWatcher = new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (s.toString().trim().equalsIgnoreCase("")) {
					paypal_phone.setEnabled(true);
				} else if (s.toString().trim().length() > 0) {
					paypal_phone.setError(null);
					paypal_phone.setEnabled(false);
					if (!isValidEmail(s.toString().trim())) {
						paypal_mail.setError("Please enter a valid mail id");
					} else {
						paypal_mail.setError(null);
					}
				}
			}
		};

		paypal_mail.addTextChangedListener(paypalMailWatcher);

		paypalMobileWatcher = new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (s.toString().trim().equalsIgnoreCase("")) {
					paypal_mail.setEnabled(true);
				} else if (s.toString().trim().length() > 0) {
					paypal_mail.setError(null);
					paypal_mail.setEnabled(false);
					if (s.toString().trim().length() < 10) {
						paypal_phone
								.setError("Please enter a valid mobile number");
					} else {
						paypal_phone.setError(null);
					}
				}
			}
		};

		paypal_phone.addTextChangedListener(paypalMobileWatcher);

		/*
		 * paypal_mail.setOnFocusChangeListener(new View.OnFocusChangeListener()
		 * {
		 * 
		 * @Override public void onFocusChange(View v, boolean hasFocus) { //
		 * TODO Auto-generated method stub if(hasFocus &&
		 * !paypal_mail.getText().toString().trim().equalsIgnoreCase("")){
		 * paypal_phone.setText(""); paypal_phone.setEnabled(false); } } });
		 * 
		 * paypal_phone.setOnFocusChangeListener(new
		 * View.OnFocusChangeListener() {
		 * 
		 * @Override public void onFocusChange(View v, boolean hasFocus) { //
		 * TODO Auto-generated method stub
		 * if(!paypal_phone.getText().toString().trim().equalsIgnoreCase("")){
		 * paypal_mail.setText(""); paypal_mail.setEnabled(false); } } });
		 */

		/*
		 * if(paypal_mail.getText().toString().trim().equalsIgnoreCase("") ||
		 * !isValidEmail(paypal_mail.getText().toString().trim())) {
		 * 
		 * }
		 */
		if (paypal_mail.getError() == null && paypal_phone.getError() == null) {

			if (paypal_mail.getText().toString().trim().equalsIgnoreCase("")
					|| paypal_phone.getText().toString().trim()
							.equalsIgnoreCase("")) {

			}

			return true;
		} else {
			return false;
		}

	}

	public final static boolean isValidPincode(String pincode) {

		Pattern pattern = Pattern.compile(IND_PINCODE_PATTERN);
		Matcher matcher = pattern.matcher(pincode);

		return matcher.matches();
	}

	public final static boolean isValidPassword(String password) {

		Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
		Matcher matcher = pattern.matcher(password);

		return matcher.matches();
	}

	/*
	 * public final static boolean isValidAccountName(String name) { Pattern
	 * pattern=Pattern.compile(ACCOUNT_NAME_PATTERN); Matcher
	 * matcher=pattern.matcher(name); Log.e("acc_name", ""+matcher.matches());
	 * return matcher.matches();
	 * 
	 * }
	 * 
	 * public final static boolean isValidAccountNo(String pincode) {
	 * 
	 * Pattern pattern = Pattern.compile(ACCOUNT_NO_PATTERN); Matcher matcher =
	 * pattern.matcher(pincode); Log.e("acc_no", ""+matcher.matches()); return
	 * matcher.matches(); }
	 * 
	 * public final static boolean isValidBranchAddress(String branchaddress) {
	 * 
	 * Pattern pattern = Pattern.compile(BRANCH_NAME_PATTERN); Matcher matcher =
	 * pattern.matcher(branchaddress);
	 * 
	 * return matcher.matches(); } public final static boolean
	 * isValidIFSC(String ifsccode) {
	 * 
	 * Pattern pattern = Pattern.compile(BRANCH_NAME_PATTERN); Matcher matcher =
	 * pattern.matcher(ifsccode);
	 * 
	 * return matcher.matches(); }
	 */

	public final static boolean isValidEmail(CharSequence target) {
		if (target == null) {
			return false;
		} else {
			return android.util.Patterns.EMAIL_ADDRESS.matcher(target)
					.matches();
		}
	}

}
