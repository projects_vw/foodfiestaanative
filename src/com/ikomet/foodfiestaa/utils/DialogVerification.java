package com.ikomet.foodfiestaa.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.fragments.FragmentCart;

public class DialogVerification extends Dialog implements
		android.view.View.OnClickListener {

	public static boolean state = false;
	int PROFILEUPDATE_DIALOG = 1;
	int SIGNUP_DIALOG = 0;
	Context cxt;
	Button dialogButton;
	public static int  NETWORK=4;
	String message;
	TextView txt_content;
	int value;

	public DialogVerification(Context context, int i, String s) {
		super(context);
		cxt = context;
		value = i;
		message = s;
	}

	public void onClick(View view) {
		if (value == SIGNUP_DIALOG) {

		} else if (value == PROFILEUPDATE_DIALOG) {
			state = true;

			((Activity) cxt).finish();

		} else if (value == 3){
			
		}
		dismiss();
	}

	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		getWindow().requestFeature(1);
		getWindow().setBackgroundDrawable(new ColorDrawable(0));
		setContentView(R.layout.dialog_verification_signup);
		txt_content = (TextView) findViewById(R.id.txt_content);
		txt_content.setText(message);
		dialogButton = (Button) findViewById(R.id.btn_ok);
		if(value!=3)
		dialogButton.setOnClickListener(this);
	}

}
