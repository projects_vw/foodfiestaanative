package com.ikomet.foodfiestaa.activities;

import java.util.Timer;

import java.util.TimerTask;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.adapter.PagerAdapter;
import com.ikomet.foodfiestaa.adapter.PagerAdapterHowCook;
import com.ikomet.foodfiestaa.adapter.PagerAdapterHowitworks;
import com.ikomet.foodfiestaa.fragments.FragmentHowConsumer;
import com.ikomet.foodfiestaa.fragments.FragmentHowCook;
import com.ikomet.foodfiestaa.interfaces.FragmentCommunicator;
import com.ikomet.foodfiestaa.utils.CirclePageIndicator;

public class HowItWorks extends ActionBarActivity {

	Toolbar mToolbar;
	TextView toolbarTitle;
	RelativeLayout layout_cook, layout_consumer;
	TextView txtCook, txtConsumer;
	ImageView imgCook, imgConsumer;

	ViewPager viewpager;
	CirclePageIndicator indicator;
	PagerAdapterHowitworks adapter;
	PagerAdapterHowCook adapter1;
	int numberOfViewPagerChildren = 3;
	public static int position = 0;

	Timer swipeTimer;

	public static boolean iscook = false, check = false;

	// Fragment frag = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.how_it_works_new);

		mToolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbarTitle = (TextView) mToolbar.findViewById(R.id.txt_toolbar);
		toolbarTitle.setText("How It Works");
		mToolbar.setTitle("");
		setSupportActionBar(mToolbar);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);

		mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		layout_consumer = (RelativeLayout) findViewById(R.id.layout_consumer);
		layout_cook = (RelativeLayout) findViewById(R.id.layout_cook);
		txtCook = (TextView) findViewById(R.id.txt_cook);
		txtConsumer = (TextView) findViewById(R.id.txt_consumer);
		imgCook = (ImageView) findViewById(R.id.img_cook);
		imgConsumer = (ImageView) findViewById(R.id.img_consumer);

		viewpager = (ViewPager) findViewById(R.id.pager_how);
		indicator = (CirclePageIndicator) findViewById(R.id.indicator);

		// selectCook();

		layout_cook.setBackgroundColor(Color.WHITE);
		txtCook.setTextColor(getResources().getColor(R.color.colorPrimary));
		imgCook.setImageResource(R.drawable.capyellowicon);

		layout_consumer.setBackgroundColor(getResources().getColor(
				R.color.tabBackground));
		txtConsumer.setTextColor(getResources().getColor(R.color.textSelected));
		imgConsumer.setImageResource(R.drawable.dishgreyicon);

		iscook = true;

		

		selectCook();

		layout_cook.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				selectCook();
				

			}
		});

		layout_consumer.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				selectConsumer();
				
			}
		});

	}

	public void selectConsumer() {

		layout_cook.setBackgroundColor(getResources().getColor(
				R.color.tabBackground));
		txtCook.setTextColor(getResources().getColor(R.color.textSelected));
		imgCook.setImageResource(R.drawable.capgreyicon);

		layout_consumer.setBackgroundColor(Color.WHITE);
		txtConsumer.setTextColor(getResources().getColor(R.color.colorPrimary));
		imgConsumer.setImageResource(R.drawable.dishyellowicon);
		adapter = new PagerAdapterHowitworks(getSupportFragmentManager(),
				numberOfViewPagerChildren);
		viewpager.setAdapter(adapter);
		indicator.setViewPager(viewpager);
		position = viewpager.getCurrentItem();

		swipeTimer = new Timer();
		starttimer();
		
	
	}

	public void selectCook() {
		layout_cook.setBackgroundColor(Color.WHITE);
		txtCook.setTextColor(getResources().getColor(R.color.colorPrimary));
		imgCook.setImageResource(R.drawable.capyellowicon);

		layout_consumer.setBackgroundColor(getResources().getColor(
				R.color.tabBackground));
		txtConsumer.setTextColor(getResources().getColor(R.color.textSelected));
		imgConsumer.setImageResource(R.drawable.dishgreyicon);
		adapter1 = new PagerAdapterHowCook(getSupportFragmentManager());
		viewpager.setAdapter(adapter1);
		indicator.setViewPager(viewpager);
		position = viewpager.getCurrentItem();

		swipeTimer = new Timer();
		starttimer();

	}
	
	public void clear()
	{
		
	}
	public void starttimer() {
		position = 0;
		swipeTimer = new Timer();
		swipeTimer.schedule(new TimerTask() {

			@Override
			public void run() {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if (position == numberOfViewPagerChildren) {
							position = 0;
						} else {
							position = position + 1;

						}
						viewpager.setCurrentItem(position, true);
						// /viewPager.setCurrentItem(position ++, true);
					}
				});
			}
		}, 500, 2000);
	}
	
}
