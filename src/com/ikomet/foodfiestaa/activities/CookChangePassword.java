package com.ikomet.foodfiestaa.activities;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.utils.ServiceHandler;
import com.ikomet.foodfiestaa.utils.SharedPrefs;
import com.ikomet.foodfiestaa.utils.Validator;

public class CookChangePassword extends ActionBarActivity {
	Toolbar toolbar;
	EditText edt_oldpwd, edt_newpwd, edt_cnfpwd;
	Button btn_save;
	ProgressDialog pDialog;
	Validator validator;
	String PASSWORD = "((?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[&*@#$%]).{8,20})";
	boolean nullCheck;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_change_password);
		toolbar = (Toolbar) findViewById(R.id.toolbar);
		pDialog = new ProgressDialog(CookChangePassword.this);
		// toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
		setSupportActionBar(toolbar);
		validator = new Validator(CookChangePassword.this);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		// getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setTitle("");
		getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
		toolbar.setNavigationOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		edt_oldpwd = (EditText) findViewById(R.id.edt_oldpwd);
		edt_newpwd = (EditText) findViewById(R.id.edt_newpwd);
		edt_cnfpwd = (EditText) findViewById(R.id.edt_cnfpwd);

		btn_save = (Button) findViewById(R.id.btn_save);

		edt_newpwd.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (edt_newpwd.getText().toString().equalsIgnoreCase("")) {
					edt_newpwd.setError("Please fill this field");
				} else {
					Pattern pattern = Pattern.compile(PASSWORD);
					Matcher macher = pattern.matcher(edt_newpwd.getText()
							.toString());
					if (!macher.matches()) {
						edt_newpwd
								.setError("Please use minimum of 8 characters containing upper case, lowercase, numbers and non alpha-numeric characters");
					} else {
					}
				}

			}
		});
		edt_cnfpwd.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (edt_newpwd.getText().toString().equalsIgnoreCase("")) {
					edt_newpwd.setError("Please fill this field");
				} else {
					if (!edt_cnfpwd.getText().toString()
							.equalsIgnoreCase(edt_newpwd.getText().toString())) {
						edt_cnfpwd.setError("Password Mismatch");
					}
				}
			}
		});
		btn_save.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				nullCheck = validator.nullCheckStep2(edt_oldpwd, edt_newpwd, edt_cnfpwd);
				Pattern pattern = Pattern.compile(PASSWORD);
				Matcher macher = pattern.matcher(edt_newpwd.getText()
						.toString());
				if(nullCheck && macher.matches() && !edt_cnfpwd.getText().toString()
						.equalsIgnoreCase(edt_newpwd.getText().toString()))
				{
					new Async_ChangePwdCook().execute();
				}
				else
				{
					//Toast.makeText(CookChangePassword.this, "", 1000).show();
				}
			
			}
		});
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		pDialog.dismiss();

	}

	class Async_ChangePwdCook extends AsyncTask<Void, Void, String> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog.setTitle("Loading");
			pDialog.setMessage("Please wait...");
			pDialog.show();
		}

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			boolean status = false;
			String id = SharedPrefs.getUid(CookChangePassword.this);
			String token = SharedPrefs.getToken(CookChangePassword.this);
			ServiceHandler handler = new ServiceHandler();
			
			String user = SharedPrefs.getUser(CookChangePassword.this);
						
			String param = "id=" + id + "&token=" + token + "&oldpassword="
					+ edt_oldpwd.getText().toString() + "&password="
					+ edt_newpwd.getText().toString()
					+ "&password_confirmation="
					+ edt_cnfpwd.getText().toString();

			Log.e("The param is", param);
			
			String result = "";
			
			if(user.equalsIgnoreCase("Cook")){
				result = handler.makeServiceCall(
						"http://beta.foodfiestaa.com/changepasswordcook", 2, param);
			}else{
				result = handler.makeServiceCall(
						"http://beta.foodfiestaa.com/changepasswordconsumer", 2, param);
			}
						
			Log.e("the result is", result);

			try {
				JSONObject obj1 = new JSONObject(result);
				status = obj1.getBoolean("status");
				String data = obj1.getString("data");
				return data;
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pDialog.dismiss();
			Log.e("the result value is", result);

			final Dialog dialog_changed = new Dialog(CookChangePassword.this);
			dialog_changed.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog_changed.getWindow().setBackgroundDrawable(
					new ColorDrawable(0));
			dialog_changed.setContentView(R.layout.dialog_verification_signup);
			final TextView txt_content = (TextView) dialog_changed
					.findViewById(R.id.txt_content);
			ImageView img_tick=(ImageView)dialog_changed.findViewById(R.id.img_tick);
			
			if (result.equalsIgnoreCase("password changed successfully") ){
				Log.e("password changed successfully", result);
				txt_content.setText("password changed successfully");
			} else if (result.equalsIgnoreCase("incorrect old password")) {
				Log.e("incorrect old password", result);
				txt_content.setText("Incorrect old password");
				img_tick.setImageResource(R.drawable.circle_uncheck);
			}

			Button btn_ok = (Button) dialog_changed.findViewById(R.id.btn_ok);
			btn_ok.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					dialog_changed.dismiss();
				}
			});
			dialog_changed.show();


		}
	}

}
