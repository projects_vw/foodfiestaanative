package com.ikomet.foodfiestaa.activities;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.adapter.GridImageAdapter;
import com.ikomet.foodfiestaa.utils.ServiceHandler;

public class GalleryActivity extends AppCompatActivity {

	String[] images;
	Toolbar mToolbar;
	GridView imageGrid;
	ProgressDialog pDialog;
	public static final String GALLERY_URL = "http://beta.foodfiestaa.com/getallGallery";
	GridImageAdapter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gallery);

		mToolbar = (Toolbar) findViewById(R.id.toolbar);
		imageGrid = (GridView) findViewById(R.id.grid_gallery);
		mToolbar.setTitle("");
		setSupportActionBar(mToolbar);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);

		mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		new GetImages().execute();

		imageGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(GalleryActivity.this, FullScreenViewActivity.class);
				intent.putExtra("position", position);
				intent.putExtra("imagesUrl", images);
				startActivity(intent);
				
			}
		});
		
	}

	class GetImages extends AsyncTask<String, String, String> {

		boolean status;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(GalleryActivity.this);
			pDialog.setMessage("Please Wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub
			// Check for success tag

			String json = "";

			try {

				ServiceHandler sh = new ServiceHandler();

				json = sh.makeServiceCall(GALLERY_URL, ServiceHandler.GET);

				JSONObject jsonObj = new JSONObject(json);

				status = jsonObj.getBoolean("status");

				if (status) {

					JSONArray jar_data = jsonObj.getJSONArray("data");

					images = new String[jar_data.length()];

					for (int i = 0; i < jar_data.length(); i++) {
						JSONObject jobj_image = jar_data.getJSONObject(i);
						String temp = jobj_image.getString("src");
						temp = temp.replace(" ", "%20");
						System.out.println(temp);
						images[i] = temp;
					}
					return "success";
				} else {
					return "Problem Getting Data";

				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			return json;

		}

		protected void onPostExecute(String value) {
			// dismiss the dialog once product deleted
			if (pDialog.isShowing()) {
				pDialog.dismiss();
			}

			if (value.equalsIgnoreCase("success")) {
				adapter = new GridImageAdapter(GalleryActivity.this, images);
				imageGrid.setAdapter(adapter);
			}

		}

	}

}
