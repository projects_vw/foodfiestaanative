package com.ikomet.foodfiestaa.activities;

import java.util.ArrayList;
import java.util.Arrays;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.adapter.FullScreenImageAdapter;

public class FullScreenViewActivity extends Activity {

	private FullScreenImageAdapter adapter;
	private ViewPager viewPager;
	String[] temp;
	ArrayList<String> imagesList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fullscreen_view);

		viewPager = (ViewPager) findViewById(R.id.pager);

		Intent i = getIntent();
		int position = i.getIntExtra("position", 0);

		temp = i.getStringArrayExtra("imagesUrl");
		imagesList = new ArrayList<String>(Arrays.asList(temp));

		adapter = new FullScreenImageAdapter(FullScreenViewActivity.this,
				imagesList);

		viewPager.setAdapter(adapter);

		// displaying selected image first
		viewPager.setCurrentItem(position);
	}
}
