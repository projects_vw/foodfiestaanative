package com.ikomet.foodfiestaa.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.RelativeLayout;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.utils.SharedPrefs;

public class SplashActivity extends Activity {

	RelativeLayout layoutsplash;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);

		layoutsplash = (RelativeLayout) findViewById(R.id.layout_splash);

		YoYo.with(Techniques.ZoomOutUp).duration(2000).playOn(layoutsplash);

		Handler handler = new Handler();

		handler.postDelayed(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				Intent intent = null;
				if (SharedPrefs.hasSPData(SplashActivity.this)) {
					if(SharedPrefs.getUser(SplashActivity.this).equalsIgnoreCase("Cook")){
						intent = new Intent(SplashActivity.this,
								CookActivity.class);
						intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
					}else{
						intent = new Intent(SplashActivity.this,
								LocationSearchResultPage.class);
						intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
					}
					
				} else {
					intent = new Intent(SplashActivity.this,
							HomeActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
					
				}
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.right_in, R.anim.left_out);
			}
		}, 3000);

	}

}
