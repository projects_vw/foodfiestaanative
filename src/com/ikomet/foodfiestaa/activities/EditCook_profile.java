package com.ikomet.foodfiestaa.activities;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.adapter.PagerAdapter;
import com.ikomet.foodfiestaa.beans.ConsumerDetailBean;
import com.ikomet.foodfiestaa.beans.CookDetailBean;
import com.ikomet.foodfiestaa.interfaces.FragmentCommunicator;
import com.ikomet.foodfiestaa.utils.CirclePageIndicator;

public class EditCook_profile extends ActionBarActivity implements
		FragmentCommunicator {

	CirclePageIndicator indicator;
	Toolbar mToolbar;
	ViewPager pager_editprofile;
	PagerAdapter adapter;
	CookDetailBean test_bean;
	ConsumerDetailBean test_bean_cons;
	TextView txt_toolbar;
	public static int value_change_state = 0;

	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.activity_editprofile);
		mToolbar = (Toolbar) findViewById(R.id.toolbar);
		txt_toolbar = (TextView) mToolbar.findViewById(R.id.txt_toolbar);
		setSupportActionBar(mToolbar);
		mToolbar.setTitle("");
		getSupportActionBar().setDisplayShowTitleEnabled(false);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		
		getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
		mToolbar.setNavigationOnClickListener(new android.view.View.OnClickListener() {

			public void onClick(View view) {
				finish();
			}

		});
		
		txt_toolbar.setText("EDIT PROFILE");
		indicator = (CirclePageIndicator) findViewById(R.id.indicator);
		pager_editprofile = (ViewPager) findViewById(R.id.pager);
		 if(getIntent().getSerializableExtra("object") instanceof CookDetailBean){
		
			 test_bean = (CookDetailBean) getIntent().getSerializableExtra("object");
			 adapter = new PagerAdapter(getSupportFragmentManager(), 3, 1,
					test_bean, null);
		
		} else {
			
			test_bean_cons = (ConsumerDetailBean) getIntent().getSerializableExtra("object");
			 adapter = new PagerAdapter(getSupportFragmentManager(), 2, 2,
					null, test_bean_cons);
			
			Log.e("test", "test_bean is null");
		}

			pager_editprofile.setAdapter(adapter);
			indicator.setViewPager(pager_editprofile);
		 
		// }else{
		// test_bean_cons =
		// (ConsumerDetailBean)getIntent().getSerializableExtra("object");
		// // adapter=new PagerAdapter(getSupportFragmentManager(), 3, 1,
		// test_bean_cons);
		// }

	}

	@Override
	public void passDataToFragment() {
		// TODO Auto-generated method stub

	}

	@Override
	public void change_state() {
		// TODO Auto-generated method stub
		Log.e("inside of ", "change state");
		value_change_state = 1;
	}

	@Override
	public void Add_click() {
		// TODO Auto-generated method stub

	}

	@Override
	public void locationToolbar(boolean isLocationSerach) {
		// TODO Auto-generated method stub

	}

}
