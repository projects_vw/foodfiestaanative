package com.ikomet.foodfiestaa.activities;


import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.utils.ServiceHandler;
import com.ikomet.foodfiestaa.utils.SharedPrefs;
import com.tmxlr.lib.driodvalidatorlight.Form;
import com.tmxlr.lib.driodvalidatorlight.helper.Range;
import com.tmxlr.lib.driodvalidatorlight.helper.RegexTemplate;

public class TestActivity extends Activity{

	Switch switch1, switch2;
	ImageView photoHolder;
	EditText fshortName,fdescription, maxOrder, maxOrderPC, pricePerOrder, ffPrice;
	EditText selectfType, selectdType, selectcType, daysBeforePre, availStarttime, availEndtime; 
	View view3;
	Button upload, add;
	RelativeLayout relSnack;
	protected static final int REQUEST_CAMERA = 0;
	protected static final int SELECT_FILE = 1;
	protected static final int CROP_PIC = 2;
	private Uri picUri;
	String imagePath = "";
	public final String ADD_DISH_URL = "http://beta.foodfiestaa.com/adddishservice";
	Form form;
	
	 /**********  File Path *************/
    final String uploadFilePath = "/storage/9016-4EF8/DCIM/Camera/";
    final String uploadFileName = "IMG_20160404_031101476.jpg";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.frag_add_dish);
		
		upload = (Button)findViewById(R.id.bt_upload_img);
		photoHolder = (ImageView)findViewById(R.id.img_photo);
		fshortName = (EditText)findViewById(R.id.ed_fshort_name);
		fdescription = (EditText)findViewById(R.id.ed_fdescription);
		selectfType = (EditText)findViewById(R.id.ed_select_ftype);
		selectdType = (EditText)findViewById(R.id.ed_select_dtype);
		selectcType = (EditText)findViewById(R.id.ed_select_ctype);
		switch1 = (Switch)findViewById(R.id.switch1);
		maxOrder = (EditText)findViewById(R.id.ed_max_order);
		view3 = (View)findViewById(R.id.view3);
		switch2 = (Switch)findViewById(R.id.switch2);
		maxOrderPC = (EditText)findViewById(R.id.max_order_per_cons);
		daysBeforePre = (EditText)findViewById(R.id.sel_days_bef_pre);
		availStarttime = (EditText)findViewById(R.id.sel_av_starttime);
		availEndtime = (EditText)findViewById(R.id.sel_av_endtime);
		pricePerOrder = (EditText)findViewById(R.id.ed_price_perorder);
		ffPrice = (EditText)findViewById(R.id.ed_ff_price);
		add = (Button)findViewById(R.id.bt_add);
		
		relSnack = (RelativeLayout)findViewById(R.id.rel_snack);
		
		form = new Form(TestActivity.this);
		verifyForm();		
		add.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(switch1.isChecked()){
					checkSwitch1();
				}else{
					form.clear();
					if(switch2.isChecked()){
						checkSwitch2();
					}
					verifyForm();
				}
				
				if(switch2.isChecked()){
					checkSwitch2();
				}else{
					form.clear();
					if(switch1.isChecked()){
						checkSwitch1();
					}
					verifyForm();
				}
											
				if(form.validate()){
					
					if(!switch1.isChecked() && !switch2.isChecked()){
						Snackbar snackbar = Snackbar
						        .make(relSnack, "Please enable Today's Special or Pre-order", Snackbar.LENGTH_LONG);
						snackbar.show();
					}else{
						if(photoHolder.getVisibility()==View.VISIBLE){
							new AddDishProcess().execute();
						}else{
							Snackbar snackbar = Snackbar
							        .make(relSnack, "Please add an image for dish", Snackbar.LENGTH_LONG);
							snackbar.show();
						}
					}
					
				}else{
					Snackbar snackbar = Snackbar
					        .make(relSnack, "Please fill all the fields", Snackbar.LENGTH_LONG);
					snackbar.show();
				}
			}
		});
		
		upload.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				selectImage();
			}
		});
		
		selectfType.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPopupfType();
			}
		});
		
		selectdType.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPopupdType();
			}
		});
		
		selectcType.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPopupcType();
			}
		});
		
		daysBeforePre.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPopdaysbefPre();
			}
		});
		
		availStarttime.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPopupStartTime();
			}
		});
		
		availEndtime.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPopupEndTime();
			}
		});
		
		switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked){
					maxOrder.setVisibility(View.VISIBLE);
					view3.setVisibility(View.VISIBLE);
				}else{
					maxOrder.setVisibility(View.GONE);
					view3.setVisibility(View.GONE);
				}
			}
		});

		switch2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
		
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked){
					maxOrderPC.setVisibility(View.VISIBLE);
					daysBeforePre.setVisibility(View.VISIBLE);
					availStarttime.setVisibility(View.VISIBLE);
					availEndtime.setVisibility(View.VISIBLE);
				}else{
					maxOrderPC.setVisibility(View.GONE);
					daysBeforePre.setVisibility(View.GONE);
					availStarttime.setVisibility(View.GONE);
					availEndtime.setVisibility(View.GONE);
				}
			}
		});
	
		pricePerOrder.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if(!pricePerOrder.getText().toString().trim().equalsIgnoreCase("")){
					int val = Integer.parseInt(pricePerOrder.getText().toString().trim());
					int ffprice = val * (35/100);
					ffPrice.setText(String.valueOf(val + ffprice));
				}
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		
}
	
	public void showPopupfType() {
		final String[] foodTypes = { "Breakfast", "Lunch", "Sweets & Snacks", "Dinner", "Desserts",
				"Drinks", "Combo", "Bakery", "ServedAllDay" };
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Select Food Type");
		builder.setItems(foodTypes, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				// Do something with the selection
				selectfType.setTextColor(Color.BLACK);
				selectfType.setText(foodTypes[item]);
				dialog.dismiss();
			}
		});

		AlertDialog alert = builder.create();
		alert.show();

	}
	
	public void showPopupdType() {
		final String[] dietTypes = { "Vegetarian", "Non-Vegetarian" };
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Select Diet Type");
		builder.setItems(dietTypes, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				// Do something with the selection
				selectdType.setTextColor(Color.BLACK);
				selectdType.setText(dietTypes[item]);
				dialog.dismiss();
			}
		});

		AlertDialog alert = builder.create();
		alert.show();

	}
	
	public void showPopupcType() {
		final String[] cuisineTypes = { "Indian", "Chinese", "Thai", "French", "Italian",
				"Mexican", "Japanese", "Greek", "Lebanese", "Malay", "American", "Others" };
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Select Cuisine Type");
		builder.setItems(cuisineTypes, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				// Do something with the selection
				selectcType.setTextColor(Color.BLACK);
				selectcType.setText(cuisineTypes[item]);
				dialog.dismiss();
			}
		});

		AlertDialog alert = builder.create();
		alert.show();

	}
	
	public void showPopdaysbefPre() {
		final String[] days = { "1" };
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Days Before Pre-order");
		builder.setItems(days, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				// Do something with the selection
				daysBeforePre.setTextColor(Color.BLACK);
				daysBeforePre.setText(days[item]);
				dialog.dismiss();
			}
		});

		AlertDialog alert = builder.create();
		alert.show();

	}
	
	public void showPopupStartTime() {
		final String[] time = { "8:00 AM", "8:30 AM", "9:00 AM", "9:30 AM", "10:00 AM",
				"10:30 AM", "11:00 AM", "11:30 AM", "12:00 PM", "12:30 PM", "1:00 PM", "1:30 PM",
				"2:00 PM", "2:30 PM", "3:00 PM", "3:30 PM", "4:00 PM", "4:30 PM", "5:00 PM", "5:30 PM",
				"6:00 PM", "6:30 PM", "7:00 PM", "7:30 PM", "8:00 PM", "8:30 PM", "9:00 PM", "9:30 PM"};
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Available Start Time");
		builder.setItems(time, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				// Do something with the selection
				availStarttime.setTextColor(Color.BLACK);
				availStarttime.setText(time[item]);
				dialog.dismiss();
			}
		});

		AlertDialog alert = builder.create();
		alert.show();

	}
	
	public void showPopupEndTime() {
		final String[] time = { "8:00 AM", "8:30 AM", "9:00 AM", "9:30 AM", "10:00 AM",
				"10:30 AM", "11:00 AM", "11:30 AM", "12:00 PM", "12:30 PM", "1:00 PM", "1:30 PM",
				"2:00 PM", "2:30 PM", "3:00 PM", "3:30 PM", "4:00 PM", "4:30 PM", "5:00 PM", "5:30 PM",
				"6:00 PM", "6:30 PM", "7:00 PM", "7:30 PM", "8:00 PM", "8:30 PM", "9:00 PM", "9:30 PM"};
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Available End Time");
		builder.setItems(time, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				// Do something with the selection
				availEndtime.setTextColor(Color.BLACK);
				availEndtime.setText(time[item]);
				dialog.dismiss();
			}
		});

		AlertDialog alert = builder.create();
		alert.show();

	}
	
	private void selectImage() {
		final CharSequence[] items = { "Take Photo", "Choose from Library",
				"Cancel" };

		AlertDialog.Builder builder = new AlertDialog.Builder(
				TestActivity.this);
		builder.setTitle("Add Photo!");
		builder.setItems(items, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int item) {

				if (items[item].equals("Take Photo")) {
					Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					//intent.putExtra("crop", "true");
					startActivityForResult(intent, REQUEST_CAMERA);
				} else if (items[item].equals("Choose from Library")) {
					Intent intent = new Intent(
							Intent.ACTION_PICK,
							android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
					intent.setType("image/*");
					//intent.putExtra("crop", "true");
					startActivityForResult(
							Intent.createChooser(intent, "Select File"),
							SELECT_FILE);
				} else if (items[item].equals("Cancel")) {
					dialog.dismiss();
				}
			}
		});
		builder.show();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == REQUEST_CAMERA) {
//				picUri = data.getData();
				Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
				ByteArrayOutputStream bytes = new ByteArrayOutputStream();
				thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
				File destination = new File(
						Environment.getExternalStorageDirectory(),
						System.currentTimeMillis() + ".jpg");
				imagePath = destination.getAbsolutePath();
				FileOutputStream fo;
				try {
					destination.createNewFile();
					fo = new FileOutputStream(destination);
					fo.write(bytes.toByteArray());
					fo.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				picUri = Uri.fromFile(destination);
				Log.e("PIC URI TAKE PIC", picUri+"");
				performCrop();				
				// ivImage.setImageBitmap(thumbnail);
				/*photoHolder.setVisibility(View.VISIBLE);
				photoHolder.setImageBitmap(thumbnail);*/
				//adapter.notifyDataSetChanged();
			} else if (requestCode == SELECT_FILE) {
				//Bundle bundle = data.getExtras();
				picUri = data.getData();
				File auxFile = new File(picUri.toString());
				imagePath = auxFile.getAbsolutePath();
				Log.e("PIC URI SELECT FILE", picUri+"");
				performCrop();
			// user is returning from cropping the image
			} else if (requestCode == CROP_PIC) {
                // get the returned data
                Bundle extras = data.getExtras();
                // get the cropped bitmap
                Bitmap thePic = extras.getParcelable("data");
                
                picUri = getImageUri(getApplicationContext(), thePic);
                photoHolder.setVisibility(View.VISIBLE);
				photoHolder.setImageBitmap(thePic);
				Log.e("PIC URI AFTER CROP", picUri+"");
            }
		}

	}
	
	public Uri getImageUri(Context inContext, Bitmap inImage) {
		  ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		  inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
		  String path = Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
		  return Uri.parse(path);
		}
	
	private void performCrop() {
        // take care of exceptions
        try {
            // call the standard crop action intent (the user device may not
            // support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            Log.e("PIC URI ON CROP", picUri+"");
            cropIntent.setDataAndType(picUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // retrieve data on return
            cropIntent.putExtra("return-data", true);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, CROP_PIC);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            Toast toast = Toast
                    .makeText(this, "This device doesn't support the crop action!", Toast.LENGTH_SHORT);
            toast.show();
        }
	
}
		
	public class AddDishProcess extends AsyncTask<String, Void, String> {

		String response=null;
		
        @Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
		}

		@Override
		protected void onProgressUpdate(Void... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
		}

		protected String doInBackground(String... params) {

            HttpURLConnection conn = null;
            DataOutputStream dos = null;
            String lineEnd = "\r\n";
            String twoHyphens = "--";
            String boundary = "*****";
            int bytesRead, bytesAvailable, bufferSize;
            byte[] buffer;
            int maxBufferSize = 1 * 1024 * 1024;
            int serverResponseCode = 0;

         
            //parameters
            Bitmap bitmap = null;
            
			String id = SharedPrefs.getUid(getApplicationContext());
			String token = SharedPrefs.getToken(getApplicationContext());
//			String id ="270";
//			String token = "ulBdWIaSjN";
			String foodname = fshortName.getText().toString().trim();
			String description = fdescription.getText().toString().trim(); 
			String foodtype = selectfType.getText().toString().trim();
			String cuisinetype = selectcType.getText().toString().trim();
			String diettype =  selectdType.getText().toString().trim();
			String price = pricePerOrder.getText().toString().trim();
			//String foodpic = "";
			String preorderdays = daysBeforePre.getText().toString().trim();
			String preorder_maxorder = maxOrderPC.getText().toString().trim();
			String start_time = availStarttime.getText().toString().trim();
			String end_time = availEndtime.getText().toString().trim();
			String todayspecial_order = maxOrder.getText().toString().trim();


            try {

                System.setProperty("http.keepAlive","false");

                /////////////////////////////////////////////
                ////UPLOADING PICTURE AND DATA
/*                bitmap = MediaStore.Images.Media.getBitmap(TestActivity.this.getContentResolver(),picUri);

                //encoding image into a byte array
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] imageBytes = baos.toByteArray();*/

                // open a URL connection to the Servlet
                Log.e("PIC URI", ""+picUri);
                File file = new File(imagePath);
                FileInputStream fileInputStream = new FileInputStream(file);
//                ByteArrayInputStream fileInputStream = new ByteArrayInputStream(imageBytes);
                URL url = new URL(ADD_DISH_URL);

                // Open a HTTP  connection to  the URL
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs

                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Accept-Encoding", "");
                //conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                //conn.setRequestProperty("uploaded_file", "profile_picture");
                conn.setRequestProperty("id", id);
                conn.setRequestProperty("token", token);
                conn.setRequestProperty("foodname", foodname);
                conn.setRequestProperty("description", description);
                conn.setRequestProperty("foodtype", foodtype);
                conn.setRequestProperty("cuisinetype", cuisinetype);
                conn.setRequestProperty("diettype", diettype);
                conn.setRequestProperty("price", price);
                conn.setRequestProperty("foodpic", "foodpic");
                if(switch2.isChecked()){
                	conn.setRequestProperty("preorderdays", preorderdays);
                    conn.setRequestProperty("preorder_maxorder", preorder_maxorder);
                    conn.setRequestProperty("start_time", start_time);
                    conn.setRequestProperty("end_time", end_time);	
                }
                if(switch1.isChecked()){
                	conn.setRequestProperty("todayspecial_order", todayspecial_order);
                }
                dos = new DataOutputStream(conn.getOutputStream());

              
                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"id\"" + lineEnd + lineEnd
                        + id + lineEnd);

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"token\"" + lineEnd + lineEnd
                        + token + lineEnd);

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"foodname\"" + lineEnd + lineEnd
                        + foodname + lineEnd);
                
                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"description\"" + lineEnd + lineEnd
                        + description + lineEnd);

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"foodtype\"" + lineEnd + lineEnd
                        + foodtype + lineEnd);

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"cuisinetype\"" + lineEnd + lineEnd
                        + cuisinetype + lineEnd);

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"diettype\"" + lineEnd + lineEnd
                        + diettype + lineEnd);

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"price\"" + lineEnd + lineEnd
                        + price + lineEnd);
                
                if(switch2.isChecked()){
                
                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"preorderdays\"" + lineEnd + lineEnd
                        + preorderdays + lineEnd);

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"preorder_maxorder\"" + lineEnd + lineEnd
                        + preorder_maxorder + lineEnd);

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"start_time\"" + lineEnd + lineEnd
                        + start_time + lineEnd);

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"end_time\"" + lineEnd + lineEnd
                        + end_time + lineEnd);
                
                }
                
                if(switch1.isChecked()){

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"todayspecial_order\"" + lineEnd + lineEnd
                        + todayspecial_order + lineEnd);
                }
                
                //forth parameter - filename
                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"foodpic\";filename=\""
                        + "foodpic" + "\"" + lineEnd);
                
                dos.writeBytes(lineEnd);

                // create a buffer of  maximum size
                bytesAvailable = fileInputStream.available();

                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];

                // read file and write it into form...
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                while (bytesRead > 0) {
                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }

                // send multipart form data necesssary after file data...
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                // Responses from the server (code and message)
                serverResponseCode = conn.getResponseCode();

                String serverResponseMessage = conn.getResponseMessage();

                Log.i("uploadFile", "HTTP Response is : "
                        + serverResponseMessage + ": " + serverResponseCode);

                //close the streams //
                fileInputStream.close();
                dos.flush();
                dos.close();
                
                try {
					InputStream inp = null;

					inp = new BufferedInputStream(conn.getInputStream());

					ServiceHandler sh = new ServiceHandler();
					
					response = sh.readStream(inp);
					// Toast.makeText(getApplicationContext(), json,
					// 1000).show();
				} finally {
					conn.disconnect();
				}
                                                

            } catch (MalformedURLException ex) {
                Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
            } catch (Exception e) {
                String err = (e.getMessage()==null)?"SD Card failed":e.getMessage();
                Log.e("The caught exception is: ", err);
            }
            return null;
        }

    }

	public void verifyForm(){
		form.check(fshortName, RegexTemplate.NOT_EMPTY_PATTERN, "Must not be empty");
		form.check(fdescription, RegexTemplate.NOT_EMPTY_PATTERN, "Must not be empty");
		form.check(selectfType, RegexTemplate.NOT_EMPTY_PATTERN, "Please select a Food type");
		form.check(selectdType, RegexTemplate.NOT_EMPTY_PATTERN, "Please select a Diet type");
		form.check(selectcType, RegexTemplate.NOT_EMPTY_PATTERN, "Please select a Cuisine type");
		form.check(pricePerOrder, RegexTemplate.NOT_EMPTY_PATTERN, "Must not be empty");
		form.check(ffPrice, RegexTemplate.NOT_EMPTY_PATTERN, "Must not be empty");
	}

	public void checkSwitch1(){
		form.check(maxOrder, RegexTemplate.NOT_EMPTY_PATTERN, "Must not be empty");
		form.checkValue(maxOrder, Range.equalsOrMoreAndEqualsOrLess(1, 100), "Max Order must be 1 - 100");
	}
	
	public void checkSwitch2(){
		form.check(maxOrderPC, RegexTemplate.NOT_EMPTY_PATTERN, "Must not be empty");
		form.checkValue(maxOrderPC, Range.equalsOrMoreAndEqualsOrLess(1, 100), "Max Order Per Customer must be 1 - 100");
		form.check(daysBeforePre, RegexTemplate.NOT_EMPTY_PATTERN, "Please select the days before pre-order");
		form.check(availStarttime, RegexTemplate.NOT_EMPTY_PATTERN, "Please select the start time");
		form.check(maxOrderPC, RegexTemplate.NOT_EMPTY_PATTERN, "Please select the end time");
	}
	
}
