package com.ikomet.foodfiestaa.activities;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.ikomet.foodfiestaa.R;

public class StaticPagesActivity extends ActionBarActivity{

	private Toolbar mToolbar;
	TextView txtTitle, txtDetail;
	String title, detail;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_static);
		
		mToolbar = (Toolbar) findViewById(R.id.toolbar);
		mToolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
		setSupportActionBar(mToolbar);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);

		mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle("");

		title = getIntent().getStringExtra("cms_name");
		detail = getIntent().getStringExtra("cms_content");
		
		 txtTitle = (TextView)findViewById(R.id.txt_title);
		 txtDetail = (TextView)findViewById(R.id.txt_detail);
		
		 txtTitle.setText(title);
		 txtDetail.setText(Html.fromHtml(detail));
		
	}
	
}
