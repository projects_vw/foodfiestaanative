package com.ikomet.foodfiestaa.activities;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.beans.DishDetailBean;
import com.ikomet.foodfiestaa.utils.ServiceHandler;
import com.ikomet.foodfiestaa.utils.SharedPrefs;
import com.tmxlr.lib.driodvalidatorlight.Form;

public class EditDish extends Fragment {

	Switch switch1, switch2;
	ImageView photoHolder;
	EditText fshortName, fdescription, maxOrder, maxOrderPC, pricePerOrder,
			ffPrice;
	EditText selectfType, selectdType, selectcType, daysBeforePre,
			availStarttime, availEndtime;
	View view3;
	Button upload, add;
	RelativeLayout relSnack;
	public final String EDIT_DISH_URL = "http://beta.foodfiestaa.com/editdishservice";
	Form form;
	ProgressDialog pDialog;
	View layout;
	DishDetailBean dish_detail;
	
	public EditDish(){
		
	}
	
	public EditDish(DishDetailBean dish_detail){
		this.dish_detail = dish_detail;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			 ViewGroup container,  Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		layout = inflater.inflate(R.layout.frag_edit_dish, container, false);
				
		photoHolder = (ImageView) layout.findViewById(R.id.img_photo);
		fshortName = (EditText) layout.findViewById(R.id.ed_fshort_name);
		fdescription = (EditText) layout.findViewById(R.id.ed_fdescription);
		selectfType = (EditText) layout.findViewById(R.id.ed_select_ftype);
		selectdType = (EditText) layout.findViewById(R.id.ed_select_dtype);
		selectcType = (EditText) layout.findViewById(R.id.ed_select_ctype);
		switch1 = (Switch) layout.findViewById(R.id.switch1);
		maxOrder = (EditText) layout.findViewById(R.id.ed_max_order);
		view3 = (View) layout.findViewById(R.id.view3);
		switch2 = (Switch) layout.findViewById(R.id.switch2);
		maxOrderPC = (EditText) layout.findViewById(R.id.max_order_per_cons);
		daysBeforePre = (EditText) layout.findViewById(R.id.sel_days_bef_pre);
		availStarttime = (EditText) layout.findViewById(R.id.sel_av_starttime);
		availEndtime = (EditText) layout.findViewById(R.id.sel_av_endtime);
		pricePerOrder = (EditText) layout.findViewById(R.id.ed_price_perorder);
		ffPrice = (EditText) layout.findViewById(R.id.ed_ff_price);
		add = (Button) layout.findViewById(R.id.bt_add);

		relSnack = (RelativeLayout) layout.findViewById(R.id.rel_snack);

		selectfType.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPopupfType();
			}
		});

		selectdType.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPopupdType();
			}
		});

		selectcType.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPopupcType();
			}
		});

		daysBeforePre.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPopdaysbefPre();
			}
		});

		availStarttime.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPopupStartTime();
			}
		});

		availEndtime.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPopupEndTime();
			}
		});

		switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				if (isChecked) {
					maxOrder.setVisibility(View.VISIBLE);
					view3.setVisibility(View.VISIBLE);
				} else {
					maxOrder.setVisibility(View.GONE);
					view3.setVisibility(View.GONE);
				}
			}
		});

		switch2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				if (isChecked) {
					maxOrderPC.setVisibility(View.VISIBLE);
					daysBeforePre.setVisibility(View.VISIBLE);
					availStarttime.setVisibility(View.VISIBLE);
					availEndtime.setVisibility(View.VISIBLE);
				} else {
					maxOrderPC.setVisibility(View.GONE);
					daysBeforePre.setVisibility(View.GONE);
					availStarttime.setVisibility(View.GONE);
					availEndtime.setVisibility(View.GONE);
				}
			}
		});

		pricePerOrder.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				if (!pricePerOrder.getText().toString().trim()
						.equalsIgnoreCase("")) {
					int val = Integer.parseInt(pricePerOrder.getText()
							.toString().trim());
					int ffprice = val * (35 / 100);
					ffPrice.setText(String.valueOf(val + ffprice));
				}

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		
		
		return layout;
		
		
	}

	public void showPopupfType() {
		final String[] foodTypes = { "Breakfast", "Lunch", "Sweets & Snacks",
				"Dinner", "Desserts", "Drinks", "Combo", "Bakery",
				"ServedAllDay" };
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Select Food Type");
		builder.setItems(foodTypes, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				// Do something with the selection
				selectfType.setTextColor(Color.BLACK);
				selectfType.setText(foodTypes[item]);
				dialog.dismiss();
			}
		});

		AlertDialog alert = builder.create();
		alert.show();

	}

	public void showPopupdType() {
		final String[] dietTypes = { "Vegetarian", "Non-Vegetarian" };
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Select Diet Type");
		builder.setItems(dietTypes, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				// Do something with the selection
				selectdType.setTextColor(Color.BLACK);
				selectdType.setText(dietTypes[item]);
				dialog.dismiss();
			}
		});

		AlertDialog alert = builder.create();
		alert.show();

	}

	public void showPopupcType() {
		final String[] cuisineTypes = { "Indian", "Chinese", "Thai", "French",
				"Italian", "Mexican", "Japanese", "Greek", "Lebanese", "Malay",
				"American", "Others" };
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Select Cuisine Type");
		builder.setItems(cuisineTypes, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				// Do something with the selection
				selectcType.setTextColor(Color.BLACK);
				selectcType.setText(cuisineTypes[item]);
				dialog.dismiss();
			}
		});

		AlertDialog alert = builder.create();
		alert.show();

	}

	public void showPopdaysbefPre() {
		final String[] days = { "1" };
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Days Before Pre-order");
		builder.setItems(days, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				// Do something with the selection
				daysBeforePre.setTextColor(Color.BLACK);
				daysBeforePre.setText(days[item]);
				dialog.dismiss();
			}
		});

		AlertDialog alert = builder.create();
		alert.show();

	}

	public void showPopupStartTime() {
		final String[] time = { "8:00 AM", "8:30 AM", "9:00 AM", "9:30 AM",
				"10:00 AM", "10:30 AM", "11:00 AM", "11:30 AM", "12:00 PM",
				"12:30 PM", "1:00 PM", "1:30 PM", "2:00 PM", "2:30 PM",
				"3:00 PM", "3:30 PM", "4:00 PM", "4:30 PM", "5:00 PM",
				"5:30 PM", "6:00 PM", "6:30 PM", "7:00 PM", "7:30 PM",
				"8:00 PM", "8:30 PM", "9:00 PM", "9:30 PM" };
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Available Start Time");
		builder.setItems(time, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				// Do something with the selection
				availStarttime.setTextColor(Color.BLACK);
				availStarttime.setText(time[item]);
				dialog.dismiss();
			}
		});

		AlertDialog alert = builder.create();
		alert.show();

	}

	public void showPopupEndTime() {
		final String[] time = { "8:00 AM", "8:30 AM", "9:00 AM", "9:30 AM",
				"10:00 AM", "10:30 AM", "11:00 AM", "11:30 AM", "12:00 PM",
				"12:30 PM", "1:00 PM", "1:30 PM", "2:00 PM", "2:30 PM",
				"3:00 PM", "3:30 PM", "4:00 PM", "4:30 PM", "5:00 PM",
				"5:30 PM", "6:00 PM", "6:30 PM", "7:00 PM", "7:30 PM",
				"8:00 PM", "8:30 PM", "9:00 PM", "9:30 PM" };
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Available End Time");
		builder.setItems(time, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				// Do something with the selection
				availEndtime.setTextColor(Color.BLACK);
				availEndtime.setText(time[item]);
				dialog.dismiss();
			}
		});

		AlertDialog alert = builder.create();
		alert.show();

	}

	class Feedback extends AsyncTask<String, String, String> {

		boolean status;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Please Wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub
			// Check for success tag

			String json = "";

			try {

				String id, token, menuid, foodtype, cuisinetype, diettype, price, foodpic, preorderdays, preorder_maxorder, start_time, end_time, todayspecial_order;
				
				/*
				 * id,token,menuid,data(,foodtype,cuisinetype,diettype,price,
				 * preorderdays
				 * ,preorder_maxorder,start_time,end_time,todayspecial_order)
				 */
				
				id = SharedPrefs.getUid(getActivity());
				token = SharedPrefs.getToken(getActivity());
				//menuid = 

				// you need to encode ONLY the values of the parameters
				String param = "id=" + URLEncoder.encode(id, "UTF-8")
						+ "&token=" + URLEncoder.encode(token, "UTF-8")
						+ "&menuid=" + URLEncoder.encode(dish_detail.getMid(), "UTF-8")
						+ "&foodtype=" + URLEncoder.encode("", "UTF-8")
						+ "&cuisinetype=" + URLEncoder.encode("", "UTF-8")
						+ "&diettype=" + URLEncoder.encode("", "UTF-8")
						+ "&price=" + URLEncoder.encode("", "UTF-8")
						+ "&preorderdays=" + URLEncoder.encode("", "UTF-8")
						+ "&preorder_maxorder="	+ URLEncoder.encode("", "UTF-8") 
						+ "&start_time=" + URLEncoder.encode("", "UTF-8") 
						+ "&end_time=" + URLEncoder.encode("", "UTF-8")
						+ "&todayspecial_order=" + URLEncoder.encode("", "UTF-8");

				ServiceHandler sh = new ServiceHandler();
				json = sh.makeServiceCall(EDIT_DISH_URL, ServiceHandler.POST,
						param);

				JSONObject jsonObj = new JSONObject(json);

				status = jsonObj.getBoolean("status");

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (status) {

				return "success";
			} else {
				return "failure";

			}

		}

		protected void onPostExecute(String value) {
			// dismiss the dialog once product deleted
			if (pDialog.isShowing()) {
				pDialog.dismiss();
			}

			if (value.equalsIgnoreCase("success")) {

				Toast.makeText(getActivity(), "Success",
						Toast.LENGTH_SHORT).show();

			} else if (value.equalsIgnoreCase("failure")) {
				Toast.makeText(getActivity(), "Problem",
						Toast.LENGTH_SHORT).show();

			}

		}

	}

}
