package com.ikomet.foodfiestaa.activities;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import static com.basgeekball.awesomevalidation.ValidationStyle.UNDERLABEL;
//import static com.basgeekball.awesomevalidation.ValidationStyle.BASIC;
//import static com.basgeekball.awesomevalidation.ValidationStyle.COLORATION;
import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.utils.Dialog_notverifiedmail;
import com.ikomet.foodfiestaa.utils.InternetCheck;
import com.ikomet.foodfiestaa.utils.ReadStream;
import com.ikomet.foodfiestaa.utils.ServiceHandler;
import com.tmxlr.lib.driodvalidatorlight.Form;
import com.tmxlr.lib.driodvalidatorlight.helper.Range;
import com.tmxlr.lib.driodvalidatorlight.helper.RegexTemplate;

public class ContactUs extends ActionBarActivity {

	private Toolbar mToolbar;
	EditText name, mail, mobile, subject, msg;
	TextWatcher Name, Mail, Mobile, Subject, Msg;
	Button contactUs;
	Form form;
	ProgressDialog pDialog;
	public static final String CONTACT_US_URL = "http://50.57.127.125:10131/feedbackservice";
	private final String USER_AGENT = "Mozilla/5.0";
	Dialog dialog;
	
	// private AwesomeValidation mAwesomeValidation;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cook_contactus);

		noNetDialog();
		
		mToolbar = (Toolbar) findViewById(R.id.toolbar);
		mToolbar.setBackgroundColor(getResources().getColor(
				R.color.colorPrimary));
		setSupportActionBar(mToolbar);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);

		mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle("");

		name = (EditText) findViewById(R.id.ed_name);
		mail = (EditText) findViewById(R.id.ed_mail);
		mobile = (EditText) findViewById(R.id.ed_mobile);
		subject = (EditText) findViewById(R.id.ed_subject);
		msg = (EditText) findViewById(R.id.ed_msg);
		contactUs = (Button) findViewById(R.id.bt_send);

		form = new Form(ContactUs.this);
		form.check(name, RegexTemplate.NOT_EMPTY_PATTERN, "Must not be empty");
		form.check(mail, RegexTemplate.EMAIL_PATTERN, "Must be an Email");
		form.check(mobile, RegexTemplate.NOT_EMPTY_PATTERN, "Must not be empty");
		form.checkLength(mobile, Range.equal(10),
				"Enter a valid 10 digit mobile number");
		form.check(subject, RegexTemplate.NOT_EMPTY_PATTERN,
				"Must not be empty");
		form.check(msg, RegexTemplate.NOT_EMPTY_PATTERN, "Must not be empty");

		contactUs.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if (form.validate()) {
					if(InternetCheck.isInternetConnected(ContactUs.this)){
						new Feedback().execute();
					}else{
						dialog.show();
					}
				}
				
			}
		});

	}

	private void noNetDialog(){
		dialog = new Dialog(ContactUs.this);
		dialog.getWindow().requestFeature(1);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
		dialog.setContentView(R.layout.dialog_verification_signup);
//		dialog.setCancelable(false);
		TextView txtTitle = (TextView)dialog.findViewById(R.id.txt_content);
		ImageView imgCross = (ImageView)dialog.findViewById(R.id.img_tick);
		Button ok = (Button) dialog.findViewById(R.id.btn_ok);
		txtTitle.setText("Please connect to Internet");
		imgCross.setImageResource(R.drawable.circle_uncheck);
		ok.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
	}
	
	private void successDialog(){
		dialog = new Dialog(ContactUs.this);
		dialog.getWindow().requestFeature(1);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
		dialog.setContentView(R.layout.dialog_verification_signup);
//		dialog.setCancelable(false);
		TextView txtTitle = (TextView)dialog.findViewById(R.id.txt_content);
		ImageView imgCross = (ImageView)dialog.findViewById(R.id.img_tick);
		Button ok = (Button) dialog.findViewById(R.id.btn_ok);
		txtTitle.setText("Mail Sent Successfully");
//		imgCross.setImageResource(R.drawable.circle_uncheck);
		ok.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
	}
	
	class Feedback extends AsyncTask<String, String, String> {

		boolean status;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(ContactUs.this);
			pDialog.setMessage("Please Wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub
			// Check for success tag

			String json = "";

			try {

				String nm = name.getText().toString().trim();
				String email = mail.getText().toString().trim();
				String mob = mobile.getText().toString().trim();
				String subj = subject.getText().toString().trim();
				String message = msg.getText().toString().trim();

				// name,subject,message,email,phone

				// you need to encode ONLY the values of the parameters
				String param = "name=" + URLEncoder.encode(nm, "UTF-8")
						+ "&email=" + URLEncoder.encode(email, "UTF-8")
						+ "&phone=" + URLEncoder.encode(mob, "UTF-8")
						+ "&subject=" + URLEncoder.encode(subj, "UTF-8")
						+ "&message=" + URLEncoder.encode(message, "UTF-8");

				ServiceHandler sh = new ServiceHandler();
				json = sh.makeServiceCall(CONTACT_US_URL, ServiceHandler.POST,
						param);

				JSONObject jsonObj = new JSONObject(json);

				status = jsonObj.getBoolean("status");

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (status) {

				return "success";
			} else {
				return "failure";

			}

		}

		protected void onPostExecute(String value) {
			// dismiss the dialog once product deleted
			if (pDialog.isShowing()) {
				pDialog.dismiss();
			}

			if (value.equalsIgnoreCase("success")) {

				name.setText("");
				mail.setText("");
				mobile.setText("");
				subject.setText("");
				msg.setText("");
				successDialog();
				dialog.show();
				
			} else if (value.equalsIgnoreCase("failure")) {
				Toast.makeText(getApplicationContext(), "Problem",
						Toast.LENGTH_SHORT).show();

			}

		}

	}

}
