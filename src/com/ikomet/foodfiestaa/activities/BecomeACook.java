package com.ikomet.foodfiestaa.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.fragments.Fragment_general_becomeacook;

public class BecomeACook extends ActionBarActivity {

	Toolbar mToolbar;
	TextView toolbarText, txtSignIn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_become_a_cook);

		mToolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbarText = (TextView) mToolbar.findViewById(R.id.txt_toolbar);
		toolbarText.setText("BECOME A COOK");
		setSupportActionBar(mToolbar);
		getSupportActionBar().setTitle("");

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);

		mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		txtSignIn = (TextView)findViewById(R.id.txt_signin);
		
		txtSignIn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(BecomeACook.this, CookSignIn.class);
				finish();
				startActivity(intent);
			}
		});
		
		FragmentManager fragmanager = getSupportFragmentManager();
		FragmentTransaction transaction = fragmanager.beginTransaction();
		Fragment frag = new Fragment_general_becomeacook();
		transaction.add(R.id.frag_becomeacook, frag);
		transaction.commit();
	}

}
