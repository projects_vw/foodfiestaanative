package com.ikomet.foodfiestaa.activities;

import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.utils.DialogCustom;
import com.ikomet.foodfiestaa.utils.DialogForgotPwd;
import com.ikomet.foodfiestaa.utils.Dialog_notverifiedmail;
import com.ikomet.foodfiestaa.utils.InternetCheck;
import com.ikomet.foodfiestaa.utils.ServiceHandler;
import com.ikomet.foodfiestaa.utils.SharedPrefs;

public class CookSignIn extends ActionBarActivity {

	RelativeLayout rel_sign_out;

	EditText email, password;
	Button buttonSignin;
	ProgressDialog pDialog;
	TextView txt_forgot_pwd;
	public static final String LOGIN_URL = "http://beta.foodfiestaa.com/cookloginservice";
	private static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{8,20})";
	TextWatcher emailWatcher;
	TextWatcher passwordWatcher;
	private final String USER_AGENT = "Mozilla/5.0";
	private Toolbar mToolbar;
	TextView signUp;
	DialogCustom dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cook_signin);

		dialog = new DialogCustom();
		
		txt_forgot_pwd = (TextView) findViewById(R.id.txt_forgot_pwd);
		mToolbar = (Toolbar) findViewById(R.id.toolbar);
		mToolbar.setBackgroundColor(Color.TRANSPARENT);
		mToolbar.setTitle("");
		setSupportActionBar(mToolbar);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);

		mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		txt_forgot_pwd.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				DialogForgotPwd forgot_pwd = new DialogForgotPwd(
						CookSignIn.this,"1");
				forgot_pwd.show();
			}
		});
		rel_sign_out = (RelativeLayout) findViewById(R.id.layout_signin_box);

		email = (EditText) findViewById(R.id.ed_email);
		password = (EditText) findViewById(R.id.ed_password);
		buttonSignin = (Button) findViewById(R.id.bt_signin);

		signUp = (TextView) findViewById(R.id.txt_consumer);

		signUp.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(CookSignIn.this, BecomeACook.class);
				finish();
				startActivity(intent);
			}
		});

		buttonSignin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (email.getText().toString().trim().equalsIgnoreCase("")
						|| password.getText().toString().trim()
								.equalsIgnoreCase("")) {
					if (email.getText().toString().trim().equalsIgnoreCase("")
							&& password.getText().toString().trim()
									.equalsIgnoreCase("")) {
						email.setError("Please fill this field");
						password.setError("Please fill this field");
					} else if (email.getText().toString().trim()
							.equalsIgnoreCase("")) {
						email.setError("Please fill this field");
					} else {
						password.setError("Please fill this field");
					}
				} else {
					if (email.getError() == null || password.getError() == null) {
						if(InternetCheck.isInternetConnected(CookSignIn.this)){
							new AttemptLogin().execute();
						}else{
							dialog.showDialog(CookSignIn.this, "Please connect to Internet", "OK", R.drawable.circle_uncheck);
						}
					} else {

					}
				}
			}
		});

		emailWatcher = new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				if (s.toString().equalsIgnoreCase("")) {
					email.setError("Please fill this field");
				} else if (!isValidEmail(s.toString())) {
					email.setError("Please enter a valid mail id");
				} else {
					email.setError(null);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				if (s.toString().equalsIgnoreCase("")) {
					email.setError("Please fill this field");
				} else if (!isValidEmail(s.toString())) {
					email.setError("Please enter a valid mail id");
				} else {
					email.setError(null);
				}
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		};

		passwordWatcher = new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				if (s.toString().equalsIgnoreCase("")) {
					password.setError("Please fill this field");
				} else if (!isValidPassword(s.toString())) {
					password.setError("Please use minimum of 8 characters containing upper case, lowercase, numbers and non alpha-numeric characters");
				} else {
					password.setError(null);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				if (s.toString().equalsIgnoreCase("")) {
					password.setError("Please fill this field");
				} else if (!isValidPassword(s.toString())) {
					password.setError("Please use minimum of 8 characters containing upper case, lowercase, numbers and non alpha-numeric characters");
				} else {
					password.setError(null);
				}
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		};

		email.addTextChangedListener(emailWatcher);
		password.addTextChangedListener(passwordWatcher);

		int alpha = (int) (0.5 * 255.0f);
		rel_sign_out.setBackgroundColor(Color.argb(alpha, 195, 193, 128));

	}

	class AttemptLogin extends AsyncTask<String, String, String> {

		boolean status;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(CookSignIn.this);
			pDialog.setMessage("Attempting login...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub
			// Check for success tag

			String json = "";

			try {

				String mail = email.getText().toString().trim();
				String pass = password.getText().toString().trim();

				// you need to encode ONLY the values of the parameters
				String param = "email=" + URLEncoder.encode(mail, "UTF-8")
						+ "&password=" + URLEncoder.encode(pass, "UTF-8");

				ServiceHandler sh = new ServiceHandler();

				json = sh
						.makeServiceCall(LOGIN_URL, ServiceHandler.POST, param);

				JSONObject jsonObj = new JSONObject(json);

				status = jsonObj.getBoolean("status");

				if (status) {

					String token = jsonObj.getString("token");
					String userid = jsonObj.getString("id");
					SharedPrefs.putSP(CookSignIn.this, userid, token, "Cook");
					// return "LogIn successfull";
					return "success";
				} else {
					return jsonObj.getString("errorMessage");

				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			return json;

		}

		protected void onPostExecute(String value) {
			// dismiss the dialog once product deleted
			if (pDialog.isShowing()) {
				pDialog.dismiss();
			}

			if (value.equalsIgnoreCase("success")) {

				Toast.makeText(getApplicationContext(), "LogIn Successfull",
						Toast.LENGTH_SHORT).show();

				Intent intent = new Intent(CookSignIn.this, CookActivity.class);
				// intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
						| Intent.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.right_in, R.anim.left_out);

			} else if (value.equalsIgnoreCase("Please activate your account")) {
				Dialog_notverifiedmail not_verified_mail = new Dialog_notverifiedmail(
						CookSignIn.this);
				not_verified_mail.show();
			} else {
				Toast.makeText(getApplicationContext(), value,
						Toast.LENGTH_SHORT).show();
			}

		}

	}

	public final static boolean isValidPassword(String password) {

		Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
		Matcher matcher = pattern.matcher(password);

		return matcher.matches();
	}

	public final static boolean isValidEmail(CharSequence target) {
		if (target == null) {
			return false;
		} else {
			return android.util.Patterns.EMAIL_ADDRESS.matcher(target)
					.matches();
		}
	}

}
