package com.ikomet.foodfiestaa.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.fragments.Frag_cook_howitworks;
import com.ikomet.foodfiestaa.fragments.Frag_customer_howitworks;

public class HowItWorksNew extends ActionBarActivity implements OnClickListener {

	RelativeLayout rel_layout_cook, rel_layout_customer;
	FragmentManager manager;
	FragmentTransaction transaction;
	Toolbar mToolbar;
	TextView toolbarText,txt_cook,txt_consumer;
	ImageView img_cook ,img_consumer;
	Fragment frag = null;
	Button bt_how_consumer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_howitworks_new);
	 mToolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbarText = (TextView) mToolbar.findViewById(R.id.txt_toolbar);
		toolbarText.setText("BECOME A COOK");
		setSupportActionBar(mToolbar);
		getSupportActionBar().setTitle("");

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);

		mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		initialize();
		Frag_initialize();

	}

	public void initialize() {
		bt_how_consumer=(Button)findViewById(R.id.bt_send);
		rel_layout_cook = (RelativeLayout) findViewById(R.id.layout_cook);
		rel_layout_customer = (RelativeLayout) findViewById(R.id.layout_consumer);
		txt_cook=(TextView)findViewById(R.id.txt_cook);
		img_cook=(ImageView)findViewById(R.id.img_cook);
		img_consumer=(ImageView)findViewById(R.id.img_consumer);
		txt_consumer=(TextView)findViewById(R.id.txt_consumer);
		rel_layout_cook.setOnClickListener(this);
		rel_layout_customer.setOnClickListener(this);

	}

	public void Frag_initialize() {

		rel_layout_cook.setBackgroundColor(Color.WHITE);
		txt_cook.setTextColor(getResources().getColor(R.color.colorPrimary));
		img_cook.setImageResource(R.drawable.capyellowicon);

		rel_layout_customer.setBackgroundColor(getResources().getColor(
				R.color.tabBackground));
		txt_consumer.setTextColor(getResources().getColor(R.color.textSelected));
		img_consumer.setImageResource(R.drawable.dishgreyicon);
		bt_how_consumer.setText("CREATE MY HOME CAFE NOW");
		manager = getSupportFragmentManager();
		transaction = manager.beginTransaction();
		frag = new Frag_cook_howitworks();
		transaction.add(R.id.frame_test, frag);
		transaction.commit();

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		int id = v.getId();
		manager = getSupportFragmentManager();
		transaction = manager.beginTransaction();

		switch (id) {
		case R.id.layout_cook:
			bt_how_consumer.setText("CREATE MY HOME CAFE NOW");
			bt_how_consumer.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(HowItWorksNew.this, BecomeACook.class);
					finish();
					startActivity(intent);
					overridePendingTransition(R.anim.left_in, R.anim.right_out);
				}
			});
			rel_layout_cook.setBackgroundColor(Color.WHITE);
			txt_cook.setTextColor(getResources().getColor(R.color.colorPrimary));
			img_cook.setImageResource(R.drawable.capyellowicon);

			rel_layout_customer.setBackgroundColor(getResources().getColor(
					R.color.tabBackground));
			txt_consumer.setTextColor(getResources().getColor(R.color.textSelected));
			img_consumer.setImageResource(R.drawable.dishgreyicon);
						frag = new Frag_cook_howitworks();

			break;
		case R.id.layout_consumer:
			bt_how_consumer.setText("SEARCH & ORDER YOUR FOOD");
			bt_how_consumer.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					finish();
				}
			});
			rel_layout_cook.setBackgroundColor(getResources().getColor(
					R.color.tabBackground));
			txt_cook.setTextColor(getResources().getColor(R.color.textSelected));
			img_cook.setImageResource(R.drawable.capgreyicon);

			rel_layout_customer.setBackgroundColor(Color.WHITE);
			txt_consumer.setTextColor(getResources().getColor(R.color.colorPrimary));
			img_consumer.setImageResource(R.drawable.dishyellowicon);

			frag = new Frag_customer_howitworks();

			break;

		default:
			break;
		}
		transaction.replace(R.id.frame_test, frag);
		transaction.commit();

		/*
		 * if(id==R.id.layout_cook) { frag=new Frag_customer_howitworks();
		 * Frag_initialize(frag);
		 * 
		 * 
		 * } else if(id==R.id.layout_consumer); { frag=new
		 * Frag_cook_howitworks(); Frag_initialize(frag);
		 * 
		 * }
		 */}
}
