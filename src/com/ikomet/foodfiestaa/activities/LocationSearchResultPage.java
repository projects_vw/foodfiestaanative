package com.ikomet.foodfiestaa.activities;

import java.util.ArrayList;
import java.util.Stack;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.beans.LocSearchKitchenDetails;
import com.ikomet.foodfiestaa.databases.DBHandler;
import com.ikomet.foodfiestaa.fragments.Frag_SearchKitchen;
import com.ikomet.foodfiestaa.fragments.Frag_orderConsumer;
import com.ikomet.foodfiestaa.fragments.Frag_profile_consumer;
import com.ikomet.foodfiestaa.fragments.FragmentCart;
import com.ikomet.foodfiestaa.fragments.FragmentDrawer;
import com.ikomet.foodfiestaa.fragments.SignedDrawerConsumer;
import com.ikomet.foodfiestaa.interfaces.Cart;
import com.ikomet.foodfiestaa.interfaces.FragmentCommunicator;
import com.ikomet.foodfiestaa.interfaces.FragmentCommunicatorNew;
import com.ikomet.foodfiestaa.interfaces.Toggle;
import com.ikomet.foodfiestaa.utils.DialogVerification;
import com.ikomet.foodfiestaa.utils.DummyTabContent;
import com.ikomet.foodfiestaa.utils.InternetCheck;
import com.ikomet.foodfiestaa.utils.SharedPrefs;
import com.ikomet.foodfiestaa.utils.ShowHideToolbar;

public class LocationSearchResultPage extends ActionBarActivity implements
		FragmentCommunicator, Cart, View.OnClickListener, ShowHideToolbar,
		OnCheckedChangeListener {

	TabHost tab_host;
	FragmentDrawer frag_drawer;
	SignedDrawerConsumer signed_drawer;
	Toolbar toolbar;
	ImageView logo;
	ToggleButton location;
	TextView title, txt_cart;
	String location_id = "";
	FragmentManager fm;
	FragmentTransaction ft;
	Frag_SearchKitchen search_kitchen;
	FragmentCart cart;
	Frag_orderConsumer order_consumer;
	Frag_profile_consumer profile_consumer;
	Bundle bundle;
	boolean isSigned, isLoctionSearch;
	TabHost.OnTabChangeListener tabChangeListener;
	public FragmentCommunicatorNew communicator;
	DBHandler db;
	public static Stack<Fragment> fragmentStack, fragmentStack1;
	public static FragmentManager frag_manager;
	Dialog dialog;
	public Toggle toggle;
	public static ArrayList<LocSearchKitchenDetails> arr_kitchen_detail;
	public static ArrayList<String> location_list;
	public static LocSearchKitchenDetails details;
	public static ArrayList<Integer> location_list_id;
	DialogVerification verify;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_locationsearchresult);

		isSigned = SharedPrefs.hasSPData(getApplicationContext());

		if (!isSigned) {
			location_id = SharedPrefs
					.get_LocationId(LocationSearchResultPage.this);
		}

		
		
		
		
		db = new DBHandler(LocationSearchResultPage.this);

		Log.e("location + in classs was", "" + location_id);
		tab_host = (TabHost) findViewById(android.R.id.tabhost);
		tab_host.setup();
		tab_host.getTabWidget().setDividerDrawable(null);
		toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setTitle("");
		logo = (ImageView) toolbar.findViewById(R.id.img_header_logo);
		location = (ToggleButton) toolbar.findViewById(R.id.img_location);
		title = (TextView) toolbar.findViewById(R.id.txt_title);

		location.setOnClickListener(this);

		fragmentStack = new Stack<Fragment>();
		fragmentStack1 = new Stack<Fragment>();
		frag_manager = getSupportFragmentManager();

		frag_drawer = (FragmentDrawer) getSupportFragmentManager()
				.findFragmentById(R.id.fragment_navigation_drawer);
		frag_drawer.setUp(R.id.fragment_navigation_drawer,
				(DrawerLayout) findViewById(R.id.drawerLayout), toolbar);
		location.setOnCheckedChangeListener(this);
			
		/*
		 * if(currentLocation.canGetLocation()){ Log.e("current Lat",
		 * ""+currentLocation.getLatitude()); }else{ Log.e("current Lat",
		 * "could not get"); }
		 */

		tabChangeListener = new TabHost.OnTabChangeListener() {

			@Override
			public void onTabChanged(String tabId) {
				// TODO Auto-generated method stub
				fm = getSupportFragmentManager();
				ft = fm.beginTransaction();
				search_kitchen = (Frag_SearchKitchen) fm
						.findFragmentByTag("kitchen");
				cart = (FragmentCart) fm.findFragmentByTag("Cart");
				order_consumer = (Frag_orderConsumer) fm
						.findFragmentByTag("Orders");
				profile_consumer = (Frag_profile_consumer) fm
						.findFragmentByTag("Profile");

				if (tabId.equalsIgnoreCase("kitchen")) {
					locToolbar();
					Log.e("inside searchkitchen tab ", "");
					if (search_kitchen != null) {
						ft.detach(search_kitchen);
					}

					if (search_kitchen == null) {
						search_kitchen = new Frag_SearchKitchen();
						bundle = new Bundle();
						if (!isSigned) {
							bundle.putString("location_id", location_id);
						}
						search_kitchen.setArguments(bundle);
						ft.add(R.id.realtabcontent, search_kitchen, "kitchen");
						fragmentStack.push(search_kitchen);

					} else if (search_kitchen != null) {
						ft.attach(search_kitchen);
					}
				} else if (tabId.equalsIgnoreCase("Cart")) {
					Log.e("inside Cart tab ", "");
					textToolbar();
					if (cart != null) {
						ft.detach(cart);
					}

					if (cart == null) {

						cart = new FragmentCart();

						ft.add(R.id.realtabcontent, cart, "Cart");
						fragmentStack1.push(cart);
					} else if (search_kitchen != null) {
						ft.attach(cart);
					}
				} else if (tabId.equalsIgnoreCase("Orders")) {
					Log.e("inside Orders tab ", "");
					if (isSigned) {
						otherToolbar();
						if (order_consumer != null) {
							ft.detach(order_consumer);
						}

						if (order_consumer == null) {

							order_consumer = new Frag_orderConsumer();
							ft.add(R.id.realtabcontent, order_consumer,
									"Orders");

						} else if (order_consumer != null) {
							ft.attach(order_consumer);
						}
					} else {
						dialog = new Dialog(LocationSearchResultPage.this);
						dialog.getWindow().requestFeature(1);
						dialog.getWindow().setBackgroundDrawable(
								new ColorDrawable(0));
						dialog.setContentView(R.layout.dialog_verification_signup);
						ImageView icon = (ImageView) dialog
								.findViewById(R.id.img_tick);
						TextView content = (TextView) dialog
								.findViewById(R.id.txt_content);
						Button ok = (Button) dialog.findViewById(R.id.btn_ok);

						icon.setImageResource(R.drawable.circle_uncheck);
						content.setText("Sign up to keep track of your order history!");
						dialog.show();
						/*
						 * LayoutParams params = new LayoutParams(
						 * LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT
						 * ); params.setMargins(30, 0, 30, 0);
						 * ok.setLayoutParams(params);
						 */
						ok.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								dialog.dismiss();
							}
						});

					}
				} else if (tabId.equalsIgnoreCase("Profile")) {
					Log.e("inside Profile tab ", "");
					if (isSigned) {
						otherToolbar();
					if (profile_consumer != null) {
						ft.detach(profile_consumer);
					}

					if (profile_consumer == null) {

						profile_consumer = new Frag_profile_consumer();
						ft.add(R.id.realtabcontent, profile_consumer, "Profile");

					} else if (profile_consumer != null) {
						ft.attach(profile_consumer);
					}
				}else {
					dialog = new Dialog(LocationSearchResultPage.this);
					dialog.getWindow().requestFeature(1);
					dialog.getWindow().setBackgroundDrawable(
							new ColorDrawable(0));
					dialog.setContentView(R.layout.dialog_verification_signup);
					ImageView icon = (ImageView) dialog
							.findViewById(R.id.img_tick);
					TextView content = (TextView) dialog
							.findViewById(R.id.txt_content);
					Button ok = (Button) dialog.findViewById(R.id.btn_ok);

					icon.setImageResource(R.drawable.circle_uncheck);
					content.setText("Sign up to view your profile Information!");
					dialog.show();
										
					ok.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							dialog.dismiss();
						}
					});
							
				
				}
				}
				ft.commit();
			}

		};
//		if(InternetCheck.isInternetConnected(LocationSearchResultPage.this)){
			tab_host.setOnTabChangedListener(tabChangeListener);	
			
			if(getIntent().hasExtra("paid")){
				dialog = new Dialog(LocationSearchResultPage.this);
				dialog.getWindow().requestFeature(1);
				dialog.getWindow().setBackgroundDrawable(
						new ColorDrawable(0));
				dialog.setContentView(R.layout.dialog_verification_signup);
				ImageView icon = (ImageView) dialog
						.findViewById(R.id.img_tick);
				TextView content = (TextView) dialog
						.findViewById(R.id.txt_content);
				Button ok = (Button) dialog.findViewById(R.id.btn_ok);

				icon.setImageResource(R.drawable.circle_check);
				content.setText("Your payment has been done");
				tab_host.setCurrentTab(1);
				dialog.show();
									
				ok.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						tab_host.setCurrentTab(2);
						dialog.dismiss();
					}
				});
			}
			
			
		/*}else{
			
			verify= new DialogVerification(LocationSearchResultPage.this, 3, "Check your Internet Connection");
			Button ok = (Button)verify.findViewById(R.id.btn_ok);
		
			ok.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(InternetCheck.isInternetConnected(LocationSearchResultPage.this)){
						tab_host.setOnTabChangedListener(tabChangeListener);
						verify.dismiss();
					}
				}
			});
			verify.show();                                                                                                                                                                                                                
		}*/
			
		
		
		TabHost.TabSpec tSpecAndroid = tab_host.newTabSpec("kitchen");
		tSpecAndroid.setIndicator(createTabView(R.drawable.tabsearch_selector1,
				"kitchen"));
		tSpecAndroid.setContent(new DummyTabContent(getBaseContext()));
		tab_host.addTab(tSpecAndroid);

		/** Defining tab builder for Apple tab */
		TabHost.TabSpec tSpecApple = tab_host.newTabSpec("Cart");
		tSpecApple.setIndicator(createTabView(R.drawable.tabsearch_selector2,
				"order"));
		tSpecApple.setContent(new DummyTabContent(getBaseContext()));
		tab_host.addTab(tSpecApple);

		TabHost.TabSpec tSpecKithchen = tab_host.newTabSpec("Orders");
		tSpecKithchen.setIndicator(createTabView(
				R.drawable.tabsearch_selector3, "pay"));
		tSpecKithchen.setContent(new DummyTabContent(getBaseContext()));
		tab_host.addTab(tSpecKithchen);

		TabHost.TabSpec tSpecProfile = tab_host.newTabSpec("Profile");
		tSpecProfile.setIndicator(createTabView(R.drawable.tab4_selector,
				"profile"));
		tSpecProfile.setContent(new DummyTabContent(getBaseContext()));
		tab_host.addTab(tSpecProfile);

		if(getIntent().hasExtra("gocart")){
			Log.e("Location Search Result Page","has extra");
			tab_host.setCurrentTab(1);
		}
		
		if(getIntent().hasExtra("paid")){
			tab_host.setCurrentTab(1);
		}
		
	}

	private View createTabView(final int id, String name) {

		if (name.equalsIgnoreCase("order")) {
			View view1 = LayoutInflater.from(this).inflate(R.layout.tabs_cart,
					null);
			RelativeLayout rel_layout = (RelativeLayout) view1
					.findViewById(R.id.tabs_icon);
			TextView txt_title1 = (TextView) view1.findViewById(R.id.txt_label);
			txt_cart = (TextView) view1.findViewById(R.id.txt_cart);
			rel_layout.setBackground(getResources().getDrawable(id));
			txt_title1.setText(name);
			txt_cart.setText(String.valueOf(db.getTotalCount()));
			if (db.getTotalCount() == 0) {
				txt_cart.setVisibility(View.GONE);
			} else {
				txt_cart.setVisibility(View.VISIBLE);
			}
			return view1;
		} else {
			View view = LayoutInflater.from(this).inflate(R.layout.tabs_icon,
					null);
			ImageView imageView = (ImageView) view.findViewById(R.id.tab_icon);
			TextView txt_title = (TextView) view.findViewById(R.id.txt_label);
			txt_title.setText(name);
			imageView.setImageDrawable(getResources().getDrawable(id));

			return view;
		}

	}

	public void locToolbar() {
		logo.setVisibility(View.VISIBLE);
		location.setVisibility(View.VISIBLE);
		title.setVisibility(View.GONE);
	}

	public void textToolbar() {
		logo.setVisibility(View.GONE);
		location.setVisibility(View.GONE);
		title.setVisibility(View.VISIBLE);
	}
	
	public void otherToolbar() {
		logo.setVisibility(View.VISIBLE);
		location.setVisibility(View.GONE);
		title.setVisibility(View.GONE);
	}

	@Override
	public void passDataToFragment() {
		// TODO Auto-generated method stub

	}

	@Override
	public void change_state() {
		// TODO Auto-generated method stub

	}

	@Override
	public void Add_click() {
		// TODO Auto-generated method stub

	}

	@Override
	public void locationToolbar(boolean isLocationSerach) {
		// TODO Auto-generated method stub
		locToolbar();
		this.isLoctionSearch = isLocationSerach;
	}

	@Override
	public void onClick(View v) {
		/*if (v.getId() == R.id.img_location) {
			communicator.passnewDataToFragment("", "");
		}*/
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
	Log.e("on Back pressed ",""+fragmentStack.size());
	Log.e("on Back pressed 1",""+fragmentStack1.size());
	
		if (fragmentStack1.size() == 2) {
			
			FragmentTransaction ft = getSupportFragmentManager()
					.beginTransaction();
			fragmentStack1.lastElement().onPause();
			ft.remove(fragmentStack1.pop());
			fragmentStack1.lastElement().onResume();
			ft.replace(R.id.realtabcontent, fragmentStack1.lastElement());
			ft.commit();
		} else if (fragmentStack.size() == 2) {
			
			FragmentTransaction ft = getSupportFragmentManager()
					.beginTransaction();
			fragmentStack.lastElement().onPause();
			ft.remove(fragmentStack.pop());
			fragmentStack.lastElement().onResume();
			ft.replace(R.id.realtabcontent, fragmentStack.lastElement());
			ft.commit();
		} else{
			super.onBackPressed();
		}
		
		

	}

	@Override
	public void cartItem(int val) {
		// TODO Auto-generated method stub
		int cart_count = db.getTotalCount();
		txt_cart.setText(String.valueOf(cart_count));
		if (cart_count == 0) {
			txt_cart.setVisibility(View.GONE);
		} else {
			txt_cart.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void nilItems() {
		// TODO Auto-generated method stub

	}

	@Override
	public void showToolbar() {
		// TODO Auto-generated method stub
		getSupportActionBar().show();
	}

	@Override
	public void hideToolbar() {
		// TODO Auto-generated method stub
		getSupportActionBar().hide();
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		// TODO Auto-generated method stub
		if (isChecked) {
			Log.e("true", "turn on");
			toggle.ControlToggle(true);

		} else {
			Log.e("false", "turn off");
			toggle.ControlToggle(false);
		}
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		Log.e("the fragment state", ""+SharedPrefs.get_FragmentState(LocationSearchResultPage.this, "FragmentA"));
		SharedPrefs.put_FragmentState(LocationSearchResultPage.this, "FragmentA", 0);
		
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		Log.e("the fragment state", ""+SharedPrefs.get_FragmentState(LocationSearchResultPage.this, "FragmentA"));
		SharedPrefs.put_FragmentState(LocationSearchResultPage.this, "FragmentA", 0);
		SharedPrefs.clearLocationId(LocationSearchResultPage.this);
		SharedPrefs.removeLatLong(LocationSearchResultPage.this);
		finish();
	}
	
}
