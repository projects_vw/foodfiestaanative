package com.ikomet.foodfiestaa.activities;

import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.http.SslError;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.beans.CartBean;
import com.ikomet.foodfiestaa.databases.DBHandler;
import com.ikomet.foodfiestaa.utils.ServiceHandler;

@SuppressLint("JavascriptInterface")
public class PaymentActivity extends ActionBarActivity {
	WebView webView;
	
    String merchant_key = "gtKFFx";
    String salt = "eCwWELxi";
    String action1 = "";
    String base_url = "https://test.payu.in";
    // int error = 0;
    // String hashString = "";
    // Map<String, String> params;
    String txnid = "TXN8367286482920";
    String amount = "10";
    String productInfo = "pet products";
    String firstName = "Ganesh";
    String emailId = "ganesh.s.mech@gmail.com";
    String payLocation, address, kitchenId, guestUser, orderType, orderData, orderDate, cartData = "";
    
    private String SUCCESS_URL = "https://payu.herokuapp.com/success";
    private String FAILED_URL = "https://payu.herokuapp.com/failure";
    private String phone = "8754211855";
   // private String serviceProvider = "payu_paisa";
    private String hash = "";

    Handler mHandler = new Handler();
    ProgressDialog pDialog;
    private final String PAY_SERVICE_URL = "http://beta.foodfiestaa.com/makepaymentconsumer";
    DBHandler db;
    ArrayList<CartBean> cartItems;
            
	@Override
    protected void onCreate(Bundle savedInstanceState) {

		getWindow().requestFeature(Window.FEATURE_PROGRESS);
		super.onCreate(savedInstanceState);
        webView = new WebView(this);
        setContentView(webView);

        db = new DBHandler(PaymentActivity.this);
        
        Bundle bundle = getIntent().getExtras();
       
        emailId = bundle.getString("emailId");
        payLocation = bundle.getString("pay_location");
        firstName = bundle.getString("firstName");
		address = bundle.getString("addrs");
		phone = bundle.getString("phon");
		amount = bundle.getString("totalAmount");
		kitchenId = bundle.getString("kitchen_id");
		guestUser = bundle.getString("guest_user");
		orderType = bundle.getString("order_type");
//		orderData = bundle.getString("order_data");
		orderDate = bundle.getString("order_date");
        
		try{
		
		JSONArray cartDataArray = new JSONArray();
		
		cartItems = new ArrayList<CartBean>();
		cartItems = db.getAllItems();
		
		for(int i=0;i<db.getTotalCount();i++){
			
			JSONObject cartObject = new JSONObject();
			CartBean cb = cartItems.get(i);			
			
			cartObject.put("menu_id", cb.getMenu_id());
			cartObject.put("food_name", cb.getFood_name());
			String price = String.valueOf(Double.parseDouble(cb.getFood_price())*cb.getItems_added());
			cartObject.put("price", price);
			cartObject.put("quantity", cb.getItems_added());
			cartObject.put("itemprice", cb.getFood_price());
			
			cartDataArray.put(cartObject);
			
		}
		
		cartData = cartDataArray.toString();
		System.out.println(cartDataArray.toString());
		
		}catch(Exception e){
			e.printStackTrace();
		}
				
        JSONObject productInfoObj = new JSONObject();
        JSONArray productPartsArr = new JSONArray();
        JSONObject productPartsObj1 = new JSONObject();
        JSONObject paymentIdenfierParent = new JSONObject();
        JSONArray paymentIdentifiersArr = new JSONArray();
        JSONObject paymentPartsObj1 = new JSONObject();
        JSONObject paymentPartsObj2 = new JSONObject();
        try {
            // Payment Parts
            productPartsObj1.put("name", "abc");
            productPartsObj1.put("description", "Test Payment");
            productPartsObj1.put("value", amount);
            productPartsObj1.put("isRequired", "true");
            productPartsObj1.put("settlementEvent", "EmailConfirmation");
            productPartsArr.put(productPartsObj1);
            productInfoObj.put("paymentParts", productPartsArr);

            Calendar c = Calendar.getInstance();
    		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    		String dt = sdf.format(c.getTime());
            
            // Payment Identifiers
            paymentPartsObj1.put("field", "CompletionDate");
            paymentPartsObj1.put("value", dt);
            paymentIdentifiersArr.put(paymentPartsObj1);

            Random rand = new Random();
            String rndm = Integer.toString(rand.nextInt())
                    + (System.currentTimeMillis() / 1000L);
            txnid = hashCal("SHA-256", rndm).substring(0, 20);
            
            paymentPartsObj2.put("field", "TxnId");
            paymentPartsObj2.put("value", txnid);
            paymentIdentifiersArr.put(paymentPartsObj2);

            paymentIdenfierParent.put("paymentIdentifiers",
                    paymentIdentifiersArr);
            productInfoObj.put("", paymentIdenfierParent);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        productInfo = productInfoObj.toString();

        Log.e("TAG", productInfoObj.toString());
       
        /*Random rand = new Random();
        String rndm = Integer.toString(rand.nextInt())
                + (System.currentTimeMillis() / 1000L);
        
        Log.e("transaction id", rndm);
        
        txnid = hashCal("SHA-256", rndm).substring(0, 20);*/
       
        hash = hashCal("SHA-512", merchant_key + "|" + txnid + "|" + amount
                + "|" + productInfo + "|" + firstName + "|" + emailId
                + "|||||||||||" + salt);

        Log.e("the hash value is", hash);
       
        Log.e("merchant_key", merchant_key);
        Log.e("txnid", txnid);    
        Log.e("amount",amount);
        Log.e("productInfo",productInfo);
        Log.e("firstName",firstName);
        Log.e("emailId",emailId);
        Log.e("salt",salt);
        action1 = base_url.concat("/_payment");

        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onReceivedError(WebView view, int errorCode,
                    String description, String failingUrl) {
                // TODO Auto-generated method stub
                Toast.makeText(PaymentActivity.this, "Oh no! " + description,
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onReceivedSslError(WebView view,
                    SslErrorHandler handler, SslError error) {
                // TODO Auto-generated method stub
                Toast.makeText(PaymentActivity.this, "SslError! " + error,
                        Toast.LENGTH_SHORT).show();
                handler.proceed();
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Toast.makeText(PaymentActivity.this, "Page Started! " + url,
                        Toast.LENGTH_SHORT).show();
                if (url.equals(SUCCESS_URL)) {
                	
   Toast.makeText(PaymentActivity.this, "THGBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA! " + url,
                            Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(PaymentActivity.this, "Failure! " + url,
                            Toast.LENGTH_SHORT).show();
                }
                return super.shouldOverrideUrlLoading(view, url);
            }
            //
            // @Override
            // public void onPageFinished(WebView view, String url) {
            // super.onPageFinished(view, url);
            //
            // Toast.makeText(PayMentGateWay.this, "" + url,
            // Toast.LENGTH_SHORT).show();
            // }
        });

        webView.setVisibility(View.VISIBLE);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setCacheMode(2);
        webView.getSettings().setDomStorageEnabled(true);
        webView.clearHistory();
        webView.clearCache(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setUseWideViewPort(false);
        webView.getSettings().setLoadWithOverviewMode(false);

       // webView.addJavascriptInterface(new PayUJavaScriptInterface(getApplicationContext()),
         //       "PayUMoney");
        webView.addJavascriptInterface(new Object() {
            @JavascriptInterface
            public void onSuccess() {
                onSuccess("");
            }

            @JavascriptInterface
            public void onSuccess(final String result) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                       /* Intent intent = new Intent();
                        intent.putExtra("result", result);
                        setResult(RESULT_OK, intent);
                        finish();*/
                    	Toast.makeText(PaymentActivity.this, "ON SUCCESSS", 1000).show();
                    	Log.e("ON SUCCESSS", "ON SUCCESSS");
                    	
                    	new PaymentConfirm().execute();
                    	
//                    	Intent intent=new Intent(PaymentActivity.this,GalleryActivity.class);
//                    	intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
//                    	intent.putExtra("paid", "paid");
//                    	startActivity(intent);
//                    	finish();
//                    	overridePendingTransition(R.anim.right_in, R.anim.left_out);
                    }
//            }
                });
            }

            @JavascriptInterface
            public void onFailure() {
                onFailure("");
            }

            @JavascriptInterface
            public void onFailure(final String result) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                    	Toast.makeText(PaymentActivity.this, "On Failure", 1000).show();
                    	Log.e("On Failure", "On Failure");
                      /*  Intent intent = new Intent();
                        intent.putExtra("result", result);
                        setResult(RESULT_CANCELED, intent);
                        finish();*/
                    }
                });
            }
        }, "PayU");
        Map<String, String> mapParams = new HashMap<String, String>();
        mapParams.put("key", merchant_key);
        mapParams.put("hash", hash);
        mapParams.put("txnid", txnid);
       // mapParams.put("service_provider", "payu_paisa");
        mapParams.put("amount", amount);
        mapParams.put("firstname", firstName);
        mapParams.put("email", emailId);
        mapParams.put("phone", phone);

        mapParams.put("productinfo", productInfo);
        mapParams.put("surl", SUCCESS_URL);
        mapParams.put("furl", FAILED_URL);
        mapParams.put("lastname", "Vijayakumar");

        mapParams.put("address1", "");
        mapParams.put("address2", "");
        mapParams.put("city", "");
        mapParams.put("state", "");

        mapParams.put("country", "");
        mapParams.put("zipcode", "");
        mapParams.put("udf1", "");
        mapParams.put("udf2", "");

        mapParams.put("udf3", "");
        mapParams.put("udf4", "");
        mapParams.put("udf5", "");
        // mapParams.put("pg", (empty(PayMentGateWay.this.params.get("pg"))) ?
        // ""
        // : PayMentGateWay.this.params.get("pg"));
        webview_ClientPost(webView, action1, mapParams.entrySet());

    }

    /*public class PayUJavaScriptInterface {
        Context mContext;

        *//** Instantiate the interface and set the context *//*
        PayUJavaScriptInterface(Context c) {
            mContext = c;
            Log.e("inside the", "PayuScript");
           // success();
        }
        @JavascriptInterface
        public void success(long id, final String paymentId) {

            mHandler.post(new Runnable() {

                public void run() {
                    mHandler = null;
                    Log.e("test intent", "activity");
                	Intent intent=new Intent(PaymentActivity.this,SecondActivity.class);
                	startActivity(intent);
                	finish();
                }
            });
        }
    }*/
    public void webview_ClientPost(WebView webView, String url,
            Collection<Map.Entry<String, String>> postData) {
        StringBuilder sb = new StringBuilder();

        sb.append("<html><head></head>");
        sb.append("<body onload='form1.submit()'>");
        sb.append(String.format("<form id='form1' action='%s' method='%s'>",
                url, "post"));
        for (Map.Entry<String, String> item : postData) {
            sb.append(String.format(
                    "<input name='%s' type='hidden' value='%s' />",
                    item.getKey(), item.getValue()));
        }
        sb.append("</form></body></html>");
        Log.d("webview", "webview_ClientPost called");
        webView.loadData(sb.toString(), "text/html", "utf-8");
    }

    @Override
		public void onBackPressed() {
			// TODO Auto-generated method stub
			super.onBackPressed();
		}

	public boolean empty(String s) {
        if (s == null || s.trim().equals(""))
            return true;
        else
            return false;
    }

    public String hashCal(String type, String str) {
        byte[] hashseq = str.getBytes();
        StringBuffer hexString = new StringBuffer();
        try {
            MessageDigest algorithm = MessageDigest.getInstance(type);
            algorithm.reset();
            algorithm.update(hashseq);
            byte messageDigest[] = algorithm.digest();

            for (int i = 0; i < messageDigest.length; i++) {
                String hex = Integer.toHexString(0xFF & messageDigest[i]);
                if (hex.length() == 1)
                    hexString.append("0");
                hexString.append(hex);
            }
        } catch (NoSuchAlgorithmException nsae) {
        }
        return hexString.toString();

    }
    
    class PaymentConfirm extends AsyncTask<String, String, String> {

//		boolean status;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(PaymentActivity.this);
			pDialog.setMessage("Please Wait ...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}
		
		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub
			// Check for success tag

			String json = "";

			try {

				// you need to encode ONLY the values of the parameters
				String param = "email=" + URLEncoder.encode(emailId, "UTF-8")
						+ "&pay_location=" + URLEncoder.encode(payLocation, "UTF-8")
						+ "&name=" + URLEncoder.encode(firstName, "UTF-8")
						+ "&address=" + URLEncoder.encode(payLocation, "UTF-8")
						+ "&phone=" + URLEncoder.encode(phone, "UTF-8")
						+ "&amount=" + URLEncoder.encode(amount, "UTF-8")
						+ "&kitchen_id=" + URLEncoder.encode(kitchenId, "UTF-8")
						+ "&guest_user=" + URLEncoder.encode(guestUser, "UTF-8")
						+ "&order_type=" + URLEncoder.encode(orderType, "UTF-8")
						+ "&order_data=" + cartData
						+ "&order_date=" + URLEncoder.encode(orderDate, "UTF-8")
						+ "&transaction_id=" + URLEncoder.encode(txnid, "UTF-8")
						+ "&status=" + URLEncoder.encode("1", "UTF-8");

				ServiceHandler sh = new ServiceHandler();

				json = sh.makeServiceCall(PAY_SERVICE_URL, ServiceHandler.POST,
						param);
				
				JSONObject jsonObj = new JSONObject(json);
				
				if(jsonObj.getBoolean("status")){
					
					return "success";
					
				}else{
					return jsonObj.toString();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;

		}
		
		protected void onPostExecute(String value) { 
			// dismiss the dialog once product deleted
			if (pDialog.isShowing()) {
				pDialog.dismiss();
			}

			if(value!=null){
			if (value.equalsIgnoreCase("success")) {

				Intent intent = new Intent(PaymentActivity.this,LocationSearchResultPage.class);
            	intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            	intent.putExtra("paid", "paid");
            	startActivity(intent);
            	finish();
            	overridePendingTransition(R.anim.right_in, R.anim.left_out);
				
				/*Toast.makeText(getApplicationContext(), "LogIn Successfull",
						Toast.LENGTH_SHORT).show();*/
								
				

			} else {
				Toast.makeText(PaymentActivity.this, value,
						Toast.LENGTH_SHORT).show();
				/*
				 * Dialog_notverifiedmail not_verified_mail = new
				 * Dialog_notverifiedmail(ConsumerSignIn.this);
				 * not_verified_mail.show();
				 */
			}
			}
		}

	}
    
}
