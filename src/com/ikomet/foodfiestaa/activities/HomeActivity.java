package com.ikomet.foodfiestaa.activities;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Matrix;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.adapter.PagerAdapter;
import com.ikomet.foodfiestaa.fragments.FragmentDrawer;
import com.ikomet.foodfiestaa.utils.CirclePageIndicator;
import com.ikomet.foodfiestaa.utils.InternetCheck;
import com.ikomet.foodfiestaa.utils.NetworkReceiver;
import com.ikomet.foodfiestaa.utils.ServiceHandler;
import com.ikomet.foodfiestaa.utils.SharedPrefs;

public class HomeActivity extends ActionBarActivity {

	int position;
	Timer swipeTimer;

	int numberOfViewPagerChildren = 5;

	private Toolbar mToolbar;
	String location;
	Integer id;
	private FragmentDrawer drawerFragment;
	ArrayList<String> location_list;
	ArrayList<Integer> location_id;
	RelativeLayout layout_one, layout_two;
	private ViewPager viewpager;
	CirclePageIndicator indicator;
	LinearLayout linear_toolbar, linear_location, linear_howitworks,
			linear_becomecook;
	AutoCompleteTextView txt_autocomplete;
	ImageView img_search;
	RelativeLayout refresh;
	Button bt_how_it_works;
	RelativeLayout no_net;	
	DrawerLayout rel_profile;
	ImageView smiley;
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		
		no_net = (RelativeLayout) findViewById(R.id.no_net);
		rel_profile = (DrawerLayout) findViewById(R.id.drawer_layout);
		smiley = (ImageView)findViewById(R.id.img_sad);
		
		if (InternetCheck.isInternetConnected(HomeActivity.this)) {
			no_net.setVisibility(View.GONE);
			rel_profile.setVisibility(View.VISIBLE);
			initializeVariables();
		} else {
			no_net.setVisibility(View.VISIBLE);
			rel_profile.setVisibility(View.GONE);
			no_net.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (InternetCheck.isInternetConnected(HomeActivity.this)) {
																
						Animation a = AnimationUtils.loadAnimation(HomeActivity.this, R.anim.rotate_anim);
						a.setDuration(1000);
						smiley.startAnimation(a);
						
						new Handler().postDelayed(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								rel_profile.setVisibility(View.VISIBLE);
								no_net.setVisibility(View.GONE);
								initializeVariables();
							}
						}, 2000);
																		
					}
				}
			});

		}
		
		/*NetworkReceiver network=new NetworkReceiver();
		this.registerReceiver(network,
				new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));*/
		
		
	}

	public void initializeVariables(){
		
		mToolbar = (Toolbar) findViewById(R.id.toolbar);

		setSupportActionBar(mToolbar);

		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setTitle("");
		new Async_LocationList().execute();
		
		// Autocomplete TextView for location list
		
		txt_autocomplete = (AutoCompleteTextView) findViewById(R.id.edt_locationsearch);
		txt_autocomplete.setDropDownWidth(LayoutParams.WRAP_CONTENT);
		
		layout_one = (RelativeLayout) findViewById(R.id.rel_one);
		layout_two = (RelativeLayout) findViewById(R.id.rel_two);

		drawerFragment = (FragmentDrawer) getSupportFragmentManager()
				.findFragmentById(R.id.fragment_navigation_drawer);

		drawerFragment.setUp(R.id.fragment_navigation_drawer,
				(DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
		img_search=(ImageView)findViewById(R.id.img_search);
		viewpager = (ViewPager) findViewById(R.id.pager);
		indicator = (CirclePageIndicator) findViewById(R.id.indicator);
		linear_toolbar = (LinearLayout) findViewById(R.id.linear_toolbar);
		linear_location = (LinearLayout) findViewById(R.id.linear_location);
		linear_becomecook = (LinearLayout) findViewById(R.id.linear_becomecook);
		linear_howitworks = (LinearLayout) findViewById(R.id.linear_howitworks);
		bt_how_it_works = (Button) findViewById(R.id.bt_how_it_works);
		bt_how_it_works.setOnClickListener(new View.OnClickListener() {
			// DialogTest test=new DialogTest(MainActivity.this);
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(HomeActivity.this,
						HowItWorksNew.class);
				startActivity(intent);
			}
		});
		
		linear_becomecook.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(HomeActivity.this, BecomeACook.class);
				startActivity(intent);
			}
		});

		img_search.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				SharedPrefs.put_FragmentState(HomeActivity.this, "FragmentA", 0);
				
				location=txt_autocomplete.getText().toString();
				if(location==null || location.equalsIgnoreCase("")){
					Toast.makeText(HomeActivity.this, "Please select a location from the list", Toast.LENGTH_SHORT).show();
				}else{
					Log.e("the location is", location);
					for(int i=0;i<location_list.size();i++)
					{Log.e("the locationloojhnb  ", location_list.get(i)+"");
						
						if(location.equalsIgnoreCase(location_list.get(i)))
						{
							
							id=location_id.get(i);
							Log.e("location ","found");
							break;
						}else{
							Log.e("location ","is  not found");
						}
					}
					
					if(id==null){
						Toast.makeText(HomeActivity.this, "Please select a location from the list", Toast.LENGTH_SHORT).show();
					}else{
					
						Log.e("the location id is", id+"");
						Intent intent=new Intent(HomeActivity.this,LocationSearchResultPage.class);
						SharedPrefs.put_locationId(HomeActivity.this, String.valueOf(id));
						//intent.putExtra("location_id", id);
						id= null;
						startActivity(intent);
					}
				}
			}
		});
		
		PagerAdapter adapt = new PagerAdapter(getSupportFragmentManager(),
				numberOfViewPagerChildren, 0, null, null);

		viewpager.setAdapter(adapt);
		indicator.setViewPager(viewpager);
		position = viewpager.getCurrentItem();

		swipeTimer = new Timer();
		swipeTimer.schedule(new TimerTask() {

			@Override
			public void run() {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if (position == numberOfViewPagerChildren) {
							position = 0;
						} else {
							position = position + 1;

						}
						viewpager.setCurrentItem(position, true);
						// /viewPager.setCurrentItem(position ++, true);
					}
				});
			}
		}, 500, 6000);
		
		
	}
	
	class Async_LocationList extends AsyncTask<Void, Void, String> {

		String result = null;
		ProgressDialog pDialog;
		
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(HomeActivity.this);
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.setMessage("Please Wait...");
			pDialog.show();
		}

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			ServiceHandler handler = new ServiceHandler();
			result = handler.makeServiceCall(
					"http://beta.foodfiestaa.com/locationservice", 1);
			location_list = new ArrayList<>();
			location_id = new ArrayList<>();
			try {
				JSONArray arr1 = new JSONArray(result);
				for (int i = 0; i < arr1.length(); i++) {
					JSONObject obj1 = arr1.getJSONObject(i);
					location_list.add(obj1.getString("area"));
					location_id.add(obj1.getInt("id"));
					Log.e("the location id and area", location_list.get(i)
							+ " " + location_id.get(i).toString());
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			if(pDialog.isShowing()){
				pDialog.dismiss();
			}
			super.onPostExecute(result);
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(HomeActivity.this,
					R.layout.spinner_item_location, location_list);
			txt_autocomplete.setThreshold(1);
			txt_autocomplete.setAdapter(adapter);
			
			
		}

	}

	

}
