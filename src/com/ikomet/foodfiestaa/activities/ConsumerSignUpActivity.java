package com.ikomet.foodfiestaa.activities;

import java.net.URLEncoder;
import java.util.Arrays;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.plus.People;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.PlusShare;
import com.google.android.gms.plus.People.LoadPeopleResult;
import com.google.android.gms.plus.Plus.PlusOptions;
import com.google.android.gms.plus.model.people.Person;
import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.activities.ConsumerSignIn.Async_social_Login;
import com.ikomet.foodfiestaa.utils.DialogCustom;
import com.ikomet.foodfiestaa.utils.DialogVerification;
import com.ikomet.foodfiestaa.utils.InternetCheck;
import com.ikomet.foodfiestaa.utils.ServiceHandler;
import com.ikomet.foodfiestaa.utils.SharedPrefs;
import com.tmxlr.lib.driodvalidatorlight.Form;
import com.tmxlr.lib.driodvalidatorlight.helper.Range;
import com.tmxlr.lib.driodvalidatorlight.helper.RegexTemplate;

public class ConsumerSignUpActivity extends ActionBarActivity implements
		View.OnClickListener, ConnectionCallbacks, OnConnectionFailedListener,
		ResultCallback<People.LoadPeopleResult> {

	public static final String STATIC_PAGES_URL = "http://beta.foodfiestaa.com/cmsservice";
	public static final String CONSUMER_SIGNUP = "http://beta.foodfiestaa.com/consumersignupservice";
	ProgressDialog pDialog;
	String cms_content;
	private Toolbar mToolbar;
	EditText email, pass, cnfmpass;
	Button signUp;
	Form form;
	TextView signin;
	private LoginButton loginButton;
	ImageButton fb, GPlus;
	String userEmail;
	private CallbackManager callbackManager;
	private boolean mIntentInProgress;
	private boolean mSignInClicked;
	public GoogleApiClient mGoogleApiClient;
	private ConnectionResult mConnectionResult;
	private static final int PICK_MEDIA_REQUEST_CODE = 8;
	private static final int SHARE_MEDIA_REQUEST_CODE = 9;
	private static final int SIGN_IN_REQUEST_CODE = 10;
	private static final int ERROR_DIALOG_REQUEST_CODE = 11;
	DialogCustom dialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		FacebookSdk.sdkInitialize(getApplicationContext());
	    callbackManager = CallbackManager.Factory.create();
		
	    setContentView(R.layout.consumer_signup);

	    dialog = new DialogCustom();
		fb = (ImageButton) findViewById(R.id.fb);
		GPlus = (ImageButton) findViewById(R.id.btn_sign_in);
		loginButton = (LoginButton) findViewById(R.id.bt_fb);
		loginButton.setReadPermissions(Arrays.asList("public_profile", "email",
				"user_birthday", "user_friends"));

		mToolbar = (Toolbar) findViewById(R.id.toolbar);
		// mToolbar.setBackgroundColor(Color.TRANSPARENT);
		mToolbar.setTitle("");
		setSupportActionBar(mToolbar);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);

		mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		loginButton.registerCallback(callbackManager,
				new FacebookCallback<LoginResult>() {
					@Override
					public void onSuccess(LoginResult loginResult) {
						Toast.makeText(
								getApplicationContext(),
								"User ID: "
										+ loginResult.getAccessToken()
												.getUserId()
										+ "\n"
										+ "Auth Token: "
										+ loginResult.getAccessToken()
												.getToken(), Toast.LENGTH_LONG)
								.show();

						GraphRequest request = GraphRequest.newMeRequest(
								loginResult.getAccessToken(),
								new GraphRequest.GraphJSONObjectCallback() {

									@Override
									public void onCompleted(JSONObject object,
											GraphResponse response) {
										try {
											Log.e("json", object.toString());
											userEmail = object
													.getString("email");
											new Async_social_Login().execute(
													"0", "true");
											Toast.makeText(
													getApplicationContext(),
													object.toString(),
													Toast.LENGTH_SHORT).show();
										} catch (Exception ex) {
											ex.printStackTrace();
										}
									}

								});
						Bundle parameters = new Bundle();
						parameters.putString("fields",
								"id,name,email,gender,birthday");
						request.setParameters(parameters);
						request.executeAsync();
					}

					@Override
					public void onCancel() {
						Toast.makeText(getApplicationContext(),
								"Login attempt canceled.", Toast.LENGTH_SHORT)
								.show();
					}

					@Override
					public void onError(FacebookException e) {
						Toast.makeText(getApplicationContext(),
								"Login attempt failed.", Toast.LENGTH_SHORT)
								.show();
					}
				});

		SpannableString ss = new SpannableString(getResources().getString(
				R.string.signup_agreement));

		ClickableSpan clickableSpan = new ClickableSpan() {
			@Override
			public void onClick(View textView) {
				if(InternetCheck.isInternetConnected(ConsumerSignUpActivity.this)){
					new TermsConditions().execute();
				}else{
					dialog.showDialog(ConsumerSignUpActivity.this, "Please connect to Internet", "OK", R.drawable.circle_uncheck);
				}
			}

			@Override
			public void updateDrawState(TextPaint ds) {
				super.updateDrawState(ds);
				ds.setUnderlineText(false);
			}
		};
		ss.setSpan(clickableSpan, 27, 47, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		ForegroundColorSpan fcs = new ForegroundColorSpan(getResources()
				.getColor(R.color.colorPrimary));
		ss.setSpan(fcs, 27, 47, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
		TextView textView = (TextView) findViewById(R.id.txt_consumer_agreement);
		textView.setText(ss);
		textView.setMovementMethod(LinkMovementMethod.getInstance());
		textView.setHighlightColor(Color.TRANSPARENT);

		email = (EditText) findViewById(R.id.ed_email);
		pass = (EditText) findViewById(R.id.ed_pass);
		cnfmpass = (EditText) findViewById(R.id.ed_cnfpass);

		signUp = (Button) findViewById(R.id.bt_signup);
		signin = (TextView) findViewById(R.id.txt_consumer);

		signin.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ConsumerSignUpActivity.this,
						ConsumerSignIn.class);
				startActivity(intent);
			}
		});

		form = new Form(ConsumerSignUpActivity.this);

		form.check(email, RegexTemplate.NOT_EMPTY_PATTERN, "Must not be empty");
		form.check(email, RegexTemplate.EMAIL_PATTERN, "Must be an Email");
		form.check(pass, RegexTemplate.NOT_EMPTY_PATTERN, "Must not be empty");
		form.check(pass, RegexTemplate.PASSWORD_PATTERN, "Invalid Password");
		form.check(cnfmpass, RegexTemplate.NOT_EMPTY_PATTERN,
				"Must not be empty");

		signUp.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (form.validate() && matchPasswords(pass, cnfmpass)) {
					if(InternetCheck.isInternetConnected(ConsumerSignUpActivity.this)){
						new AttempSignUp().execute();
					}else{
						dialog.showDialog(ConsumerSignUpActivity.this, "Please connect to Internet", "OK", R.drawable.circle_uncheck);
					}
					// Toast.makeText(ConsumerSignUpActivity.this,
					// "validation success", 2000).show();
				}
			}
		});

	}

	public boolean matchPasswords(EditText ed1, EditText ed2) {

		if (ed1.getText().toString().trim()
				.equals(ed2.getText().toString().trim())) {
			ed2.setError(null);
			return true;
		} else {
			ed2.setError("Password Mismatch");
			return false;
		}

	}

	class TermsConditions extends AsyncTask<String, String, String> {

		boolean status;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(ConsumerSignUpActivity.this);
			pDialog.setMessage("Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();

		}

		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub
			// Check for success tag
			try {
				String json = "";

				ServiceHandler sh = new ServiceHandler();

				String params = "cmsid=" + URLEncoder.encode("3", "UTF-8");

				json = sh.makeServiceCall(STATIC_PAGES_URL, ServiceHandler.GET,
						params);

				JSONObject jsonObj;

				jsonObj = new JSONObject(json);

				// json success tag

				status = jsonObj.getBoolean("status");

				if (status) {
					cms_content = jsonObj.getString("cms_content");
					return "success";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;

		}

		protected void onPostExecute(String value) {
			// dismiss the dialog once product deleted
			if (pDialog.isShowing()) {
				pDialog.dismiss();
			}
			// pBar.setActivated(false);
			System.out.println("value = " + value);
			if (value != null) {

				Intent intent = new Intent(ConsumerSignUpActivity.this,
						StaticPagesActivity.class);
				intent.putExtra("cms_name", "Terms & Conditions");
				intent.putExtra("cms_content", cms_content);
				startActivity(intent);

			}

		}

	}

	class AttempSignUp extends AsyncTask<String, String, String> {

		boolean status;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(ConsumerSignUpActivity.this);
			pDialog.setMessage("Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();

		}

		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub
			// Check for success tag
			try {
				String json = "";

				ServiceHandler sh = new ServiceHandler();

				String mail = email.getText().toString().trim();
				String passw = pass.getText().toString().trim();
				String cnfmpassw = cnfmpass.getText().toString().trim();

				String params = "email=" + URLEncoder.encode(mail, "UTF-8")
						+ "&password=" + URLEncoder.encode(passw, "UTF-8")
						+ "&password_confirmation="
						+ URLEncoder.encode(cnfmpassw, "UTF-8");

				json = sh.makeServiceCall(CONSUMER_SIGNUP, ServiceHandler.POST,
						params);

				JSONObject jsonObj;

				jsonObj = new JSONObject(json);

				status = jsonObj.getBoolean("status");

				if (status) {
					return jsonObj.getString("message");
				} else {

					JSONObject errorMsg = jsonObj.optJSONObject("message");

					if (errorMsg == null) {
						return jsonObj.getString("message");
					} else {
						if (errorMsg.has("email") && errorMsg.has("password")) {
							return errorMsg.getJSONArray("email").getString(0)
									+ "\n"
									+ errorMsg.getJSONArray("password")
											.getString(0);
						} else if (errorMsg.has("email")) {
							return errorMsg.getJSONArray("email").getString(0);
						} else {
							return errorMsg.getJSONArray("password").getString(
									0);
						}
					}

				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;

		}

		protected void onPostExecute(String value) {
			// dismiss the dialog once product deleted
			if (pDialog.isShowing()) {
				pDialog.dismiss();
			}
			// pBar.setActivated(false);
			System.out.println("value = " + value);
			if (value != null) {
				if (status) {
					DialogVerification verify = new DialogVerification(
							ConsumerSignUpActivity.this, 0,
							"A Verification mail has been sent");

					verify.show();
				}
				Toast.makeText(ConsumerSignUpActivity.this, value,
						Toast.LENGTH_SHORT).show();

			}

		}

	}

	@Override
	protected void onStart() {
		super.onStart();
		// make sure to initiate connection
		mGoogleApiClient = buildGoogleAPIClient();
		mGoogleApiClient.connect();
		Log.e("inside", "onstart");
	}

	@Override
	protected void onStop() {
		super.onStop();
		// disconnect api if it is connected
		if (mGoogleApiClient.isConnected())
			mGoogleApiClient.disconnect();
		Log.e("inside", "onStop");
	}
		
	private GoogleApiClient buildGoogleAPIClient() {
		return new GoogleApiClient.Builder(this).addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.addApi(Plus.API, PlusOptions.builder().build())
				.addScope(Plus.SCOPE_PLUS_LOGIN).build();
	}
	
	/**
     * Sign-out from google
     * */
    public void signOutFromGplus() {
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            mGoogleApiClient.disconnect();
            mGoogleApiClient.connect();
//            updateUI(false);
        }
    }
    
    public void logout(){

        LoginManager.getInstance().logOut();

    }
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == SIGN_IN_REQUEST_CODE) {
			if (resultCode != RESULT_OK) {
				mSignInClicked = false;
			}

			mIntentInProgress = false;

			if (!mGoogleApiClient.isConnecting()) {
				mGoogleApiClient.connect();
			}
		} else if (requestCode == PICK_MEDIA_REQUEST_CODE) {
			// If picking media is success, create share post using
			// PlusShare.Builder
			if (resultCode == RESULT_OK) {
				Uri selectedImage = data.getData();
				ContentResolver cr = this.getContentResolver();
				String mime = cr.getType(selectedImage);

				PlusShare.Builder share = new PlusShare.Builder(this);
				share.setText("Hello from AndroidSRC.net");
				share.addStream(selectedImage);
				share.setType(mime);
				startActivityForResult(share.getIntent(),
						SHARE_MEDIA_REQUEST_CODE);
			}
		} else {
			callbackManager.onActivityResult(requestCode, resultCode, data);
		}
	}

	@Override
	public void onConnectionSuspended(int arg0) {
		// TODO Auto-generated method stub
		mGoogleApiClient.connect();

	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub
		Log.v("Reult", "onConnectionFailed");
//		dialog.showDialog(ConsumerSignUpActivity.this, "Please connect to Internet", "OK", R.drawable.circle_uncheck);
		if (!arg0.hasResolution()) {
			GooglePlayServicesUtil.getErrorDialog(arg0.getErrorCode(), this,
					ERROR_DIALOG_REQUEST_CODE).show();
			return;
		}
		if (!mIntentInProgress) {
			mConnectionResult = arg0;

			if (mSignInClicked) {
				processSignInError();
			}
		}

	}

	@Override
	public void onConnected(Bundle arg0) {
		// TODO Auto-generated method stub
		mSignInClicked = false;
		Toast.makeText(getApplicationContext(), "Signed In Successfully",
				Toast.LENGTH_LONG).show();
		/*
		 * Plus.PeopleApi.loadVisible(mGoogleApiClient, null)
		 * .setResultCallback(this);
		 */
		Plus.PeopleApi.loadVisible(mGoogleApiClient, null).setResultCallback(
				this);

		if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {

			Person currentPerson = Plus.PeopleApi
					.getCurrentPerson(mGoogleApiClient);
			String personName = currentPerson.getDisplayName();
			// String personPhoto = currentPerson.getImage();
			String personGooglePlusProfile = currentPerson.getUrl();
			userEmail = Plus.AccountApi.getAccountName(mGoogleApiClient);
			Log.e("personName", personName);
			Log.e("userEmail", userEmail);
			Log.e("personGooglePlusProfile", personGooglePlusProfile);

			new Async_social_Login().execute("1", "false");

		} else if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) == null) {
			Log.e("people Api value", "null");
		}

	}

	class Async_social_Login extends AsyncTask<String, Void, String> {
		String result = null, param = null;
		String user_id, token;

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			ServiceHandler handler = new ServiceHandler();
			param = "email=" + userEmail + "&user_type=" + params[0];
			Log.e("inside doInBackground of", "Async_social_Login");
			result = handler.makeServiceCall(
					"http://beta.foodfiestaa.com/socialloginservice",
					ServiceHandler.POST, param);
			try {
				JSONObject obj = new JSONObject(result);
				if (obj.getBoolean("status")) {
					user_id = obj.getString("id");
					token = obj.getString("token");
					SharedPrefs.putSP(ConsumerSignUpActivity.this, user_id, token,
							"Consumer");
					if (Boolean.parseBoolean(params[1])) {
						SharedPrefs.putSocialLogin(ConsumerSignUpActivity.this,
								"Facebook");
						logout();
					} else {
						SharedPrefs.putSocialLogin(ConsumerSignUpActivity.this,
								"Google Plus");
						signOutFromGplus();
					}
					return "success";
				} else {
					return "failure";
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (result.equalsIgnoreCase("success")) {
				Toast.makeText(getApplicationContext(), "LogIn Successfull",
						Toast.LENGTH_SHORT).show();

				Intent intent = new Intent(ConsumerSignUpActivity.this,
						LocationSearchResultPage.class);
				// intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
						| Intent.FLAG_ACTIVITY_CLEAR_TASK);
				signOutFromGplus();
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.right_in, R.anim.left_out);

			} else {
				Toast.makeText(getApplicationContext(), result,
						Toast.LENGTH_SHORT).show();
			}
			pDialog.dismiss();
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(ConsumerSignUpActivity.this);
			pDialog.setMessage("Attempting login...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();
		if (id == R.id.btn_sign_in) {
			// Signin button clicked
			if(InternetCheck.isInternetConnected(ConsumerSignUpActivity.this)){
				processSignIn();
			}else{
				
			}
		} else if (id == R.id.fb) {
			if(InternetCheck.isInternetConnected(ConsumerSignUpActivity.this)){
				loginButton.performClick();
			}else{
				
			}
		}

	}

	private void processSignIn() {

		if (!mGoogleApiClient.isConnecting()) {
			processSignInError();
			mSignInClicked = true;
		}

	}

	private void processSignInError() {

		if (mConnectionResult != null && mConnectionResult.hasResolution()) {
			try {
				mIntentInProgress = true;
				mConnectionResult.startResolutionForResult(this,
						SIGN_IN_REQUEST_CODE);
			} catch (SendIntentException e) {
				mIntentInProgress = false;
				mGoogleApiClient.connect();
			}
		}
	}

	@Override
	public void onResult(LoadPeopleResult arg0) {
		// TODO Auto-generated method stub

	}

}
