package com.ikomet.foodfiestaa.activities;

import java.net.URLEncoder;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.plus.People;
import com.google.android.gms.plus.People.LoadPeopleResult;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.Plus.PlusOptions;
import com.google.android.gms.plus.PlusShare;
import com.google.android.gms.plus.model.people.Person;
import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.utils.DialogCustom;
import com.ikomet.foodfiestaa.utils.DialogForgotPwd;
import com.ikomet.foodfiestaa.utils.InternetCheck;
import com.ikomet.foodfiestaa.utils.ServiceHandler;
import com.ikomet.foodfiestaa.utils.SharedPrefs;

public class ConsumerSignIn extends ActionBarActivity implements
		View.OnClickListener, ConnectionCallbacks, OnConnectionFailedListener, ResultCallback<People.LoadPeopleResult> {

	RelativeLayout rel_sign_out;

	EditText email, password;
	Button buttonSignin;
	ProgressDialog pDialog;
	TextView txt_forgot_pwd;
	public static final String CUST_LOGIN_URL = "http://beta.foodfiestaa.com/consumerloginservice";
	private static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{8,20})";
	TextWatcher emailWatcher;
	TextWatcher passwordWatcher;
	private Toolbar mToolbar;
	TextView signUp;
	ImageButton fb, GPlus;
	
	// Google client to interact with Google API
	public static GoogleApiClient mGoogleApiClient;
	//private SignInButton GPlus;
	
	private LoginButton loginButton;
	private CallbackManager callbackManager;
	/**
	 * A flag indicating that a PendingIntent is in progress and prevents us
	 * from starting further intents.
	 */
	private boolean mIntentInProgress;

	private boolean mSignInClicked;

	private ConnectionResult mConnectionResult;
	private static final int PICK_MEDIA_REQUEST_CODE = 8;
	private static final int SHARE_MEDIA_REQUEST_CODE = 9;
	private static final int SIGN_IN_REQUEST_CODE = 10;
	private static final int ERROR_DIALOG_REQUEST_CODE = 11;
	String userEmail;
	DialogCustom dialog; 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		FacebookSdk.sdkInitialize(getApplicationContext());
//	    AppEventsLogger.activateApp(this);
	    callbackManager = CallbackManager.Factory.create();
		
	    setContentView(R.layout.consumer_signin);

	    dialog = new DialogCustom();
		txt_forgot_pwd = (TextView) findViewById(R.id.txt_forgot_pwd);
		fb = (ImageButton) findViewById(R.id.fb);
		GPlus = (ImageButton) findViewById(R.id.btn_sign_in);
//		GPlus = (SignInButton) findViewById(R.id.btn_sign_in);
		loginButton = (LoginButton) findViewById(R.id.bt_fb);
		loginButton.setReadPermissions(Arrays.asList(
	            "public_profile", "email", "user_birthday", "user_friends"));
//		loginButton.setBackgroundResource(R.drawable.f_login);
		//loginButton.setText("");
//		logout();	
		mGoogleApiClient = buildGoogleAPIClient();
		GPlus.setOnClickListener(this);
		mToolbar = (Toolbar) findViewById(R.id.toolbar);
		mToolbar.setBackgroundColor(Color.TRANSPARENT);
		mToolbar.setTitle("");
		setSupportActionBar(mToolbar);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);

		mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
				
		txt_forgot_pwd.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				DialogForgotPwd forgot_pwd = new DialogForgotPwd(
						ConsumerSignIn.this, "2");
				forgot_pwd.show();
			}
		});
		
		
		
		loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
		    @Override
		    public void onSuccess(LoginResult loginResult) {
		    	/*Toast.makeText(getApplicationContext(), "User ID: "
					    + loginResult.getAccessToken().getUserId() 
					    + "\n" +
					    "Auth Token: "
					    + loginResult.getAccessToken().getToken(), Toast.LENGTH_LONG).show();*/
		    	
		    	
		    	GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
		    			new GraphRequest.GraphJSONObjectCallback() {
		    			
		    			@Override
		    			public void onCompleted(JSONObject object,GraphResponse response) {
		    			try {
		    				Log.e("json", object.toString());
		    				userEmail = object.getString("email");
		    				new Async_social_Login().execute("0","true");
		    				Toast.makeText(getApplicationContext(), object.toString(),Toast.LENGTH_SHORT).show();
		    			} catch(Exception ex) {
		    				ex.printStackTrace();
		    			}
		    			}

		    			});
		    			Bundle parameters = new Bundle();
		    			parameters.putString("fields", "id,name,email,gender,birthday");
		    			request.setParameters(parameters);
		    			request.executeAsync();		    	
		    }
		 
		    @Override
		    public void onCancel() {
		    	Toast.makeText(getApplicationContext(), "Login attempt canceled.", Toast.LENGTH_SHORT).show();
		    }
		 
		    @Override
		    public void onError(FacebookException e) {
		    	
		    	e.printStackTrace();
		    	
//		    	dialog.showDialog(ConsumerSignIn.this, "Please connect to Internet", "OK", R.drawable.circle_uncheck);
		    	
//		    	Toast.makeText(getApplicationContext(), "Login attempt failed.", Toast.LENGTH_SHORT).show();
		    }
		});
		
		rel_sign_out = (RelativeLayout) findViewById(R.id.layout_signin_box);

		email = (EditText) findViewById(R.id.ed_email);
		password = (EditText) findViewById(R.id.ed_password);
		buttonSignin = (Button) findViewById(R.id.bt_signin);

		signUp = (TextView) findViewById(R.id.txt_consumer);

		signUp.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ConsumerSignIn.this,
						ConsumerSignUpActivity.class);
				finish();
				startActivity(intent);
			}
		});

		buttonSignin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (email.getText().toString().trim().equalsIgnoreCase("")
						|| password.getText().toString().trim()
								.equalsIgnoreCase("")) {

					if (email.getText().toString().trim().equalsIgnoreCase("")
							&& password.getText().toString().trim()
									.equalsIgnoreCase("")) {
						email.setError("Please fill this field");
						password.setError("Please fill this field");
					} else if (email.getText().toString().trim()
							.equalsIgnoreCase("")) {
						email.setError("Please fill this field");
					} else {
						password.setError("Please fill this field");
					}
				} else {
					if (email.getError() == null || password.getError() == null) {
						if(InternetCheck.isInternetConnected(ConsumerSignIn.this)){
							new AttemptLogin().execute();
						}else{
							dialog.showDialog(ConsumerSignIn.this, "Please connect to Internet", "OK", R.drawable.circle_uncheck);
						}
					} else {
						
					}
				}

			}
		});

		emailWatcher = new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				if (s.toString().equalsIgnoreCase("")) {
					email.setError("Please fill this field");
				} else if (!isValidEmail(s.toString())) {
					email.setError("Please enter a valid mail id");
				} else {
					email.setError(null);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				if (s.toString().equalsIgnoreCase("")) {
					email.setError("Please fill this field");
				} else if (!isValidEmail(s.toString())) {
					email.setError("Please enter a valid mail id");
				} else {
					email.setError(null);
				}
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		};

		passwordWatcher = new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				if (s.toString().equalsIgnoreCase("")) {
					password.setError("Please fill this field");
				} else if (!isValidPassword(s.toString())) {
					password.setError("Please use minimum of 8 characters containing upper case, lowercase, numbers and non alpha-numeric characters");
				} else {
					password.setError(null);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				if (s.toString().equalsIgnoreCase("")) {
					password.setError("Please fill this field");
				} else if (!isValidPassword(s.toString())) {
					password.setError("Please use minimum of 8 characters containing upper case, lowercase, numbers and non alpha-numeric characters");
				} else {
					password.setError(null);
				}
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		};

		// email.addTextChangedListener(emailWatcher);
		// password.addTextChangedListener(passwordWatcher);

		int alpha = (int) (0.5 * 255.0f);
		rel_sign_out.setBackgroundColor(Color.argb(alpha, 195, 193, 128));

	}
	
	@Override
	protected void onStart() {
		super.onStart();
		// make sure to initiate connection
		mGoogleApiClient = buildGoogleAPIClient();
		mGoogleApiClient.connect();
		Log.e("inside", "onstart");
	}

	@Override
	protected void onStop() {
		super.onStop();
		// disconnect api if it is connected
		if (mGoogleApiClient.isConnected())
			mGoogleApiClient.disconnect();
		Log.e("inside", "onStop");
	}
		
	private GoogleApiClient buildGoogleAPIClient() {
		return new GoogleApiClient.Builder(this).addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.addApi(Plus.API, PlusOptions.builder().build())
				.addScope(Plus.SCOPE_PLUS_LOGIN).build();
	}
	
	/**
     * Sign-out from google
     * */
    public void signOutFromGplus() {
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            mGoogleApiClient.disconnect();
            mGoogleApiClient.connect();
//            updateUI(false);
        }
    }
    
    public void logout(){
//    	FacebookSdk.sdkInitialize(getApplicationContext());
        LoginManager.getInstance().logOut();
        /*Intent login = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(login);
        finish();*/
    }

	class AttemptLogin extends AsyncTask<String, String, String> {

		boolean status;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(ConsumerSignIn.this);
			pDialog.setMessage("Attempting login...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}
		
		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub
			// Check for success tag

			String json = "";

			try {

				String mail = email.getText().toString().trim();
				String pass = password.getText().toString().trim();

				// you need to encode ONLY the values of the parameters
				String param = "email=" + URLEncoder.encode(mail, "UTF-8")
						+ "&password=" + URLEncoder.encode(pass, "UTF-8");

				ServiceHandler sh = new ServiceHandler();

				json = sh.makeServiceCall(CUST_LOGIN_URL, ServiceHandler.POST,
						param);

				JSONObject jsonObj = null;

				Object jsonn = new JSONTokener(json).nextValue();
				if (jsonn instanceof JSONObject) {
					// you have an object
					jsonObj = new JSONObject(json);
				}

				else if (jsonn instanceof JSONArray) {
					// you have an array
					JSONArray jar = new JSONArray(json);
					jsonObj = jar.getJSONObject(0);
				}

				status = jsonObj.getBoolean("status");

				if (status) {
					String token = jsonObj.getString("token");
					String userid = jsonObj.getString("id");
					SharedPrefs.putSP(ConsumerSignIn.this, userid, token,
							"Consumer");
					// return "LogIn successfull";
					return "success";
				} else {
					JSONObject errorMsg = jsonObj.optJSONObject("errorMessage");

					if (errorMsg == null) {
						return jsonObj.getString("errorMessage");
					} else {
						if (errorMsg.has("email") && errorMsg.has("password")) {
							return errorMsg.getJSONArray("email").getString(0)
									+ "\n"
									+ errorMsg.getJSONArray("password")
											.getString(0);
						} else if (errorMsg.has("email")) {
							return errorMsg.getJSONArray("email").getString(0);
						} else {
							return errorMsg.getJSONArray("password").getString(
									0);
						}
					}

				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			return "";

		}
		
		protected void onPostExecute(String value) {
			// dismiss the dialog once product deleted
			if (pDialog.isShowing()) {
				pDialog.dismiss();
			}

			if (value.equalsIgnoreCase("success")) {

				Toast.makeText(getApplicationContext(), "LogIn Successfull",
						Toast.LENGTH_SHORT).show();
								
				Intent intent = new Intent(ConsumerSignIn.this,
						LocationSearchResultPage.class);
				// intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
						| Intent.FLAG_ACTIVITY_CLEAR_TASK);
				
				if(getIntent().hasExtra("gocart")){
					Log.e("consumer sign in","has extra");
					intent.putExtra("gocart", "gocart");
				}
				
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.right_in, R.anim.left_out);

			} else {
				Toast.makeText(getApplicationContext(), value,
						Toast.LENGTH_SHORT).show();
				/*
				 * Dialog_notverifiedmail not_verified_mail = new
				 * Dialog_notverifiedmail(ConsumerSignIn.this);
				 * not_verified_mail.show();
				 */
			}

		}

	}
	
	public final static boolean isValidPassword(String password) {

		Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
		Matcher matcher = pattern.matcher(password);

		return matcher.matches();
	}
	
	public final static boolean isValidEmail(CharSequence target) {
		if (target == null) {
			return false;
		} else {
			return android.util.Patterns.EMAIL_ADDRESS.matcher(target)
					.matches();
		}
	}
		
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);

	    if (requestCode == SIGN_IN_REQUEST_CODE) {
			if (resultCode != RESULT_OK) {
				mSignInClicked = false;
			}

			mIntentInProgress = false;

			if (!mGoogleApiClient.isConnecting()) {
				mGoogleApiClient.connect();
			}
		} else if (requestCode == PICK_MEDIA_REQUEST_CODE) {
			// If picking media is success, create share post using
			// PlusShare.Builder
			if (resultCode == RESULT_OK) {
				Uri selectedImage = data.getData();
				ContentResolver cr = this.getContentResolver();
				String mime = cr.getType(selectedImage);

				PlusShare.Builder share = new PlusShare.Builder(this);
				share.setText("Hello from AndroidSRC.net");
				share.addStream(selectedImage);
				share.setType(mime);
				startActivityForResult(share.getIntent(),
						SHARE_MEDIA_REQUEST_CODE);
			}
		}else{
	    	callbackManager.onActivityResult(requestCode, resultCode, data);
	    }
	}
	
	@Override
	public void onConnectionSuspended(int arg0) {
		// TODO Auto-generated method stub
		mGoogleApiClient.connect();
		
	}

	@Override
	public void onConnectionFailed( ConnectionResult arg0) {
		// TODO Auto-generated method stub
		Log.v("Reult", "onConnectionFailed");
//		dialog.showDialog(ConsumerSignIn.this, "Please connect to Internet", "OK", R.drawable.circle_uncheck);
		if (!arg0.hasResolution()) {
			GooglePlayServicesUtil.getErrorDialog(arg0.getErrorCode(), this,
					ERROR_DIALOG_REQUEST_CODE).show();
			return;
		}
		if (!mIntentInProgress) {
			mConnectionResult = arg0;

			if (mSignInClicked) {
				processSignInError();
			}
		}

	}
		
	@Override
	public void onConnected(Bundle arg0) {
		// TODO Auto-generated method stub
		mSignInClicked = false;
		Toast.makeText(getApplicationContext(), "Signed In Successfully",
				Toast.LENGTH_LONG).show();
		/* Plus.PeopleApi.loadVisible(mGoogleApiClient, null)
	      .setResultCallback(this);*/
		 Plus.PeopleApi.loadVisible(mGoogleApiClient, null).setResultCallback(this);
		 
		 if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
			   
			 Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
			 String personName = currentPerson.getDisplayName();
			  //  String personPhoto = currentPerson.getImage();
			    String personGooglePlusProfile = currentPerson.getUrl();
				userEmail = Plus.AccountApi.getAccountName(mGoogleApiClient);
			    Log.e("personName", personName);
			    Log.e("userEmail", userEmail);
			    Log.e("personGooglePlusProfile", personGooglePlusProfile);
			   
			    new Async_social_Login().execute("1","false");
			  
			  }
		 else if(Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) == null)
		 {
			 Log.e("people Api value", "null");
		 }
		 
	}

	class Async_social_Login extends AsyncTask<String, Void, String>
	{
		String result=null,param=null;
		String user_id,token;

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			ServiceHandler handler=new ServiceHandler();
			param="email="+userEmail+"&user_type="+params[0];
			Log.e("inside doInBackground of", "Async_social_Login");
			result=handler.makeServiceCall("http://beta.foodfiestaa.com/socialloginservice", ServiceHandler.POST, param);
			try {
				JSONObject obj=new JSONObject(result);
				if(obj.getBoolean("status"))
				{
					user_id=obj.getString("id");
					token=obj.getString("token");
					SharedPrefs.putSP(ConsumerSignIn.this, user_id, token, "Consumer");
					if(Boolean.parseBoolean(params[1])){
						SharedPrefs.putSocialLogin(ConsumerSignIn.this, "Facebook");
						logout();
					}else{
						SharedPrefs.putSocialLogin(ConsumerSignIn.this, "Google Plus");
						signOutFromGplus();
					}
					return "success";
				}else{
					return "failure";
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(result.equalsIgnoreCase("success"))
			{
				Toast.makeText(getApplicationContext(), "LogIn Successfull",
						Toast.LENGTH_SHORT).show();

				Intent intent = new Intent(ConsumerSignIn.this,
						LocationSearchResultPage.class);
				// intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
						| Intent.FLAG_ACTIVITY_CLEAR_TASK);
				 signOutFromGplus();
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.right_in, R.anim.left_out);
								
			}else{
				Toast.makeText(getApplicationContext(), result,
						Toast.LENGTH_SHORT).show();
			}
			pDialog.dismiss();
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(ConsumerSignIn.this);
			pDialog.setMessage("Attempting login...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();
		if (id == R.id.btn_sign_in) {
			// Signin button clicked
			if(InternetCheck.isInternetConnected(ConsumerSignIn.this)){
				processSignIn();
			}else{
				dialog.showDialog(ConsumerSignIn.this, "Please connect to Internet", "OK", R.drawable.circle_uncheck);
			}
		}else if(id == R.id.fb){
			if(InternetCheck.isInternetConnected(ConsumerSignIn.this)){
				loginButton.performClick();
			}else{
				dialog.showDialog(ConsumerSignIn.this, "Please connect to Internet", "OK", R.drawable.circle_uncheck);
			}
		}

	}

	private void processSignIn() {

		if (!mGoogleApiClient.isConnecting()) {
			processSignInError();
			mSignInClicked = true;
		}

	}

	private void processSignInError() {
		 
		if (mConnectionResult != null && mConnectionResult.hasResolution()) {
			try {
				mIntentInProgress = true;
				mConnectionResult.startResolutionForResult(this,
						SIGN_IN_REQUEST_CODE);
			} catch (SendIntentException e) {
				mIntentInProgress = false;
				mGoogleApiClient.connect();
			}
		}
	}

	@Override
	public void onResult(LoadPeopleResult arg0) {
		// TODO Auto-generated method stub
		
	}

}
