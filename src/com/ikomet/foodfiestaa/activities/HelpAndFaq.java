package com.ikomet.foodfiestaa.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.fragments.FragFaqCook;
import com.ikomet.foodfiestaa.fragments.FragFaqGeneral;

public class HelpAndFaq extends ActionBarActivity implements OnClickListener {

	RelativeLayout layout_general, layout_cook;
	ImageView img_general, img_cook;
	TextView txt_general, txt_cook;
	FragmentManager frag_manager;
	Fragment fragment;
	FragmentTransaction frag_transaction;
	Toolbar mToolbar;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_helpandfaq);
		initialize();
		
		mToolbar = (Toolbar) findViewById(R.id.toolbar);
		mToolbar.setTitle("");
		setSupportActionBar(mToolbar);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);

		mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		layout_general.setOnClickListener(this);
		layout_cook.setOnClickListener(this);
		
		layout_general.setBackgroundColor(Color.WHITE);
		txt_general.setTextColor(getResources().getColor(R.color.colorPrimary));
		img_general.setImageResource(R.drawable.tabprofileiconyellow);
		//making cook tab disable(ash themed)
		layout_cook.setBackgroundColor(getResources().getColor(R.color.colorUnselectTabBg));
		img_cook.setImageResource(R.drawable.capgreyicon);
		txt_cook.setTextColor(getResources().getColor(R.color.colorUnselectTxt));
		fragment=new FragFaqGeneral();
		Frag_initial(fragment);

	}

	public void initialize() {
		layout_general = (RelativeLayout) findViewById(R.id.layout_general);
		layout_cook = (RelativeLayout) findViewById(R.id.layout_cook);
		img_general = (ImageView) findViewById(R.id.img_general);
		img_cook = (ImageView) findViewById(R.id.img_cook);
		txt_general = (TextView) findViewById(R.id.txt_general);
		txt_cook = (TextView) findViewById(R.id.txt_cook);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int view_id = v.getId();
		
		if (view_id == R.id.layout_general) {
		Log.e("test", "in");
		fragment=new FragFaqGeneral();
		//making general tab enable(yellow themed)
		layout_general.setBackgroundColor(Color.WHITE);
		txt_general.setTextColor(getResources().getColor(R.color.colorPrimary));
		img_general.setImageResource(R.drawable.tabprofileiconyellow);
		//making cook tab disable(ash themed)
		layout_cook.setBackgroundColor(getResources().getColor(R.color.colorUnselectTabBg));
		img_cook.setImageResource(R.drawable.capgreyicon);
		txt_cook.setTextColor(getResources().getColor(R.color.colorUnselectTxt));
		Frag_initial(fragment);
		
	} else {
		
		fragment=new FragFaqCook();
		//making general tab disable(ash themed)
		layout_general.setBackgroundColor(getResources().getColor(R.color.colorUnselectTabBg));
		txt_general.setTextColor(getResources().getColor(R.color.colorUnselectTxt));
		img_general.setImageResource(R.drawable.tabprofileicongrey);
	
		//making cook tab enable(yellow themed)
		layout_cook.setBackgroundColor(Color.WHITE);
		img_cook.setImageResource(R.drawable.capyellowicon);
		txt_cook.setTextColor(getResources().getColor(R.color.colorPrimary));
		Frag_initial(fragment);
		
		
		
	}
				
	}

	public void Frag_initial(Fragment frag) {

		frag_manager = getSupportFragmentManager();
		frag_transaction=frag_manager.beginTransaction();
		frag_transaction.replace(R.id.frame_faq, frag);
		frag_transaction.commit();
                                     
	}

	public void General_tab() {

	}

}
