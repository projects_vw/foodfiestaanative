package com.ikomet.foodfiestaa.activities;

import java.io.UnsupportedEncodingException;

import java.net.URLEncoder;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.ikomet.foodfiestaa.R;
import com.ikomet.foodfiestaa.fragments.Frag_Payhistory_Cook;
import com.ikomet.foodfiestaa.fragments.Frag_cook_howitworks;
import com.ikomet.foodfiestaa.fragments.Frag_orderCook;
import com.ikomet.foodfiestaa.fragments.Frag_profile_cook;
import com.ikomet.foodfiestaa.fragments.Frag_signaturedishes;
import com.ikomet.foodfiestaa.fragments.FragmentAddDish;
import com.ikomet.foodfiestaa.fragments.Kitchen_welcomeboard;
import com.ikomet.foodfiestaa.fragments.SignedDrawerCook;
import com.ikomet.foodfiestaa.interfaces.FragmentCommunicator;
import com.ikomet.foodfiestaa.interfaces.FragmentCommunicatorNew;
import com.ikomet.foodfiestaa.interfaces.Toggle;
import com.ikomet.foodfiestaa.utils.DummyTabContent;
import com.ikomet.foodfiestaa.utils.ServiceHandler;
import com.ikomet.foodfiestaa.utils.SharedPrefs;

public class CookActivity extends FragmentActivity implements
		OnCheckedChangeListener, Toggle,FragmentCommunicator{
	TabHost tHost;
	private Toolbar mToolbar;
	Fragment frag;
	Context context;
	private SignedDrawerCook drawerFragment;
	ToggleButton tg_btn;
	public Toggle toggle;
	Frag_cook_howitworks fragcook;
	Frag_orderCook frag_order;
	Kitchen_welcomeboard welocme;
	Frag_signaturedishes signature_dishes;
	Frag_profile_cook frag_profile;
	FragmentTransaction ft, ft1;
	FloatingActionButton action_btn;
	FragmentManager fm, fm1;
	FragmentAddDish fragadddish;
	ImageView delete_dish;
	String classname;
	public FragmentCommunicatorNew communicator;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_nav_tabhost_new);

		classname = getIntent().getStringExtra("classname");
		Toast.makeText(CookActivity.this, classname, 1000).show();
		
		tHost = (TabHost) findViewById(android.R.id.tabhost);
		tHost.setup();
		tHost.getTabWidget().setDividerDrawable(null);
		action_btn = (FloatingActionButton) findViewById(R.id.btn_floating_action);
		
		action_btn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				fm1 = getSupportFragmentManager();

				ft1 = fm1.beginTransaction();
				fragadddish = (FragmentAddDish) fm
						.findFragmentByTag("fragadddish");

				ft1.replace(R.id.realtabcontent, new FragmentAddDish(),
						"fragadddish");
				ft1.commit();

				action_btn.setVisibility(View.GONE);

			}
		});
		drawerFragment = (SignedDrawerCook) getSupportFragmentManager()
				.findFragmentById(R.id.fragment_navigation_drawer);
		mToolbar = (Toolbar) findViewById(R.id.toolbar);
		tg_btn = (ToggleButton) mToolbar.findViewById(R.id.tg_btn);
		delete_dish=(ImageView)mToolbar.findViewById(R.id.img_delete);
		tg_btn.setOnCheckedChangeListener(this);
		drawerFragment.setUp(R.id.fragment_navigation_drawer,
				(DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);

		/**
		 * Defining Tab Change Listener event. This is invoked when tab is
		 * changed
		 */
		
		delete_dish.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				communicator.passnewDataToFragment("", "");
			}
		});
		TabHost.OnTabChangeListener tabChangeListener = new TabHost.OnTabChangeListener() {

			@Override
			public void onTabChanged(String tabId) {

				if (!tabId.equalsIgnoreCase("frag_kitchen")) {
					tg_btn.setVisibility(View.VISIBLE);
					delete_dish.setVisibility(View.GONE);
				}
				fm = getSupportFragmentManager();

				fragcook = (Frag_cook_howitworks) fm
						.findFragmentByTag("frag_cook");
				frag_order = (Frag_orderCook) fm
						.findFragmentByTag("frag_order");
				signature_dishes = (Frag_signaturedishes) fm
						.findFragmentByTag("signature_dishes");
				welocme = (Kitchen_welcomeboard) fm
						.findFragmentByTag("frag_kitchen");
				frag_profile = (Frag_profile_cook) fm
						.findFragmentByTag("frag_profile");

				ft = fm.beginTransaction();

				/** Detaches the androidfragment if exists */
				if (fragcook != null)
					ft.detach(fragcook);

				/** Detaches the applefragment if exists */
				if (frag_order != null)
					ft.detach(frag_order);

				if (signature_dishes != null)
					ft.detach(signature_dishes);
				if (frag_profile != null)
					ft.detach(frag_profile);

				/** If current tab is android */
				if (tabId.equalsIgnoreCase("frag_cook")) {

					if (fragcook == null) {
						/**
						 * Create AndroidFragment and adding to
						 * fragmenttransaction
						 */
						ft.add(R.id.realtabcontent, new Frag_Payhistory_Cook(),
								"frag_cook");
					} else {
						/**
						 * Bring to the front, if already exists in the
						 * fragmenttransaction
						 */
						ft.attach(fragcook);
					}

				} else if (tabId.equalsIgnoreCase("frag_order")) {

					/** If current tab is apple */
					if (frag_order == null) {
						/**
						 * Create AppleFragment and adding to
						 * fragmenttransaction
						 */
						action_btn.setVisibility(View.GONE);
						ft.add(R.id.realtabcontent, new Frag_orderCook(),
								"frag_order");
					} else {
						/**
						 * Bring to the front, if already exists in the
						 * fragmenttransaction
						 */
						ft.attach(frag_order);
						action_btn.setVisibility(View.GONE);
					}
				} else if (tabId.equalsIgnoreCase("signature_dishes")) {

					if (fragadddish != null) {
						ft1.detach(fragadddish);
					}

					if (signature_dishes == null) {
						action_btn.setVisibility(View.VISIBLE);
						ft.add(R.id.realtabcontent, new Frag_signaturedishes(),
								"signature_dishes");
					} else {
						action_btn.setVisibility(View.VISIBLE);
						ft.attach(signature_dishes);
					}

				} else if (tabId.equalsIgnoreCase("frag_profile")) {
					if (frag_profile == null) {
						action_btn.setVisibility(View.GONE);
						ft.add(R.id.realtabcontent, new Frag_profile_cook(),
								"frag_profile");
					} else {
						action_btn.setVisibility(View.GONE);
						ft.attach(frag_profile);
					}
				}

				ft.commit();
			}

		};

		/** Setting tabchangelistener for the tab */
		tHost.setOnTabChangedListener(tabChangeListener);

		/** Defining tab builder for kitchen welcome tab */
		TabHost.TabSpec tSpecAndroid = tHost.newTabSpec("signature_dishes");
		tSpecAndroid.setIndicator(createTabView(R.drawable.tab1_selector,
				"kitchen"));
		tSpecAndroid.setContent(new DummyTabContent(getBaseContext()));
		tHost.addTab(tSpecAndroid);

		/** Defining tab builder for Apple tab */
		TabHost.TabSpec tSpecApple = tHost.newTabSpec("frag_order");
		tSpecApple
				.setIndicator(createTabView(R.drawable.tab2_selector, "order"));
		tSpecApple.setContent(new DummyTabContent(getBaseContext()));
		tHost.addTab(tSpecApple);

		TabHost.TabSpec tSpecKithchen = tHost.newTabSpec("frag_cook");
		tSpecKithchen.setIndicator(createTabView(R.drawable.tab3_selector,
				"pay"));
		tSpecKithchen.setContent(new DummyTabContent(getBaseContext()));
		tHost.addTab(tSpecKithchen);

		TabHost.TabSpec tSpecProfile = tHost.newTabSpec("frag_profile");
		tSpecProfile.setIndicator(createTabView(R.drawable.tab4_selector,
				"profile"));
		tSpecProfile.setContent(new DummyTabContent(getBaseContext()));
		tHost.addTab(tSpecProfile);

		if ((EditCook_profile.value_change_state) == 1) {
			Log.e("the value in ", "current state is"
					+ EditCook_profile.value_change_state);
			tHost.setCurrentTab(3);
			EditCook_profile.value_change_state = 0;
		} else {
			Log.e("the value is", "Wrong");
		}

	}

	private View createTabView(final int id, String name) {
		View view = LayoutInflater.from(this).inflate(R.layout.tabs_icon, null);
		ImageView imageView = (ImageView) view.findViewById(R.id.tab_icon);
		TextView txt_title = (TextView) view.findViewById(R.id.txt_label);
		txt_title.setText(name);

		// ImageView imageView = new ImageView(this);
		// imageView.setLayoutParams(new LinearLayout.LayoutParams(0,
		// LayoutParams.WRAP_CONTENT,1.0f));
		// img1.setImageDrawable(d);
		imageView.setImageDrawable(getResources().getDrawable(id));

		return view;
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		// TODO Auto-generated method stub
		String s = SharedPrefs.getUid(this);
		String s1 = SharedPrefs.getToken(this);
		String param = null;
		int i;
		if (isChecked) {
			Log.e("true", "turn on");
			i = 1;
			toggle.ControlToggle(true);
		} else {
			Log.e("false", "turn off");
			i = 0;
			toggle.ControlToggle(false);
		}
		try {
			param = (new StringBuilder("id="))
					.append(URLEncoder.encode(s, "UTF-8")).append("&token=")
					.append(URLEncoder.encode(s1, "UTF-8"))
					.append("&status_flag=")
					.append(URLEncoder.encode(String.valueOf(i), "UTF-8"))
					.toString();
			new async_status().execute(param);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	class async_status extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String result = (new ServiceHandler()).makeServiceCall(
					"http://beta.foodfiestaa.com/statusservice", 1, params[0]);
			Log.e("result", result);

			return null;
		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Log.e("inside", "onResume");

	}

	@Override
	public void ControlToggle(boolean value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void EditDish_flnt_actionbar() {
		// TODO Auto-generated method stub
		action_btn.setVisibility(View.GONE);
	}

	@Override
	public void passDataToFragment() {
		// TODO Auto-generated method stub
		tg_btn.setVisibility(View.GONE);
		delete_dish.setVisibility(View.VISIBLE);
	}

	@Override
	public void change_state() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void Add_click() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void locationToolbar(boolean isLocationSerach) {
		// TODO Auto-generated method stub
		
	}
}
