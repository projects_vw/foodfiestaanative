package com.ikomet.foodfiestaa.beans;

import java.util.ArrayList;

public class OrderGroup {

	String status, order_no, total_price;
	ArrayList<OrderChild> childList;
	
	public OrderGroup() {
		super();
		// TODO Auto-generated constructor stub
	}

	public OrderGroup(String status, String order_no, String total_price,
			ArrayList<OrderChild> childList) {
		super();
		this.status = status;
		this.order_no = order_no;
		this.total_price = total_price;
		this.childList = childList;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOrder_no() {
		return order_no;
	}

	public void setOrder_no(String order_no) {
		this.order_no = order_no;
	}

	public String getTotal_price() {
		return total_price;
	}

	public void setTotal_price(String total_price) {
		this.total_price = total_price;
	}

	public ArrayList<OrderChild> getChildList() {
		return childList;
	}

	public void setChild(ArrayList<OrderChild> childList) {
		this.childList = childList;
	}

}
