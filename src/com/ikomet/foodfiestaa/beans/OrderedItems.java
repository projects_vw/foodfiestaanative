package com.ikomet.foodfiestaa.beans;

public class OrderedItems {

	String item_name, qty, avaliable_order, price, oid;

	public OrderedItems() {
		super();
		// TODO Auto-generated constructor stub
	}

	public OrderedItems(String item_name, String qty, String avaliable_order,
			String price, String oid) {
		super();
		this.item_name = item_name;
		this.qty = qty;
		this.avaliable_order = avaliable_order;
		this.price = price;
		this.oid = oid;
	}

	public String getItem_name() {
		return item_name;
	}

	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}

	public String getQty() {
		return qty;
	}

	public void setQty(String qty) {
		this.qty = qty;
	}

	public String getAvaliable_order() {
		return avaliable_order;
	}

	public void setAvaliable_order(String avaliable_order) {
		this.avaliable_order = avaliable_order;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}

}
