package com.ikomet.foodfiestaa.beans;

public class Reviews {

	String name, pic, message, rating, date, time;

	public Reviews() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Reviews(String name, String pic, String message, String rating,
			String date, String time) {
		super();
		this.name = name;
		this.pic = pic;
		this.message = message;
		this.rating = rating;
		this.date = date;
		this.time = time;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

}
