package com.ikomet.foodfiestaa.beans;

public class CartBean {
	int id, items_added, order_type;
	String kitchen_id, menu_id, food_name, food_price;


	public CartBean(int id, int items_added, String kitchen_id, String menu_id,
			String food_name, String food_price, int order_type) {
		super();
		this.id = id;
		this.items_added = items_added;
		this.kitchen_id = kitchen_id;
		this.menu_id = menu_id;
		this.food_name = food_name;
		this.food_price = food_price;
		this.order_type = order_type;
	}

	public void setOrder_type(int order_type) {
		this.order_type = order_type;
	}

	public int getOrder_type() {
		return order_type;
	}
	
	public void setItems_added(int items_added) {
		this.items_added = items_added;
	}

	public String getKitchen_id() {
		return kitchen_id;
	}

	public void setKitchen_id(String kitchen_id) {
		this.kitchen_id = kitchen_id;
	}

	public String getMenu_id() {
		return menu_id;
	}

	public void setMenu_id(String menu_id) {
		this.menu_id = menu_id;
	}

	public String getFood_name() {
		return food_name;
	}

	public void setFood_name(String food_name) {
		this.food_name = food_name;
	}

	public String getFood_price() {
		return food_price;
	}

	public void setFood_price(String food_price) {
		this.food_price = food_price;
	}

	public CartBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getItems_added() {
		return items_added;
	}
}
