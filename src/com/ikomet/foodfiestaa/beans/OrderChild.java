package com.ikomet.foodfiestaa.beans;

public class OrderChild {

	String ordered_by, trans_id, date, time;

	public OrderChild() {
		super();
		// TODO Auto-generated constructor stub
	}

	public OrderChild(String ordered_by, String trans_id, String date,
			String time) {
		super();
		this.ordered_by = ordered_by;
		this.trans_id = trans_id;
		this.date = date;
		this.time = time;
	}

	public String getOrdered_by() {
		return ordered_by;
	}

	public void setOrdered_by(String ordered_by) {
		this.ordered_by = ordered_by;
	}

	public String getTrans_id() {
		return trans_id;
	}

	public void setTrans_id(String trans_id) {
		this.trans_id = trans_id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

}
