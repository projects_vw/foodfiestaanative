package com.ikomet.foodfiestaa.beans;

import java.io.Serializable;

public class ConsumerDetailBean implements Serializable {

	private static final long serialVersionUID = 6319781300831613326L;
	String email, fname, lname, mobile, address, city, state, country, pic,
			pincode, location;

	public ConsumerDetailBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ConsumerDetailBean(String email, String fname, String lname,
			String mobile, String address, String city, String state,
			String country, String pic, String pincode, String location) {
		super();
		this.email = email;
		this.fname = fname;
		this.lname = lname;
		this.mobile = mobile;
		this.address = address;
		this.city = city;
		this.state = state;
		this.country = country;
		this.pic = pic;
		this.pincode = pincode;
		this.location = location;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

}
