package com.ikomet.foodfiestaa.beans;

import java.io.Serializable;

public class CookDetailBean
    implements Serializable
{

    String acname;
    String acno;
    String city;
    String cook_uniqueid;
    String country;
    String email;
    String fname;
    String ifsc;
    String kitchen;
    String lname;
    String location;
    String mobile;
    String paypal_mail_or_phone;
    String pincode;
    String routing;
    String state;
    String street;

    public CookDetailBean(String s, String s1, String s2, String s3, String s4, String s5, String s6, 
            String s7, String s8, String s9, String s10, String s11, String s12, String s13, 
            String s14, String s15, String s16)
    {
        cook_uniqueid = s;
        email = s1;
        fname = s2;
        lname = s3;
        mobile = s4;
        kitchen = s5;
        street = s6;
        location = s7;
        city = s8;
        state = s9;
        country = s10;
        pincode = s11;
        acno = s12;
        acname = s13;
        routing = s14;
        ifsc = s15;
        paypal_mail_or_phone = s16;
    }

    public String getAcname()
    {
        return acname;
    }

    public String getAcno()
    {
        return acno;
    }

    public String getCity()
    {
        return city;
    }

    public String getCook_uniqueid()
    {
        return cook_uniqueid;
    }

    public String getCountry()
    {
        return country;
    }

    public String getEmail()
    {
        return email;
    }

    public String getFname()
    {
        return fname;
    }

    public String getIfsc()
    {
        return ifsc;
    }

    public String getKitchen()
    {
        return kitchen;
    }

    public String getLname()
    {
        return lname;
    }

    public String getLocation()
    {
        return location;
    }

    public String getMobile()
    {
        return mobile;
    }

    public String getPaypal_mail_or_phone()
    {
        return paypal_mail_or_phone;
    }

    public String getPincode()
    {
        return pincode;
    }

    public String getRouting()
    {
        return routing;
    }

    public String getState()
    {
        return state;
    }

    public String getStreet()
    {
        return street;
    }

    public void setAcname(String s)
    {
        acname = s;
    }

    public void setAcno(String s)
    {
        acno = s;
    }

    public void setCity(String s)
    {
        city = s;
    }

    public void setCook_uniqueid(String s)
    {
        cook_uniqueid = s;
    }

    public void setCountry(String s)
    {
        country = s;
    }

    public void setEmail(String s)
    {
        email = s;
    }

    public void setFname(String s)
    {
        fname = s;
    }

    public void setIfsc(String s)
    {
        ifsc = s;
    }

    public void setKitchen(String s)
    {
        kitchen = s;
    }

    public void setLname(String s)
    {
        lname = s;
    }

    public void setLocation(String s)
    {
        location = s;
    }

    public void setMobile(String s)
    {
        mobile = s;
    }

    public void setPaypal_mail_or_phone(String s)
    {
        paypal_mail_or_phone = s;
    }

    public void setPincode(String s)
    {
        pincode = s;
    }

    public void setRouting(String s)
    {
        routing = s;
    }

    public void setState(String s)
    {
        state = s;
    }

    public void setStreet(String s)
    {
        street = s;
    }
}
