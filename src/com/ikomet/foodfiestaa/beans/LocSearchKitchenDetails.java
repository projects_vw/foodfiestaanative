package com.ikomet.foodfiestaa.beans;

public class LocSearchKitchenDetails {
String kitchen_id,pic,location,location_id,kitchen_name,minimumprice;

public String getKitchen_id() {
	return kitchen_id;
}

public LocSearchKitchenDetails(String kitchen_id, String pic, String location,
		String location_id, String kitchen_name, String minimumprice) {
	super();
	this.kitchen_id = kitchen_id;
	this.pic = pic;
	this.location = location;
	this.location_id = location_id;
	this.kitchen_name = kitchen_name;
	this.minimumprice = minimumprice;
}

public void setKitchen_id(String kitchen_id) {
	this.kitchen_id = kitchen_id;
}

public String getPic() {
	return pic;
}

public void setPic(String pic) {
	this.pic = pic;
}

public String getLocation() {
	return location;
}

public void setLocation(String location) {
	this.location = location;
}

public String getLocation_id() {
	return location_id;
}

public void setLocation_id(String location_id) {
	this.location_id = location_id;
}

public String getKitchen_name() {
	return kitchen_name;
}

public void setKitchen_name(String kitchen_name) {
	this.kitchen_name = kitchen_name;
}

public String getMinimumprice() {
	return minimumprice;
}

public void setMinimumprice(String minimumprice) {
	this.minimumprice = minimumprice;
}


}
