package com.ikomet.foodfiestaa.beans;

public class CookAddressInformation {

	String street1, street2, location, city, state, country, pincode;

	public CookAddressInformation() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CookAddressInformation(String street1, String street2,
			String location, String city, String state, String country,
			String pincode) {
		super();
		this.street1 = street1;
		this.street2 = street2;
		this.location = location;
		this.city = city;
		this.state = state;
		this.country = country;
		this.pincode = pincode;
	}

	public String getStreet1() {
		return street1;
	}

	public void setStreet1(String street1) {
		this.street1 = street1;
	}

	public String getStreet2() {
		return street2;
	}

	public void setStreet2(String street2) {
		this.street2 = street2;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

}
