package com.ikomet.foodfiestaa.beans;

import java.util.ArrayList;

public class Orders {

	String order_id, order_number, status, order_date, order_time,
			delivery_date, delivery_time, order_type, consumer_name, txnid,
			total_price;

	ArrayList<OrderedItems> orItems;

	public Orders() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Orders(String order_id, String order_number, String status,
			String order_date, String order_time, String delivery_date,
			String delivery_time, String order_type, String consumer_name,
			String txnid, String total_price, ArrayList<OrderedItems> orItems) {
		super();
		this.order_id = order_id;
		this.order_number = order_number;
		this.status = status;
		this.order_date = order_date;
		this.order_time = order_time;
		this.delivery_date = delivery_date;
		this.delivery_time = delivery_time;
		this.order_type = order_type;
		this.consumer_name = consumer_name;
		this.txnid = txnid;
		this.total_price = total_price;
		this.orItems = orItems;
	}

	public String getOrder_id() {
		return order_id;
	}

	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}

	public String getOrder_number() {
		return order_number;
	}

	public void setOrder_number(String order_number) {
		this.order_number = order_number;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOrder_date() {
		return order_date;
	}

	public void setOrder_date(String order_date) {
		this.order_date = order_date;
	}

	public String getOrder_time() {
		return order_time;
	}

	public void setOrder_time(String order_time) {
		this.order_time = order_time;
	}

	public String getDelivery_date() {
		return delivery_date;
	}

	public void setDelivery_date(String delivery_date) {
		this.delivery_date = delivery_date;
	}

	public String getDelivery_time() {
		return delivery_time;
	}

	public void setDelivery_time(String delivery_time) {
		this.delivery_time = delivery_time;
	}

	public String getOrder_type() {
		return order_type;
	}

	public void setOrder_type(String order_type) {
		this.order_type = order_type;
	}

	public String getConsumer_name() {
		return consumer_name;
	}

	public void setConsumer_name(String consumer_name) {
		this.consumer_name = consumer_name;
	}

	public String getTxnid() {
		return txnid;
	}

	public void setTxnid(String txnid) {
		this.txnid = txnid;
	}

	public String getTotal_price() {
		return total_price;
	}

	public void setTotal_price(String total_price) {
		this.total_price = total_price;
	}

	public ArrayList<OrderedItems> getOrItems() {
		return orItems;
	}

	public void setOrItems(ArrayList<OrderedItems> orItems) {
		this.orItems = orItems;
	}

}
