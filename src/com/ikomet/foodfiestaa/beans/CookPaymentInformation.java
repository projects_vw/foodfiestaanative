package com.ikomet.foodfiestaa.beans;

public class CookPaymentInformation {

	String accNo, accName, branchAddr, Ifsc, paypal_emailorphone;

	public CookPaymentInformation() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CookPaymentInformation(String accNo, String accName,
			String branchAddr, String ifsc, String paypal_emailorphone) {
		super();
		this.accNo = accNo;
		this.accName = accName;
		this.branchAddr = branchAddr;
		Ifsc = ifsc;
		this.paypal_emailorphone = paypal_emailorphone;
	}

	public String getAccNo() {
		return accNo;
	}

	public void setAccNo(String accNo) {
		this.accNo = accNo;
	}

	public String getAccName() {
		return accName;
	}

	public void setAccName(String accName) {
		this.accName = accName;
	}

	public String getBranchAddr() {
		return branchAddr;
	}

	public void setBranchAddr(String branchAddr) {
		this.branchAddr = branchAddr;
	}

	public String getIfsc() {
		return Ifsc;
	}

	public void setIfsc(String ifsc) {
		Ifsc = ifsc;
	}

	public String getPaypal_emailorphone() {
		return paypal_emailorphone;
	}

	public void setPaypal_emailorphone(String paypal_emailorphone) {
		this.paypal_emailorphone = paypal_emailorphone;
	}

}
