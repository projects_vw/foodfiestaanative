package com.ikomet.foodfiestaa.beans;

public class DishDetailBean {

	String mid, fshortname, fdes, foodname, diettype, cuisientype,
	preorder, todayspecial, price, foodprice, fpic, status, updated_at,
	todayspecial_active, preorder_active, is_delete, preorder_maxorder,
	start_time, end_time, is_front_new_arrival, is_front_veg,
	is_front_non_veg, is_front;

	public DishDetailBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DishDetailBean(String mid,String fshortname,
			String fdes, String foodname, String diettype, String cuisientype,
			String preorder, String todayspecial, String price,
			String foodprice, String fpic, String status, String updated_at,
			String todayspecial_active, String preorder_active,
			String is_delete, String preorder_maxorder, String start_time,
			String end_time, String is_front_new_arrival, String is_front_veg,
			String is_front_non_veg, String is_front) {
		super();
		this.mid = mid;
		this.fshortname = fshortname;
		this.fdes = fdes;
		this.foodname = foodname;
		this.diettype = diettype;
		this.cuisientype = cuisientype;
		this.preorder = preorder;
		this.todayspecial = todayspecial;
		this.price = price;
		this.foodprice = foodprice;
		this.fpic = fpic;
		this.status = status;
		this.updated_at = updated_at;
		this.todayspecial_active = todayspecial_active;
		this.preorder_active = preorder_active;
		this.is_delete = is_delete;
		this.preorder_maxorder = preorder_maxorder;
		this.start_time = start_time;
		this.end_time = end_time;
		this.is_front_new_arrival = is_front_new_arrival;
		this.is_front_veg = is_front_veg;
		this.is_front_non_veg = is_front_non_veg;
		this.is_front = is_front;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}
	public String getFshortname() {
		return fshortname;
	}

	public void setFshortname(String fshortname) {
		this.fshortname = fshortname;
	}

	public String getFdes() {
		return fdes;
	}

	public void setFdes(String fdes) {
		this.fdes = fdes;
	}

	public String getFoodname() {
		return foodname;
	}

	public void setFoodname(String foodname) {
		this.foodname = foodname;
	}

	public String getDiettype() {
		return diettype;
	}

	public void setDiettype(String diettype) {
		this.diettype = diettype;
	}

	public String getCuisientype() {
		return cuisientype;
	}

	public void setCuisientype(String cuisientype) {
		this.cuisientype = cuisientype;
	}

	public String getPreorder() {
		return preorder;
	}

	public void setPreorder(String preorder) {
		this.preorder = preorder;
	}

	public String getTodayspecial() {
		return todayspecial;
	}

	public void setTodayspecial(String todayspecial) {
		this.todayspecial = todayspecial;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getFoodprice() {
		return foodprice;
	}

	public void setFoodprice(String foodprice) {
		this.foodprice = foodprice;
	}

	public String getFpic() {
		return fpic;
	}

	public void setFpic(String fpic) {
		this.fpic = fpic;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getTodayspecial_active() {
		return todayspecial_active;
	}

	public void setTodayspecial_active(String todayspecial_active) {
		this.todayspecial_active = todayspecial_active;
	}

	public String getPreorder_active() {
		return preorder_active;
	}

	public void setPreorder_active(String preorder_active) {
		this.preorder_active = preorder_active;
	}

	public String getIs_delete() {
		return is_delete;
	}

	public void setIs_delete(String is_delete) {
		this.is_delete = is_delete;
	}

	public String getPreorder_maxorder() {
		return preorder_maxorder;
	}

	public void setPreorder_maxorder(String preorder_maxorder) {
		this.preorder_maxorder = preorder_maxorder;
	}

	public String getStart_time() {
		return start_time;
	}

	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}

	public String getEnd_time() {
		return end_time;
	}

	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}

	public String getIs_front_new_arrival() {
		return is_front_new_arrival;
	}

	public void setIs_front_new_arrival(String is_front_new_arrival) {
		this.is_front_new_arrival = is_front_new_arrival;
	}

	public String getIs_front_veg() {
		return is_front_veg;
	}

	public void setIs_front_veg(String is_front_veg) {
		this.is_front_veg = is_front_veg;
	}

	public String getIs_front_non_veg() {
		return is_front_non_veg;
	}

	public void setIs_front_non_veg(String is_front_non_veg) {
		this.is_front_non_veg = is_front_non_veg;
	}

	public String getIs_front() {
		return is_front;
	}

	public void setIs_front(String is_front) {
		this.is_front = is_front;
	}
	
	
	
}
