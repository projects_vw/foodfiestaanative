package com.ikomet.foodfiestaa.beans;

public class FilterChild {

	boolean state;
	String name;

	public FilterChild() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FilterChild(boolean state, String name) {
		super();
		this.state = state;
		this.name = name;
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
