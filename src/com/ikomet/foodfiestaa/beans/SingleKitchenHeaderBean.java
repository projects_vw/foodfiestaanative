package com.ikomet.foodfiestaa.beans;

public class SingleKitchenHeaderBean {
	String txt_title,txt_area,txt_minimum;
	
	public SingleKitchenHeaderBean(String txt_title, String txt_area,
			String txt_minimum) {
		super();
		this.txt_title = txt_title;
		this.txt_area = txt_area;
		this.txt_minimum = txt_minimum;
	}

	public String getTxt_title() {
		return txt_title;
	}

	public void setTxt_title(String txt_title) {
		this.txt_title = txt_title;
	}

	public String getTxt_area() {
		return txt_area;
	}

	public void setTxt_area(String txt_area) {
		this.txt_area = txt_area;
	}

	
	public String getTxt_minimum() {
		return txt_minimum;
	}

	public void setTxt_minimum(String txt_minimum) {
		this.txt_minimum = txt_minimum;
	}

}
