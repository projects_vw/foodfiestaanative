package com.ikomet.foodfiestaa.beans;

public class CookGeneralInformation {

	String email, password, confirm_password, fname, lname, mobile,
			kitchen_name;

	public CookGeneralInformation() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CookGeneralInformation(String email, String password,
			String confirm_password, String fname, String lname, String mobile,
			String kitchen_name) {
		super();
		this.email = email;
		this.password = password;
		this.confirm_password = confirm_password;
		this.fname = fname;
		this.lname = lname;
		this.mobile = mobile;
		this.kitchen_name = kitchen_name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirm_password() {
		return confirm_password;
	}

	public void setConfirm_password(String confirm_password) {
		this.confirm_password = confirm_password;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getKitchen_name() {
		return kitchen_name;
	}

	public void setKitchen_name(String kitchen_name) {
		this.kitchen_name = kitchen_name;
	}

}
