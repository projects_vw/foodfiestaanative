package com.ikomet.foodfiestaa.beans;

public class LocSrchSingleKitchen {
	String mid, fshortname, pic, foodprice, description, preorder_days,
			preorder_availble, tdyspl_availble, avgrating, kitchen_id;
	int item_quantity, order_type;

	public LocSrchSingleKitchen() {
		super();
	}

	public LocSrchSingleKitchen(String kitchen_id, String mid,
			String fshortname, String pic, String foodprice,
			String description, String preorder_days, String preorder_availble,
			String tdyspl_availble, String avgrating, int item_quantity,
			int order_type) {
		super();
		this.mid = mid;
		this.kitchen_id = kitchen_id;
		this.fshortname = fshortname;
		this.pic = pic;
		this.foodprice = foodprice;
		this.description = description;
		this.preorder_days = preorder_days;
		this.preorder_availble = preorder_availble;
		this.tdyspl_availble = tdyspl_availble;
		this.avgrating = avgrating;
		this.item_quantity = item_quantity;
		this.order_type = order_type;
	}

	public String getKitchen_id() {
		return kitchen_id;
	}

	public void setKitchen_id(String kitchen_id) {
		this.kitchen_id = kitchen_id;
	}
	
	public int getOrder_type() {
		return order_type;
	}

	public void setOrder_type(int order_type) {
		this.order_type = order_type;
	}

	public int getItem_quantity() {
		return item_quantity;
	}

	public void setItem_quantity(int item_quantity) {
		this.item_quantity = item_quantity;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getFshortname() {
		return fshortname;
	}

	public void setFshortname(String fshortname) {
		this.fshortname = fshortname;
	}

	public String getFoodprice() {
		return foodprice;
	}

	public void setFoodprice(String foodprice) {
		this.foodprice = foodprice;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPreorder_days() {
		return preorder_days;
	}

	public void setPreorder_days(String preorder_days) {
		this.preorder_days = preorder_days;
	}

	public String getPreorder_availble() {
		return preorder_availble;
	}

	public void setPreorder_availble(String preorder_availble) {
		this.preorder_availble = preorder_availble;
	}

	public String getTdyspl_availble() {
		return tdyspl_availble;
	}

	public void setTdyspl_availble(String tdyspl_availble) {
		this.tdyspl_availble = tdyspl_availble;
	}

	public String getAvgrating() {
		return avgrating;
	}

	public void setAvgrating(String avgrating) {
		this.avgrating = avgrating;
	}

}
