package com.ikomet.foodfiestaa.databases;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.ikomet.foodfiestaa.beans.CartBean;

public class DBHandler extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "foodfiesta";

	// Contacts table name
	private static final String CART_ITEMS = "cart_items";

	// Contacts Table Columns names
	private static final String KEY_ID = "id";
	private static final String KEY_KITCHEN_ID = "kitchen_id";
	private static final String KEY_MENU_ID = "menu_id";
	private static final String KEY_FOOD_SHORTNAME = "food_name";
	private static final String KEY_FOOD_PRICE = "food_price";
	private static final String KEY_ITEMS_ADDED = "items_added";
	private static final String KEY_ORDER_TYPE = "order_type";

	public DBHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		String CREATE_CART_ITEMS = "create table " + CART_ITEMS + "(" + KEY_ID
				+ " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_KITCHEN_ID
				+ " TEXT," + KEY_MENU_ID + " TEXT," + KEY_FOOD_SHORTNAME
				+ " TEXT," + KEY_FOOD_PRICE + " TEXT," + KEY_ORDER_TYPE + " INTEGER,"
				+ KEY_ITEMS_ADDED + " INTEGER)";
		db.execSQL(CREATE_CART_ITEMS);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + CART_ITEMS);

		// Create tables again
		onCreate(db);
	}

	// Adding new contact
	public void addItem(CartBean cart) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_KITCHEN_ID, cart.getKitchen_id()); // kitchen id
		values.put(KEY_MENU_ID, cart.getMenu_id()); // Menu_id
		values.put(KEY_FOOD_SHORTNAME, cart.getFood_name()); // Food_name
		values.put(KEY_FOOD_PRICE, cart.getFood_price()); // food price
		values.put(KEY_ITEMS_ADDED, cart.getItems_added()); // Items_added
		values.put(KEY_ORDER_TYPE, cart.getOrder_type());
		// Inserting Row
		db.insert(CART_ITEMS, null, values);
		db.close(); // Closing database connection
	}

	// GETTING SINGLE BEAN
	public CartBean getItem(String Mid) {
		CartBean cart = null;
		SQLiteDatabase db = this.getReadableDatabase();
		String selectQuery = "SELECT  * FROM " + CART_ITEMS + " WHERE "
				+ KEY_MENU_ID + " = '" + Mid + "'";
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor != null) {

			if (cursor.moveToFirst()) {
				do {
					cart = new CartBean();
					cart.setId(cursor.getInt(0));
					cart.setKitchen_id(cursor.getString(1));
					cart.setMenu_id(cursor.getString(2));
					cart.setFood_name(cursor.getString(3));
					cart.setFood_price(cursor.getString(4));
					cart.setItems_added(cursor.getInt(5));
					
				} while (cursor.moveToNext());

			}
		}
		db.close();
		// return contact
		return cart;
	}
	
	// GETTING ALL ITEMS IN CART
		public ArrayList<CartBean> getAllItems() {
			ArrayList<CartBean> cartList = new ArrayList<CartBean>();
			SQLiteDatabase db = this.getReadableDatabase();
			String selectQuery = "SELECT  * FROM " + CART_ITEMS;
			Cursor cursor = db.rawQuery(selectQuery, null);

			if (cursor != null) {
				
				if (cursor.moveToFirst()) {
					do {
						CartBean cart = new CartBean();
						cart.setId(cursor.getInt(0));
						cart.setKitchen_id(cursor.getString(1));
						cart.setMenu_id(cursor.getString(2));
						cart.setFood_name(cursor.getString(3));
						cart.setFood_price(cursor.getString(4));
						cart.setOrder_type(cursor.getInt(5));
						cart.setItems_added(cursor.getInt(6));
						cartList.add(cart);
					} while (cursor.moveToNext());

				}
			}
			db.close();
			// return contact
			return cartList;
		}

		public String getKitchenId(){
			
			SQLiteDatabase db = this.getReadableDatabase();
			String selectQuery = "SELECT "+KEY_KITCHEN_ID+" FROM " + CART_ITEMS;
			
			Cursor cursor = db.rawQuery(selectQuery, null);

			if (cursor.getCount()>0) {

				if (cursor.moveToFirst()) {
					do {
						return cursor.getString(0);
					} while (cursor.moveToNext());

				}
			}else{
				return "";
			}
			db.close();
			
			return "";
		}
		
		public int getOrderType(){
			
			SQLiteDatabase db = this.getReadableDatabase();
			String selectQuery = "SELECT "+KEY_ORDER_TYPE+" FROM " + CART_ITEMS;
			
			Cursor cursor = db.rawQuery(selectQuery, null);

			if (cursor.getCount()>0) {

				if (cursor.moveToFirst()) {
					do {
						return cursor.getInt(0);
					} while (cursor.moveToNext());

				}
			}else{
				return -1;
			}
			db.close();
			
			return -1;
		}
		
		
		
	// GETTING SINGLE BEAN
		public int getItemCount(String Mid) {
			int item_count=0;
			SQLiteDatabase db = this.getReadableDatabase();
			String selectQuery = "SELECT  "+KEY_ITEMS_ADDED+" FROM " + CART_ITEMS + " WHERE "
					+ KEY_MENU_ID + " = '" + Mid + "'";
			Cursor cursor = db.rawQuery(selectQuery, null);

			if (cursor.getCount()>0) {

				if (cursor.moveToFirst()) {
					do {
						item_count=cursor.getInt(0);
					} while (cursor.moveToNext());

				}
			}else{
				item_count=0;
			}
			db.close();
			// return contact
			return item_count;
		}
		
		public boolean isKitchenAvailable(String kid) {
			boolean isKitchenAvailable=true;
			
			Log.e("the kitchen id is", ""+kid);
			SQLiteDatabase db = this.getReadableDatabase();
			String selectQuery = "SELECT * FROM " + CART_ITEMS;
			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor!=null) {
				if(cursor.getCount()>0){
				if (cursor.moveToFirst()) {
					do {
						Log.e("the test var", ""+cursor.getString(1));
						 if(cursor.getString(1).equalsIgnoreCase(kid)){
							 
						 }else{
							 isKitchenAvailable = false;
						 }
					} while (cursor.moveToNext());

				}
				}else{
					isKitchenAvailable = true;
				}
			}else{
				isKitchenAvailable = true;
			}
			db.close();
			// return contact
			return isKitchenAvailable;
			
		}
		
		public boolean isTodayorPre(int todayorpre) {
			boolean isTodayorPre=true;
						
			SQLiteDatabase db = this.getReadableDatabase();
			String selectQuery = "SELECT " + KEY_ORDER_TYPE +" FROM " + CART_ITEMS;
			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor!=null) {
				if(cursor.getCount()>0){
				if (cursor.moveToFirst()) {
					do {
						Log.e("todayorpre value", ""+todayorpre);
						Log.e("the test var", ""+cursor.getInt(0));
						 if(cursor.getInt(0) == todayorpre){
							 isTodayorPre = true;
						 }else{
							 isTodayorPre = false;
						 }
					} while (cursor.moveToNext());

				}
				}else{
					isTodayorPre = true;
				}
			}else{
				isTodayorPre = true;
			}
			db.close();
			// return contact
			return isTodayorPre;
			
		}
		
		public boolean isSameOrderType(int type) {
			boolean isSameOrderType=true;
			
			Log.e("the kitchen id is", ""+type);
			SQLiteDatabase db = this.getReadableDatabase();
			String selectQuery = "SELECT * FROM " + CART_ITEMS;
			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor!=null) {
				if(cursor.getCount()>0){
				if (cursor.moveToFirst()) {
					do {
						Log.e("the test var", ""+cursor.getInt(5));
						 if(cursor.getInt(5)==type){
							 
						 }else{
							 isSameOrderType = false;
						 }
					} while (cursor.moveToNext());

				}
				}else{
					isSameOrderType = true;
				}
			}else{
				isSameOrderType = true;
			}
			db.close();
			// return contact
			return isSameOrderType;
			
		}
		
		// GETTING SINGLE BEAN
				public int getTotalCount() {
					int item_count=0;
					SQLiteDatabase db = this.getReadableDatabase();
					String selectQuery = "SELECT * FROM " + CART_ITEMS;
					Cursor cursor = db.rawQuery(selectQuery, null);

					if (cursor.getCount()>0) {
						item_count=cursor.getCount();
								
						
					}else{
						item_count=0;
					}
					db.close();
					// return contact
					return item_count;
				}
		
	// Updating single contact
	public void updateCart(int items_added, String mid) {
		SQLiteDatabase db = this.getWritableDatabase();

		String update = "UPDATE " + CART_ITEMS + " SET " + KEY_ITEMS_ADDED
				+ "='" + items_added + "'" + " WHERE " + KEY_MENU_ID + "='"
				+ mid + "'";

		// updating row
		db.execSQL(update);
		db.close();

	}

	public void deleteItem(String mid) {
		SQLiteDatabase db = this.getWritableDatabase();

		String delete = "DELETE FROM " + CART_ITEMS + " WHERE " + KEY_MENU_ID
				+ "='" + mid + "'";

		db.execSQL(delete);
		db.close();
	}
	
	public void deleteItem(int id) {
		SQLiteDatabase db = this.getWritableDatabase();

		String delete = "DELETE FROM " + CART_ITEMS + " WHERE " + KEY_ID
				+ "='" + id + "'";

		db.execSQL(delete);
		db.close();
	}

}
